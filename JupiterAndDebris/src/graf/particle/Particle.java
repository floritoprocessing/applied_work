package graf.particle;

import graf.math.vector.Vec;

public class Particle {
	
	private double mass = 1;
	private Vec pos = new Vec();
	private Vec newPos = new Vec();
	private Vec mov = new Vec();
	
	public Particle() {
		
	}
	public Particle(double mass) {
		this.mass = mass;
	}
	
	public Particle(Vec position,double mass) {
		this.mass = mass;
		this.pos = position;
	}

	public Particle(Vec position, Vec movement,double mass) {
		this.mass = mass;
		this.pos = position;
		this.mov = movement;
	}
	
	public double getMass() {
		return mass;
	}
	
	public Vec getPosition() {
		return pos;
	}
	
	public void integrate(double time) {
		pos = Vec.add(pos, Vec.mul(mov,time));
	}
	
	public void integrate(double time, Vec acceleration) {
		newPos = Vec.add(pos, Vec.mul(mov,time), Vec.mul(acceleration, 0.5*time*time));
		mov = Vec.sub(newPos,pos);
		pos = newPos;
	}

	public void set(Particle particle) {
		mass=particle.mass;
		pos.set(particle.pos);
		mov.set(particle.mov);
		newPos.set(particle.newPos);
	}

	public void setMass(double mass) {
		this.mass = mass;
	}

	public void setMovement(Vec vector) {
		mov.set(vector);
	}
	public void setPosition(Vec vector) {
		pos.set(vector);
	}
	
	

}
