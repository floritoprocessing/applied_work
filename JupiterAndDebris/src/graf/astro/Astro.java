package graf.astro;

public class Astro {
	
	/**
	 * Gravitational constant G.<br>
	 * (6.67428 +/- 0.00067) x 10^-11 m^3 kg^-1 s^-2
	 */
	public static final double G = 6.67428 * Math.pow(10,-11);
		
	
	/**
	 * Gravitational parameter of sun in km^3 s^-2
	 */
	public static final double SunGM = 132712440018.0; 	
	
	/**
	 * Gravitational parameter of earth in km^3 s^-2<br>
	 * 398,600 	.4418 	�0.0008
	 */
	public static final double EarthGM = 398600.4418; 
	
	/**
	 * Sun Mass: 1.98892 � 1030 kilograms
	 */
	public static final double sunMassKg = 1.98892 * Math.pow(10,30);
	
	/**
	 * Sun Radius: 695500 km
	 */
	public static final double sunRadiusKm = 695500;
	
	/**
	 * Jupiter Mass: 1.8987 � 1027 kilograms
	 */
	public static final double jupiterMassKg = 1.8987 * Math.pow(10,27);
	
	/**
	 * Jupiter Radius: 71492 kilometers
	 */
	public static final double jupiterRadiusKm = 71492;

	/**
	 * Jupiter average distance to sun: 778,412,020 km
	 */
	public static final double jupiterDistanceToSunKm = 778412020;
	
	/**
	 * Jupiter mean orbital velocity: 47,051 km/h
	 */
	public static final double jupiterOrbitVelocityKmH = 47051;
	
	public static final double jupiterOrbitVelocityMS = jupiterOrbitVelocityKmH / 3.6;

	/**
	 * Get attractive force between two bodies using the standard gravitational constant
	 * @param m1 mass of body 1 in kilograms
	 * @param m2 mass of body 2 in kilograms
	 * @param r distance between the two bodies in metres
	 * @return force in newtons (1 N = 1 kg*m / s^2)
	 * @see Astro#G;
	 */
	public static double getAttractiveForce(double m1, double m2, double r) {
		return G*m1*m2/(r*r);
	}

	/**
	 * Get attractive force between two bodies
	 * @param g gravitational constant
	 * @param m1 mass of body 1
	 * @param m2 mass of body 2
	 * @param r distance between the two bodies
	 * @return force
	 */
	public static double getAttractiveForce(double g, double m1, double m2, double r) {
		return g*m1*m2/(r*r);
	}

	public static double kmPerHourToKmPerSecond(double v) {
		return v/3600;
	}

	public static double kmPerSecondToKmPerHour(double v) {
		return v*3600;
	}

	/**
	 * 
	 * @param GM standard gravitational parameter
	 * @param r distance between the orbiting body and the central body
	 * @return
	 */
	public static double orbitalVelocityCircular(double GM, double r) {
		return Math.sqrt(GM/r);
	}

	/**
	 * 
	 * @param GM standard gravitational parameter
	 * @param r distance between the orbiting body and the central body
	 * @param a semi-major axis
	 * @return
	 */
	public static double orbitalVelocityElliptic(double GM, double r, double a) {
		return Math.sqrt(GM * (2/r - 1/a));
	}
	
}
