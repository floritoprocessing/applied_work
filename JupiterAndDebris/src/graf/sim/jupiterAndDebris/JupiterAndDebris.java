package graf.sim.jupiterAndDebris;

import graf.astro.Astro;
import graf.particle.Particle;
import graf.math.vector.Vec;
import processing.core.*;

public class JupiterAndDebris extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"graf.sim.jupiterAndDebris.JupiterAndDebris"});
	}

	Particle[] debris = new Particle[25000];
	Particle jupiter;
	Particle sun;
	
	boolean jupiterOn = true;
	boolean rotateFrame = false;
	double offset = 0;
	
	private int iterations = 1;

	public static final double kmToPixel = 0.25*Math.pow(10, -6);

	public void settings() {
		size(600,600,P3D);
	}
	public void setup() {
		jupiter = new Particle(
				new Vec(Astro.jupiterDistanceToSunKm,0,0),
				new Vec(0,Astro.jupiterOrbitVelocityKmH,0),
				Astro.jupiterMassKg);

		System.out.println(Astro.jupiterOrbitVelocityKmH);
		System.out.println(Astro.orbitalVelocityCircular(Astro.SunGM, Astro.jupiterDistanceToSunKm)*3600);

		sun = new Particle(new Vec(),Astro.sunMassKg);

		for (int i=0;i<debris.length;i++) {
			debris[i] = new Particle();
			newDebris(debris[i]);
		}

		ellipseMode(CENTER);
		
		System.out.println("[r] - rotateFrame");
		System.out.println("[j] - jupiterOn");
		System.out.println("[UP]/[DOWN] - iterations");
	}
	
	void newDebris(Particle p) {
		double r = (0.25+0.75*Math.random())*1.5*Astro.jupiterDistanceToSunKm;
		//double r = Math.sqrt(Math.random())*1.5*Astro.jupiterDistanceToSunKm;
		double sm = (0.8 + 0.4*Math.random()) * r;
		Vec pos = new Vec(r,0,0);
		double ov = Astro.orbitalVelocityElliptic(Astro.SunGM, r, sm) * 3600; // km/s
		Vec mov = new Vec(0,ov,0);
		double rd = Math.random()*Math.PI*2;
		pos.rotZ(rd);
		mov.rotZ(rd);
		p.setPosition(pos);
		p.setMovement(mov);
	}
	
	public void keyPressed() {
		if (key==CODED) {
			if (keyCode==UP) {
				iterations++;
				System.out.println(iterations);
			}
			else if (keyCode==DOWN && iterations>1) {
				iterations--;
				System.out.println(iterations);
			}
		} else if (key=='j') {
			jupiterOn = !jupiterOn;
		} else if (key=='r') {
			rotateFrame = !rotateFrame;
		}
	}

	public void draw() {
		background(0);

		translate(width/2,height/2);

		Vec sunJup = Vec.sub(sun.getPosition(), jupiter.getPosition());
		double rd = Math.atan2(sunJup.getY(), sunJup.getX());
		
		if (rotateFrame) {
			rd -= offset;
		} else {
			offset = rd;
		}
		
		/*
		 * Draw sun
		 */
		
		pushMatrix();
		if (rotateFrame) {
			Vec sunRot = new Vec();
			translate(Vec.mul(sunRot, kmToPixel));
		} else {
			translate(Vec.mul(sun.getPosition(), kmToPixel));
		}
		noStroke();
		fill(255,255,0);
		circle(10.0);
		popMatrix();

		/*
		 * Draw Jupiter
		 */
		if (jupiterOn) {
			
			pushMatrix();
			if (rotateFrame) {
				Vec jupRot = new Vec(jupiter.getPosition());
				jupRot.sub(sun.getPosition());
				jupRot.rotZ(-rd);
				jupRot.add(sun.getPosition());
				translate(Vec.mul(jupRot, kmToPixel));
			} else {
				translate(Vec.mul(jupiter.getPosition(), kmToPixel));
			}
			noStroke();
			fill(64,255,64);
			circle(5.0);
			popMatrix();
		}

		for (int it=0;it<iterations;it++) {

			/*
			 * Draw debris
			 */
			stroke(255,255,255,64);
			for (int i=0;i<debris.length;i++) {
				if (rotateFrame) {
					Vec debrisRot = new Vec(debris[i].getPosition());
					debrisRot.sub(sun.getPosition());
					debrisRot.rotZ(-rd);
					debrisRot.add(sun.getPosition());
					point(Vec.mul(debrisRot, kmToPixel));
				} else {
					point(Vec.mul(debris[i].getPosition(), kmToPixel));
				}
			}

			//for (int it=0;it<iterations;it++) {

			/*
			 * Calculate force of Sun/Jupiter on each other
			 * force in newtons (1 N = 1 kg*m / s^2)
			 */
			double rKm = Vec.distance(sun.getPosition(),jupiter.getPosition());
			double F = Astro.getAttractiveForce(Astro.sunMassKg, Astro.jupiterMassKg, rKm*1000);

			/*
			 * Calculate acceleration for both
			 */
			double aFromSunOnJupiter = F / Astro.jupiterMassKg;
			Vec jupiterToSun = Vec.sub(sun.getPosition(),jupiter.getPosition());
			Vec jupiterToSunNorm = Vec.normalize(jupiterToSun);
			Vec jupiterToSunAccelerationMs = Vec.mul(jupiterToSunNorm, aFromSunOnJupiter);
			Vec jupiterToSunAccelerationKmh = Vec.mul(jupiterToSunAccelerationMs,3600*3600/1000.0);

			double aFromJupiterOnSun = F / Astro.sunMassKg;
			Vec sunToJupiter = Vec.invert(jupiterToSun);
			Vec sunToJupiterNorm = Vec.normalize(sunToJupiter);
			Vec sunToJupiterAccelerationMs = Vec.mul(sunToJupiterNorm, aFromJupiterOnSun);
			Vec sunToJupiterAccelerationKmh = Vec.mul(sunToJupiterAccelerationMs,3600*3600/1000.0);

			jupiter.integrate(100,jupiterToSunAccelerationKmh);
			sun.integrate(100,sunToJupiterAccelerationKmh);

			boolean oneNew = false;
			for (int i=0;i<debris.length;i++) {
				double rKmToSun = Vec.distance(sun.getPosition(),debris[i].getPosition());
				if (rKmToSun>3*Astro.jupiterDistanceToSunKm && !oneNew) {
					newDebris(debris[i]);
					oneNew=true;
				}
				else {
					double FToSun = Astro.getAttractiveForce(Astro.sunMassKg, debris[i].getMass(), rKmToSun*1000);
					double rKmToJupiter = Vec.distance(jupiter.getPosition(),debris[i].getPosition());
					double FToJupiter = Astro.getAttractiveForce(Astro.jupiterMassKg, debris[i].getMass(), rKmToJupiter*1000);

					double aFromSun = FToSun / debris[i].getMass();
					double aFromJupiter = FToJupiter / debris[i].getMass();

					Vec toSunNorm = Vec.normalize(Vec.sub(sun.getPosition(),debris[i].getPosition()));
					Vec toJupiterNorm = Vec.normalize(Vec.sub(jupiter.getPosition(),debris[i].getPosition()));

					Vec toSunAcc = Vec.mul(toSunNorm, aFromSun * 3600 * 3600 / 1000.0);
					Vec toJupiterAcc = Vec.mul(toJupiterNorm, aFromJupiter * 3600 * 3600 / 1000.0);

					if (!jupiterOn) toJupiterAcc.setNull();
					Vec totalAcc = Vec.add(toSunAcc,toJupiterAcc);

					debris[i].integrate(100, totalAcc);
				}
			}


		}

	}

	private void line(Vec vec1, Vec vec2) {
		line((float)vec1.getX(),(float)vec1.getY(),(float)vec1.getZ(),(float)vec2.getX(),(float)vec2.getY(),(float)vec2.getZ());
	}

	private void point(Vec vec) {
		point((float)vec.getX(),(float)vec.getY(),(float)vec.getZ());
	}

	private void translate(Vec vec) {
		translate((float)vec.getX(),(float)vec.getY(),(float)vec.getZ());
	}

	private void circle(double radius) {
		ellipse(0,0,(float)radius,(float)radius);
	}

}
