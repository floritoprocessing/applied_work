package esa.moonmap;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import javax.swing.*;

import esa.moonmap.images.Icons;
import esa.moonmap.project.Outline;
import esa.moonmap.project.Project;

public class ImageFrame extends JFrame 
       implements MouseListener, MouseMotionListener, ActionListener
{
	private static final long serialVersionUID = 8349070035741313057L;
	
	private MainGui main;
	private Project project;
	
	private JToggleButton selectButton;
	private JToggleButton selectAddButton;
	//private JToggleButton marqueeButton;
	//private JToggleButton marqueeAllButton;
	private JToggleButton dragButton;
	private JToggleButton zoomButton;
	
	private JButton clearMapButton;
	private JButton zoomInButton;
    private JButton zoomOutButton;
    private JButton zoomOriginalButton;
    
    private enum Tool {
    	SELECT_SINGLE,
    	SELECT_ADD,
    	DRAG,
    	ZOOM_IN;//, MARQUEE, MARQUEE_ALL
    }
    
    private Tool tool = Tool.SELECT_SINGLE;
	
	private JPanel imageContainer;
    private JLabel infoLabel;
    
    private ImagePanel imagePanel;
    private JScrollPane scrollPane;

	private ArrayList<Outline> selection;// = new ArrayList<Outline>();
	private ArrayList<Outline> underMouseOutlines;
	
	private final int MARQUEE_SELECT_RESOLUTION = 8;
	private Point marqueeBase = null;
	private Rectangle marquee = null;

	private boolean selectionVisible = true;

    /**
     * Constructor
     * @param image
     * @param zoomPercentage
     * @param imageName
     */    
    public ImageFrame(MainGui main, String imageName, int requestedX, int requestedY)
    {
        super("Map [" + imageName + "]");
        
        this.main = main;
        this.project = main.getProject();
        
        selection = new ArrayList<Outline>(project.getModificationFile().length);
        //setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
                
        //this.project = project;
        BufferedImage image = project.getMapImage();
       
        ButtonGroup toggleButtons = new ButtonGroup();
        selectButton = new JToggleButton(Icons.getIcon("IconArrow.png"),true);
        selectButton.setPreferredSize(new Dimension(24,24));
        selectButton.addActionListener(this);
        selectAddButton = new JToggleButton(Icons.getIcon("IconArrowPlus.png"),true);
        selectAddButton.setPreferredSize(new Dimension(24,24));
        selectAddButton.addActionListener(this);
        /*marqueeButton = new JToggleButton(Icons.getIcon("IconMarquee.png"),true);
        marqueeButton.setPreferredSize(new Dimension(24,24));
        marqueeButton.addActionListener(this);
        marqueeAllButton = new JToggleButton(Icons.getIcon("IconMarqueeLayers.png"),true);
        marqueeAllButton.setPreferredSize(new Dimension(24,24));
        marqueeAllButton.addActionListener(this);*/
        dragButton = new JToggleButton(Icons.getIcon("IconHand.png"),false);
        dragButton.setPreferredSize(new Dimension(24,24));
        dragButton.addActionListener(this);
        zoomButton = new JToggleButton(Icons.getIcon("IconZoom.png"),false);
        zoomButton.setPreferredSize(new Dimension(24,24));
        zoomButton.addActionListener(this);
        toggleButtons.add(selectButton);
        toggleButtons.add(selectAddButton);
        /*toggleButtons.add(marqueeButton);
        toggleButtons.add(marqueeAllButton);*/
        toggleButtons.add(dragButton);
        toggleButtons.add(zoomButton);
        
        
        if(image == null)
        {
            add(new JLabel("Image " + imageName + " not Found"));
        }
        else
        {
            JPanel topPanel = new JPanel(new FlowLayout(FlowLayout.CENTER));
            
            clearMapButton = new JButton("Clear");
            clearMapButton.addActionListener(this);
            
            zoomInButton = new JButton("Zoom In");
            zoomInButton.addActionListener(this);
            
            zoomOutButton = new JButton("Zoom Out");
            zoomOutButton.addActionListener(this);
            
            zoomOriginalButton = new JButton("Original");
            zoomOriginalButton.addActionListener(this);
            
            infoLabel = new JLabel("Zoomed to 100%");
            
            topPanel.add(selectButton);
            topPanel.add(selectAddButton);
            /*topPanel.add(marqueeButton);
            topPanel.add(marqueeAllButton);*/
            topPanel.add(dragButton);
            topPanel.add(zoomButton);
            
            topPanel.add(clearMapButton);
            /*topPanel.add(new JLabel("Zoom Percentage is " + 
                                    (int)zoomPercentage + "%"));*/
            topPanel.add(zoomInButton);
            topPanel.add(zoomOriginalButton);
            topPanel.add(zoomOutButton);
            topPanel.add(infoLabel);
            
            //setFocusable(true);
            
            
            imagePanel = new ImagePanel(image);
            imagePanel.addMouseListener(this);
            imagePanel.addMouseMotionListener(this);
            //his.addKeyListener(this);
            //this.setFocusable(true);
           
            
            imageContainer = new JPanel(new FlowLayout(FlowLayout.CENTER));
            imageContainer.setBackground(Color.BLACK);
            imageContainer.add(imagePanel);
            
            scrollPane = new JScrollPane(imageContainer);
            scrollPane.setAutoscrolls(true);
            
            getContentPane().add(BorderLayout.NORTH, topPanel);
            getContentPane().add(BorderLayout.CENTER, scrollPane);
            getContentPane().add(BorderLayout.SOUTH, 
                         new JLabel("Left Click to Zoom In," +
                         " Right Click to Zoom Out", JLabel.CENTER));
            
            imagePanel.repaint();
            pack();
            
            GraphicsDevice[] gs = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
            DisplayMode dm = gs[0].getDisplayMode();
            int screenWidth = dm.getWidth();
            if (image.getWidth(null)+requestedX>screenWidth) {
            	int wantedPanelWidth = screenWidth-requestedX-20;
            	//System.out.println(wantedPanelWidth);
            	//System.out.println(getWidth());
            	//System.out.println(imagePanel.getWidth());
            	//System.out.println((double)wantedPanelWidth/imagePanel.getWidth());
            	double ratio = (double)wantedPanelWidth/imagePanel.getWidth();
            	double closestZoomFactor = ((int)(ratio/imagePanel.m_zoomChange));
            	imagePanel.setZoom(closestZoomFactor*imagePanel.m_zoomChange);
            	adjustLayout();
            }
        }
        
        pack();
        
        
        

        setVisible(true);
        
        setLocation(requestedX,requestedY);
    }
    
    /*public void updateImage(Image image) {
    	m_imagePanel.updateImage(image);
    }*/
    
    public Image getImage() {
    	return imagePanel.m_image;
    }
    
    /**
     * Action Listener method taking care of 
     * actions on the buttons
     */
    public void actionPerformed(ActionEvent evt)
    {
    	
    	if (evt.getSource().equals(selectButton)) {
    		tool = Tool.SELECT_SINGLE;
    	}
    	else if (evt.getSource().equals(selectAddButton)) {
    		tool = Tool.SELECT_ADD;
    	}
    	/*else if (evt.getSource().equals(marqueeButton)) {
    		tool = Tool.MARQUEE;
    	}
    	else if (evt.getSource().equals(marqueeAllButton)) {
    		tool = Tool.MARQUEE_ALL;
    	}*/
    	else if (evt.getSource().equals(dragButton)) {
    		tool = Tool.DRAG;
    	}
    	else if (evt.getSource().equals(zoomButton)) {
    		tool = Tool.ZOOM_IN;
    	}
    	
    	else if(evt.getSource().equals(zoomInButton))
        {
            imagePanel.zoomIn();
            adjustLayout();
        }
        else if(evt.getSource().equals(zoomOutButton))
        {
            imagePanel.zoomOut();
            adjustLayout();
        }
        else if(evt.getSource().equals(zoomOriginalButton))
        {
            imagePanel.originalSize();
            adjustLayout();
        }
        else if (evt.getSource().equals(clearMapButton)) {
        	int answer = JOptionPane.showConfirmDialog(this, "Are you sure you want to clear the map?", "Clear map?", JOptionPane.YES_NO_OPTION);
        	if (answer==JOptionPane.YES_OPTION) {
        		/*Graphics g = getImage().getGraphics();
        		g.setColor(Color.WHITE);
        		g.fillRect(0, 0, getImage().getWidth(null), getImage().getHeight(null));
        		repaint();*/
        		
        		project.clearMapImage();
        		project.clearOutlines();
        		selection.clear();
        		
        		repaint();
        	}
        }
    }
    
    /**
     * This method adjusts the layout after 
     * zooming
     *
     */
    private void adjustLayout()
    {
        imageContainer.doLayout();        
        scrollPane.doLayout();

        infoLabel.setText("Zoomed to " + (int)imagePanel.getZoomedTo() + "%");
    }
    
    /*
     * (non-Javadoc)
     * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
     */
    public void mouseDragged(MouseEvent arg0) {
    	//if (tool==Tool.MARQUEE || tool==Tool.MARQUEE_ALL) {
    	if (tool==Tool.SELECT_SINGLE || tool==Tool.SELECT_ADD) {
    		Point p = scaleMouseToImage(arg0);
    		
    		//int x, y, width, height;
    		if (p.x>=marqueeBase.x) {
    			marquee.x=marqueeBase.x;
    			marquee.width=p.x-marqueeBase.x;
    		} else {
    			marquee.x=p.x;
    			marquee.width=marqueeBase.x-p.x;
    		}
    		if (p.y>=marqueeBase.y) {
    			marquee.y=marqueeBase.y;
    			marquee.height=p.y-marqueeBase.y;
    		} else {
    			marquee.y=p.y;
    			marquee.height=marqueeBase.y-p.y;
    		}

    		repaint();
    		//System.out.println(arg0);
    	}
	}

    public void clearUnderMouseOutlines() {
    	if (underMouseOutlines!=null)
    		underMouseOutlines.clear();
    }
    
    
    /*
     * (non-Javadoc)
     * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
     */
	public void mouseMoved(MouseEvent e) {
		updateUnderMouseOutlines(e);
	}
	
	private Point scaleMouseToImage(MouseEvent e) {
		Point scaledPoint = e.getPoint();
		scaledPoint.x /= imagePanel.m_zoom;
		scaledPoint.y /= imagePanel.m_zoom;
		return scaledPoint;
	}
	
	private void updateUnderMouseOutlines(MouseEvent e) {
		Point scaledPoint = scaleMouseToImage(e);
		
		ArrayList<Outline> newOutlines =
			project.getOutlineCollection().getOutlinesUnder(scaledPoint);
		
		if (!newOutlines.equals(underMouseOutlines)) {
			underMouseOutlines = newOutlines;
			if (newOutlines!=null) {
				imagePanel.repaint();
			}
		}	
	}
	
	private void selectMouseClicked(MouseEvent e, boolean addToSelection) {
		if (underMouseOutlines!=null && underMouseOutlines.size()>0) {
						
			Outline selectedOutline = underMouseOutlines.get(underMouseOutlines.size()-1);
			String name = selectedOutline.getFilename();
			
			if (!addToSelection) {
				selection.clear();
				main.selectRow(name);//, addToSelection);
			}
			if (selection.contains(selectedOutline)) {
				selection.remove(selectedOutline);
				main.selectRemoveRow(name);//, addToSelection);
			} else {
				selection.add(selectedOutline);
				main.selectAddRow(name);//, addToSelection);
			}
			imagePanel.repaint();
			
			
			
		}
	}
    
    /**
     * This method handles mouse clicks
     */
    public void mouseClicked(MouseEvent e) 
    {
    	
    	if (tool==Tool.ZOOM_IN) {
	        if(e.getButton() == MouseEvent.BUTTON1)
	        {
	            imagePanel.zoomIn();                 
	        }
	        else if(e.getButton() == MouseEvent.BUTTON3)
	        {
	            imagePanel.zoomOut();
	        }
	        adjustLayout();
    	}
    	
    	else if (tool==Tool.SELECT_SINGLE) {
    		selectMouseClicked(e, false);
    	}
    	else if (tool==Tool.SELECT_ADD) {
    		selectMouseClicked(e, true);
    	}
    	        
    }
        
    public void mouseEntered(MouseEvent e)
    {
    	//setFocusable(true);
    }
           
    public void mouseExited(MouseEvent e)
    {
    }
           
    
    public void mousePressed(MouseEvent e)
    {
    	//if (tool==Tool.MARQUEE || tool==Tool.MARQUEE_ALL) {
    	if (tool==Tool.SELECT_SINGLE || tool==Tool.SELECT_ADD) {
    		//Point p = scaledPoint(e);
    		marqueeBase = scaleMouseToImage(e);
    		marquee = new Rectangle(marqueeBase.x,marqueeBase.y,0,0);
    	}
    }
    
    
    public void setSelectionVisible(boolean vis) {
    	selectionVisible  = vis;
    }
    
    
    public void mouseReleased(MouseEvent e)
    {
    	//if (tool==Tool.MARQUEE || tool==Tool.MARQUEE_ALL) {
    	if (tool==Tool.SELECT_SINGLE || tool==Tool.SELECT_ADD) {
    		
    		if (marquee.width>0 && marquee.height>0) {
    			//System.out.println("marquee");
	    		if (tool==Tool.SELECT_SINGLE) selection.clear();
	    		for (int xo=0;xo<marquee.width;xo+=MARQUEE_SELECT_RESOLUTION) {
	    			for (int yo=0;yo<marquee.height;yo+=MARQUEE_SELECT_RESOLUTION) {
	    				Point p = new Point(marquee.x+xo,marquee.y+yo);
	    				ArrayList<Outline> added = project.getOutlineCollection().getOutlinesUnder(p);
	    				for (int i=0;i<added.size();i++) {
	    					if (!selection.contains(added.get(i)))
	    						selection.add(added.get(i));
	    				}
	    			}
	    		}
	    		String[] names = new String[selection.size()];
	    		for (int i=0;i<selection.size();i++) {
	    			names[i] = selection.get(i).getFilename();
	    		}
	    		if (names.length>0) {
	    			main.selectRow(names);
	    			imagePanel.repaint();
	    		}
	    		
	    		
	    		marquee = null;//new Rectangle(e.getPoint().x,e.getPoint().y,0,0);
    		}
    	}
    }

    /**
     * This class is the Image Panel where the image
     * is drawn and scaled.
     * 
     * @author Rahul Sapkal(rahul@javareference.com)
     */
    public class ImagePanel extends JPanel
    {
		private static final long serialVersionUID = -6984543917287627667L;
		private double m_zoom = 1.0;
        private double m_zoomChange = 0.05;
        private Image m_image;
        
        /**
         * Constructor
         * 
         * @param image
         * @param zoomPercentage
         */                
        public ImagePanel(Image image)//, double zoomPercentage)
        {
            m_image = image;
            //m_zoomChange = 0.1;//zoomPercentage / 100;
        }
        
        /*public void updateImage(Image image) {
        	m_image = image;
        }*/
        
        /**
         * This method is overriden to draw the image
         * and scale the graphics accordingly
         */
        public void paintComponent(Graphics grp) 
        { 
            Graphics2D g2D = (Graphics2D)grp;
            
            //set the background color to white
            g2D.setColor(Color.WHITE);
            //fill the rect
            g2D.fillRect(0, 0, getWidth(), getHeight());
            
            //scale the graphics to get the zoom effect
            g2D.scale(m_zoom, m_zoom);
            
            //draw the image
            g2D.drawImage(m_image, 0, 0, this); 
            
            /*
             * Draw outline collection
             */
            
            if (underMouseOutlines!=null && underMouseOutlines.size()>0) {
            	g2D.setColor(Color.RED);
            	int amount = underMouseOutlines.size();
            	
            	//TODO Implement MouseWheel listener for changing level
            	int SELECTED_LEVEL = 1;
            	
            	for (int i=0;i<amount;i++) {
            		if (i==amount-SELECTED_LEVEL) {
            			g2D.setColor(Color.RED);
            			int x = underMouseOutlines.get(i).getBounds().x;
            			int y = underMouseOutlines.get(i).getBounds().y -5;
            			String name = underMouseOutlines.get(i).getFilename();
            			int w = (int)g2D.getFont().getStringBounds(name, g2D.getFontRenderContext()).getWidth();
            			
            			if (x+w>m_image.getWidth(null)) {
            				x = m_image.getWidth(null)-w;
            			}
            			else if (x<10) {
            				x=10;
            			}
            			if (y<10) y=10;
            			g2D.drawString(name, x, y);
            		}
            		else
            			g2D.setColor(new Color(128,64,64,128));//)
            		g2D.drawPolygon(underMouseOutlines.get(i));
            	}
            }
            
            if (selectionVisible) {
	            if (selection!=null && selection.size()>0) {
	            	g2D.setColor(Color.GREEN);
	            	int amount = selection.size();
	            	for (int i=0;i<amount;i++) {
	            		g2D.drawPolygon(selection.get(i));
	            	}
	            }
            }
            
            if (marquee!=null) {
            	g2D.setColor(Color.CYAN);
            	g2D.draw(marquee);
            }
            
            /*
            }*/
        }
         
        /**
         * This method is overriden to return the preferred size
         * which will be the width and height of the image plus
         * the zoomed width width and height. 
         * while zooming out the zoomed width and height is negative
         */
        public Dimension getPreferredSize()
        {
            return new Dimension((int)(m_image.getWidth(this) + 
                                      (m_image.getWidth(this) * (m_zoom - 1))),
                                 (int)(m_image.getHeight(this) + 
                                      (m_image.getHeight(this) * (m_zoom -1 ))));
        }
        
        /**
         * Sets the new zoomed percentage
         * @param zoomPercentage
         */
        /*public void setZoomPercentage(int zoomPercentage)
        {
            m_zoomChange = ((double)zoomPercentage) / 100;    
        }*/
        
        
        /**
         * This method set the image to the original size
         * by setting the zoom factor to 1. i.e. 100%
         */
        public void originalSize()
        {
            m_zoom = 1; 
        }
        
        public void setZoom(double m_zoom) {
        	this.m_zoom = m_zoom;
        }
        
        /**
         * This method increments the zoom factor with
         * the zoom percentage, to create the zoom in effect 
         */
        public void zoomIn()
        {
            m_zoom += m_zoomChange;
        }            
        
        /**
         * This method decrements the zoom factor with the 
         * zoom percentage, to create the zoom out effect 
         */
        public void zoomOut()
        {
            m_zoom -= m_zoomChange;
            if (m_zoom<=m_zoomChange) m_zoom+=m_zoomChange;
            
            /*if(m_zoom < m_zoomPercentage)
            {
                if(m_zoomPercentage > 1.0)
                {
                    m_zoom = 1.0;
                }
                else
                {
                    zoomIn();
                }
            }*/
        }
        
        /**
         * This method returns the currently
         * zoomed percentage
         * 
         * @return
         */
        public double getZoomedTo()
        {
            return m_zoom * 100; 
        }
    }

	
} 