package esa.moonmap.tablemodels;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SortOrder;
import javax.swing.RowSorter.SortKey;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public abstract class PresetTableModel extends AbstractTableModel {
	
	public static final int COLUMN_INDEX = 0;
	public static final int COLUMN_FILENAME = 1;
	public static final int COLUMN_UNIQUE_IMAGE_NUMBER = 2;
	public static final int COLUMN_FILTER_NAME = 3;
	public static final int COLUMN_ORBIT_NUMBER = 4;
	
	public static final int COLUMN_VISIBLE = 5;
	public static final int COLUMN_GAIN = 6;
	public static final int COLUMN_TOP_CUT = 7;
	public static final int COLUMN_EDGE_SMOOTHING = 8;
	public static final int COLUMN_LEVEL = 9;
	
	public static final int COLUMN_LONGITUDE = 10;
	public static final int COLUMN_LATITUDE = 11;
	public static final int COLUMN_PIXEL_SCALE_X = 12;
	public static final int COLUMN_PIXEL_SCALE_Y = 13;
	
	public static final int COLUMNS = 14;
	
	protected static String[] columnNames = {
		"nr",
		"Filename",
		"unique",
		"Filter",
		"orbit",
        "visible",
        "gain",
        "top cut",
        "smooth",
        "level",
        "lon",
        "lat",
        "scaleX",
        "scaleY"};

	public static String getColumnNameOf(int column) {
		return columnNames[column];
	}
	
	public static class SortKeyColumn extends SortKey {
		private final String description;
		public SortKeyColumn(int arg0, SortOrder arg1) {
			super(arg0, arg1);
			//System.out.println(arg0+" "+arg1);
			description = getColumnNameOf(arg0)+" "+(arg1.equals(SortOrder.ASCENDING)?"asc":"desc");
		}
		public String toString() {
			return description;
		}
	}
	
	
	protected boolean[] editable = {
			false,
			false,
			false,
			false,
			false,
			true,
			true,
			true,
			true,
			true,
			false,
			false,
			false,
			false
	};
	
	protected Object[][] data = {{
		new Integer(0),
		new String(""),
		new String(""),
		new String(""),
		new Integer(0),
		new Boolean(true), 
		new Double(1.0), 
		new Integer(0), 
		new Boolean(false),
		new Integer(0),
		new Double(1.0),
		new Double(1.0),
		new Double(1.0),
		new Double(1.0)
	}};
	
	protected final Object[] longValues = {
		new Integer(25000),
		"AMI_ER1_031003_00001_00010.IMG ",
		"00040_00004",
		"123456",
		new Integer(40000),
		new Boolean(true), 
		new Double(-111.111), 
		new Integer(100), 
		new Boolean(false),
		new Integer(-100),
		new Double("-190.33"),
		new Double("-70.22"),
		new Double("4444"),
		new Double("4444")
	};
	
	public static int initColumnSizes(JTable table) {
		PresetTableModel model = (PresetTableModel)table.getModel();
        TableColumn column = null;
        Component comp = null;
        int headerWidth = 0;
        int cellWidth = 0;
        Object[] longValues = model.longValues;
        TableCellRenderer headerRenderer =
            table.getTableHeader().getDefaultRenderer();

        int totalPreferredWidth = 0;
        for (int i = 0; i < COLUMNS; i++) {
            column = table.getColumnModel().getColumn(i);

            comp = headerRenderer.getTableCellRendererComponent(
                                 null, column.getHeaderValue(),
                                 false, false, 0, 0);
            headerWidth = comp.getPreferredSize().width;

            comp = table.getDefaultRenderer(model.getColumnClass(i)).
                             getTableCellRendererComponent(
                                 table, longValues[i],
                                 false, false, 0, i);
            cellWidth = comp.getPreferredSize().width;
            int wid = Math.max(headerWidth, cellWidth);
            column.setPreferredWidth(wid);
            totalPreferredWidth += wid;
            //column.setMinWidth(Math.max(headerWidth, cellWidth));
        }
        return totalPreferredWidth;
        //table.getParent().pack();
    }

	
	public PresetTableModel() {
	}

	
	public int getColumnCount() {
		return columnNames.length;
	}

	public int getRowCount() {
		return data.length;
	}
	
	public String getColumnName(int col) {
		return columnNames[col];
    }


	public Object getValueAt(int row, int col) {
		return data[row][col];
	}
	
	public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }
	
	public boolean isCellEditable(int row, int col) {
        return editable[col];
    }
	
	 public void setValueAt(Object value, int row, int col) {
         data[row][col] = value;
         fireTableCellUpdated(row, col);
     }

	 


}
