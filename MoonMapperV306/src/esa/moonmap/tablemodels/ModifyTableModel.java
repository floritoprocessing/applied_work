package esa.moonmap.tablemodels;

import javax.swing.table.AbstractTableModel;

public class ModifyTableModel extends PresetTableModel {
	
	public ModifyTableModel() {
		super();
	}
	
	public void clearUneditableContent() {
		for (int i=0;i<editable.length;i++) {
			if (!editable[i]) {
				data[0][i] = new String("");
				columnNames[i] = "";
			}
		}
	}

}
