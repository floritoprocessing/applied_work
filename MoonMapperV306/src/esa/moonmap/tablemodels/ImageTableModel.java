package esa.moonmap.tablemodels;

import esa.moonmap.MainGui;
import esa.moonmap.project.DefaultModificationFile;
import esa.pds.PDSValueNotFoundException;
import esa.pds.component.PDSObject;
import esa.pds.dictionary.Element;

public class ImageTableModel extends PresetTableModel {

	private static final long serialVersionUID = -5537428374007360333L;
	private MainGui main;
	
	public ImageTableModel(MainGui main) {
		this.main = main;
	}

	public void updateTableFromProject() {
		if (main.getProject()==null) {
			throw new RuntimeException("Illegal call to updateTableFromProject(), when having no project");
		}
		PDSObject[] index = main.getProject().getIndexFile();
		PDSObject[] mod = main.getProject().getModificationFile();
		data = new Object[index.length][COLUMNS];
		//System.out.println(index[0].toPDSString());
		try {
			for (int row=0;row<index.length;row++) {
				for (int col=0;col<COLUMNS;col++) {
					switch (col) {
						case COLUMN_INDEX:
							data[row][col] = 
								new Integer(mod[row].getPDSValue(DefaultModificationFile.INDEX_KEYWORD).getValue()); break;
						case COLUMN_FILENAME:
							data[row][col] = mod[row].getPDSValue(DefaultModificationFile.FILENAME_KEYWORD).getValue(); break;
						case COLUMN_UNIQUE_IMAGE_NUMBER:
							data[row][col] = index[row].getPDSValue(Element.UNIQUE_IMAGE_NUMBER.name()).getValue(); break;
						case COLUMN_FILTER_NAME:
							data[row][col] = index[row].getPDSValue(Element.FILTER_NAME.name()).getValue(); break;
						case COLUMN_ORBIT_NUMBER:
							data[row][col] =
								new Integer(index[row].getPDSValue(Element.ORBIT_NUMBER.name()).getValue()); break;
						case COLUMN_VISIBLE:
							data[row][col] = 
								new Boolean(mod[row].getPDSValue(DefaultModificationFile.VISIBLE_KEYWORD).getValue()); break;
						case COLUMN_GAIN:
							data[row][col] = 
								new Double(mod[row].getPDSValue(DefaultModificationFile.GAIN_VALUE_KEYWORD).getValue()); break;
						case COLUMN_TOP_CUT:
							data[row][col] = 
								new Integer(mod[row].getPDSValue(DefaultModificationFile.TOP_CUT_KEYWORD).getValue()); break;
						case COLUMN_EDGE_SMOOTHING:
							data[row][col] = 
								new Boolean(mod[row].getPDSValue(DefaultModificationFile.EDGE_SMOOTH_KEYWORD).getValue()); break;
						case COLUMN_LEVEL:
							data[row][col] = 
								new Integer(mod[row].getPDSValue(DefaultModificationFile.ARRANGE_LEVEL_KEYWORD).getValue()); break;
						case COLUMN_LONGITUDE:
							data[row][col] = 
								new Double(index[row].getPDSValue("CENTER_LONGITUDE").getValue());
								break;
						case COLUMN_LATITUDE:
							data[row][col] = 
								new Double(index[row].getPDSValue("CENTER_LATITUDE").getValue()); 
								break;
						case COLUMN_PIXEL_SCALE_X:
							data[row][col] =
								new Double(index[row].getPDSValue(Element.HORIZONTAL_PIXEL_SCALE.name()).getValue());
							break;
						case COLUMN_PIXEL_SCALE_Y:
							data[row][col] =
								new Double(index[row].getPDSValue(Element.VERTICAL_PIXEL_SCALE.name()).getValue());
						default: new RuntimeException("Error!");
					}
					
				}
			}
			fireTableDataChanged();
		} catch (PDSValueNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	/*
	 * (non-Javadoc)
	 * @see esa.moonmap.tablemodels.PresetTableModel#setValueAt(java.lang.Object, int, int)
	 */
	public void setValueAt(Object value, int row, int col) {
		super.setValueAt(value, row, col);
		updateProjectModificationFile(row,col);
	}
	
	
	private void updateProjectModificationFile(int row, int col) {
		//System.out.println("update project mod file");
		PDSObject pdsValueMod = (PDSObject)main.getProject().getModificationFile()[row];
		//main.getProject().getImageMemory().clearImageAt(row);
		try {
			String keyword="";
			switch (col) {
				case COLUMN_VISIBLE:
					keyword = DefaultModificationFile.VISIBLE_KEYWORD;
					break;
				case COLUMN_GAIN:
					keyword = DefaultModificationFile.GAIN_VALUE_KEYWORD;
					break;
				case COLUMN_TOP_CUT:
					keyword = DefaultModificationFile.TOP_CUT_KEYWORD;
					break;
				case COLUMN_EDGE_SMOOTHING:
					keyword = DefaultModificationFile.EDGE_SMOOTH_KEYWORD;
					break;
				case COLUMN_LEVEL:
					keyword = DefaultModificationFile.ARRANGE_LEVEL_KEYWORD;
					break;
				default: new RuntimeException("Error!");
			}
			pdsValueMod.setValueOfPDSValue(keyword, data[row][col].toString());
		} catch (PDSValueNotFoundException e) {
			e.printStackTrace();
		}
	}


}
