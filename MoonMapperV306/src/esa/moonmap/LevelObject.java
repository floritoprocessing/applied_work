package esa.moonmap;

public class LevelObject implements Comparable {

	private Integer level;
	private int index;
	
	public LevelObject(Integer level, int index) {
		this.level = level;
		this.index = index;
	}

	public int compareTo(Object obj2) {
		Integer level2 = ((LevelObject)obj2).level;
		if (level<level2) return -1;
		else if (level>level2) return 1;
		else return 0;
	}
	
	public int getIndex() {
		return index;
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public int getLevel() {
		return level;
	}
	
	public String toString() {
		return level+"\t("+index+")";
	}
}
