package esa.moonmap;

import esa.pds.component.PDSValue;
import esa.pds.component.PDSValueCreator;

import java.awt.Component;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.util.prefs.Preferences;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

//import processing.core.PApplet;

public class Prefs {

	//private static final String pathToFile = ".\\src\\preferences.ini";
	
	private static final String LUNAR_PATH = "lunar.path";
	private static final String LUNAR_MAXORBITNR = "lunar.maxorbitnr";
	
	private static final String EXTENDEDMISSION_PATH = "extendedmission.path";
	private static final String EXTENDEDMISSION_MAXORBITNR = "extendedmission.maxorbitnr";
	
	private static final String LOW_RES_GEOM_PATH = "low_res_geom.path";
	private static final String LAST_INDEX_FILE_PATH = "last.index_file.path";
	
	private static final String LAST_IMAGE_CREATOR_FOLDER_PATH = "image_creator.sourcefolder.path";
	private static final String LAST_IMAGE_CREATOR_FILE_PATH = "image_creator.sourcefile.path";
	
	private static final String LAST_IMAGE_CREATOR_TARGET_PATH = "image_creator.target.path";
	private static final String LAST_PROJECT_PATH = "project.path";
	
	private static final String AUTO_LOAD = "project.autoload";
	
	private static final String IMAGE_TABLE_GEOM_X = "image_table.geom.x";	
	private static final String IMAGE_TABLE_GEOM_Y = "image_table.geom.y";
	private static final String IMAGE_TABLE_GEOM_WIDTH = "image_table.geom.width";
	private static final String IMAGE_TABLE_GEOM_HEIGHT = "image_table.geom.height";
	
	private File lunarPhaseImagePath;// = "G:\\S1-L-X-AMIE-3-RDR-CAL-V0.3.1-2\\S1-L-X-AMIE-3-RDR-LUNAR_PHASE-V1.0\\data";
	
	private int lunarPhaseMaxOrbit;// = 977;
	private File extendedMissionImagePath;// = "G:\\S1-L-X-AMIE-3-RDR-CAL-V0.3.1-2\\S1-L-X-AMIE-3-RDR-EXTENDED_MISSION-V1.0\\data";
	
	private int extendedMissionMaxOrbit;// = 2890;
	private File lowResGeomPath;// = "G:\\S1-L-X-AMIE-3-RDR-CAL-V0.3.1-2\\low_res_geom";
	
	private File lastIndexFilePath;
	private File lastImageCreatorSourceFolderPath;
	
	private File lastImageCreatorSourceFilePath;
	private File lastImageCreatorTargetPath;
	private File lastProjectPath;
	private boolean autoLoadProject;// = true;
	private int imageTableX;// = 10;
	private int imageTableY;// = 10;
	private int imageTableWidth;//= 400;
	private int imageTableHeight;// = 400;
	
	private Preferences preferences;
	private final DecimalFormat df5 = new DecimalFormat("00000");
	private final DecimalFormat df6 = new DecimalFormat("000000");
	
	JFileChooser chooser = new JFileChooser();
	
	
	
	public Prefs(MainGui main) {
		preferences = Preferences.userNodeForPackage(Prefs.class);
		
		chooser.setDialogType(JFileChooser.OPEN_DIALOG);
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		//chooser.setControlButtonsAreShown(false);
		
		/*
		 * Lunar phase
		 */
		
		lunarPhaseImagePath = showChooser(main,"Select lunar phase path",LUNAR_PATH);
		lunarPhaseMaxOrbit = preferences.getInt(LUNAR_MAXORBITNR, 977);
		
		/*
		 * Extended mission phase
		 */
		extendedMissionImagePath = showChooser(main,"Select extended mission phase path",EXTENDEDMISSION_PATH);
		extendedMissionMaxOrbit = preferences.getInt(EXTENDEDMISSION_MAXORBITNR, 2890);
		
		/*
		 * Low res geometry path
		 */
		lowResGeomPath = showChooser(main,"Select low res geometry path",LOW_RES_GEOM_PATH);
		
		/*
		 * Last index file path
		 */
		lastIndexFilePath = new File(preferences.get(LAST_INDEX_FILE_PATH, ""));
		
		/*
		 * Last image creator paths
		 */
		lastImageCreatorSourceFilePath = new File(preferences.get(LAST_IMAGE_CREATOR_FILE_PATH,""));
		lastImageCreatorSourceFolderPath = new File(preferences.get(LAST_IMAGE_CREATOR_FOLDER_PATH,""));
		lastImageCreatorTargetPath = new File(preferences.get(LAST_IMAGE_CREATOR_TARGET_PATH,""));
		
		/*
		 * Last project path
		 */
		lastProjectPath = new File(preferences.get(LAST_PROJECT_PATH,""));
		
		/*
		 * Auto load project
		 */
		autoLoadProject = preferences.getBoolean(AUTO_LOAD, false);
		
		/*
		 * Image table screen positions
		 */
		imageTableX = preferences.getInt(IMAGE_TABLE_GEOM_X, 10);
		imageTableY = preferences.getInt(IMAGE_TABLE_GEOM_Y, 10);
		imageTableWidth = preferences.getInt(IMAGE_TABLE_GEOM_WIDTH, 400);
		imageTableHeight = preferences.getInt(IMAGE_TABLE_GEOM_HEIGHT, 400);
	}
	
	
	public boolean getAutoLoadProject() {
		return autoLoadProject;
		//return "true".equals(autoLoadProject.toLowerCase());
	}
	
	public File getExtendedMissionImagePath() {
		return extendedMissionImagePath;
	}
	
	//public String 
	
	
	
	//private String lastImageCreatorSourfeFolderPath;	
	//public String getLastImageCreat
	
	/*private MainGui main;
	
	public Prefs(MainGui main) {
		this.main = main;
		final File file = new File(pathToFile);
		if (file.exists()) {
			load();
			//if (LAST_PROJECT.equals("") || LAST_PROJECT.length()==0) AUTO_LOAD_PROJECT = "false";
		}
		else save();
	}*/
	/*
	public int getSmoothPixels() {
		return smoothPixels;
	}*/


	/*public void load() {
		try {
			final BufferedReader reader = new BufferedReader(new FileReader(pathToFile));
			String line="";
			while ((line=reader.readLine())!=null) {
				PDSValue val = PDSValueCreator.createFromPDSString(line);
				this.getClass().getField(val.getName()).set(this, val.getValue());
			}
			reader.close();
		} catch (final FileNotFoundException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (final IOException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (SecurityException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (IllegalAccessException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
			System.exit(0);
		}
	}*/

	/*public void save() {
		main.statusArea.println("saving preferences.. ");
		System.out.println("saving preferences.. ");
		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(pathToFile));
			Field[] fields = this.getClass().getFields();
			for (int i=0;i<fields.length;i++) {
				PDSValue val = new PDSValue(fields[i].getName(),(String)fields[i].get(this));
				writer.write(val.toPDSString());
				writer.newLine();
			}
			writer.flush();
			writer.close();
			main.statusArea.println("done");
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}*/


	public File getFile_ImageArchive(String filename) {
		int orbitNumber = Integer.parseInt(filename.substring(9,14));
		String path = getPath_ImageArchive(orbitNumber);
		if (path==null) throw new RuntimeException("Preferences: Orbit number not found for "+filename);
		
		File directory = new File(path);
		if (!directory.exists()) throw new RuntimeException("Non-existent File: "+path);
		if (!directory.isDirectory()) throw new RuntimeException("Non-existent Directory: "+path);
		
		File file = new File(path+"\\"+filename);
		if (!file.exists()) throw new RuntimeException("Does not exist: "+file);
		
		return file;
	}

	/**
	 * @param filename
	 * @return File[2]: fileDescription and fileBinary
	 */
	public File[] getFiles_BinaryArchive(String filename) {
		int orbitNumber = Integer.parseInt(filename.substring(9,14));
		int imageNumber = Integer.parseInt(filename.substring(15,20));
		String[] fullpaths = getPaths_BinaryArchive(orbitNumber,imageNumber);
		
		File fileDescription = new File(fullpaths[0]);
		if (!fileDescription.exists()) throw new RuntimeException("Non-existent File: "+fullpaths[0]);
		if (fileDescription.isDirectory()) throw new RuntimeException("Path is directory: "+fullpaths[0]);
		
		File fileBinary = new File(fullpaths[1]);
		if (!fileBinary.exists()) throw new RuntimeException("Non-existent File: "+fullpaths[1]);
		if (fileBinary.isDirectory()) throw new RuntimeException("Path is directory: "+fullpaths[1]);
		
		return new File[] {fileDescription, fileBinary };
	}
	
	public int getImageTableHeight() {
		return imageTableHeight;
	}
	
	public int getImageTableWidth() {
		return imageTableWidth;
	}

	public File getLastImageCreatorSourceFilePath() {
		return lastImageCreatorSourceFilePath;
	}
	
	public File getLastImageCreatorSourceFolderPath() {
		return lastImageCreatorSourceFolderPath;
	}


	public File getLastImageCreatorTargetPath() {
		return lastImageCreatorTargetPath;
	}


	public File getLastIndexFilePath() {
		return lastIndexFilePath;
	}


	public File getLastProjectPath() {
		return lastProjectPath;
	}


	public File getLowResGeomPath() {
		return lowResGeomPath;
	}


	public File getLunarPhaseImagePath() {
		return lunarPhaseImagePath;
	}


	public String getPath_ImageArchive(int orbitNumber) {
		String out = null;
		int orbitHun=orbitNumber/100;
		if (orbitNumber<=lunarPhaseMaxOrbit) {
		//if (orbitNumber<=LUNAR_PHASE_MAX_ORBIT) {
			out = lunarPhaseImagePath + "\\ORBIT_";
			if (orbitNumber<900) {
				out += df5.format(orbitHun*100) + "_TO_" + df5.format(orbitHun*100+99);
			} else {
				out += df5.format(orbitHun*100) + "_TO_" + df5.format(orbitHun*100+77);
			}
		}
		else if (orbitNumber<=extendedMissionMaxOrbit) {
			out = extendedMissionImagePath + "\\ORBIT_";
			if (orbitNumber<2800) {
				out += df5.format(orbitHun*100) + "_TO_" + df5.format(orbitHun*100+99);
			} else {
				out += df5.format(orbitHun*100) + "_TO_" + df5.format(orbitHun*100+90);
			}
		}
		else 
			throw new RuntimeException("path for orbit number "+orbitNumber+" not found");
		
		if (out!=null) out += "\\ORBIT_"+df6.format(orbitNumber);
		return out;
	}


	public String[] getPaths_BinaryArchive(int orbitNumber, int imageNumber) {
		int orbitHun=orbitNumber/100;
		String out = "";
		out = lowResGeomPath + "\\ORBIT_";
		out += df5.format(orbitHun*100) + "_TO_" + df5.format(orbitHun*100+99);
		out += "\\ORBIT_"+df6.format(orbitNumber)+"\\";
		out += "img_"+df6.format(orbitNumber)+"_"+df5.format(imageNumber);
		return new String[] { out+".dx", out+".bin" };
	}


	public void save() {
		//preferences = Preferences.userNodeForPackage(Prefs.class);
		
		/*
		 * Lunar phase
		 */
		if (lunarPhaseImagePath.exists()) {
			preferences.put(LUNAR_PATH, lunarPhaseImagePath.getAbsolutePath());
		} else {
			preferences.put(LUNAR_PATH, "");
		}
		preferences.putInt(LUNAR_MAXORBITNR, lunarPhaseMaxOrbit);
		
		/*
		 * Extended mission phase
		 */
		if (extendedMissionImagePath.exists()) {
			preferences.put(EXTENDEDMISSION_PATH, extendedMissionImagePath.getAbsolutePath());
		} else {
			preferences.put(EXTENDEDMISSION_PATH, "");
		}
		preferences.putInt(EXTENDEDMISSION_MAXORBITNR, extendedMissionMaxOrbit);
		
		/*
		 * Low res geometry path
		 */
		if (lowResGeomPath.exists()) {
			preferences.put(LOW_RES_GEOM_PATH, lowResGeomPath.getAbsolutePath());
		} else {
			preferences.put(LOW_RES_GEOM_PATH, "");
		}
		
		/*
		 * Last index file path
		 */
		if (lastIndexFilePath.exists()) {
			preferences.put(LAST_INDEX_FILE_PATH, lastIndexFilePath.getAbsolutePath());
		} else {
			preferences.put(LAST_INDEX_FILE_PATH, "");
		}
		
		/*
		 * Last image creator paths
		 */
		if (lastImageCreatorSourceFilePath.exists()) {
			preferences.put(LAST_IMAGE_CREATOR_FILE_PATH,lastImageCreatorSourceFilePath.getAbsolutePath());
		} else {
			preferences.put(LAST_IMAGE_CREATOR_FILE_PATH, "");
		}
		if (lastImageCreatorSourceFolderPath.exists()) {
			preferences.put(LAST_IMAGE_CREATOR_FOLDER_PATH,lastImageCreatorSourceFolderPath.getAbsolutePath());
		} else {
			preferences.put(LAST_IMAGE_CREATOR_FOLDER_PATH,"");
		}
		if (lastImageCreatorTargetPath.exists()) {
			preferences.put(LAST_IMAGE_CREATOR_TARGET_PATH,lastImageCreatorTargetPath.getAbsolutePath());
		} else {
			preferences.put(LAST_IMAGE_CREATOR_TARGET_PATH,"");
		}
		
		
		/*
		 * Last project path
		 */
		if (lastProjectPath.exists()) {
			preferences.put(LAST_PROJECT_PATH,lastProjectPath.getAbsolutePath());
		} else {
			preferences.put(LAST_PROJECT_PATH,"");
		}
		        
		
		/*
		 * Auto load project
		 */
		preferences.putBoolean(AUTO_LOAD, autoLoadProject);
		
		/*
		 * Image table screen positions
		 */
		preferences.putInt(IMAGE_TABLE_GEOM_X, imageTableX);
		preferences.putInt(IMAGE_TABLE_GEOM_Y, imageTableY);
		preferences.putInt(IMAGE_TABLE_GEOM_WIDTH, imageTableWidth);
		preferences.putInt(IMAGE_TABLE_GEOM_HEIGHT, imageTableHeight);
	}


	public void setAutoLoadProject(boolean autoLoadProject) {
		this.autoLoadProject = autoLoadProject;// Boolean.toString(autoLoadProject);
	}


	public void setExtendedMissionImagePath(File extendedMissionImagePath) {
		this.extendedMissionImagePath = extendedMissionImagePath;
	}


	public void setImageTableHeight(int imageTableHeight) {
		this.imageTableHeight = imageTableHeight;
	}


	public void setImageTableWidth(int imageTableWidth) {
		this.imageTableWidth = imageTableWidth;
	}


	public void setLastImageCreatorSourceFilePath(
			File lastImageCreatorSourceFilePath) {
		this.lastImageCreatorSourceFilePath = lastImageCreatorSourceFilePath;
	}


	public void setLastImageCreatorSourceFolderPath(
			File lastImageCreatorSourceFolderPath) {
		this.lastImageCreatorSourceFolderPath = lastImageCreatorSourceFolderPath;
	}


	public void setLastImageCreatorTargetPath(File lastImageCreatorTargetPath) {
		this.lastImageCreatorTargetPath = lastImageCreatorTargetPath;
	}


	public void setLastIndexFilePath(File lastIndexFilePath) {
		this.lastIndexFilePath = lastIndexFilePath;
	}


	public void setLastProjectPath(File lastProjectPath) {
		this.lastProjectPath = lastProjectPath;
	}


	public void setLowResGeomPath(File lowResGeomPath) {
		this.lowResGeomPath = lowResGeomPath;
	}


	public void setLunarPhaseImagePath(File lunarPhaseImagePath) {
		this.lunarPhaseImagePath = lunarPhaseImagePath;
	}


	private File showChooser(Component c, String msg, String preferencesName) {
		
		String path = preferences.get(preferencesName,"");
		File f = new File(path);
		if (f.exists()) {
			return new File(path);
		} else {
			chooser.setDialogTitle(msg);
			int returnVal = JFileChooser.CANCEL_OPTION;
			while (returnVal==JFileChooser.CANCEL_OPTION) {
				returnVal = chooser.showOpenDialog(c);
				if (returnVal==JFileChooser.APPROVE_OPTION) {
					return chooser.getSelectedFile();
				}
			}
			return null;
		}
		
		
	}

	
	



}
