package esa.moonmap;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JComponent;

public class MemoryPanel extends JComponent {

	private static final long serialVersionUID = -7499914901139373055L;
	private Runner runner;
	
	private class Runner implements Runnable {
		private boolean autoUpdate = true;
		Runner() {
		}
		public void run() {
			while (autoUpdate) {
				try  {
					Thread.sleep(1000);
					repaint();
				} catch (InterruptedException e) {}
			}
		}
	}
	
	public MemoryPanel(int width, int height) {
		super();
		setPreferredSize(new Dimension(width,height));
		runner = new Runner();
		(new Thread(runner)).start();
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	public void paintComponent(Graphics g) {
		//System.out.println("paint");
		// Get current size of heap in bytes
	    long heapSize = Runtime.getRuntime().totalMemory()/(1024*1024);
	    
	    // Get maximum size of heap in bytes. The heap cannot grow beyond this size.
	    // Any attempt will result in an OutOfMemoryException.
	    long heapMaxSize = Runtime.getRuntime().maxMemory()/(1024*1024);
	    
	    // Get amount of free memory within the heap in bytes. This size will increase
	    // after garbage collection and decrease as new objects are created.
	    long heapFreeSize = Runtime.getRuntime().freeMemory()/(1024*1024);

	    g.setColor(Color.BLACK);
	    g.drawRect(0, 0, getWidth()-1, getHeight()-1);
	    
	    int w = (int)((getWidth()-1.0f)*heapSize/heapMaxSize);
	    g.setColor(new Color(224,224,224));
	    g.fillRect(0, 0, w, getHeight()-1);
	    g.setColor(Color.BLACK);
	    g.drawRect(0, 0, w, getHeight()-1);
	    
	    w = (int)((getWidth()-1.0f)*(heapSize-heapFreeSize)/heapMaxSize);
	    g.setColor(Color.LIGHT_GRAY);
	    g.fillRect(0, 0, w, getHeight()-1);
	    g.setColor(Color.BLACK);
	    g.drawRect(0, 0, w, getHeight()-1);
	    
	    
	    g.drawString("used: "+(heapSize-heapFreeSize)+" MB, heap: "+heapSize+" MB, max: "+heapMaxSize+" MB", 0, 15);
	    
	}
	
	public void setAutoUpdate(boolean autoUpdate) {
		runner.autoUpdate = autoUpdate;
	}

	

}
