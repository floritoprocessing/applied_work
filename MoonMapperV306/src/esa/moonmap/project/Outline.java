package esa.moonmap.project;

import java.awt.Polygon;
import java.io.Serializable;

public class Outline extends Polygon implements Serializable {

	private static final long serialVersionUID = -2297199129334023498L;
	
	private String filename;

	public Outline() {
		super();
	}

	public Outline(String filename, int[] arg0, int[] arg1, int arg2) {
		super(arg0, arg1, arg2);
		this.filename = filename;
		//System.out.println("new outline "+filename);
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return filename+": "+this.getBounds();
	}

	public String getFilename() {
		return filename;
	}

}
