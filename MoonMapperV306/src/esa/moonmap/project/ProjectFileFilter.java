package esa.moonmap.project;

import java.io.File;

import javax.swing.filechooser.FileFilter;


public class ProjectFileFilter extends FileFilter {

	public static final String extension = ".mmp";
	
	@Override
	public boolean accept(File f) {
		if (f.isDirectory()) {
			return true;
		}
		
		String filename = f.getName();
		if (filename.endsWith(extension))
			return true;
		return false;
	}

	@Override
	public String getDescription() {
		return "Project files";
	}

}
