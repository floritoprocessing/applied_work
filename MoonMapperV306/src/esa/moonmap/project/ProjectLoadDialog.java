package esa.moonmap.project;

import java.io.File;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.SwingWorker;

import esa.moonmap.MainGui;

public class ProjectLoadDialog extends JDialog {

	private final JProgressBar bar;// = new JProgressBar();
	//private final JLabel message;// = new JLabel("");
	private static final String PHASE="phase";
	private static final String FINISHED="finished";
	
	public ProjectLoadDialog(MainGui main, File file) {
		super(main,"Loading project",true);
		
		bar = new JProgressBar();
		bar.setStringPainted(true);
		bar.setIndeterminate(true);
		
	}
	
	private class ProjectLoadTask extends SwingWorker<Void, Void> {
		public ProjectLoadTask(MainGui main, File file) {
			//Project.l
		}
		protected Void doInBackground() throws Exception {
			return null;
		}
		public void done() {
			firePropertyChange(FINISHED, false, true);
			setProgress(100);
			boolean done=false;
			for (;!done;) {
				try {
					get();
					done=true;
				} catch (InterruptedException e) {
					done=false;
					continue;
				} catch (CancellationException e) {
					done=false;
					break;
				} catch (ExecutionException e) {
					done=false;
					e.getCause().printStackTrace();
					break;
				}
			}
		}
	}
	
	
	
	
	
}
