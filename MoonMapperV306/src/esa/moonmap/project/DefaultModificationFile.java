package esa.moonmap.project;

import esa.pds.component.PDSObject;
import esa.pds.component.PDSValue;

public class DefaultModificationFile {

	public static final String FILENAME_KEYWORD = "FILE_NAME";
	public static final String INDEX_KEYWORD = "INDEX";
	public static final String VISIBLE_KEYWORD = "VISIBLE";
	public static final String GAIN_VALUE_KEYWORD = "GAIN_VALUE";
	public static final String TOP_CUT_KEYWORD = "TOP_CUT";
	public static final String EDGE_SMOOTH_KEYWORD = "EDGE_SMOOTH";
	public static final String ARRANGE_LEVEL_KEYWORD = "ARRANGE_LEVEL";
	
	public static final Object[][] getOneLineData() {
		return oneLineData;
	}
	private static final Object[][] oneLineData = { 
		{ 
			new String(""),
			new Integer(0),
			new Boolean(true),
			new Double(1),
			new Integer(0),
			new Boolean(false),
			new Integer(0)
		} 
	};
	
	/*public static final String[] keywords = { 
		FILENAME_KEYWORD, 
		VISIBLE_KEYWORD,
		GAIN_VALUE_KEYWORD,
		TOP_CUT_KEYWORD,
		EDGE_SMOOTH_KEYWORD,
		ARRANGE_LEVEL_KEYWORD
	};*/
	
	public static final PDSValue[] pdsValues = { 
		new PDSValue(FILENAME_KEYWORD,""),
		new PDSValue(INDEX_KEYWORD,"0"),
		new PDSValue(VISIBLE_KEYWORD,"true"),
		new PDSValue(GAIN_VALUE_KEYWORD,"1.0"),
		new PDSValue(TOP_CUT_KEYWORD,"0"),
		new PDSValue(EDGE_SMOOTH_KEYWORD,"false"),
		new PDSValue(ARRANGE_LEVEL_KEYWORD,"0")
	};
	public static final PDSObject pdsObject = new PDSObject(pdsValues);
	
	
}
