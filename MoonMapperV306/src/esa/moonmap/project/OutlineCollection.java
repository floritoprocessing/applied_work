package esa.moonmap.project;

import java.awt.Point;
import java.io.Serializable;
import java.util.ArrayList;

public class OutlineCollection implements Serializable {

	private static final long serialVersionUID = -3844273880380128876L;
	
	private ArrayList<Outline> outlines;
	
	public OutlineCollection() {
		outlines = new ArrayList<Outline>();
	}
	
	public void addOutline(Outline polygon) {
		if (!outlines.contains(polygon))
			outlines.add(polygon);
	}
	
	public ArrayList<Outline> getOutlines() {
		return outlines;
	}
	
	public void clear() {
		outlines.clear();
	}

	public ArrayList<Outline> getOutlinesUnder(Point point) {
		ArrayList<Outline> out = new ArrayList<Outline>();
		for (Outline p:outlines) {
			if (p.contains(point)) out.add(p);
		}
		return out;
	}

}
