package esa.moonmap.project;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.Field;

import javax.imageio.ImageIO;
import javax.swing.SwingWorker;

import java.util.Hashtable;

import esa.file.tools.FileCopy;
import esa.moonmap.MainGui;
import esa.pds.PDSUnStringableException;
import esa.pds.PDSValueNotFoundException;
import esa.pds.component.PDSComponent;
import esa.pds.component.PDSObject;
import esa.pds.component.PDSValue;
import esa.pds.component.PDSValueCreator;
import esa.projection.Projection;

public class Project {

	/*
	 * THESE SETTINGS WILL BE SAVED WITH THE PROJECT FILE
	 */
	
	public String MAP_WIDTH = "";
	public String MAP_HEIGHT = "";
	public String PROJECTION_PIXEL_SCALE = "";
	public String PROJECTION_TYPE = "";
	public String PROJECTION_CENTER_LON = "";
	public String PROJECTION_CENTER_LAT = "";
	public String MAP_WEST = "";
	public String MAP_EAST = "";
	public String MAP_NORTH = "";
	public String MAP_SOUTH = "";
	
	public String INDEX_FILE = "";
	private static final String indexFileNameExt = "_indexFile.csv";
	
	public String MODIFICATION_FILE = "";
	private static final String modListNameExt = "_modList.csv";
	
	public String MAP_IMAGE_FILE = "";
	private static final String saveImageExt = "png";
	private static final String mapImageExt = ".png";
	
	public String MAP_OUTLINES_FILE = "";
	private static final String mapOutlinesNameExt = ".mol";
	
	public String CACHED_LO_RES = "";
	private static final String cachedProjectedNameExt = ".pch";
	
	public String CACHED_HEADERS = "";
	private static final String cachedHeadersNameExt = ".hch";

	private PDSObject[] indexFile;
	private PDSObject[] modificationFile;
	private int mapWidthPx, mapHeightPx;
	private Projection projection;
	private BufferedImage mapImage;
	
	/*
	 * Internal variables
	 */
	
	/**
	 * Stores the outlines of all images in an object.
	 * This object is also saved to _mapOutlines
	 * @see #mapOutlinesNameExt
	 */
    private OutlineCollection outlineCollection;
	//private String pathToFile = "";// ://.\\src\\default.mmp";
    private File projectFile = null;
	
	private MainGui main;
	private Hashtable cachedHeaders;
	
	private Hashtable cachedLowResProjected;

	
	
	//private ImageMemory imageMemory;
	
	public Project(MainGui main) {
		this.main = main;
	}

	public Project(MainGui main, 
			String indexFile, String pathToModfile, 
			int width, int height,
			Projection.Type projectionType, 
			float projectionCenterLongitude, float projectionCenterLatitude, 
			float west, float east,
			float north, float south,
			float projectionPixelScale) {
		
		System.out.println("NEW PROJECT");
		
		INDEX_FILE = indexFile;
		this.main = main;
		this.mapWidthPx = width;
		this.mapHeightPx = height;
		newMapImage();
		outlineCollection = new OutlineCollection();
		
		projection = Projection.getInstance(
				projectionType, projectionCenterLongitude, projectionCenterLatitude, projectionPixelScale);
		
		MAP_WIDTH = Integer.toString(width);
		MAP_HEIGHT = Integer.toString(height);
		PROJECTION_TYPE = projectionType.getName();
		PROJECTION_CENTER_LON = Float.toString(projectionCenterLongitude);
		PROJECTION_CENTER_LAT = Float.toString(projectionCenterLatitude);
		//PROJECTION_EAST_WEST = Float.toString(eastWestRange);
		//PROJECTION_NORTH_SOUTH = Float.toString(northSouthRange);
		MAP_WEST = Float.toString(west);
		MAP_EAST = Float.toString(east);
		MAP_NORTH = Float.toString(north);
		MAP_SOUTH = Float.toString(south);
		PROJECTION_PIXEL_SCALE = Float.toString(projectionPixelScale);
		
		boolean createModFile = true;
		if (pathToModfile!=null 
				&& pathToModfile.length()>0
				&& (new File(pathToModfile)).exists()
				&& (new File(pathToModfile).isFile())) 
				createModFile = false;
		
		loadIndexFile(createModFile);
		loadModificationFile(pathToModfile);
		
		//loadModificationFile(projectFile)
	}

	
	
	public void clearMapImage() {
		//mapImage
		Graphics g = mapImage.getGraphics();
		g.setColor(new Color(32,32,64));//Color.WHITE);
		g.fillRect(0, 0, mapImage.getWidth(null), mapImage.getHeight(null));
		//repaint();
	}

	public void clearOutlines() {
		outlineCollection.clear();
	}

	public Hashtable getCachedHeaders() {
		return cachedHeaders;
	}

	/*
	 * public ImageMemory getImageMemory() { return imageMemory; }
	 */
	
	public Hashtable getCachedLowResProjected() {
		return cachedLowResProjected;
	}
	
	public String getFullPath_index() {
		return INDEX_FILE;
	}
	
	public PDSObject[] getIndexFile() {
		return indexFile;
	}

	public String getMapEast() {
		return MAP_EAST;
	}

	public String getMapHeight() {
		return MAP_HEIGHT;
	}
	
	public BufferedImage getMapImage() {
		return mapImage;
	}

	
	public String getMapNorth() {
		return MAP_NORTH;
	}
	
	public String getMapSouth() {
		return MAP_SOUTH;
	}
	
	public String getMapWest() {
		return MAP_WEST;
	}
	
	public String getMapWidth() {
		return MAP_WIDTH;
	}
	
	public PDSObject[] getModificationFile() {
		return modificationFile;
	}

	public OutlineCollection getOutlineCollection() {
		return outlineCollection;
	}

	/*
	 * public String getPath_index() { return INDEX_PATH; }
	 * 
	 * public void setPath_index(final String value) { INDEX_PATH = value; }
	 */

	/*public String getPathToProjectFile() {
		return pathToFile;
	}*/
	
	public File getProjectFile() {
		return projectFile;
	}
	
	public Projection getProjection() {
		return projection;
	}

	public String getProjectionLatitude() {
		return PROJECTION_CENTER_LAT;
	}

	public String getProjectionLongitude() {
		return PROJECTION_CENTER_LON;
	}
	
	/*public int getWidth() {
		return mapWidthPx;
	}*/
	
	/*public int getHeight() {
		return mapHeightPx;
	}*/
	
	public String getProjectionPixelScale() {
		return PROJECTION_PIXEL_SCALE;
	}
	
	public String getProjectionType() {
		return PROJECTION_TYPE;
	}
	
	public static Project load(MainGui main, File file) {
		
		//this.projectFile = file;
		//Project project = this;
		Project project = new Project(main);
		project.projectFile = file;
			
		/*
		 * Load project file and set fields
		 */
		
		if (!project.loadProjectFileAndSetFields(file)) return null;
		
		/*
		 * Set variables from fields
		 */
		
		project.mapWidthPx = Integer.parseInt(project.MAP_WIDTH);
		project.mapHeightPx = Integer.parseInt(project.MAP_HEIGHT);
		
		
		/*
		 * Set Projection
		 */
		float projLon = Float.parseFloat(project.PROJECTION_CENTER_LON);
		float projLat = Float.parseFloat(project.PROJECTION_CENTER_LAT);
		float projDToPix = Float.parseFloat(project.PROJECTION_PIXEL_SCALE);
		Projection.Type foundType = null;
		for (Projection.Type type:Projection.Type.values()) {
			if (project.PROJECTION_TYPE.equals(type.getName()))
				foundType = type;
		}
		if (foundType==null) {
			main.statusArea.println("Unknown projection type "+project.PROJECTION_TYPE+" while loading project file");
			return null;
		}
		project.projection = Projection.getInstance(foundType, projLon, projLat, projDToPix);
		
		/*
		 * Load index file
		 */
		
		if (!project.loadIndexFile(false))
			return null;
		
		/*
		 * Load modification file
		 */
		
		project.MODIFICATION_FILE = file.getAbsolutePath().replaceAll(ProjectFileFilter.extension, modListNameExt);
		if (!project.loadModificationFile(project.MODIFICATION_FILE))
			return null;
		
		/*
		 * Load imageMap
		 */
		
		project.loadImageMap();
		
		/*
		 * Load OutlineCollection
		 */
		
		project.loadOutlineCollection();
		
		/*
		 * Load Cached low res
		 */
		
		project.loadCachedLowResProjected();
		
		/*
		 * Load Cached headers
		 */
		
		project.loadCachedHeaders();
		
		return project;

	}
	
	/**
	 * Loads the {@link #cachedLowResProjected} as specified by {@link #CACHED_LO_RES}
	 *
	 */
	private void loadCachedLowResProjected() {
		File cachedLowResFile = new File(CACHED_LO_RES);
		if (cachedLowResFile.exists()) {
			try {
				FileInputStream fis = new FileInputStream(cachedLowResFile);
				ObjectInputStream ois = new ObjectInputStream(fis);
				cachedLowResProjected = (Hashtable)ois.readObject();
				ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Loads the {@link #cachedHeaders} as specified by {@link #CACHED_HEADERS}
	 *
	 */
	private void loadCachedHeaders() {
		File cachedHeadersFile = new File(CACHED_HEADERS);
		if (cachedHeadersFile.exists()) {
			try {
				FileInputStream fis = new FileInputStream(cachedHeadersFile);
				ObjectInputStream ois = new ObjectInputStream(fis);
				cachedHeaders = (Hashtable)ois.readObject();
				ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * Loads the image map from the variable {@link #MAP_IMAGE_FILE}
	 * If the file does not exist, creates a new map image
	 * @see #newMapImage()
	 *
	 */
	private void loadImageMap() {
		File mapImageFile = new File(MAP_IMAGE_FILE);
		if (mapImageFile.exists()) {
			try {
				mapImage = ImageIO.read(mapImageFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			newMapImage();
		}
	}
	
	private boolean loadIndexFile(boolean createModificationFile) {
		
		main.statusArea.println("loading index file " + INDEX_FILE);

		indexFile = null;
		try {
			indexFile = PDSObject.loadCSVFile(INDEX_FILE);
		} catch (FileNotFoundException e) {
			main.statusArea.println(e.toString());
			return false;
		} catch (IOException e) {
			main.statusArea.println(e.toString());
			return false;
		}
		if (indexFile == null) {
			main.statusArea.println("failed to load");
			return false;
		}
		main.statusArea.println("done");

		// System.out.println(indexFile);

		if (indexFile != null) {
			//imageMemory = new ImageMemory(indexFile.length);
			
			try {
				main.statusArea.println("loadeded " + indexFile.length + " lines");
				main.statusArea.println("converting to indexFileAsData");

				int rows = indexFile.length;
				
				//cachedLowResGeometry = new Hashtable(rows);
				cachedHeaders = new Hashtable(rows);
				cachedLowResProjected = new Hashtable(rows);
				//imageCache = new Hashtable(rows);
				
				int cols = DefaultModificationFile.getOneLineData()[0].length;

				if (createModificationFile) {
					modificationFile = new PDSObject[rows];
					for (int row = 0; row < rows; row++) {
						for (int col = 0; col < cols; col++) {
							if (col == 0) {
								modificationFile[row] = new PDSObject();
								modificationFile[row]
										.addNewValue(indexFile[row]
												.getPDSValue(DefaultModificationFile.FILENAME_KEYWORD)); //"Filename"
							} else if (DefaultModificationFile.pdsValues[col].getName().equals(DefaultModificationFile.INDEX_KEYWORD))
								modificationFile[row].addNewValue(new PDSValue(DefaultModificationFile.INDEX_KEYWORD,row));
							else
								modificationFile[row]
										.addNewValue(DefaultModificationFile.pdsValues[col]);
						}
					}
				}
				main.statusArea.println("done");

				return true;
			} catch (PDSValueNotFoundException e) {
				main.statusArea.println("Illegal index file, missing filename");
				return false;
			}
		}

		indexFile = null;
		main.statusArea.println("problem loading index file");
		return false;
	}
	/**
	 * Loads the modification file from the specified path and sets the
	 * modificationFile
	 * @param pathToModfile
	 * @return
	 */
	private boolean loadModificationFile(String pathToModfile) {
	//private boolean loadModificationFile(File projectFile) {
		/*MODIFICATION_FILE = projectFile.getAbsolutePath().replaceAll(
				ProjectFileFilter.extension, modListNameExt);*/
		modificationFile = null;
		try {
			modificationFile = PDSObject.loadCSVFile(pathToModfile);
		} catch (FileNotFoundException e) {
			main.statusArea.println(e.toString());
			e.printStackTrace();
		} catch (IOException e) {
			main.statusArea.println(e.toString());
			e.printStackTrace();
		}
		
		if (modificationFile!=null) {
			for (int i=0;i<modificationFile.length;i++) {
				if (modificationFile[i].size()<DefaultModificationFile.pdsValues.length)
					for (int col=modificationFile[i].size();col<DefaultModificationFile.pdsValues.length;col++) {
						modificationFile[i].addNewValue(DefaultModificationFile.pdsValues[col]);
					}
			}
		}
		return (modificationFile != null);
	}
	/**
	 * Loads the OutlineCollection as specified by {@link #MAP_OUTLINES_FILE}
	 * If the file does not exists, creates an empty OutlineCollection
	 *
	 */
	private void loadOutlineCollection() {
		File mapOutlinesFile = new File(MAP_OUTLINES_FILE);
		outlineCollection = new OutlineCollection();
		if (mapOutlinesFile.exists()) {
			try {
				FileInputStream fis = new FileInputStream(mapOutlinesFile);
				ObjectInputStream ois = new ObjectInputStream(fis);
				outlineCollection = (OutlineCollection)ois.readObject();
				ois.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	private boolean loadProjectFileAndSetFields(File file) {
		boolean ok = true;
		BufferedReader reader = null;
		PDSObject vars = new PDSObject();
		try {
			reader = new BufferedReader(new FileReader(file));
			String line = "";
			while ((line = reader.readLine()) != null) {
				vars.addNewValue(PDSValueCreator.createFromPDSString(line));
			}
			reader.close();
			reader = null;
			ok = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			ok = false;
		} catch (IOException e) {
			e.printStackTrace();
			ok = false;
		} finally {
			if (reader!=null) {
				try {
					reader.close();
				} catch (IOException e) {}
			}
		}
		
		if (!ok) return ok;
		
		/*
		 * Create PDS values and set fields
		 */
		for (int i = 0; i < vars.size(); i++) {
			PDSComponent component = vars.getComponentAt(i);
			if (component.isPDSValue()) {
				PDSValue pdsval = (PDSValue) component;
				Field f = null;
				try {
					f = this.getClass().getField(pdsval.getName());
					f.set(this, pdsval.getValue());
				} catch (SecurityException e) {
					main.statusArea.println(e.getLocalizedMessage());
					e.printStackTrace();
					return false;
				} catch (NoSuchFieldException e) {
					main.statusArea.println("No such field: "+e.getMessage()+". Ignoring it");
				} catch (IllegalArgumentException e) {
					main.statusArea.println(e.getLocalizedMessage());
					e.printStackTrace();
					return false;
				} catch (IllegalAccessException e) {
					main.statusArea.println(e.getLocalizedMessage());
					e.printStackTrace();
					return false;
				}
				if (f!=null) {
					main.statusArea.println("Setting field "+f.getName()+" to "+pdsval.getValue());
				}
			}
		}
		return ok;
	}

	/*public Hashtable getCachedLowResGeometry() {
		return cachedLowResGeometry;
	}*/
	
	//throws IllegalArgumentException, SecurityException, InstantiationException, IllegalAccessException, InvocationTargetException, NoSuchMethodException, ProjectionNotFoundError
	public void modify(
			int width, int height, 
			Projection.Type projectionType, 
			float projectionCenterLongitude, float projectionCenterLatitude, 
			float west, float east,
			float north, float south,
			float pixelScale) {
		
		this.mapWidthPx = width;
		this.mapHeightPx = height;
		
		projection = Projection.getInstance(projectionType, projectionCenterLongitude, projectionCenterLatitude, pixelScale);
		
		MAP_WIDTH = Integer.toString(width);
		MAP_HEIGHT = Integer.toString(height);
		PROJECTION_TYPE = projectionType.getName();
		PROJECTION_CENTER_LON = Float.toString(projectionCenterLongitude);
		PROJECTION_CENTER_LAT = Float.toString(projectionCenterLatitude);
		MAP_WEST = Float.toString(west);
		MAP_EAST = Float.toString(east);
		MAP_NORTH = Float.toString(north);
		MAP_SOUTH = Float.toString(south);
		PROJECTION_PIXEL_SCALE = Float.toString(pixelScale);
		
		//cachedHeaders.clear();
		cachedLowResProjected.clear();
		//imageCache.clear();
		
		newMapImage();
		outlineCollection.clear();
	}
	
	public void newMapImage() {
		mapImage = new BufferedImage(mapWidthPx,mapHeightPx,BufferedImage.TYPE_INT_RGB);
		Graphics2D g2d = mapImage.createGraphics();
		g2d.setColor(Color.WHITE);
		g2d.fillRect(0, 0, mapWidthPx, mapHeightPx);
	}
	
	/*public Hashtable getImageCache() {
		return imageCache;
	}*/

	public void save() {
		//save(new File(pathToFile));
		save(projectFile);
	}

	//private boolean
	
	public void save(File projectFile) {

		this.projectFile = projectFile;
		//this.pathToFile = projectFile.getAbsolutePath();

		/*
		 * SAVE INDEX FILE
		 */

		String newIndexFileName = projectFile.getAbsolutePath().replaceAll(
				ProjectFileFilter.extension, indexFileNameExt);
		File indexFile = new File(INDEX_FILE);
		File projectIndexFile = new File(newIndexFileName);
		
		if (!projectIndexFile.exists()) {
			main.statusArea.println("Saving index file "+projectIndexFile.getName());
			try {
				FileCopy.copyWithTempFile(indexFile, projectIndexFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		INDEX_FILE = newIndexFileName;

		/*
		 * SAVE MODIFICATION FILE
		 */

		MODIFICATION_FILE = projectFile.getAbsolutePath().replaceAll(
				ProjectFileFilter.extension, modListNameExt);
		File mFile = new File(MODIFICATION_FILE);
		main.statusArea.println("Saving modification file "+mFile.getName());
		try {
			BufferedWriter modWriter = new BufferedWriter(new FileWriter(mFile));
			modWriter.write(modificationFile[0].toCSVStrings(true));
			modWriter.newLine();
			for (int i = 0; i < modificationFile.length; i++) {
				modWriter.write(modificationFile[i].toCSVStrings(false));
				modWriter.newLine();
			}
			modWriter.flush();
			modWriter.close();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (PDSUnStringableException e) {
			e.printStackTrace();
		}
		
		/*
		 * Save mapImage
		 */
		
		if (mapImage!=null) {
			MAP_IMAGE_FILE = projectFile.getAbsolutePath().replaceAll(
					ProjectFileFilter.extension, mapImageExt);
			
			File mapImageFile = new File(MAP_IMAGE_FILE);
			try {
				ImageIO.write(mapImage,saveImageExt,mapImageFile);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * Save OutlineCollection
		 */
		
		if (outlineCollection!=null) {
			MAP_OUTLINES_FILE = projectFile.getAbsolutePath().replaceAll(
					ProjectFileFilter.extension, mapOutlinesNameExt);
			
			File mapOutlinesFile = new File(MAP_OUTLINES_FILE);
			try {
				FileOutputStream fos = new FileOutputStream(mapOutlinesFile);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(outlineCollection);
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * SAVE CACHED LOW RES
		 */
		
		if (cachedLowResProjected!=null) {
			CACHED_LO_RES = projectFile.getAbsolutePath().replaceAll(
					ProjectFileFilter.extension, cachedProjectedNameExt);
			
			File cachedLowResFile = new File(CACHED_LO_RES);
			try {
				FileOutputStream fos = new FileOutputStream(cachedLowResFile);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(cachedLowResProjected);
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		/*
		 * SAVE CACHED HEADERS
		 */
		//TODO
		
		if (cachedHeaders!=null) {
			CACHED_HEADERS = projectFile.getAbsolutePath().replaceAll(
					ProjectFileFilter.extension, cachedHeadersNameExt);
			
			File cachedHeadersFile = new File(CACHED_HEADERS);
			try {
				FileOutputStream fos = new FileOutputStream(cachedHeadersFile);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				oos.writeObject(cachedHeaders);
				oos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		

		/*
		 * SAVE PROJECT FILE
		 */

		Field[] fields = this.getClass().getFields();
		PDSObject vars = new PDSObject();
		main.statusArea.println("Saving project file "+projectFile.getName());
		for (int i = 0; i < fields.length; i++) {
			try {
				String varName = fields[i].getName();
				String value = (String) fields[i].get(this);
				vars.addNewValue(new PDSValue(varName, value));
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		}

		try {
			final BufferedWriter writer = new BufferedWriter(new FileWriter(projectFile));
			final String[] lines = vars.toPDSString().split("\r\n");
			for (int i = 0; i < lines.length; i++) {
				writer.write(lines[i]);
				writer.newLine();
			}
			writer.flush();
			writer.close();
		} catch (final IOException e) {
			e.printStackTrace();
		}
		
		

	}

	public void setFullPath_index(final String value) {
		INDEX_FILE = value;
	}
	
}
