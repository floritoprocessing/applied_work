package esa.moonmap;

import java.lang.reflect.InvocationTargetException;

import javax.swing.SwingUtilities;

import processing.core.PApplet;
import processing.core.PGraphics;

public class MoonMapper extends PApplet {

	private static final long serialVersionUID = 3283727971808921886L;

	public static void main(String[] args) {
		PApplet.main(new String[] {"--location=0,0","esa.moonmap.MoonMapper"});
	}
	
	public MainGui gui;
	private static String irenderer = P3D;
	
	public void createAndShowGui() {
		gui = new MainGui(this);
	}
	
	
	public PGraphics createGraphics(int width, int height) {
		return createGraphics(width, height, irenderer);
	}
	
	public void settings() {
		size(100,100,irenderer);
	}
	
	public void setup() {
		
		noLoop();
	}
	
	public void draw() {
		try {
			SwingUtilities.invokeAndWait(new Runnable() {
				public void run() {
					createAndShowGui();
				}
			});
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

}
