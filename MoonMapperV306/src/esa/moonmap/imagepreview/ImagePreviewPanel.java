package esa.moonmap.imagepreview;

import esa.pds.amie.AMIEFilter;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ImagePreviewPanel extends JPanel {

	private static final long serialVersionUID = -7181974197375624220L;
	private BufferedImage img;
	private Rectangle filterRectangle;

	//private int scaledWidth, scaledHeight;

	private static final int checkerSize = 4;
	private static final int checkerSize2 = checkerSize * 2;
	private static int previewSize = 160;

	public ImagePreviewPanel(BufferedImage img) {
		super();
		setBufferedImage(img,null);
		repaint();
	}

	/**
	 * This method is overriden to draw the image and scale the graphics
	 * accordingly
	 */
	public void paintComponent(Graphics grp) {

		previewSize = Math.min(getSize().width,getSize().height);
		Graphics2D g2D = (Graphics2D) grp;

		g2D.setColor(Color.WHITE);
		g2D.fillRect(1, 1, previewSize, previewSize);
		g2D.setColor(Color.LIGHT_GRAY);
		for (int y = 0; y < previewSize; y += checkerSize) {
			for (int x = y % checkerSize2; x < previewSize; x += checkerSize2) {
				g2D.fillRect(x + 1, y + 1, checkerSize, checkerSize);
			}
		}

		// checkered background
		g2D.setColor(Color.BLACK);
		g2D.drawLine(0, 0, previewSize + 1, 0);
		g2D.drawLine(0, previewSize + 1, previewSize + 1, previewSize + 1);
		g2D.drawLine(0, 0, 0, previewSize + 1);
		g2D.drawLine(previewSize + 1, 0, previewSize + 1, previewSize + 1);
		
		// filter lines
		g2D.setColor(new Color(96,96,96));
		g2D.setFont(new Font(Font.SANS_SERIF,Font.BOLD,12));
		for (int i=0;i<AMIEFilter.names.length;i++) {
			//g2D.setColor(new Color(0,0,0,128));
			Rectangle r = createDisplayRectangle(AMIEFilter.getFilterRectangle(i));
			g2D.drawLine(r.x,r.y,r.x+r.width,r.y); // top
			g2D.drawLine(r.x,r.y,r.x,r.y+r.height); // left
			g2D.drawLine(r.x+r.width,r.y,r.x+r.width,r.y+r.height); // right
			g2D.drawLine(r.x,r.y+r.height,r.x+r.width,r.y+r.height); // bottom
			Rectangle2D r2 = g2D.getFontMetrics().getStringBounds(AMIEFilter.names[i], g2D);
			//g2D.setColor(new Color(0,0,0));
			g2D.drawString(AMIEFilter.names[i],r.x+r.width/2-(int)r2.getCenterX(),r.y+r.height/2-(int)r2.getCenterY());
			//System.out.println(r2);
		}

		if (img != null && filterRectangle!=null) {
			
			Rectangle imageRect = createDisplayRectangle(filterRectangle);
			g2D.drawImage(img, imageRect.x, imageRect.y, imageRect.width, imageRect.height, this);
		}
			
	}
	
	private Rectangle createDisplayRectangle(Rectangle filterRectangle) {
		Rectangle out = new Rectangle(filterRectangle);
		int fac = previewSize/4;
		out.x *= fac;
		out.y *= fac;
		out.width *= fac;
		out.height *= fac;
		out.translate(1, 1);
		return out;
	}

	/**
	 * This method is overriden to return the preferred size which will be the
	 * width and height of the image
	 */
	public Dimension getPreferredSize() {
		//System.out.println("getPreferredSize");
		return new Dimension(previewSize + 2, previewSize + 2);
	}
	
	public void setBufferedImage(BufferedImage img, Rectangle filterRectangle) {
		this.img = img;
		this.filterRectangle = filterRectangle;
		repaint();
	}

}
