package esa.moonmap.indexfilecreator;

import esa.pds.CSVFileWriter;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

public class ReduceTaskDialog extends JDialog implements ActionListener, PropertyChangeListener{

	private static final long serialVersionUID = 4362678624037475534L;
	private JLabel barLabel = new JLabel("Parsing file");
	private JProgressBar bar = new JProgressBar();
	private JLabel filesParsed = new JLabel("");
	private JButton cancelButton = new JButton("Cancel");
	
	private ReduceTask task;
	boolean taskIsFinished = false;
	
	public ReduceTaskDialog(JDialog parent, ReduceTask task) {
		super(parent,true);
		this.task = task;
		task.addPropertyChangeListener(this);
		getContentPane().setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		bar.setIndeterminate(true);
		bar.setStringPainted(false);
		getContentPane().add(barLabel,c);
		c.gridx++;
		getContentPane().add(bar,c);
		
		c.gridx=0;
		c.gridy++;
		getContentPane().add(new JLabel("lines parsed: "),c);
		c.gridx++;
		getContentPane().add(filesParsed,c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		cancelButton.addActionListener(this);
		getContentPane().add(cancelButton,c);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
		        cancelOrDone();
		    }
		});
		pack();
	}
	
	private void cancelOrDone() {
		if (!taskIsFinished) {
			int n = JOptionPane.showConfirmDialog(this, "Are you sure you want to cancel?", "Cancel progress?", JOptionPane.YES_NO_OPTION);
			if (n==JOptionPane.YES_OPTION) {
				task.cancel(true);
				setVisible(false);
				dispose();
			}
		} else {
			setVisible(false);
			dispose();
		}
	}
	
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource().equals(cancelButton)) cancelOrDone();
	}

	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getPropertyName().equals(CSVFileWriter.PROPERTY_LINES_PARSED)) {
			int fp = ((Integer)evt.getNewValue()).intValue();
			filesParsed.setText(""+fp);
			//pack();
		}
		else if (evt.getPropertyName().equals(ReduceTask.PROPERTY_FINISHED)) {
			taskIsFinished = ((Boolean)evt.getNewValue()).booleanValue();
			bar.setIndeterminate(false);
			bar.setValue(100);
			//cancelButton.removeActionListener(this);
			cancelButton.setText("Done");
		}
	}

}
