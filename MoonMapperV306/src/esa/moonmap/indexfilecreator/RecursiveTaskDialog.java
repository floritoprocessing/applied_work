package esa.moonmap.indexfilecreator;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

public class RecursiveTaskDialog extends JDialog implements ActionListener, PropertyChangeListener {
	
	private static final long serialVersionUID = 8471133859023940458L;
	//private JDialog parent;
	private JLabel barLabel = new JLabel("Counting files");
	private JProgressBar bar = new JProgressBar();
	private JLabel pathLabel = new JLabel("...");
	private JLabel fileLabel = new JLabel("...");
	private JLabel filesCounted = new JLabel("Files counted: ");
	private JLabel filesCountedLabel = new JLabel("...");
	private JButton cancelButton = new JButton("Cancel");
	
	private RecursiveTask task;
	private boolean taskIsFinished = false;
	
	public RecursiveTaskDialog(JDialog parent, RecursiveTask task) {
		super(parent,"Progress",true);
		this.task = task;
		task.addPropertyChangeListener(this);
		//this.parent = parent;
		getContentPane().setLayout(new GridBagLayout());
		//setBorder(BorderFactory.createEtchedBorder());
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		bar.setValue(0);
		bar.setStringPainted(true);
		getContentPane().add(barLabel,c);
		c.gridx++;
		getContentPane().add(bar,c);
		c.gridx=0;
		c.gridy++;
		getContentPane().add(new JLabel("current path"),c);
		c.gridx++;
		getContentPane().add(pathLabel,c);
		c.gridx=0;
		c.gridy++;
		getContentPane().add(new JLabel("current file"),c);
		c.gridx++;
		getContentPane().add(fileLabel,c);
		c.gridx=0;
		c.gridy++;
		getContentPane().add(filesCounted,c);
		c.gridx++;
		getContentPane().add(filesCountedLabel,c);
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		cancelButton.addActionListener(this);
		getContentPane().add(cancelButton,c);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
		        cancelOrDone();
		    }
		});
		pack();
	}
	
	private void cancelOrDone() {
		if (!taskIsFinished) {
			int n = JOptionPane.showConfirmDialog(this, "Are you sure you want to cancel?", "Cancel progress?", JOptionPane.YES_NO_OPTION);
			if (n==JOptionPane.YES_OPTION) {
				task.stopRecursing();
				task.cancel(true);
				task.finish();
				setVisible(false);
				dispose();
			}
		} else {
			setVisible(false);
			dispose();
		}
	}
	
	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource().equals(cancelButton)) cancelOrDone();
	}
	
	/*
	 * (non-Javadoc)
	 * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
	 */
	public void propertyChange(PropertyChangeEvent evt) {
		
		if (evt.getPropertyName()==RecursiveTask.PROPERTY_INDETERMINATE) {
			boolean indeterminate = (Boolean) evt.getNewValue();
			if (indeterminate) {
				barLabel.setText("counting files");
				bar.setString("please wait");
				filesCounted.setText("Files counted: ");
			}
			else {
				barLabel.setText("parsing files");
				bar.setString(null);
				filesCounted.setText("Files parsed: ");
			}
			bar.setIndeterminate(indeterminate);
		}
		else if (evt.getPropertyName()=="progress") {
			int progress = (Integer) evt.getNewValue();
            bar.setValue(progress);
		}
		else if (evt.getPropertyName()==RecursiveTask.PROPERTY_PATH_CHANGE) {
			String path = ((File)evt.getNewValue()).getAbsolutePath();
			if (path.length()>60) {
				path = path.substring(0,path.indexOf("\\",3)+1)+" ... "+path.substring(path.lastIndexOf("\\"),path.length());
			}
			pathLabel.setText(path);
			//frame.pack();
			pack();
		}
		else if (evt.getPropertyName()==RecursiveTask.PROPERTY_FILE_CHANGE) {
			fileLabel.setText(((File)evt.getNewValue()).getName());
		}
		else if (evt.getPropertyName()==RecursiveTask.PROPERTY_FILE_COUNT_CHANGE) {
			filesCountedLabel.setText(""+((Integer)evt.getNewValue()).intValue());
		}
		else if (evt.getPropertyName()==RecursiveTask.PROPERTY_PARSED_COUNT_CHANGE) {
			Point counts = (Point)evt.getNewValue();
			filesCountedLabel.setText(counts.y+"/"+counts.x);
		}
		else if (evt.getPropertyName()==RecursiveTask.PROPERTY_FINISHED) {
			boolean done = (Boolean)evt.getNewValue();
			if (done) {
				taskIsFinished = true;
				cancelButton.setText("Done");
				//bar.setString("Finished!");
				//setVisible(false);
				//dispose();
			}
		}
	}

	

}
