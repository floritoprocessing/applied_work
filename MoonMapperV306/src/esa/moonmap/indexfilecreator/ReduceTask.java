package esa.moonmap.indexfilecreator;

import esa.pds.CSVFileLoader;
import esa.pds.CSVFileWriter;
import esa.pds.PDSFileConstrain;

import java.io.File;

import javax.swing.SwingWorker;

public class ReduceTask extends SwingWorker<Void, Void> {

	public static final String PROPERTY_FINISHED = "finished";
	
	private CSVFileLoader loader;
	private CSVFileWriter writer;
	private PDSFileConstrain constrain;
	
	public ReduceTask(File inputFile, File outputFile, PDSFileConstrain constrain) {
		loader = new CSVFileLoader(inputFile);
		writer = new CSVFileWriter(outputFile);
		this.constrain = constrain;
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		if ( !writer.processAndSave(loader,constrain,200,this) )
			throw new RuntimeException("Problem creating file");
		return null;
	}
	
	public void done() {
		firePropertyChange(PROPERTY_FINISHED, false, true);
	}
}
