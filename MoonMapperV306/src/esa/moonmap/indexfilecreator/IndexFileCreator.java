package esa.moonmap.indexfilecreator;

import esa.moonmap.MainGui;
import esa.pds.PDSCompare;
import esa.pds.PDSFileConstrain;
import esa.pds.dictionary.Element;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class IndexFileCreator extends JDialog implements ActionListener {

	private static final long serialVersionUID = 916121076137584266L;
	public static final int FROM_FOLDERS = 0;
	public static final int FROM_FILE = 1;
	
	//private Frame frame;
	private int source;
	
	private JButton sourceFolderBrowseButton = new JButton("Browse");
	private JLabel sourceFolderLabel = new JLabel("___");
	
	private JButton sourceFileBrowseButton = new JButton("Browse");
	private JLabel sourceFileLabel = new JLabel("___");
	
	private File sourceFolder;// = new File("G:\\S1-L-X-AMIE-3-RDR-CAL-V0.3.1-2\\S1-L-X-AMIE-3-RDR-LUNAR_PHASE-V1.0\\data\\");
	private File sourceFile;
	
	private JPanel variablesPanel;
	private JLabel variablesLabel = new JLabel("Variables to index");
	private Vector<JLabel> variableNamesLabel = new Vector<JLabel>();
	private Vector<JButton> variableRemoveButtons = new Vector<JButton>();
	private JButton variableAddButton = new JButton("Add variable");
	
	private JPanel filterPanel = new JPanel();
	private JLabel filterLabel = new JLabel("Filter");
	private Vector<JLabel> filterNameLabels = new Vector<JLabel>();
	private Vector<JButton> filterRemoveButtons = new Vector<JButton>();
	private JButton filterAddButton = new JButton("Add filter");
	private FilterSelectDialog filterSelectDialog;
	
	private JPanel outputPanel = new JPanel();
	private JLabel outputLabel = new JLabel("Output");
	private JButton outputFileBrowseButton = new JButton("Select");
	private JLabel outputFileLabel = new JLabel("___");
	
	private JButton okButton = new JButton("Ok");
	private JButton cancelButton = new JButton("Cancel");
	
	private File outputFile;
	
	/*
	 * 
	 */
	private Vector<PDSCompare> filters = new Vector<PDSCompare>();
	
	private MainGui mainGui;
	
	public IndexFileCreator(MainGui mainGui, int source) {
		super(mainGui,true);
		//this.frame = frame;
		this.mainGui = mainGui;
		setTitle("Create index file");
		
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
		        close();
		    }
		});
		
		this.source = source;
		
		//JOptionPane optionsPane = new JOptionPane()
		
		//Container pane = getContentPane();
		JPanel pane = new JPanel();
		pane.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		
		/*
		 * Source panel
		 */
		c.gridx=0;
		c.gridy=0;
		c.insets = new Insets(5,5,5,5);
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth=2;
		pane.add(new JLabel("Source"),c);
		
		JPanel sourcePanel = new JPanel();
		sourcePanel.setLayout(new GridBagLayout());
		sourcePanel.setBorder(BorderFactory.createEtchedBorder());
		
		if (source==FROM_FOLDERS) {
			c.gridx=0;
			c.gridy=0;
			c.gridwidth=1;
			c.anchor = GridBagConstraints.WEST;
			sourcePanel.add(new JLabel("Select root folder"),c);
			c.gridx=1;
			c.gridy=0;
			sourceFolderBrowseButton.addActionListener(this);
			sourcePanel.add(sourceFolderBrowseButton,c);
			c.gridx=0;
			c.gridy=1;
			c.gridwidth=2;
			sourceFolderLabel.setVisible(false);
			sourcePanel.add(sourceFolderLabel,c);
		} else if (source==FROM_FILE) {
			c.gridx=0;
			c.gridy=0;
			c.gridwidth=1;
			c.anchor = GridBagConstraints.WEST;
			sourcePanel.add(new JLabel("Select index file"),c);
			c.gridx=1;
			c.gridy=0;
			sourceFileBrowseButton.addActionListener(this);
			sourcePanel.add(sourceFileBrowseButton,c);
			c.gridx=0;
			c.gridy=1;
			c.gridwidth=2;
			sourceFileLabel.setVisible(false);
			sourcePanel.add(sourceFileLabel,c);
		}
		
		c.gridx=0;
		c.gridy=1;
		c.gridwidth=2;
		pane.add(sourcePanel,c);
		
		/*
		 * Variables panel
		 */
		
		if (source==FROM_FOLDERS) {
			variablesPanel = new JPanel();
			variablesPanel.setLayout(new GridBagLayout());
			variablesPanel.setBorder(BorderFactory.createEtchedBorder());
			
			c.gridx=0;
			c.gridy=0;
			c.gridwidth=1;
			variableAddButton.addActionListener(this);
			variablesPanel.add(variableAddButton,c);
			
			c.gridx=0;
			c.gridy=2;
			c.gridwidth=2;
			pane.add(variablesLabel,c);
			
			c.gridx=0;
			c.gridy=3;
			pane.add(variablesPanel,c);
		}
		
		/*
		 * Filter panel
		 */
		
		filterPanel.setLayout(new GridBagLayout());
		filterPanel.setBorder(BorderFactory.createEtchedBorder());
		
		c.gridx=0;
		c.gridy=0;
		c.gridwidth=1;
		filterAddButton.addActionListener(this);
		filterPanel.add(filterAddButton,c);
		
		c.gridx=0;
		c.gridy=4;
		c.gridwidth=2;
		pane.add(filterLabel,c);
		
		c.gridx=0;
		c.gridy=5;
		pane.add(filterPanel,c);
		
		/*
		 * Output panel
		 */
		
		outputPanel.setLayout(new GridBagLayout());
		outputPanel.setBorder(BorderFactory.createEtchedBorder());
		
		c.gridx=0;
		c.gridy=0;
		c.gridwidth=1;
		outputPanel.add(new JLabel("Select output file"),c);
		
		c.gridx=1;
		c.gridy=0;
		outputFileBrowseButton.addActionListener(this);
		outputPanel.add(outputFileBrowseButton,c);
		
		c.gridx=0;
		c.gridy=1;		
		c.gridwidth=2;
		outputFileLabel.setVisible(false);
		outputPanel.add(outputFileLabel,c);
		
		
		c.gridx=0;
		c.gridy=6;
		c.gridwidth=2;
		//outputLabel.setVisible(false);
		pane.add(outputLabel,c);
		
		c.gridx=0;
		c.gridy=7;
		outputPanel.setBorder(BorderFactory.createEtchedBorder());
		//outputPanel.setVisible(false);
		pane.add(outputPanel,c);
		
		
		c.gridx=0;
		c.gridy=8;
		c.gridwidth=2;
		pane.add(new JLabel("Create index file?"),c);
		c.gridwidth=1;
		c.gridx=0;
		c.gridy=9;
		okButton.setEnabled(false);
		okButton.addActionListener(this);
		pane.add(okButton,c);
		c.gridx=1;
		cancelButton.addActionListener(this);
		pane.add(cancelButton,c);
		
		filterSelectDialog = new FilterSelectDialog(this);
        filterSelectDialog.pack();
		
		setContentPane(pane);
		
		pack();
		setVisible(true);
	}

	public void actionPerformed(ActionEvent arg0) {
		Object source = arg0.getSource();
		
		int variablesRemoveButtonPressed = -1;
		for (int i=0;i<variableRemoveButtons.size();i++) 
			if (source.equals(variableRemoveButtons.elementAt(i))) 
				variablesRemoveButtonPressed = i;
		
		int filterRemoveButtonPressed = -1;
		for (int i=0;i<filterRemoveButtons.size();i++) 
			if (source.equals(filterRemoveButtons.elementAt(i))) 
				filterRemoveButtonPressed = i;
		
		if (source.equals(sourceFolderBrowseButton)) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogType(JFileChooser.OPEN_DIALOG);
			if (sourceFolder!=null)
				chooser.setCurrentDirectory(sourceFolder);
			else
				chooser.setCurrentDirectory(mainGui.getPreferences().getLastImageCreatorSourceFolderPath());
						//new File(mainGui.getPreferences().imageCreatorSourceFolderPath));
			chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			chooser.setMultiSelectionEnabled(false);
			int returnVal = chooser.showDialog(this, "Select directory");
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				sourceFolder = chooser.getSelectedFile();
				sourceFolderLabel.setText(sourceFolder.getAbsolutePath());
				sourceFolderLabel.setVisible(true);
				variableAddButton.requestFocus();
				updateOkButton();
				pack();
			}
		}
		
		else if (source.equals(sourceFileBrowseButton)) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogType(JFileChooser.OPEN_DIALOG);
			if (sourceFile!=null) {
				chooser.setCurrentDirectory(sourceFile);
				chooser.setSelectedFile(sourceFile);
			}
			else {
				//File f = new File(mainGui.getPreferences().imageCreatorSourceFilePath);
				File f = mainGui.getPreferences().getLastImageCreatorSourceFilePath();
				chooser.setCurrentDirectory(f);
				chooser.setSelectedFile(f);
			}
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setMultiSelectionEnabled(false);
			int returnVal = chooser.showDialog(this, "Select index file");
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				sourceFile = chooser.getSelectedFile();
				sourceFileLabel.setText(sourceFile.getAbsolutePath());
				sourceFileLabel.setVisible(true);
				variableAddButton.requestFocus();
				updateOkButton();
				pack();
			}
		}
		
		else if (source.equals(variableAddButton)) {
			
			Vector<String> tmp = new Vector<String>();
			for (int i=0;i<Element.values().length;i++) {
				boolean used=false;
				for (int j=0;j<variableNamesLabel.size();j++) {
					if (variableNamesLabel.elementAt(j).getText().equals(Element.values()[i].name())) used=true;
				}
				if (!used) tmp.add(Element.values()[i].name());
			}
			String[] names = new String[tmp.size()];
			for (int i=0;i<names.length;i++) names[i]=tmp.elementAt(i);
			
			if (names.length>0) {
				String s = (String)JOptionPane.showInputDialog(this,
	                    "Choose PDS variable to add",
	                    "Add variable",
	                    JOptionPane.PLAIN_MESSAGE,
	                    null,
	                    names,
	                    names[0]);
				
				if (s!=null) {
					clearVariablePanel();
					variableNamesLabel.add(new JLabel(s));
					JButton but = new JButton("remove");
					but.addActionListener(this);
					variableRemoveButtons.add(but);
					updateVariablesPanel();
					if (names.length>1)
						variableAddButton.requestFocus();
					else {
						variableAddButton.setEnabled(false);
						filterAddButton.requestFocus();
					}
					updateOkButton();
					pack();
				}
			}	
		}
		
		else if (variablesRemoveButtonPressed!=-1) {
			clearVariablePanel();
			variableNamesLabel.removeElementAt(variablesRemoveButtonPressed);
			variableRemoveButtons.removeElementAt(variablesRemoveButtonPressed);
			updateVariablesPanel();
			variableAddButton.setEnabled(true);
			variableAddButton.requestFocus();
			updateOkButton();
			pack();
		}
		
		else if (source.equals(filterAddButton)) {
			filterSelectDialog.setLocationRelativeTo(this);
            filterSelectDialog.setVisible(true);

            PDSCompare filter = filterSelectDialog.getValidatedFilter();
            if (filter!=null) {
            	filters.add(filter);
            	System.out.println("adding "+filter);
            	clearFilterPanel();
            	filterNameLabels.add(new JLabel(filter.toString()));
            	JButton but = new JButton("remove");
            	but.addActionListener(this);
            	filterRemoveButtons.add(but);
            	updateFilterPanel();
            	updateOkButton();
            	pack();
            }
		}
		
		else if (filterRemoveButtonPressed!=-1) {
			filters.removeElementAt(filterRemoveButtonPressed);
			clearFilterPanel();
			filterNameLabels.removeElementAt(filterRemoveButtonPressed);
			filterRemoveButtons.removeElementAt(filterRemoveButtonPressed);
			updateFilterPanel();
			updateOkButton();
			filterAddButton.requestFocus();
			pack();
		}
		
		else if (source.equals(outputFileBrowseButton)) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogType(JFileChooser.SAVE_DIALOG);
			if (outputFile!=null) {
				chooser.setCurrentDirectory(outputFile);
				chooser.setSelectedFile(outputFile);
			}
			else {
				//File f = new File(mainGui.getPreferences().imageCreatorTargetPath);
				File f = mainGui.getPreferences().getLastImageCreatorTargetPath();
				chooser.setCurrentDirectory(f);
				chooser.setSelectedFile(f);
			}
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setMultiSelectionEnabled(false);
			int returnVal = chooser.showDialog(this, "Select index file to save to");
			if (returnVal == JFileChooser.APPROVE_OPTION) {
				outputFile = chooser.getSelectedFile();
				outputFileLabel.setText(outputFile.getAbsolutePath());
				outputFileLabel.setVisible(true);
				updateOkButton();
				pack();
			}
		}
		
		else if (source.equals(okButton)) {
			makeTheIndexFile();
		}
		
		else if (source.equals(cancelButton)) {
			close();
		}
		
		
		else {
			JOptionPane.showMessageDialog(this, "Unimplemented action", "Unimplemented action", JOptionPane.ERROR_MESSAGE);
			System.out.println(source);
		}
	}
	
	private void close() {
		if (source==FROM_FOLDERS) {
			if (sourceFolder!=null)
				mainGui.getPreferences().setLastImageCreatorSourceFolderPath(sourceFolder);
				//mainGui.getPreferences().imageCreatorSourceFolderPath = sourceFolder.getAbsolutePath();
		} else if (source==FROM_FILE) {
			if (sourceFile!=null)
				mainGui.getPreferences().setLastImageCreatorSourceFilePath(sourceFile);
				//mainGui.getPreferences().imageCreatorSourceFilePath = sourceFile.getAbsolutePath();
		}
		if (outputFile!=null)
			mainGui.getPreferences().setLastImageCreatorTargetPath(outputFile);
			//mainGui.getPreferences().imageCreatorTargetPath = outputFile.getAbsolutePath();
		dispose();
	}
	
	private void makeTheIndexFile() {
		PDSFileConstrain constrain = new PDSFileConstrain();
		for (int i=0;i<filters.size();i++) constrain.addPDSCompareConstrain(filters.elementAt(i));
		
		if (source==FROM_FOLDERS) {
			String rootPath = sourceFolder.getAbsolutePath();
			String extension = "IMG";
			String[] variablesToIndex = new String[variableNamesLabel.size()];
			for (int i=0;i<variablesToIndex.length;i++) variablesToIndex[i] = variableNamesLabel.elementAt(i).getText();
			
			int filesToRead = 200;
			
			boolean goAhead=true;
			if (outputFile.exists()) {
				goAhead=false;
				int n = JOptionPane.showConfirmDialog(
						this, "Overwrite output file?", "Overwrite file?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (n==JOptionPane.YES_OPTION) goAhead=true;
				if (n==JOptionPane.NO_OPTION) goAhead=false;
			}
			
			if (goAhead) {
				RecursiveTask recurseTask = new RecursiveTask(
						new File(rootPath),
						extension,
						filesToRead,
						variablesToIndex,
						constrain,
						outputFile,
						new String[] {"S1-L-X-AMIE-3-RDR-EARTH_ESCAPE_PHASE-V1.0", "low_res_geom"});
				RecursiveTaskDialog dialog = new RecursiveTaskDialog(this,recurseTask);
				recurseTask.execute();
				dialog.setVisible(true);
			}
		}
		
		else if (source==FROM_FILE) {
			boolean goAhead=true;
			if (outputFile.exists()) {
				goAhead=false;
				int n = JOptionPane.showConfirmDialog(
						this, "Overwrite output file?", "Overwrite file?", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
				if (n==JOptionPane.YES_OPTION) goAhead=true;
				if (n==JOptionPane.NO_OPTION) goAhead=false;
			}
			
			if (goAhead) {
				ReduceTask reduceTask = new ReduceTask(sourceFile,outputFile,constrain);
				ReduceTaskDialog dialog = new ReduceTaskDialog(this,reduceTask);
				reduceTask.execute();
				dialog.setVisible(true);
			}
		}
	}
	
	
	
	private void updateOkButton() {
		okButton.setEnabled(
				(source==FROM_FOLDERS
				&& sourceFolder!=null
				&& sourceFolder.exists()
				&& variableNamesLabel.size() > 0
				&& outputFile!=null)
				||
				(source==FROM_FILE
				&& sourceFile!=null
				&& sourceFile.exists()
				&& filterNameLabels.size() > 0
				&& outputFile!=null)
				);
	}

	private void clearFilterPanel() {
		for (int i=0;i<filterNameLabels.size();i++) {
			filterPanel.remove(filterNameLabels.elementAt(i));
			filterPanel.remove(filterRemoveButtons.elementAt(i));
		}
		filterPanel.remove(filterAddButton);
	}
	
	private void updateFilterPanel() {
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth=1;
		for (int i=0;i<filterNameLabels.size();i++) {
			c.gridx=0;
			c.gridy=i;
			filterPanel.add(filterNameLabels.elementAt(i),c);
			c.gridx=1;
			filterPanel.add(filterRemoveButtons.elementAt(i),c);
		}
		c.gridx=0;
		c.gridwidth=2;
		c.gridy++;
		filterPanel.add(filterAddButton,c);
	}
	
	private void clearVariablePanel() {
		for (int i=0;i<variableNamesLabel.size();i++) {
			variablesPanel.remove(variableNamesLabel.elementAt(i));
			variablesPanel.remove(variableRemoveButtons.elementAt(i));
		}
		variablesPanel.remove(variableAddButton);
	}
	
	private void updateVariablesPanel() {
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		c.gridwidth=1;
		for (int i=0;i<variableNamesLabel.size();i++) {
			c.gridx=0;
			c.gridy=i;
			variablesPanel.add(variableNamesLabel.elementAt(i),c);
			c.gridx=1;
			variablesPanel.add(variableRemoveButtons.elementAt(i),c);
		}
		c.gridx=0;
		c.gridwidth=2;
		c.gridy++;
		variablesPanel.add(variableAddButton,c);
	}

	

}
