package esa.moonmap.indexfilecreator;

import esa.pds.PDSCompare;
import esa.pds.PDSCompareNumber;
import esa.pds.PDSCompareStringEqual;
import esa.pds.PDSCompareStringUnequal;
import esa.pds.dictionary.Element;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

class FilterSelectDialog extends JDialog implements ActionListener,
		PropertyChangeListener {

	private static final long serialVersionUID = 100794047288727992L;
	private static final String NUMBER_FILTER = "Number filter";
	private static final String STRING_FILTER_EQUAL = "String filter equal";
	private static final String STRING_FILTER_UNEQUAL = "String filter unequal";
	
	private PDSCompare filter = null;
	//private JTextField textField;
	private JComboBox filterBox = new JComboBox(new String[] {NUMBER_FILTER,STRING_FILTER_EQUAL,STRING_FILTER_UNEQUAL});
	
	private JPanel numberFilterPanel = new JPanel();
	private JPanel stringCompareEqualPanel = new JPanel();
	private JPanel stringCompareUnequalPanel = new JPanel();
	private JPanel filterPanel = new JPanel();
	private JComboBox pdsValueSelectorNumber, pdsValueSelectorStringEqual, pdsValueSelectorStringUnequal;
	private JTextField pdsValueMin = new JTextField(10);
	private JTextField pdsValueMax = new JTextField(10);
	private JTextField pdsValueEqual = new JTextField(50);
	private JTextField pdsValueUnequal = new JTextField(50);
	
	private JOptionPane optionPane;
	private String okString = "Ok";
	private String cancelString = "Cancel";
	
	int pdsValueSelectorIndex = 0;
	
	/**
	 * Returns null if the typed string was invalid; otherwise, returns the
	 * string as the user entered it.
	 */
	public PDSCompare getValidatedFilter() {
		return filter;
	}

	/** Creates the reusable dialog. */
	public FilterSelectDialog(JDialog dialog) {
		super(dialog, true);

		setTitle("Quiz");

		filterBox.addActionListener(this);
		
		pdsValueSelectorNumber = new JComboBox(Element.values());
		pdsValueSelectorStringEqual = new JComboBox(Element.values());
		pdsValueSelectorStringUnequal = new JComboBox(Element.values());
		
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		numberFilterPanel.setLayout(new GridBagLayout());
		numberFilterPanel.setVisible(true);
		c.gridx=0;
		c.gridy=0;
		numberFilterPanel.add(new JLabel("Variable"),c);
		c.gridx=1;
		pdsValueSelectorNumber.addPropertyChangeListener(this);
		numberFilterPanel.add(pdsValueSelectorNumber,c);
		c.gridx=2;
		numberFilterPanel.add(new JLabel("minValue: "),c);
		c.gridx=3;
		numberFilterPanel.add(pdsValueMin,c);
		c.gridy=1;
		c.gridx=2;
		numberFilterPanel.add(new JLabel("maxValue: "),c);
		c.gridx=3;
		numberFilterPanel.add(pdsValueMax,c);
		
		stringCompareEqualPanel.setVisible(false);
		c.gridx=0;
		c.gridy=0;
		stringCompareEqualPanel.add(new JLabel("Variable"),c);
		c.gridx=1;
		pdsValueSelectorStringEqual.addPropertyChangeListener(this);
		stringCompareEqualPanel.add(pdsValueSelectorStringEqual,c);
		c.gridx=2;
		stringCompareEqualPanel.add(new JLabel(" = "),c);
		c.gridx=3;
		stringCompareEqualPanel.add(pdsValueEqual,c);
		
		stringCompareUnequalPanel.setVisible(false);
		c.gridx=0;
		c.gridy=0;
		stringCompareUnequalPanel.add(new JLabel("Variable"),c);
		c.gridx=1;
		pdsValueSelectorStringUnequal.addPropertyChangeListener(this);
		stringCompareUnequalPanel.add(pdsValueSelectorStringUnequal,c);
		c.gridx=2;
		stringCompareUnequalPanel.add(new JLabel(" != "),c);
		c.gridx=3;
		stringCompareUnequalPanel.add(pdsValueUnequal,c);
		
		filterPanel.add(numberFilterPanel);
		filterPanel.add(stringCompareEqualPanel);
		filterPanel.add(stringCompareUnequalPanel);
		

		// Create an array of the text and components to be displayed.
		String msgString1 = "Choose a PDS value filter";
		Object[] array = { msgString1, filterBox, filterPanel };//, textField };

		// Create an array specifying the number of dialog buttons
		// and their text.
		Object[] options = { okString, cancelString };

		// Create the JOptionPane.
		optionPane = new JOptionPane(array, JOptionPane.QUESTION_MESSAGE,
				JOptionPane.YES_NO_OPTION, null, options, options[0]);

		// Make this dialog display it.
		setContentPane(optionPane);

		// Handle window closing correctly.
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				/*
				 * Instead of directly closing the window, we're going to change
				 * the JOptionPane's value property.
				 */
				optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
			}
		});

		// Ensure the text field always gets the first focus.
		addComponentListener(new ComponentAdapter() {
			public void componentShown(ComponentEvent ce) {
				filterBox.requestFocusInWindow();
			}
		});

		// Register an event handler that reacts to option pane state changes.
		optionPane.addPropertyChangeListener(this);
	}

	/** This method handles events for the text field. */
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(filterBox)) {
			Object select = ((JComboBox)e.getSource()).getSelectedItem();
			if (select.equals(STRING_FILTER_EQUAL)) {
				stringCompareEqualPanel.setVisible(true);
				stringCompareUnequalPanel.setVisible(false);
				numberFilterPanel.setVisible(false);
				pdsValueSelectorStringEqual.setSelectedIndex(pdsValueSelectorIndex);
				pack();
			} else if (select.equals(STRING_FILTER_UNEQUAL)) {
				stringCompareEqualPanel.setVisible(false);
				stringCompareUnequalPanel.setVisible(true);
				numberFilterPanel.setVisible(false);
				pdsValueSelectorStringUnequal.setSelectedIndex(pdsValueSelectorIndex);
				pack();
			} else if (select.equals(NUMBER_FILTER)) {
				stringCompareEqualPanel.setVisible(false);
				stringCompareUnequalPanel.setVisible(false);
				numberFilterPanel.setVisible(true);
				pdsValueSelectorNumber.setSelectedIndex(pdsValueSelectorIndex);
				pack();
			}
		}
	}
	
	

	/** This method reacts to state changes in the option pane. */
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible()
				&& (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			Object value = optionPane.getValue();

			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				// ignore reset
				return;
			}

			// Reset the JOptionPane's value.
			// If you don't do this, then if the user
			// presses the same button next time, no
			// property change event will be fired.
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (okString.equals(value)) {
				// OK ACTION
				
				if (numberFilterPanel.isVisible()) {
						double min = 0, max = 0;
					boolean minOk = false, maxOk = false, minMaxOrderOk = false;

					try {
						min = Double.parseDouble(pdsValueMin.getText());
						minOk = true;
					} catch (NumberFormatException nfe) {
					}

					try {
						max = Double.parseDouble(pdsValueMax.getText());
						maxOk = true;
					} catch (NumberFormatException nfe) {
					}

					minMaxOrderOk = min < max;

					if (minOk && maxOk && minMaxOrderOk)
						filter = new PDSCompareNumber(
								(String) pdsValueSelectorNumber
										.getSelectedItem(), min, max);
					else {
						String msg = "";
						if (!minOk) msg = "Illegal minValue! ";
						else if (!maxOk) msg = "Illegal maxValue! ";
						else if (!minMaxOrderOk) msg = "minValue >= maxValue! ";
						JOptionPane.showMessageDialog(this,
								msg+"Try again!",
								"Illegal number(s)", JOptionPane.ERROR_MESSAGE);
						if (!minOk)
							pdsValueMin.requestFocus();
						else if (!maxOk)
							pdsValueMax.requestFocus();
						else
							pdsValueMin.requestFocus();
						filter = null;
					}
					
				} else if (stringCompareEqualPanel.isVisible()) {
					filter = new PDSCompareStringEqual(
							(String)pdsValueSelectorStringEqual.getSelectedItem(),
							pdsValueEqual.getText());
				} else if (stringCompareUnequalPanel.isVisible()) {
					filter = new PDSCompareStringUnequal(
							(String)pdsValueSelectorStringUnequal.getSelectedItem(),
							pdsValueUnequal.getText());
				}
				
				if (filter!=null) clearAndHide();	
			} else { // user closed dialog or clicked cancel
				filter = null;
				clearAndHide();
			}
		}
		
		else if (isVisible()) {
			if (e.getSource().equals(pdsValueSelectorNumber)) 
				pdsValueSelectorIndex = pdsValueSelectorNumber.getSelectedIndex();
			else if (e.getSource().equals(pdsValueSelectorStringEqual)) 
				pdsValueSelectorIndex = pdsValueSelectorStringEqual.getSelectedIndex();
			else if (e.getSource().equals(pdsValueSelectorStringUnequal)) 
				pdsValueSelectorIndex = pdsValueSelectorStringUnequal.getSelectedIndex();
			System.out.println(pdsValueSelectorIndex);
		}
		
	}

	/** This method clears the dialog and hides it. */
	public void clearAndHide() {
		pdsValueMin.setText(null);
		pdsValueMax.setText(null);
		pdsValueEqual.setText(null);
		pdsValueUnequal.setText(null);
		setVisible(false);
	}
}
