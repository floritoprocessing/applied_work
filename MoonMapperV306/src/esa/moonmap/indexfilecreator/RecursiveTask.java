package esa.moonmap.indexfilecreator;

import esa.pds.PDSFileConstrain;
import esa.pds.PDSFileProcessor;
import esa.pds.PDSValueNotFoundException;
import esa.pds.dictionary.Element;

import java.awt.Point;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

public class RecursiveTask extends SwingWorker<Void, Void> {

	public static final String PROPERTY_INDETERMINATE = "indeterminate";
	public static final String PROPERTY_PATH_CHANGE = "pathChange";
	public static final String PROPERTY_FILE_CHANGE = "fileChange";
	public static final String PROPERTY_FILE_COUNT_CHANGE = "fileCountChange";
	public static final String PROPERTY_PARSED_COUNT_CHANGE = "parsedCountChange";
	public static final String PROPERTY_FINISHED = "finished";
	
	private BufferedWriter writer;
	private File root;
	private String extension;
	private int filesToRead;
	private String[] variablesToIndex;
	private String[] origVariablesToIndex;
	//private String[] variablesToCompare;
	private PDSFileConstrain constrain;
	private File outputFile;
	
	private int writeLine = 0;
	private String[] lines;
	
	private String[] directoriesToIgnore;
	
	private Point fileCount = new Point(0,0);
	private File lastPath, lastFile;
	private File currentPath, currentFile;

	private boolean stopRecursing = false;
	private boolean specialCaseUniqueImageNumber;
	private int specialCaseUniqueImageNumberIndex;
	
	public RecursiveTask(
			File root,
			String extension,
			int filesToRead,
			String[] variablesToIndex,
			PDSFileConstrain constrain,
			File outputFile,
			String... directoriesToIgnore
			) {
		this.root = root;
		this.extension = extension;
		this.filesToRead = filesToRead;
		lines = new String[filesToRead];
		this.variablesToIndex = variablesToIndex;
		this.directoriesToIgnore = directoriesToIgnore;
		origVariablesToIndex = new String[variablesToIndex.length];
		if (directoriesToIgnore!=null && directoriesToIgnore.length!=0) {
			for (String s:directoriesToIgnore) System.out.println("IGNORING DIRECTORIES: "+s);
		}
		System.arraycopy(variablesToIndex, 0, origVariablesToIndex, 0, variablesToIndex.length);
		//variablesToCompare = constrain.getPDSCompareVariableNames();
		this.constrain = constrain;
		this.outputFile = outputFile;
		writeLine = 0;
		
		
		//SPECIAL CASE IMAGE_NUMBER
		//Is not in file, but in fileName!
		specialCaseUniqueImageNumber = false;
		specialCaseUniqueImageNumberIndex = -1;
		
		for (int i=0;i<variablesToIndex.length;i++) {
			//System.out.println(variablesToIndex[i]);
			if (variablesToIndex[i].equals(Element.UNIQUE_IMAGE_NUMBER.name())) {
				specialCaseUniqueImageNumber = true;
				specialCaseUniqueImageNumberIndex = i;
				break;
			}
		}
		
		if (specialCaseUniqueImageNumber) {
			String[] tmp = new String[variablesToIndex.length-1];
			int tar=0;
			for (int i=0;i<variablesToIndex.length;i++) {
				if (!variablesToIndex[i].equals(Element.UNIQUE_IMAGE_NUMBER.name())) {
					tmp[tar++]=variablesToIndex[i];
				}
			}
			this.variablesToIndex = tmp;
		}
	}
	
	private void recurse(File path, boolean countFiles, Point count) {
		//System.out.println("directory "+path.getName());
		if (stopRecursing) return;
		lastPath = currentPath;
		currentPath = path;
		firePropertyChange(PROPERTY_PATH_CHANGE, lastPath, currentPath);
		File[] files = path.listFiles();
		int filesInThisDirectory = 0;
		for (int i=0;i<files.length;i++) {
			lastFile = currentFile;
			currentFile = files[i];
			firePropertyChange(PROPERTY_FILE_CHANGE, lastFile, currentFile);
			if (files[i].isDirectory()) {
				String filename = files[i].getName();
				boolean ignore=false;
				for (String dir:directoriesToIgnore) {
					if (filename.equals(dir)) {
						System.out.println("ignoring "+filename);
						ignore = true;
						break;
					}
				}
				
				if (!ignore) recurse(files[i],countFiles,count);
				
			}
			else if (files[i].isFile()) {
				if (stopRecursing) return;
				if (countFiles) {
					firePropertyChange(PROPERTY_FILE_COUNT_CHANGE, count.x, ++count.x);
					filesInThisDirectory++;
				} else {
					Point oldCount = new Point(count);
					count.y++;
					if (files[i].getName().endsWith(extension))
						addContentOfFile(files[i]);
					firePropertyChange(PROPERTY_PARSED_COUNT_CHANGE, oldCount, count);
					setProgress((int)(100*(float)count.y/count.x));
				}
			}
		}
		if (filesInThisDirectory%8!=0) {
			System.err.println("Files in directory "+path+" not dividable by 8!");
			//throw new RuntimeException()
		}
	}
	
	public void stopRecursing() {
		stopRecursing = true;
	}
	
	private void addContentOfFile(File file) {
		boolean addFile = true;
		
		
		try {
			
			String[] values = PDSFileProcessor.getValuesOfFile(file, variablesToIndex);
			
			if (specialCaseUniqueImageNumber) {
				String imageNumber = file.getName().substring(9,20); //AMI_LR1_R00077_00010_00089.IMG
				String[] tmp = new String[values.length+1];
				int src=0, tar=0;
				for (int i=0;i<values.length+1;i++) {
					if (tar!=specialCaseUniqueImageNumberIndex) {
						tmp[tar++]=values[src++];
					} else {
						tmp[tar++]=imageNumber;
					}
				}
				/*values = tmp;
				for (int i=0;i<values.length;i++) {
					System.out.print(values[i]+"\t");
				}
				System.out.println();*/
			}
			
			if (constrain!=null)
				addFile = constrain.withinRange(variablesToIndex, values);
			if (addFile) {
				//String line = file.getName()+", "; 
				String line = "";
				if (values!=null) {
					for (int i=0;i<values.length-1;i++) line+=(values[i]+", ");
					line+=values[values.length-1];
				} else {
					line+=" one or more illegal variables!";
				}
				appendLine(line,false);
			}
		} catch (PDSValueNotFoundException e) {
			e.printStackTrace();
		}
		
	}
	
	private void appendLine(String line, boolean forceWrite) {
		//System.out.println("appending "+line);
		if (writeLine<filesToRead && !forceWrite) {
			lines[writeLine] = line;
			writeLine++;
		} else {
			System.out.println("writing "+writeLine+" lines");
			try {
				for (int i=0;i<(forceWrite?writeLine:lines.length);i++) {
					writer.write(lines[i]);
					writer.newLine();
				}
				writer.flush();
			} catch (IOException e) {
				System.out.println(e.getMessage());
			}
			writeLine=0;
		}
	}
	
	public void finish() {
		appendLine("",true);
		try {
			writer.flush();
			writer.close();
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		System.out.println("finished writing file");
	}
	
	@Override
	protected Void doInBackground() throws Exception {
		try {
			writer = new BufferedWriter(new FileWriter(outputFile));
			String[] commentLines = constrain.toStrings();
			for (int i=0;i<commentLines.length;i++) {
				writer.write(commentLines[i]);
				writer.newLine();
			}
			writer.flush();
			String firstLine="";//"Filename, ";
			
			if (specialCaseUniqueImageNumber) {
				for (int i=0;i<origVariablesToIndex.length-1;i++) firstLine += (origVariablesToIndex[i]+",");
				firstLine += origVariablesToIndex[origVariablesToIndex.length-1];
			} else {
				for (int i=0;i<variablesToIndex.length-1;i++) firstLine += (variablesToIndex[i]+",");
				firstLine += variablesToIndex[variablesToIndex.length-1];
			}
			
			
			writer.write(firstLine);
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			System.out.println(e.getMessage());
		}
		firePropertyChange(PROPERTY_INDETERMINATE, false, true);
		setProgress(0);
		recurse(root,true,fileCount);
		firePropertyChange(PROPERTY_INDETERMINATE, true, false);
		setProgress(0);
		recurse(root,false,fileCount);
		return null;
	}
	
	
	public void done() {
		finish();
		firePropertyChange(PROPERTY_FINISHED, false, true);
		
		boolean done=false;
		for (;!done;) {
			try {
				get();
				done=true;
			} catch (InterruptedException e) {
				done=false;
				continue;
			} catch (CancellationException e) {
				done=false;
				break;
			} catch (ExecutionException e) {
				done=false;
				e.getCause().printStackTrace();
				break;
			}
		}
	}

}
