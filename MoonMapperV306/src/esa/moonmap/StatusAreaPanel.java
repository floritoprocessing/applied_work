package esa.moonmap;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class StatusAreaPanel extends JPanel implements ActionListener {

	private static final long serialVersionUID = -6264964555328017750L;

	final JTextArea statusArea;
	final JButton clearButton;
	
	String clearButtonText = "Clear";
	
	public StatusAreaPanel(int prefWidth) {
		
		super();
		
		setLayout(new BorderLayout());
		setBorder(BorderFactory.createTitledBorder("Status"));
		statusArea = new JTextArea();//15,40);//15,40);
		final JScrollPane statusPane = new JScrollPane(statusArea);
		statusPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		if (prefWidth>0) statusPane.setPreferredSize(new Dimension(prefWidth,100));
		/*GridBagConstraints c = new GridBagConstraints();
		c.gridy=0;
		c.anchor = GridBagConstraints.WEST;
		c.insets = new Insets(10,10,5,10);*/
		add(statusPane,BorderLayout.CENTER);
		
		Panel buttonPanel = new Panel(new FlowLayout());
		clearButton = new JButton(clearButtonText);
		clearButton.addActionListener(this);
		buttonPanel.add(clearButton);
		/*c.gridy=1;
		c.insets = new Insets(0,5,10,10);*/
		add(buttonPanel,BorderLayout.SOUTH);
		
		setVisible(true);
	}
	
	
	public void println(String msg) {
		statusArea.append(msg+"\r\n");
		statusArea.setCaretPosition(statusArea.getText().length());
	}
	
	public void println(String[] msg) {
		if (msg==null) return;
		for (int i=0;i<msg.length;i++)
			statusArea.append(msg+"\r\n");
		statusArea.setCaretPosition(statusArea.getText().length());
	}
	
	public void println(Vector msg) {
		if (msg==null) return;
		for (int i=0;i<msg.size();i++)
			statusArea.append(msg.elementAt(i)+"\r\n");
		statusArea.setCaretPosition(statusArea.getText().length());
	}
	
	
	public void println(double[][] msg) {
		println(msg.getClass().getName());
	}

	public void actionPerformed(ActionEvent e) {
		String action = e.getActionCommand();
		if (action.equals(clearButtonText)) {
			statusArea.setText("");
		}
	}
}
