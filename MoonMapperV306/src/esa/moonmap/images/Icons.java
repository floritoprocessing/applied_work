package esa.moonmap.images;

import javax.swing.ImageIcon;

public class Icons {
	
	public static ImageIcon getIcon(String name) {
		java.net.URL imgURL = Icons.class.getResource(name);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + name);
            return null;
        }
	}

}
