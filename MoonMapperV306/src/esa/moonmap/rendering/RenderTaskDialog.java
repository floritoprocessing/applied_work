package esa.moonmap.rendering;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;

import esa.moonmap.ImageFrame;
import esa.moonmap.MainGui;
import esa.pds.component.PDSObject;

public class RenderTaskDialog extends JDialog implements ActionListener, PropertyChangeListener {
	
	private static final long serialVersionUID = 13919124356419875L;
	private JProgressBar bar = new JProgressBar();
	private JLabel descriptionLabel = new JLabel("Processing file: ");
	private JLabel fileLabel = new JLabel();
	private JLabel timeRemainingLabel = new JLabel();
	private JLabel timePassedLabel = new JLabel();
	private JButton cancelDoneButton = new JButton("Cancel");
	private JButton updateImageButton = new JButton("Update image");
	private JCheckBox updateAlways = new JCheckBox("Update always",true);
	private RenderTask task;
	private ImageFrame imageFrame;
	
	private int maxIndex=0, currentIndex=0, skipped=0;
	DecimalFormat df2 = new DecimalFormat("00");
	double averageETA = 0;
	
	public RenderTaskDialog(MainGui parent, Vector<PDSObject> pdsObject) { //, Project project
		super(parent,"Rendering map",true);
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		setLocationRelativeTo(parent);
		
		imageFrame = parent.getImageFrame();
		
		task = new RenderTask(pdsObject,parent);
		task.addPropertyChangeListener(this);
		//task.init();
		getContentPane().setLayout(new GridBagLayout());
		
		GridBagConstraints c = new GridBagConstraints();
		c.gridx=0;
		c.gridy=0;
		c.insets = new Insets(2,2,2,2);
		c.gridwidth=2;
		c.anchor = GridBagConstraints.CENTER;
		bar.setStringPainted(true);
		getContentPane().add(bar,c);
		
		c.gridy++;
		c.gridwidth=1;
		c.anchor = GridBagConstraints.WEST;
		getContentPane().add(descriptionLabel,c);
		
		c.gridx++;
		getContentPane().add(fileLabel,c);
		
		c.gridx=0;
		c.gridy++;
		getContentPane().add(new JLabel("Estimated time remaining:"),c);
		c.gridx++;
		getContentPane().add(timeRemainingLabel,c);
		
		c.gridx=0;
		c.gridy++;
		getContentPane().add(new JLabel("Time passed:"),c);
		c.gridx++;
		getContentPane().add(timePassedLabel,c);
		
		c.gridx=0;
		c.gridy++;
		c.gridwidth=2;
		c.anchor = GridBagConstraints.CENTER;
		updateImageButton.addActionListener(this);
		updateImageButton.setEnabled(false);
		getContentPane().add(updateImageButton,c);
		
		c.gridy++;
		updateAlways.addActionListener(this);
		getContentPane().add(updateAlways,c);
		
		
		c.gridy++;
		cancelDoneButton.addActionListener(this);
		getContentPane().add(cancelDoneButton,c);
		setPreferredSize(new Dimension(480,200));
		setResizable(false);
		pack();
		
		
		
		addWindowListener(new WindowAdapter() {
		    public void windowClosing(WindowEvent we) {
		        cancelOrDone();
		    }
		});
		
		task.execute();
		setVisible(true);
	}
	
	private void cancelOrDone() {
		if (task==null) {
			imageFrame.repaint();
			dispose();
		} else {
			int n = JOptionPane.showConfirmDialog(this, "Are you sure you want to cancel?","Cancel render",JOptionPane.YES_NO_OPTION);
			if (n==JOptionPane.YES_OPTION) {
				task.cancel(true);
				task=null;
				imageFrame.repaint();
				dispose();
			}
		}
	}

	public void propertyChange(PropertyChangeEvent evt) {
		String propName = evt.getPropertyName();
		if (propName.equals(RenderTask.PROPERTY_MESSAGE_FILE)) {
			String msg = (String)evt.getNewValue();
			msg += " (" + (currentIndex + 1) + " of " + maxIndex + ")";
			fileLabel.setText(msg);
		}
		else if (propName.equals(RenderTask.PROPERTY_MAX_INDEX)) {
			maxIndex = (Integer)evt.getNewValue();
		}
		else if (propName.equals(RenderTask.PROPERTY_SKIPPED_FILES)) {
			skipped = (Integer)evt.getNewValue();
			//System.out.println(skipped);
		}
		else if (propName.equals(RenderTask.PROPERTY_CURRENT_INDEX)) {
			currentIndex = (Integer)evt.getNewValue();
		}
		else if (propName.equals(RenderTask.PROPERTY_TIME_PASSED)) {
			long passed = (Long)evt.getNewValue();
			
			int sec = (int)(passed/1000);
			int min = sec/60;
			sec = sec%60;
			String msg = df2.format(min)+"m "+df2.format(sec)+"s";
			timePassedLabel.setText(msg);
			
			//System.out.println(currentIndex+"/"+maxIndex+" s:"+skipped);
			double eta = passed * ((maxIndex+1.0)/(currentIndex+1)-1);
			//System.out.println(eta);
			averageETA = averageETA*0.8 + eta*0.2;
			int etaSec = (int)(averageETA/1000);
			int etaMin = etaSec/60;
			etaSec = etaSec%60;
			msg = df2.format(etaMin)+"m "+df2.format(etaSec)+"s";
			timeRemainingLabel.setText(msg);
		}
		else if (propName.equals("progress")) {
			bar.setValue((Integer)evt.getNewValue());
		}
		else if (propName.equals(RenderTask.PROPERTY_FINISHED)) {
			if ((Boolean)evt.getNewValue()==true) {
				bar.setValue(100);
				task=null;
				cancelDoneButton.setText("Done");
				updateImageButton.setEnabled(false);
				imageFrame.repaint();
			}
		}
		else if (propName.equals(RenderTask.PROPERTY_FILE_VISIBLE)) {
			if ((Boolean)evt.getNewValue()==true) {
				descriptionLabel.setText("Processing file: ");
			} else {
				descriptionLabel.setText("Skipping file (invisible): ");
			}
		}
		else if (propName.equals(RenderTask.PROPERTY_SUMMARY)) {
			descriptionLabel.setText((String)evt.getNewValue());
			remove(fileLabel);
			pack();
		}
		else if (propName.equals(RenderTask.PROPERTY_IMAGE_DRAWN)) {
			if (updateAlways.isSelected())
				imageFrame.repaint();
		}
	}

	public void actionPerformed(ActionEvent evt) {
		if (evt.getSource().equals(cancelDoneButton)) {
			cancelOrDone();
		}
		else if (evt.getSource().equals(updateImageButton)) {
			imageFrame.repaint();
		}
		else if (evt.getSource().equals(updateAlways)) {
			if (updateAlways.isSelected()) {
				updateImageButton.setEnabled(false);
			} else {
				updateImageButton.setEnabled(true);
			}
		}
	}


}
