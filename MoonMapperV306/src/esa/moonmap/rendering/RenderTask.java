	package esa.moonmap.rendering;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.text.DecimalFormat;
import java.util.Vector;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;

import processing.core.PGraphics;
import esa.moonmap.MainGui;
import esa.moonmap.project.DefaultModificationFile;
import esa.moonmap.project.Outline;
import esa.moonmap.project.OutlineCollection;
import esa.pds.PDSValueNotFoundException;
import esa.pds.amie.AMIEFilter;
import esa.pds.component.PDSObject;
import esa.pds.dictionary.Element;
import esa.pds.image.PDSImage;
import esa.pds.image.PDSImageLoader;
import esa.pds.image.PDSProcessedImage;
import esa.projection.Mercator;
import esa.projection.Projection;
import esa.projection.ProjectionInputException;
import esa.projection.Rectangular;
import esa.tools.Timer;

public class RenderTask extends SwingWorker<Void, Void> {

	public static final String PROPERTY_MESSAGE_FILE = "file";
	public static final String PROPERTY_TIME_PASSED = "passed";
	public static final String PROPERTY_FINISHED = "finished";
	public static final String PROPERTY_FILE_VISIBLE = "fileVisible";
	public static final String PROPERTY_SUMMARY = "summary";
	public static final String PROPERTY_IMAGE_DRAWN = "imageDrawn";
	
	public static final String PROPERTY_CURRENT_INDEX = "c";
	public static final String PROPERTY_SKIPPED_FILES = "s";
	public static final String PROPERTY_MAX_INDEX = "m";
	
	private Vector<PDSObject> pdsObject;
	private MainGui main;
	private Projection projection;
	private BufferedImage mapImage;
	private OutlineCollection outlineCollection;
	
	private int skipped = 0;
	private int total = 0;
	
	DecimalFormat df = new DecimalFormat("00.00");
	
	public RenderTask(Vector<PDSObject> pdsObject, MainGui main) {
		this.pdsObject = pdsObject;
		//System.out.println("new render task with "+pdsObject.size()+" pdsObjects");
		this.main = main;
		projection = main.getProject().getProjection(); // 0..360
		mapImage = main.getProject().getMapImage();
		outlineCollection = main.getProject().getOutlineCollection();
	}
	
	//TODO meridian error check
	
	//TODO
	/**
	 * Work around method for the processing texture() method,
	 * that does not seem to take alpha channels very well.
	 * 
	 * the alpha channel will be stored in the red channel
	 * the gray values will be stored in the green channel
	 */
	public PGraphics convertToFakeARGBPGraphics(BufferedImage tmp) {
		int width = tmp.getWidth();
		int height = tmp.getHeight();
		PGraphics image = main.getMoonMapperPApplet().createGraphics(width,height);
		int red, green;
		int y=0,x=0;
		for (int p=0;p<image.pixels.length;p++) {
			red = tmp.getRGB(x,y)>>24&0xff;
			green = tmp.getRGB(x,y)>>16&0xff;
			image.pixels[p] = 0xff<<24 | red<<16 | green<<8 | 0;
			x++;
			if (x==width) {
				x=0;
				y++;
			}
		}
		image.updatePixels();
		return image;
	}
	
	public PGraphics convertToFakeARGBPGraphics(int[] pixels, int width, int height) {
		PGraphics image = main.getMoonMapperPApplet().createGraphics(width,height);
		int red, green;
		for (int p=0;p<image.pixels.length;p++) {
			red = pixels[p]>>8 & 0x00ff0000;
			green = pixels[p]>>8 & 0x0000ff00;
			image.pixels[p] = 0xff000000 | red | green;// | 0; //0xff<<24 |
			
		}
		//image.updatePixels();
		return image;
	}
	
	/**
	 * THe texture() method is not working as expected, it does not deal with alpha channels very well
	 * The alpha channel is actuall stored in the red channel
	 * The gray values are actually stored in the green channel
	 */
	//public void convertFromFakeARGBPGraphics(PGraphics pgraphics) {
	public BufferedImage convertFromFakeARGBPGraphics(PGraphics pgraphics) {
		BufferedImage out = new BufferedImage(pgraphics.width,pgraphics.height,BufferedImage.TYPE_INT_ARGB);
		int val, alph;
		//pgraphics.format = PGraphics.ARGB;
		for (int p=0;p<pgraphics.pixels.length;p++) {
			/*alph = pgraphics.pixels[p]>>16&0xff;
			val = pgraphics.pixels[p]>>8&0xff;
			pgraphics.pixels[p] = alph<<24|val<<16|val<<8|val;*/
			alph = pgraphics.pixels[p]<<8 & 0xff000000;
			val = pgraphics.pixels[p] & 0x0000ff00;
			pgraphics.pixels[p] = alph|val<<8|val|val>>8;
		}
		//pgraphics.updatePixels();
		out.setRGB(0, 0, pgraphics.width, pgraphics.height, pgraphics.pixels, 0, pgraphics.width);
		return out;
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.SwingWorker#doInBackground()
	 */
	@Override
	protected Void doInBackground() {

		String currentMessage = "";
		int max = pdsObject.size();
		boolean visible = true;//, lastVisible = true;
		
		long startTime = System.currentTimeMillis();
		Timer timerProjection = new Timer(false,0);
		Timer timerLoadImage = new Timer(false);
		Timer timerCreateImage = new Timer(false);
		Timer timerConvertImage = new Timer(false);
		Timer timerSubset = new Timer(false);
		Timer timerExtend = new Timer(false);
		Timer timerDrawQuads = new Timer(false);
		Timer timerDrawIntoMap = new Timer(false);
		Timer timerTotal = new Timer(false);
		
		firePropertyChange(PROPERTY_MAX_INDEX,-1,max);

		for (int i = 0; i < max; i++) {

			timerTotal.start();
			
			String imageFilename = "";
			String imageFullFrameName = "";

			if (isCancelled())
				return null;

			try {

				/*
				 * Check visibility
				 */
				visible = new Boolean(pdsObject.elementAt(i)
						.getValueOfPDSValue(
								DefaultModificationFile.VISIBLE_KEYWORD))
						.booleanValue();

				if (!visible) {
					//skipped++;
					firePropertyChange(PROPERTY_SKIPPED_FILES, skipped, ++skipped);
				}
				total = i;

				/*
				 * Update progress 
				 */
				setProgress((i + 1) * 100 / (max));
				firePropertyChange(PROPERTY_FILE_VISIBLE, !visible, visible);

				
				
				/*
				 * Get Object
				 * Get filename 
				 * Get fullframe name
				 */
				PDSObject imageObject = pdsObject.elementAt(i);
				imageFilename = imageObject.getValueOfPDSValue(Element.FILE_NAME.name());
				imageFullFrameName = imageObject.getValueOfPDSValue(Element.UNIQUE_IMAGE_NUMBER.name());
				
				long passed = System.currentTimeMillis()-startTime;
				//currentMessage = imageFilename + " (" + (i + 1) + " of " + max + ")";
				firePropertyChange(PROPERTY_CURRENT_INDEX, -1, i);
				firePropertyChange(PROPERTY_MESSAGE_FILE, null, imageFilename);
				firePropertyChange(PROPERTY_TIME_PASSED, null, passed);//
				//"eta: "+df2.format(etaMin)+"m"+df2.format(etaSec)+"s");

				if (visible) {

					/*
					 * Reference to map:
					 */

					Graphics2D mapImageGraphics = mapImage.createGraphics();// getGraphics();

					/*
					 * Load / get cached lo res projected geometry
					 */
					
					timerProjection.start();
					float[] lowResProjected = getOrCreateProjected(imageFilename, imageFullFrameName);
					timerProjection.stop();
					
					/*
					 * Load image from archive
					 */

					int width, height;
					PGraphics texture;
					timerLoadImage.start();
					String filename = pdsObject.elementAt(i).getPDSValue(
							Element.FILE_NAME.name()).getValue();
					
					//PDSImage pdsImage = getOrCreatePDSImage(filename);
					File file = main.getPreferences().getFile_ImageArchive(filename);
					PDSObject header = main.loadCachedHeader(file);
					PDSImage pdsImage = main.loadPDSImage(file,header); // filename
					String filterName = 
						pdsObject.elementAt(i)
						.getPDSValue(
								Element.FILTER_NAME.name()).getValue();
					PDSProcessedImage pdsImageProcessed = 
						main.createPDSProcessedImage(pdsImage, filterName, pdsObject.elementAt(i));
					timerLoadImage.stop();
					
					
					
					width = pdsImage.getWidth();
					height = pdsImage.getHeight();
					timerCreateImage.start();
					//TODO the createPixelArrayWithProcessorStack method is the slow one here
					int[] out = pdsImageProcessed.createPixelArrayWithProcessorStack();
					timerCreateImage.stop();
					timerConvertImage.start();
					texture = convertToFakeARGBPGraphics(out, width, height);
					timerConvertImage.stop();
					

					
					timerSubset.start();
					
					/*
					 * Get normalized texture points
					 */

					/*String filterName = pdsObject
							.elementAt(i)
							.getValueOfPDSValue(
									PDSDictionary.names[PDSDictionary.FILTER_NAME]);*/
					float[] normalizedTexturePointsOfQuads1 = AMIEFilter
							.getNormalizedTexturePointsOfFilter(filterName);
					int coGridWidth = width / AMIEFilter.LO_RES_SIZE;
					int coGridHeight = height / AMIEFilter.LO_RES_SIZE;

					/*
					 * Create subset
					 */

					Rectangle r = AMIEFilter.getFilterRectangle(filterName);
					int lonCornerIndexOffset = r.x
							* AMIEFilter.CORNER_INDEX_TO_LO_RES_CORNER_INDEX;
					int latCornerIndexOffset = r.y
							* AMIEFilter.CORNER_INDEX_TO_LO_RES_CORNER_INDEX;

					
					float[] projectedSubset = new float[coGridWidth * coGridHeight
							* 8];
					int index1 = 0;
					int x0, x1, y0, y1, i00, i01, i11, i10;
					int cornerIndex[] = new int[4];
					for (int yq = 0; yq < coGridHeight; yq++) {
						for (int xq = 0; xq < coGridWidth; xq++) {
							x0 = xq + lonCornerIndexOffset;
							x1 = x0 + 1;
							y0 = (yq + latCornerIndexOffset) * AMIEFilter.LO_RES_WIDTH;
							y1 = (yq + latCornerIndexOffset + 1) * AMIEFilter.LO_RES_WIDTH;
							i00 = (y0 + x0) * 2;
							i01 = (y0 + x1) * 2;
							i10 = (y1 + x0) * 2;
							i11 = (y1 + x1) * 2;
							if (yq==0&&xq==0) {
								cornerIndex[0] = index1;
							}
							projectedSubset[index1++] = lowResProjected[i00];
							projectedSubset[index1++] = lowResProjected[i00 + 1];
							if (yq==0&&xq==coGridWidth-1) {
								cornerIndex[1] = index1;
							}
							projectedSubset[index1++] = lowResProjected[i01];
							projectedSubset[index1++] = lowResProjected[i01 + 1];
							if (yq==coGridHeight-1&&xq==coGridWidth-1) {
								cornerIndex[2]= index1;
							}
							projectedSubset[index1++] = lowResProjected[i11];
							projectedSubset[index1++] = lowResProjected[i11 + 1];
							if (yq==coGridHeight-1&&xq==0) {
								cornerIndex[3]= index1;
							}
							projectedSubset[index1++] = lowResProjected[i10];
							projectedSubset[index1++] = lowResProjected[i10 + 1];
						}
					}
					timerSubset.stop();
					
					
					/*
					 * find min and max 
					 * time: about 0.4 ms
					 */
					
					float minX = Float.MAX_VALUE;
					float maxX = Float.MIN_VALUE;
					float minY = Float.MAX_VALUE;
					float maxY = Float.MIN_VALUE;
					for (int c=0;c<projectedSubset.length;c+=2) {
						minX = Math.min(projectedSubset[c], minX);
						maxX = Math.max(projectedSubset[c], maxX);
						minY = Math.min(projectedSubset[c + 1], minY);
						maxY = Math.max(projectedSubset[c + 1], maxY);
					}
					// (int) to avoid rounding error
					minX=(int)minX;
					maxX=(int)(maxX+1); // to be safe
					minY=(int)minY;
					maxY=(int)(maxY+1); // to be safe
					int drawWidth = (int) (maxX - minX);
					int drawHeight = (int) (maxY - minY);
					
					
					/*
					 * alpha info is in red channel gray info is in green
					 * channel
					 * time: average about 4 ms or so
					 */
					
					PGraphics pg = main.getMoonMapperPApplet().createGraphics(drawWidth, drawHeight);
					pg.beginDraw();
					pg.background(0, 255, 0);
					pg.noStroke();

					
					/*
					 * Extend image to increase texture interpolation towards
					 * the edge of image
					 * time: about 2-8 ms
					 */
					
					timerExtend.start();
					PGraphics extendedTexture = extendImageBy1(main, texture);
					float extendedFacU = (float) texture.width / extendedTexture.width;
					float extendedFacV = (float) texture.height / extendedTexture.height;
					float extendedOffsetU = (1 - extendedFacU) / 2;
					float extendedOffsetV = (1 - extendedFacV) / 2;
					timerExtend.stop();

					
					/*
					 * Draw all quads
					 */
					
					timerDrawQuads.start();
					pg.beginShape(PGraphics.QUADS);
					pg.texture(extendedTexture);
					pg.textureMode(PGraphics.NORMAL);// NORMALIZED);
					for (int c = 0; c < projectedSubset.length; c += 2) {
						pg.vertex(
							projectedSubset[c] - minX,
							projectedSubset[c + 1] - minY,
							extendedOffsetU
									+ normalizedTexturePointsOfQuads1[c]
									* extendedFacU,
							extendedOffsetV
									+ normalizedTexturePointsOfQuads1[c + 1]
									* extendedFacV);
					}
					pg.endShape();
					pg.endDraw(); 
					timerDrawQuads.stop();
					
					/*
					 * Polygoon to ImageFrame
					 */
					
					int[] cornerX = new int[4];
					int[] cornerY = new int[4];
					for (int c=0;c<4;c++) {
						cornerX[c] = (int)projectedSubset[cornerIndex[c]];
						cornerY[c] = (int)projectedSubset[cornerIndex[c]+1];
					}
					outlineCollection.addOutline(new Outline(imageFilename,cornerX,cornerY,4));
					
					
					/*
					 * Convert fake argb into a real argb image
					 * time: 5-15 ms
					 */
					 
					BufferedImage end = convertFromFakeARGBPGraphics(pg);
					
					
					/*
					 * Draw into map
					 * 5-50 ms..
					 * maybe not use BufferedImage?
					 */
					timerDrawIntoMap.start();
					mapImageGraphics.drawImage(end, (int) minX, (int) minY,null);
					mapImageGraphics.dispose();
					timerDrawIntoMap.stop();

					
					firePropertyChange(PROPERTY_IMAGE_DRAWN, false, true);

					timerTotal.stop();
					
					String timeMsg = "";
					timeMsg += "project: "+df.format(timerProjection.getAverageTime());
					timeMsg += "  load: "+df.format(timerLoadImage.getAverageTime());
					timeMsg += "  create: "+df.format(timerCreateImage.getAverageTime());
					timeMsg += "  conv: "+df.format(timerConvertImage.getAverageTime());
					timeMsg += "  sub: "+df.format(timerSubset.getAverageTime());
					timeMsg += "  extnd: "+df.format(timerExtend.getAverageTime());
					timeMsg += "  quads: "+df.format(timerDrawQuads.getAverageTime());
					timeMsg += "  drawToMap: "+df.format(timerDrawIntoMap.getAverageTime());
					timeMsg += "  total: "+df.format(timerTotal.getAverageTime());
					main.statusArea.println(timeMsg);
					//System.out.println(timeMsg);
					
				}
			} catch (PDSValueNotFoundException e) {
				e.printStackTrace();
			} catch (ProjectionInputException e) {
				main.statusArea.println("Image: " + imageFilename + " caused "
						+ e.toString());
			}

		}

		//System.out.println("Done with run()");
		return null;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	/*private float[] getOrCreateGeometry(String filename, String fullFrameName) {
		float[] lowResGeometry;
		Object cachedGeometry = main.getProject().getCachedLowResGeometry().get(fullFrameName);
		if (cachedGeometry != null) {
			//
			// Geometry from hashTable
			//
			lowResGeometry = (float[]) ((HashableObjectWrapper) cachedGeometry).getObject();
		} else {
			//
			// Create geometry
			//
			File[] lowResGeomFiles = main.getPreferences().getFiles_BinaryArchive(filename);
			PDSImage lowResImage = PDSImage.createAndLoadBinary(lowResGeomFiles, PDSImage.BINARY_CORNERS);
			lowResGeometry = lowResImage.getCornerData();
			//
			// adjust corner data to projection (null meridian)
			//
			if (projection instanceof Mercator|| projection instanceof Rectangular) {
				for (int c = 0; c < lowResGeometry.length; c += 2) {
					if (lowResGeometry[c] - projection.getCenterLongitude() > 180)
						lowResGeometry[c] -= 360;
					else if (lowResGeometry[c] - projection.getCenterLongitude() < -180)
						lowResGeometry[c] += 360;
				}

			}
			//
			// Store into hashtable
			//
			main.getProject().getCachedLowResGeometry().put(
					fullFrameName,
					new HashableObjectWrapper(fullFrameName,
							lowResGeometry));
		}
		return lowResGeometry;
	}*/
	
	
	
	
	
	
	
	
	/*private PDSImage getOrCreatePDSImage(String filename) {
		
		Object cachedImage = main.getProject().getImageCache().get(filename);
		if (cachedImage!=null) {
			return (PDSImage)cachedImage;
		} else {
			PDSImage pdsImage = main.loadPDSImage(filename);
			main.getProject().getImageCache().put(
					filename,
					new HashableObjectWrapper(filename,
							pdsImage));
			return pdsImage;
		}
		
		
		//return pdsImage;
	}*/

	
	private float[] getOrCreateProjected(String filename, String fullFrameName) {
		float[] lowResProjected;
		Object cachedProjected = main.getProject().getCachedLowResProjected().get(fullFrameName);
		if (cachedProjected != null) {
			lowResProjected = (float[])cachedProjected;
		} else {
			File[] lowResGeomFiles = main.getPreferences().getFiles_BinaryArchive(filename);
			
			
			float[] lowResGeometry = 
				PDSImageLoader
				.createAndLoadBinary(lowResGeomFiles, PDSImageLoader.BINARY_CORNERS);			
			
			/*
			 * adjust corner data to projection (null meridian)
			 */
			if (projection instanceof Mercator|| projection instanceof Rectangular) {
				for (int c = 0; c < lowResGeometry.length; c += 2) {
					if (lowResGeometry[c] - projection.getCenterLongitude() > 180)
						lowResGeometry[c] -= 360;
					else if (lowResGeometry[c] - projection.getCenterLongitude() < -180)
						lowResGeometry[c] += 360;
				}
			}
			/*
			 * Project
			 */
			lowResProjected = new float[lowResGeometry.length];
			float longitude, latitude;
			float centerLongitudeInPixels = mapImage.getWidth() / 2.0f;
			float northInPixels = projection.lonLatToXY(projection
					.getCenterLongitude(), Float.parseFloat(main
					.getProject().getMapNorth()))[1];
			float centerLatitudeInPixels = 0 - northInPixels;

			float[] xy;
			for (int index = 0; index < lowResGeometry.length; index += 2) {
				longitude = lowResGeometry[index];
				latitude = lowResGeometry[index + 1];
				xy = projection.lonLatToXY(longitude, latitude);
				lowResProjected[index] = xy[0] + centerLongitudeInPixels;
				lowResProjected[index + 1] = xy[1] + centerLatitudeInPixels;
			}
			/*
			 * Store into hashtable
			 */
			/*main.getProject().getCachedLowResProjected().put(
					fullFrameName,
					new HashableObjectWrapper(fullFrameName,
							lowResProjected));*/
			
			main.getProject().getCachedLowResProjected().put(
					fullFrameName,lowResProjected);
		}
		return lowResProjected;
	}
	
	
	
	
	private static PGraphics extendImageBy1(MainGui main, PGraphics image) {
		PGraphics imageExtended = main.getMoonMapperPApplet().createGraphics(image.width+2, image.height+2);
		int ti=imageExtended.width + 1;
		for (int sy=0;sy<image.height;sy++) {
			System.arraycopy(
					image.pixels, sy*image.width, 
					imageExtended.pixels, ti, image.width);
			ti += imageExtended.width;
		}
		//top:
		System.arraycopy(image.pixels,0,imageExtended.pixels,1,image.width);
		//bottom:
		System.arraycopy(image.pixels,(image.height-1)*image.width,
				imageExtended.pixels,(imageExtended.height-1)*imageExtended.width+1, image.width);
		//left & right:
		int si=0; // start of 1st row
		ti=imageExtended.width; // start of 2nd row
		for (int sy=0;sy<image.height;sy++) {
			imageExtended.pixels[ti] = image.pixels[si];
			si += image.width - 1; // end of row
			ti += imageExtended.width - 1; // end of row
			imageExtended.pixels[ti] = image.pixels[si];
			si += 1;
			ti += 1;
		}
		// top left:
		int c = 0;
		imageExtended.pixels[c] = interpolateRGB(
				imageExtended.pixels[c+1], 
				imageExtended.pixels[c+imageExtended.width]);
		// top right:
		c = imageExtended.width-1;
		imageExtended.pixels[c] = interpolateRGB(
				imageExtended.pixels[c-1],
				imageExtended.pixels[c+imageExtended.width]);
		// bottom left:
		c = imageExtended.pixels.length-imageExtended.width;
		imageExtended.pixels[c] = interpolateRGB(
				imageExtended.pixels[c+1],
				imageExtended.pixels[c-imageExtended.width]);
		// bottom right:
		c = imageExtended.pixels.length-1;
		imageExtended.pixels[c] = interpolateRGB(
				imageExtended.pixels[c-1],
				imageExtended.pixels[c-imageExtended.width]);
		return imageExtended;
	}
	
	
	/*private static PGraphics extendImage(MainGui main, PGraphics image, int offset) {
		
		if (offset==1) return extendImage1(main, image);
		
		//int offset=2; 
		PGraphics imageExtended = main.getMoonMapperPApplet().createGraphics(image.width+offset*2, image.height+offset*2);
		
		int off2 = offset*2;
		imageExtended.loadPixels();
		int ti=imageExtended.width*offset + offset;
		int si=0;
		for (int sy=0;sy<image.height;sy++) {
			for (int sx=0;sx<image.width;sx++) {
				imageExtended.pixels[ti]=image.pixels[si];
				if (sx==0) for (int xo=1;xo<=offset;xo++) 
					imageExtended.pixels[ti-xo]=image.pixels[si];
				else if (sx==image.width-1) for (int xo=1;xo<=offset;xo++) 
					imageExtended.pixels[ti+xo]=image.pixels[si];
				if (sy==0) for (int yo=1;yo<=offset;yo++) 
					imageExtended.pixels[ti-yo*imageExtended.width]=image.pixels[si];
				else if (sy==image.height-1) for (int yo=1;yo<=offset;yo++) 
					imageExtended.pixels[ti+yo*imageExtended.width]=image.pixels[si];
				si++;
				ti++;
			}
			ti += off2;
		}
		
		
		//imageExtended.updatePixels();
		
		int[][] rootTopLeft = {
				{0,0},
				{imageExtended.width-offset,0},
				{imageExtended.width-offset,imageExtended.height-offset},
				{0,imageExtended.height-offset}
		};
		
		int[] val = {
				interpolateRGBA(imageExtended.get(offset,0),imageExtended.get(0,offset)),
				interpolateRGBA(imageExtended.get(imageExtended.width-1-offset,0),imageExtended.get(imageExtended.width-1,offset)),
				interpolateRGBA(imageExtended.get(imageExtended.width-1,imageExtended.height-1-offset),imageExtended.get(imageExtended.width-1-offset,imageExtended.height-1)),
				interpolateRGBA(imageExtended.get(offset,imageExtended.height-1),imageExtended.get(0,imageExtended.height-1-offset))
		};
		
		for (int c=0;c<4;c++) {
			for (int xo=0;xo<offset;xo++) {
				for (int yo=0;yo<offset;yo++) {
					imageExtended.set(rootTopLeft[c][0]+xo,rootTopLeft[c][1]+yo,val[c]);
				}
			}
		}
		
		return imageExtended;
	}*/
	
	/*private static int interpolateRGB(int color1, int color2) {
		int r = (color1>>1&0x7f0000) + (color2>>1&0x7f0000);
		int g = (color1>>1&0x007f00) + (color2>>1&0x007f00);
		int b = (color1>>1&0x00007f) + (color2>>1&0x00007f);
		return r|g|b;
	}*/
	
	private static int interpolateRGB(int... colors) {
		int r=0, g=0, b=0;
		for (int color:colors) {
			r += (color>>16&0xff);
			g += (color>>8&0xff);
			b += (color&0xff);
		}
		r /= colors.length;
		g /= colors.length;
		b /= colors.length;
		return r<<16|g<<8|b;
	}
	
	/*private static int interpolateRGBA(int... colors) {
		int r=0, g=0, b=0, a=0;
		for (int color:colors) {
			a += (color>>24&0xff);
			r += (color>>16&0xff);
			g += (color>>8&0xff);
			b += (color&0xff);
		}
		a /= colors.length;
		r /= colors.length;
		g /= colors.length;
		b /= colors.length;
		return a<<24|r<<16|g<<8|b;
	}*/
	
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.SwingWorker#done()
	 */
	public void done() {
		firePropertyChange(PROPERTY_SUMMARY, "", "Processed "+(total+1)+" files ( skipping "+skipped+" invisibles)");
		firePropertyChange(PROPERTY_FINISHED, false, true);
		setProgress(100);
	
		boolean done=false;
		for (;!done;) {
			try {
				get();
				done=true;
			} catch (InterruptedException e) {
				done=false;
				continue;
			} catch (CancellationException e) {
				done=false;
				break;
			} catch (ExecutionException e) {
				done=false;
				e.getCause().printStackTrace();
				break;
			}
		}
		
	}

}
