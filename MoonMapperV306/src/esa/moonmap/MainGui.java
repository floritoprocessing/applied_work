package esa.moonmap;

import esa.moonmap.dialogs.Dialogs;
import esa.moonmap.dialogs.PreferencesDialog;
import esa.moonmap.dialogs.ProjectDialogV3;
import esa.moonmap.imagepreview.ImagePreviewPanel;
import esa.moonmap.indexfilecreator.IndexFileCreator;
import esa.moonmap.project.DefaultModificationFile;
import esa.moonmap.project.Project;
import esa.moonmap.project.ProjectFileFilter;
import esa.moonmap.rendering.RenderTaskDialog;
import esa.moonmap.tablemodels.ImageTableModel;
import esa.moonmap.tablemodels.ModifyTableModel;
import esa.moonmap.tablemodels.PresetTableModel;
import esa.pds.PDSFileLoader;
import esa.pds.PDSObjectNotFoundException;
import esa.pds.PDSValueNotFoundException;
import esa.pds.amie.AMIEFilter;
import esa.pds.component.PDSObject;
import esa.pds.dictionary.Element;
import esa.pds.image.PDSImage;
import esa.pds.image.PDSImageLoader;
import esa.pds.image.PDSProcessedImage;
import esa.pds.image.processing.PDSImageProcessorEdgeCutAndSmooth;
import esa.pds.image.processing.PDSImageProcessorEdgeCutAndSmooth1;
import esa.pds.image.processing.PDSImageProcessorGain;
import esa.pds.image.processing.PDSImageProcessorTrim;
import esa.projection.Projection;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.image.BufferedImage;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SortOrder;
import javax.swing.RowSorter.SortKey;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class MainGui extends JFrame implements ActionListener, ListSelectionListener, TableModelListener, PropertyChangeListener, WindowListener {

	public static final int SMOOTH_PIXELS = 20;
	
	private static final long serialVersionUID = -4759086752626600614L;
	//private static final String AUTO_GAIN_TEXT = "auto gain";
	private MoonMapper moonMapperPApplet;
	private Prefs preferences;
	
	private Project project = null;
	/*
	 * Menu Bar
	 */
	private JMenuItem newProject = new JMenuItem("New");
	private JMenuItem openProject = new JMenuItem("Open");
	private JMenuItem closeProject = new JMenuItem("Close");
	private JMenuItem saveProject = new JMenuItem("Save");
	private JMenuItem saveAsProject = new JMenuItem("Save as");
	private JMenuItem projSettings = new JMenuItem("Settings");
	private JCheckBoxMenuItem autoLoad = new JCheckBoxMenuItem("Auto-load last",false);
	
	private JMenuItem quit = new JMenuItem("Quit");
	private JMenuItem selectNone = new JMenuItem("Select none");
	private JMenuItem selectAll = new JMenuItem("Select all");
	private JMenu selectionAdd = new JMenu("Selection add");
	private JMenuItem[] selectFilterNr;
	
	private JMenuItem preferencesMenuItem = new JMenuItem("Preferences");
	private JMenu view = new JMenu("View");
	
	private JMenuItem viewFileInfo = new JMenuItem("File info");
	
	private JMenu modify = new JMenu("Modify");
	private JMenu visibility = new JMenu("visibility");
	private JMenuItem visShow = new JMenuItem("show");
	
	private JMenuItem visHide = new JMenuItem("hide");
	private JMenu gain = new JMenu("gain");
	private JMenuItem gainAbsolute = new JMenuItem("absolute");
	private JMenuItem gainRelative = new JMenuItem("relative");
	
	private JMenuItem gainAutomatic = new JMenuItem("automatic");
	private JMenu topCut = new JMenu("top cut");
	private JMenuItem topCutAbsolute = new JMenuItem("absolute");
	
	private JMenuItem topCutRelative = new JMenuItem("relative");
	private JMenu edgeSmoothing = new JMenu("edge smoothing");
	private JMenuItem edgeSmoothingEnabled = new JMenuItem("enabled");
	
	private JMenuItem edgeSmoothingDisabled = new JMenuItem("disabled");
	private JMenu level = new JMenu("level");
	private JMenuItem levelSetAbsolute = new JMenuItem("absolute");
	private JMenuItem levelSetRelative = new JMenuItem("relative");
	private JMenuItem levelFront = new JMenuItem("bring to front");
	private JMenuItem levelUp = new JMenuItem("bring forward");
	private JMenuItem levelDown = new JMenuItem("send backward");
	private JMenuItem levelBack = new JMenuItem("send to back");
	private JMenuItem levelSimplifyNumbers = new JMenuItem("simplify numbers");
	
	private JMenuItem levelSortByResolution = new JMenuItem("sort by resolution");
	
	private JMenu render = new JMenu("Render");
	private JMenuItem renderSelection = new JMenuItem("selection");
	private JMenuItem renderAll = new JMenuItem("all");
	private JMenuItem renderSelectionClear = new JMenuItem("selection (clear & render)");
	private JMenuItem renderAllClear = new JMenuItem("all (clear & render)");
	
	private JMenu tools = new JMenu("Tools");
	private JMenuItem toolCreateIndexFile = new JMenuItem("Index file creator");
	
	private JMenu windowMenu = new JMenu("Window");
	private ArrayList<JMenuItem> windowMenuItems = new ArrayList<JMenuItem>();
	private ArrayList<JFrame> hidableWindows = new ArrayList<JFrame>();
	
	private JPanel rootPane = new JPanel();
	
	/*
	 * Memory panel
	 */
	private JPanel memoryPanel;
	//private JButton memoryRefreshButton = new JButton("Refresh");
	private final MemoryPanel memoryComponent = new MemoryPanel(500,20);
	
	/*
	 * Image table
	 */
	private JLabel projectLabel = new JLabel();
	private JScrollPane imageTableScrollPane;
	private JPanel imageListPanel;// = new JPanel();
	private JTable imageTable = new JTable();
	
	
	private JScrollPane modifyTableScrollPane;
	private JTable modifyTable = new JTable();
	
	/*
	 * Commands
	 */
	private JPanel commandPanel;
	//private JToggleButton viewSelection = new JToggleButton("View Selection",false);
	private JButton renderSelectionButton = new JButton("Render selection");
	private JButton renderAllButton = new JButton("Render all");
	private JButton renderSelectionButtonClear = new JButton("Clear & render selection");
	private JButton renderAllButtonClear = new JButton("Clear & render all");
	
	/*
	 * Pre-render sorting
	 */
	private JPanel renderSortingPanel = new JPanel();
	private JButton renderSortingAddButton = new JButton("Add sorter");
	private JButton sortButton = new JButton("Sort");
	/*private static String[] sortKeyNames = {
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_LEVEL)+" ascending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_LEVEL)+" descending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_PIXEL_SCALE_X)+" ascending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_PIXEL_SCALE_X)+" descending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_PIXEL_SCALE_Y)+" ascending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_PIXEL_SCALE_Y)+" descending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_UNIQUE_IMAGE_NUMBER)+" ascending",
		PresetTableModel.getColumnNameOf(PresetTableModel.COLUMN_UNIQUE_IMAGE_NUMBER)+" ascending"
	};*/
	//private ArrayList<String> sortKeyNamesUsed = new ArrayList<String>();
	
	
	
	private static ArrayList<SortKey> sortKeys = new ArrayList<SortKey>();
	{
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_LEVEL,SortOrder.ASCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_LEVEL,SortOrder.DESCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_UNIQUE_IMAGE_NUMBER,SortOrder.ASCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_UNIQUE_IMAGE_NUMBER,SortOrder.DESCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_PIXEL_SCALE_X,SortOrder.ASCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_PIXEL_SCALE_X,SortOrder.DESCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_PIXEL_SCALE_Y,SortOrder.ASCENDING));
		sortKeys.add(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_PIXEL_SCALE_Y,SortOrder.DESCENDING));
	}
	private ArrayList<SortKey> sortKeysUsed = new ArrayList<SortKey>();

	
	
	
	private JButton viewFileInfoButton = new JButton("View file info");
	/*
	 * Preview
	 */
	private JFrame imagePreviewFrame;
	private ImagePreviewPanel imagePreviewPanel;
	private JCheckBox alwaysOnTop = new JCheckBox("Always on top",false);
	
	/*
	 * Status area
	 */
	public JFrame statusAreaFrame;
	public StatusAreaPanel statusArea;
	
	/*
	 * Image Frame
	 */
	private ImageFrame imageFrame;
	
	
	
	
	
	/**
	 * Creates the main gui
	 * @param papplet
	 */
	public MainGui(MoonMapper moonMapperPApplet) {
		super("Moon Mapper V3");
		this.moonMapperPApplet = moonMapperPApplet;
		rootPane.setLayout(new BorderLayout());
		
		preferences = new Prefs(this);
		
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		Point p = moonMapperPApplet.frame.getParent().getLocation();
		p.move(20, 20);
		setLocation(p);
		addWindowListener(this);
		
		/*
		 * Setup menu bar
		 */
		setJMenuBar( createMenuBar() );
		
		/*
		 * Setup memory panel
		 */
		memoryPanel = createMemoryPanel();
		rootPane.add( memoryPanel, BorderLayout.NORTH);
		
		/*
		 * Setup image list panel
		 */
		imageListPanel = createImageListPanel();
		rootPane.add( imageListPanel ,BorderLayout.CENTER);
		
		/*
		 * Setup command panel
		 */
		commandPanel = createCommandPanel();
		rootPane.add(commandPanel, BorderLayout.SOUTH);
		
		setContentPane(rootPane);
		pack();
		//System.out.println(preferences.getImageTableWidth()+"/"+preferences.getImageTableHeight());
		//setPreferredSize(new Dimension(preferences.getImageTableWidth(),preferences.getImageTableHeight()));
		//System.out.println(getSize());
		//pack();
		setVisible(true);
		
		
		
		
		
		/*
		 * Setup image preview frame
		 */
		imagePreviewFrame = createPreviewImageFrame();//new JFrame("Preview image");
		imagePreviewFrame.setLocation(this.getLocation().x, this.getLocation().y + this.getSize().height);
		imagePreviewFrame.setVisible(true);
		registerHidableWindow(imagePreviewFrame);
		
		
		
		
		/*
		 * Setup status area
		 */
		statusArea = new StatusAreaPanel(660);//scrollPane.getSize().width);
		statusAreaFrame = new JFrame("Status");
		statusAreaFrame.setLocation(
				imagePreviewFrame.getLocation().x+imagePreviewFrame.getSize().width, 
				imagePreviewFrame.getLocation().y);
		statusAreaFrame.setContentPane(statusArea);
		statusAreaFrame.pack();
		statusAreaFrame.setVisible(true);
		registerHidableWindow(statusAreaFrame);
		
		
		
		
		
		
		if (preferences.getAutoLoadProject()) {
			projectLoad(preferences.getLastProjectPath());
		}
			
		updateGuiFromProject();
		
		//setPreferredSize(new Dimension(preferences.getImageTableWidth(),preferences.getImageTableHeight()));
		//pack();
		//System.out.println(preferences.getImageTableWidth()+"/"+preferences.getImageTableHeight());
		this.requestFocus();
	}
	
	
	
	
	/*
	 * BUTTON ACTIONS
	 * 
	 * (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	
	private JPanel createMemoryPanel() {
		JPanel panel = new JPanel();
		//memoryRefreshButton.setHorizontalAlignment(SwingConstants.LEFT);
		/*panel.add(memoryRefreshButton,BorderLayout.WEST);
		memoryRefreshButton.addActionListener(new AbstractAction() {
			public void actionPerformed(ActionEvent arg0) {
				memoryComponent.repaint();
			}
		});*/
		//memoryComponent.setPreferredSize(new Dimension(this.getWidth(),20));
		panel.add(memoryComponent,BorderLayout.CENTER);
		return panel;
	}




	public void actionPerformed(ActionEvent e) {
		
		
		Object source = e.getSource();
		int selectFilterNrClicked=-1;
		for (int i=0;i<selectFilterNr.length;i++) {
			if (e.getSource().equals(selectFilterNr[i])) selectFilterNrClicked = i;
		}
		
		/*int windowMenuItemClicked=-1;
		for (int i=0;i<)*/
		
		/*
		 * Project
		 */
		
		if (source.equals(newProject)) {
			projectNew();
		}
		else if (source.equals(openProject)) {
			if (projectClose()) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogType(JFileChooser.OPEN_DIALOG);
				chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
				chooser.setMultiSelectionEnabled(false);
				chooser.addChoosableFileFilter(new ProjectFileFilter());
				chooser.setAcceptAllFileFilterUsed(false);
				chooser.setCurrentDirectory(preferences.getLastProjectPath().getParentFile());
				int returnVal = chooser.showOpenDialog(this);
				if (returnVal==JFileChooser.APPROVE_OPTION) {
					System.out.println(chooser.getSelectedFile());
					projectLoad(chooser.getSelectedFile());
					updateGuiFromProject();
				}
			}
		}
		else if (source.equals(closeProject)) {
			projectClose();
		}
		else if (source.equals(saveProject)) {
			projectSave();
		}
		else if (source.equals(saveAsProject)) {
			projectSaveAs();
		}
		else if (source.equals(projSettings)) {
			projectSettings();
		}
		else if (source.equals(autoLoad)) {
			preferences.setAutoLoadProject(autoLoad.getState());
			preferences.save();
		}
		else if (source.equals(quit)) {
			quit();
		}
		
		/*
		 * Edit
		 */
		else if (source.equals(preferencesMenuItem)) {
			//PreferencesDialog dialog = new PreferencesDialog(this,moonMapper.preferences);
			new PreferencesDialog(this,preferences);
		}
		else if (source.equals(selectNone)) {
			imageTable.clearSelection();
		}
		else if (source.equals(selectAll)) {
			imageTable.selectAll();
		}
		else if (selectFilterNrClicked!=-1) {
			//imageTable.clearSelection();
			//TableModel tm = imageTable.getModel();
			String filterName = AMIEFilter.names[selectFilterNrClicked];
			imageTable.getSelectionModel().setValueIsAdjusting(true);
			for (int row=0;row<imageTable.getRowCount();row++) {
				String rowFilter = (String)imageTable.getValueAt(row, PresetTableModel.COLUMN_FILTER_NAME);
				if (rowFilter.equals(filterName)) {
					imageTable.addRowSelectionInterval(row, row);
				}
			}
			imageTable.getSelectionModel().setValueIsAdjusting(false);
		}
		/*
		 * Modify - visibility
		 */
		else if (source.equals(visShow)) {
			changeValuesAbsolute(new Boolean(true), PresetTableModel.COLUMN_VISIBLE);
		}
		else if (source.equals(visHide)) {
			changeValuesAbsolute(new Boolean(false), PresetTableModel.COLUMN_VISIBLE);
		}
		/*
		 * Modify - gain
		 */
		else if (source.equals(gainAbsolute)) {
			Double d = Dialogs.createDoubleDialog(this,"Enter new gain value","Absolute gain change");
			if (d!=null)
				changeValuesAbsolute(d,PresetTableModel.COLUMN_GAIN);
		}
		else if (source.equals(gainRelative)) {
			Double d = Dialogs.createDoubleDialog(this,"Enter gain change factor","Relative gain change");
			if (d!=null)
				changeDoublesRelativeMultiplicative(d,PresetTableModel.COLUMN_GAIN);
		}
		else if (source.equals(gainAutomatic)) {
			modifyGainAutomaticallyOfSelectedRows();
		}
		/*
		 * Modify - top cut
		 */
		else if (source.equals(topCutAbsolute)) {
			Integer i = Dialogs.createIntegerDialog(this,"Enter new top cut value","Absolute top cut change");
			if (i!=null)
				changeValuesAbsolute(i,PresetTableModel.COLUMN_TOP_CUT);
		}
		else if (source.equals(topCutRelative)) {
			Integer i = Dialogs.createIntegerDialog(this,"Enter top cut change","Relative top cut change");
			if (i!=null)
				changeIntegersRelativeAdditive(i,PresetTableModel.COLUMN_TOP_CUT);
		}
		/*
		 * Modify - edge smoothing
		 */
		else if (source.equals(edgeSmoothingEnabled)) {
			changeValuesAbsolute(new Boolean(true), PresetTableModel.COLUMN_EDGE_SMOOTHING);
		}
		else if (source.equals(edgeSmoothingDisabled)) {
			changeValuesAbsolute(new Boolean(false), PresetTableModel.COLUMN_EDGE_SMOOTHING);
		}
		/*
		 * Modify - level
		 */
		else if (source.equals(levelSetAbsolute)) {
			Integer i = Dialogs.createIntegerDialog(this, "Enter new level", "Absolute level adjustment");
			if (i!=null)
				changeValuesAbsolute(i, PresetTableModel.COLUMN_LEVEL);
		}
		else if (source.equals(levelSetRelative)) {
			Integer i = Dialogs.createIntegerDialog(this, "Enter level change", "Relative level adjustment");
			if (i!=null)
				changeIntegersRelativeAdditive(i, PresetTableModel.COLUMN_LEVEL);
		}
		else if (source.equals(levelFront)) {
			int max = Integer.MIN_VALUE;
			for (int i=0;i<imageTable.getModel().getRowCount();i++) {
				Integer cVal =(Integer)imageTable.getModel().getValueAt(i, PresetTableModel.COLUMN_LEVEL);
				boolean thisRowIsPartOfSelection=false;
				for (int j=0;j<imageTable.getSelectedRows().length;j++)
					if (imageTable.getSelectedRows()[j]==i) thisRowIsPartOfSelection=true; 
				if (!thisRowIsPartOfSelection && max<cVal) max=cVal;
			}
			if (max==Integer.MIN_VALUE) max=-1;
			changeValuesAbsolute(max+1, PresetTableModel.COLUMN_LEVEL);
		}
		else if (source.equals(levelBack)) {
			int min = Integer.MAX_VALUE;
			for (int i=0;i<imageTable.getModel().getRowCount();i++) {
				Integer cVal =(Integer)imageTable.getModel().getValueAt(i, PresetTableModel.COLUMN_LEVEL);
				boolean thisRowIsPartOfSelection=false;
				for (int j=0;j<imageTable.getSelectedRows().length;j++)
					if (imageTable.getSelectedRows()[j]==i) thisRowIsPartOfSelection=true; 
				if (!thisRowIsPartOfSelection && min>cVal) min=cVal;
			}
			if (min==Integer.MAX_VALUE) min=1;
			changeValuesAbsolute(min-1, PresetTableModel.COLUMN_LEVEL);	
		}
		else if (source.equals(levelSimplifyNumbers)) {
			LevelObject[] data = new LevelObject[imageTable.getModel().getRowCount()];
			for (int i=0;i<data.length;i++)
				data[i] = new LevelObject((Integer)imageTable.getModel().getValueAt(i,PresetTableModel.COLUMN_LEVEL),i);
			Arrays.sort(data);
			boolean[] isNewLevel = new boolean[data.length];
			isNewLevel[0]=true;
			int previousLevel=data[0].getLevel();
			int totalLevels=1;
			for (int i=1;i<data.length;i++) {
				if (data[i].getLevel()!=previousLevel) {
					isNewLevel[i]=true;
					totalLevels++;
				}
				previousLevel=data[i].getLevel();
			}
			int newLevel=-1;
			for (int i=0;i<data.length;i++) {
				if (isNewLevel[i]) newLevel++;
				data[i].setLevel(newLevel);
			}
			for (int i=0;i<data.length;i++) {
				imageTable.getModel().setValueAt(data[i].getLevel(), data[i].getIndex(), PresetTableModel.COLUMN_LEVEL);
			}
		}
		
		/*
		 * Index file creator
		 */
		
		else if (source.equals(toolCreateIndexFile)) {
			Object[] options = {"From all files",
                    "From existing index file",
                    "Cancel"};
			int n = JOptionPane.showOptionDialog(this, 
					"Create index file from\n" +
					"All files or other index file", 
					"Choose source",
					JOptionPane.YES_NO_CANCEL_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[0]
					);
			
			if (n==JOptionPane.YES_OPTION) {
				new IndexFileCreator(this,IndexFileCreator.FROM_FOLDERS);
			} else if (n==JOptionPane.NO_OPTION) {
				new IndexFileCreator(this,IndexFileCreator.FROM_FILE);
			}
		}
		
		/*
		 * WINDOW
		 */
		
		//else if ()
		
		/*
		 * Button: Render selection // render all
		 * MENU: Commands 
		 */
		else if (source.equals(renderSelectionButton) || source.equals(renderSelection)) {
			renderWithSortKeys(false,false); 
		}
		else if (source.equals(renderAllButton) || source.equals(renderAll)) {
			renderWithSortKeys(false,true);
		}
		else if (source.equals(renderSelectionButtonClear) || source.equals(renderSelectionClear)) {
			renderWithSortKeys(true,false); 
		}
		else if (source.equals(renderAllButtonClear) || source.equals(renderAllClear)) {
			renderWithSortKeys(true,true);
		}
		/*
		 * Add sorter
		 */
		else if (source.equals(renderSortingAddButton)) {
			actionPerformedSorterAddButton();
		}
		
		else if (source.equals(sortButton)) {
			actionPerformedSortButton();
		}

		
		/*
		 * Button: showFileInfo
		 */
		else if (source.equals(viewFileInfoButton) || source.equals(viewFileInfo)) {
			//imageTable.getModel().getValueAt(arg0, arg1)
			String filename = (String)getValueOfLastSelectedRow(PresetTableModel.COLUMN_FILENAME);
			if (filename!=null) {
				int orbitNumber = (Integer)getValueOfLastSelectedRow(PresetTableModel.COLUMN_ORBIT_NUMBER);
				String path = preferences.getPath_ImageArchive(orbitNumber);
				PDSObject pdsfile = PDSFileLoader.loadHeader(new File(path+"\\"+filename));
				JTextArea txt = new JTextArea(pdsfile.toPDSString());
				txt.setFont(new Font(Font.MONOSPACED,Font.PLAIN,12));
				JScrollPane pane = new JScrollPane(txt);
				pane.setPreferredSize(new Dimension(600,400));
				JOptionPane.showMessageDialog(this, pane, "File info for "+filename, JOptionPane.YES_OPTION);
			}
		}
		
		
		/*
		 * Preview panel: always on top
		 */
		else if (source.equals(alwaysOnTop)) {
			imagePreviewFrame.setAlwaysOnTop(alwaysOnTop.isSelected());
		}
		
		/*
		 * Preview panel: gain
		 */
		/*else if (source.equals(gainAuto)) {
			renderPreviewImage();
		}
		else if (source.equals(gainFromTable)) {
			//gainAuto.setText(AUTO_GAIN_TEXT);
			gainAutoAmount.setText("");//AUTO_GAIN_TEXT);
			renderPreviewImage();
		}*/
		
		else {
			System.out.println(e.getActionCommand());
			JOptionPane.showMessageDialog(this, "Unimplemented action ("+e.getActionCommand()+")", "Unimplemented action", JOptionPane.ERROR_MESSAGE);
		}
	}


	private SortKey[] getAvailableSorters() {
		ArrayList<SortKey> tmp = new ArrayList<SortKey>();
		for (int i=0;i<sortKeys.size();i++) {
			boolean used=false;
			for (int j=0;j<sortKeysUsed.size();j++) {
				if (sortKeysUsed.get(j).equals(sortKeys.get(i))) {
					used=true;
				}
			}
			if (!used) {
				tmp.add(sortKeys.get(i));
			}
		}
		SortKey[] availableSorters = new SortKey[tmp.size()];
		for (int i=0;i<tmp.size();i++) availableSorters[i]=tmp.get(i);
		return availableSorters;
	}
	
	private void actionPerformedSortButton() {
		imageTable.getRowSorter().setSortKeys(sortKeysUsed);
	}
	
	private void actionPerformedSorterAddButton() {
		
		SortKey[] possibleSorters = getAvailableSorters();
		
        SortKey selection = (SortKey)JOptionPane.showInputDialog(
                            this,
                            "Add a sorter:",
                            "New sorter",
                            JOptionPane.PLAIN_MESSAGE,
                            null,
                            possibleSorters,
                            "ham");
        
        addSortKey(selection);
	}
	
	private void addSortKey(SortKey selection) {
		if (selection!=null) {
			System.out.println(selection);
        	sortKeysUsed.add(selection);
        	renderSortingPanel.remove(renderSortingAddButton);
        	
        	final JPanel sortPanel = new JPanel();
        	sortPanel.setLayout(new BoxLayout(sortPanel,BoxLayout.Y_AXIS));
        	JLabel sortLabel = new JLabel(selection.toString()); 
        	sortPanel.add(sortLabel);
        	sortPanel.setName(selection.toString());
        	JButton remove = new JButton("Remove");
        	remove.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					Container c = sortPanel.getParent();
					c.remove(sortPanel);
					for (int i=0;i<sortKeysUsed.size();i++) {
						if (sortKeysUsed.get(i).toString().equals(sortPanel.getName()))
							sortKeysUsed.remove(i);
					}
					//pack();
				}
        	});
        	sortPanel.add(remove);
        	
        	renderSortingPanel.add(sortPanel);
        	renderSortingPanel.add(renderSortingAddButton);
        	
        	
        	//this.pack();
        }
	}
	
	


	
	
	
	/*
	 * 
	 * 
	 * CREATE COMMAND PANEL
	 * 
	 * 
	 */
	
	private JPanel createCommandPanel() {
		
		JPanel commandPanel = new JPanel(new FlowLayout());
		commandPanel.setBorder(BorderFactory.createTitledBorder("Commands"));
		
		JPanel rSel = new JPanel();
		rSel.setLayout(new BoxLayout(rSel,BoxLayout.Y_AXIS));
		renderSelectionButton.setEnabled(false);
		renderSelectionButton.addActionListener(this);
		rSel.add(renderSelectionButton);//,c);
		renderSelectionButtonClear.setEnabled(false);
		renderSelectionButtonClear.addActionListener(this);
		rSel.add(renderSelectionButtonClear);
		commandPanel.add(rSel);
		
		JPanel rAll = new JPanel();
		rAll.setLayout(new BoxLayout(rAll,BoxLayout.Y_AXIS));
		renderAllButton.addActionListener(this);
		rAll.add(renderAllButton);//,c);
		renderAllButtonClear.addActionListener(this);
		rAll.add(renderAllButtonClear);//,c);
		commandPanel.add(rAll);
		
		
		renderSortingPanel.setLayout(new FlowLayout());
		renderSortingPanel.add(renderSortingAddButton);
		renderSortingAddButton.addActionListener(this);
		renderSortingPanel.setBorder(BorderFactory.createEtchedBorder());
		//addSortKey(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_UNIQUE_IMAGE_NUMBER,SortOrder.ASCENDING));
		addSortKey(new PresetTableModel.SortKeyColumn(PresetTableModel.COLUMN_LEVEL,SortOrder.ASCENDING));
		commandPanel.add(renderSortingPanel);
		
		sortButton.addActionListener(this);
		commandPanel.add(sortButton);
		
		viewFileInfoButton.addActionListener(this);
		viewFileInfoButton.setEnabled(false);
		commandPanel.add(viewFileInfoButton);//,c);
		
		
		
		return commandPanel;
	}
	
	
	
	
	
	/*
	 * 
	 * 
	 * CREATE IMAGE LIST PANEL
	 * 
	 * 
	 */
	
	private JPanel createImageListPanel() {
		/*
		 * Setup image List Panel
		 */
		
		JPanel panel = new JPanel(new BorderLayout());
		panel.setBorder(BorderFactory.createTitledBorder("Image list"));
		
		imageTable.setModel(new ImageTableModel(this));
		imageTable.addPropertyChangeListener(this);
		imageTable.setFont(new Font(Font.MONOSPACED,Font.PLAIN,13));
		imageTable.setFillsViewportHeight(true);
		imageTable.getSelectionModel().addListSelectionListener(this);
		imageTable.setAutoCreateRowSorter(true);
		imageTable.getTableHeader().setResizingAllowed(false);
		imageTable.getTableHeader().setReorderingAllowed(false);
		PresetTableModel.initColumnSizes(imageTable);
		//int totWidth = PresetTableModel.initColumnSizes(imageTable);
		/*imageTable.setPreferredScrollableViewportSize(
				new Dimension(
						Integer.parseInt(preferences.IMAGE_TABLE_WIDTH),
						Integer.parseInt(preferences.IMAGE_TABLE_HEIGHT)));*/
		imageTableScrollPane = new JScrollPane(imageTable);
		imageTableScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		imageTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		
		JPanel imageTablePanel = new JPanel(new BorderLayout());
		imageTablePanel.add(projectLabel,BorderLayout.PAGE_START);
		imageTablePanel.add(imageTableScrollPane,BorderLayout.CENTER);
		panel.add(imageTablePanel,BorderLayout.CENTER);
		
		modifyTable.setModel(new ModifyTableModel());
		modifyTable.setFont(imageTable.getFont());
		modifyTable.getModel().addTableModelListener(this);
		modifyTable.getTableHeader().setResizingAllowed(false);
		modifyTable.getTableHeader().setReorderingAllowed(false);
		PresetTableModel.initColumnSizes(modifyTable);
		//int wid = PresetTableModel.initColumnSizes(modifyTable);
		/*modifyTable.setPreferredScrollableViewportSize(new Dimension(wid,20));*/
		((ModifyTableModel)modifyTable.getModel()).clearUneditableContent();
		modifyTable.clearSelection();
		modifyTable.setVisible(true);
		modifyTableScrollPane = new JScrollPane(modifyTable);
		modifyTableScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		
		JPanel modifyTablePanel = new JPanel(new BorderLayout());
		modifyTablePanel.add(new JLabel("Modify"),BorderLayout.PAGE_START);
		modifyTablePanel.add(modifyTableScrollPane,BorderLayout.CENTER);
		panel.add(modifyTablePanel,BorderLayout.PAGE_END);
		
		panel.setPreferredSize(
				new Dimension(
						preferences.getImageTableWidth(),
						preferences.getImageTableHeight()));
		
		return panel;
	}
	
	
	
	
	
	/*
	 * 
	 * 
	 * CREATE MENU BAR
	 * 
	 * 
	 */
	
	private JMenuBar createMenuBar() {
		/*
		 * Setup Menu Bar: Project
		 */
		JMenuBar menuBar = new JMenuBar();
		
		JMenu projectMenu = new JMenu("Project");
		projectMenu.setMnemonic(KeyEvent.VK_P);
		
		newProject.addActionListener(this);
		newProject.setMnemonic(KeyEvent.VK_N);
		newProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		projectMenu.add(newProject);
		
		openProject.setMnemonic(KeyEvent.VK_O);
		openProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		openProject.addActionListener(this);
		projectMenu.add(openProject);
		
		closeProject.setMnemonic(KeyEvent.VK_C);
		closeProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_W, ActionEvent.CTRL_MASK));
		closeProject.addActionListener(this);
		closeProject.setEnabled(false);
		projectMenu.add(closeProject);
		
		projectMenu.addSeparator();
		
		saveProject.setMnemonic(KeyEvent.VK_S);
		saveProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		saveProject.addActionListener(this);
		saveProject.setEnabled(false);
		projectMenu.add(saveProject);
		
		saveAsProject.setMnemonic(KeyEvent.VK_A);
		saveAsProject.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK+ActionEvent.SHIFT_MASK));
		saveAsProject.addActionListener(this);
		saveAsProject.setEnabled(false);
		projectMenu.add(saveAsProject);
		
		projectMenu.addSeparator();
		
		projSettings.setMnemonic(KeyEvent.VK_E);
		projSettings.addActionListener(this);
		projectMenu.add(projSettings);
		
		autoLoad.setMnemonic(KeyEvent.VK_L);
		autoLoad.addActionListener(this);
		autoLoad.setEnabled(true);
		autoLoad.setState(preferences.getAutoLoadProject());
		projectMenu.add(autoLoad);
		
		projectMenu.addSeparator();
		
		quit.setMnemonic(KeyEvent.VK_Q);
		quit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		quit.addActionListener(this);
		projectMenu.add(quit);
		menuBar.add(projectMenu);
		
		/*
		 * Setup Menu Bar: Edit
		 */
		JMenu editMenu = new JMenu("Edit");
		editMenu.setMnemonic(KeyEvent.VK_E);
		
		selectNone.setMnemonic(KeyEvent.VK_N);		
		selectNone.addActionListener(this);
		editMenu.add(selectNone);
		
		selectAll.setMnemonic(KeyEvent.VK_A);
		selectAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		selectAll.addActionListener(this);
		editMenu.add(selectAll);
		
		selectFilterNr = new JMenuItem[AMIEFilter.names.length];
		for (int i=0;i<selectFilterNr.length;i++) {
			selectFilterNr[i] = new JMenuItem(AMIEFilter.names[i]+" ("+(i+1)+")");
			selectFilterNr[i].setMnemonic(KeyEvent.VK_1+i);
			selectFilterNr[i].addActionListener(this);
			selectionAdd.add(selectFilterNr[i]);
		}
		selectionAdd.setMnemonic(KeyEvent.VK_D);
		editMenu.add(selectionAdd);
		
		editMenu.addSeparator();
		
		preferencesMenuItem.setMnemonic(KeyEvent.VK_P);
		preferencesMenuItem.addActionListener(this);
		editMenu.add(preferencesMenuItem);
		menuBar.add(editMenu);
		
		/*
		 * Setup Menu Bar: View
		 */
		
		viewFileInfo.setEnabled(false);
		viewFileInfo.addActionListener(this);
		viewFileInfo.setMnemonic(KeyEvent.VK_F);
		viewFileInfo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, ActionEvent.CTRL_MASK));
		view.add(viewFileInfo);
		view.setMnemonic(KeyEvent.VK_V);
		menuBar.add(view);
		
		/*
		 * Setup Menu Bar: Modify
		 */

		modify.setMnemonic(KeyEvent.VK_M);
		visibility.setEnabled(false);
		visibility.setMnemonic(KeyEvent.VK_V);
		visShow.setMnemonic(KeyEvent.VK_S);
		visShow.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		visShow.addActionListener(this);
		visibility.add(visShow);
		visHide.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_V, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK));
		visHide.setMnemonic(KeyEvent.VK_H);
		visHide.addActionListener(this);
		visibility.add(visHide);
		modify.add(visibility);
		
		gain.setMnemonic(KeyEvent.VK_G);
		//gain.setEnabled(false);
		gainAbsolute.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		gainAbsolute.setMnemonic(KeyEvent.VK_A);
		gainAbsolute.addActionListener(this);
		gainAbsolute.setEnabled(false);
		gain.add(gainAbsolute);
		gainRelative.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK));
		gainRelative.setMnemonic(KeyEvent.VK_R);
		gainRelative.addActionListener(this);
		gainRelative.setEnabled(false);
		gain.add(gainRelative);
		gain.addSeparator();
		gainAutomatic.setMnemonic(KeyEvent.VK_T);
		gainAutomatic.addActionListener(this);
		gain.add(gainAutomatic);
		modify.add(gain);
		
		topCut.setMnemonic(KeyEvent.VK_T);
		topCut.setEnabled(false);
		topCutAbsolute.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		topCutAbsolute.setMnemonic(KeyEvent.VK_A);
		topCutAbsolute.addActionListener(this);
		topCut.add(topCutAbsolute);
		topCutRelative.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK));
		topCutRelative.setMnemonic(KeyEvent.VK_R);
		topCutRelative.addActionListener(this);
		topCut.add(topCutRelative);
		modify.add(topCut);
		
		edgeSmoothing.setMnemonic(KeyEvent.VK_E);
		edgeSmoothing.setEnabled(false);
		edgeSmoothingEnabled.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		edgeSmoothingEnabled.setMnemonic(KeyEvent.VK_E);
		edgeSmoothingEnabled.addActionListener(this);
		edgeSmoothing.add(edgeSmoothingEnabled);
		edgeSmoothingDisabled.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK));
		edgeSmoothingDisabled.setMnemonic(KeyEvent.VK_D);
		edgeSmoothingDisabled.addActionListener(this);
		edgeSmoothing.add(edgeSmoothingDisabled);
		modify.add(edgeSmoothing);
		
		level.setMnemonic(KeyEvent.VK_L);
		levelSetAbsolute.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		levelSetAbsolute.setMnemonic(KeyEvent.VK_A);
		levelSetAbsolute.addActionListener(this);
		levelSetAbsolute.setEnabled(false);
		level.add(levelSetAbsolute);
		levelSetRelative.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK + ActionEvent.ALT_MASK));
		levelSetRelative.setMnemonic(KeyEvent.VK_R);
		levelSetRelative.addActionListener(this);
		levelSetRelative.setEnabled(false);
		level.add(levelSetRelative);
		level.addSeparator();
		levelFront.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_CLOSE_BRACKET, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		levelFront.setMnemonic(KeyEvent.VK_F);
		levelFront.addActionListener(this);
		levelFront.setEnabled(false);
		level.add(levelFront);
		levelUp.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_CLOSE_BRACKET, ActionEvent.CTRL_MASK));
		levelUp.setMnemonic(KeyEvent.VK_O);
		levelUp.addActionListener(this);
		levelUp.setEnabled(false);
		level.add(levelUp);
		levelDown.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_OPEN_BRACKET, ActionEvent.CTRL_MASK));
		levelDown.setMnemonic(KeyEvent.VK_B);
		levelDown.addActionListener(this);
		levelDown.setEnabled(false);
		level.add(levelDown);
		levelBack.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_OPEN_BRACKET, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		levelBack.setMnemonic(KeyEvent.VK_K);
		levelBack.addActionListener(this);
		levelBack.setEnabled(false);
		level.add(levelBack);
		level.addSeparator();
		levelSimplifyNumbers.setMnemonic(KeyEvent.VK_I);
		levelSimplifyNumbers.addActionListener(this);
		level.add(levelSimplifyNumbers);
		level.addSeparator();
		levelSortByResolution.setMnemonic(KeyEvent.VK_S);
		levelSortByResolution.addActionListener(this);
		level.add(levelSortByResolution);
		modify.add(level);
		
		menuBar.add(modify);
		
		/*
		 * Seupt Menu Bar: Commands
		 */
		
		renderSelection.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		renderSelection.setMnemonic(KeyEvent.VK_S);
		renderSelection.addActionListener(this);
		render.add(renderSelection);
		//renderSelectionClear.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		renderSelectionClear.setMnemonic(KeyEvent.VK_E);
		renderSelectionClear.addActionListener(this);
		render.add(renderSelectionClear);
		renderAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		renderAll.setMnemonic(KeyEvent.VK_A);
		renderAll.addActionListener(this);
		render.add(renderAll);
		//renderAll.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_R, ActionEvent.CTRL_MASK));
		renderAllClear.setMnemonic(KeyEvent.VK_C);
		renderAllClear.addActionListener(this);
		render.add(renderAllClear);
		render.setMnemonic(KeyEvent.VK_R);
		menuBar.add(render);
		
		
		/*
		 * Setup Menu Bar: Tools
		 */
		
		tools.setMnemonic(KeyEvent.VK_T);
		toolCreateIndexFile.setMnemonic(KeyEvent.VK_I);
		toolCreateIndexFile.addActionListener(this);
		tools.add(toolCreateIndexFile);
		menuBar.add(tools);
		
		windowMenu.setMnemonic(KeyEvent.VK_W);
		menuBar.add(windowMenu);
		
		return menuBar;
		
	}
	
	
	
	
	
	/*
	 * 
	 * 
	 * CREATE PREVIEW IMAGE FRAME
	 * 
	 * 
	 */
	
	private JFrame createPreviewImageFrame() {
		JFrame f = new JFrame("Preview");
		//imagePreviewFrame = new JFrame();
		//f.addWindowListener(this);
		//imagePreviewFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
				
		imagePreviewPanel = new ImagePreviewPanel(null);
		f.getContentPane().add(BorderLayout.CENTER,imagePreviewPanel);//,c);
		
		/*ButtonGroup buttonGroup = new ButtonGroup();
		buttonGroup.add(gainAuto);
		buttonGroup.add(gainFromTable);*/
		
		JPanel buttonPanel = new JPanel(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		c.insets = new Insets(2,2,2,2);
		c.anchor = GridBagConstraints.WEST;
		alwaysOnTop.addActionListener(this);
		c.gridx=0;
		c.gridy=0;
		buttonPanel.add(alwaysOnTop,c);
		/*c.gridx=0;
		c.gridy++;
		gainAuto.addActionListener(this);
		buttonPanel.add(gainAuto,c);//,c);
		c.gridx=0;
		c.gridy++;
		gainFromTable.addActionListener(this);
		buttonPanel.add(gainFromTable,c);//,c);
		c.gridx=0;
		c.gridy++;
		buttonPanel.add(gainAutoAmount,c);*/
		
		f.getContentPane().add(BorderLayout.EAST,buttonPanel);
		//imagePreviewFrame.setAlwaysOnTop(true);
		f.pack();
		//f.setVisible(true);
		return f;
	}
	
	
	
	
	
	public MoonMapper getMoonMapperPApplet() {
		return moonMapperPApplet;
	}
	
	
	
	
	
	public Prefs getPreferences() {
		return preferences;
	}
	
	
	
	
	
	public Project getProject() {
		return project;
	}
	
	
	
	
	
	/**
	 * Returns a Vector of PDSObjects containing all information per image.<br>
	 * <ul>
	 * <li><code>all = true</code>: Returns <i>all</i> objects from the {@link #imageTable}.
	 * <li><code>all = false</code>: Returns <i>selected</i> objects from the {@link #imageTable}.
	 * </ul>
	 * @param all (boolean)
	 * @return
	 */
	private Vector<PDSObject> getSelectedPDSObjects(boolean all) {
		Vector<PDSObject> out = new Vector<PDSObject>();
		int[] selectedRows = imageTable.getSelectedRows();
		int nrOfRows = all ? imageTable.getModel().getRowCount() : selectedRows.length;
        for (int i=0;i<nrOfRows;i++) {
        	int rowNrOfModel = all?i:selectedRows[i];
        	int uniqueIndex = (Integer)imageTable.getModel().getValueAt(imageTable.convertRowIndexToModel(rowNrOfModel),ImageTableModel.COLUMN_INDEX);
        	PDSObject obj = new PDSObject();
        	obj.addNewValues(project.getIndexFile()[uniqueIndex].getOnlyPDSValues());
        	obj.addNewValues(project.getModificationFile()[uniqueIndex].getOnlyPDSValues());
        	//obj.removeLastValueNamed(PDSDictionary.names[PDSDictionary.FILE_NAME]);
        	obj.removeLastValueNamed(Element.FILE_NAME.name());
        	out.add(obj);
        }
		return out;
	}
	
	
	
	
	
	
	/**
	 * Returns the PDSObject from the last selected row in the table
	 * @return
	 */
	private PDSObject getSingleSelectedPDSObject() {
		int rowNrOfModel = imageTable.getSelectionModel().getLeadSelectionIndex();
		PDSObject obj = null;
		if (rowNrOfModel>=0) {
			int uniqueIndex = (Integer)imageTable.getModel().getValueAt(imageTable.convertRowIndexToModel(rowNrOfModel),ImageTableModel.COLUMN_INDEX);
			obj = new PDSObject();
	    	obj.addNewValues(project.getIndexFile()[uniqueIndex].getOnlyPDSValues());
	    	obj.addNewValues(project.getModificationFile()[uniqueIndex].getOnlyPDSValues());
	    	//obj.removeLastValueNamed(PDSDictionary.names[PDSDictionary.FILE_NAME]);
	    	obj.removeLastValueNamed(Element.FILE_NAME.name());
		}
		return obj;
	}
	
	
	
	
	
	
	/**
	 * Returns the Object located at the (in time) last selected row and supplied <i>column</i>
	 * @param column
	 * @return
	 */
	private Object getValueOfLastSelectedRow(int column) {
		//int row = imageTable.getSelectedRow();
		int row = imageTable.getSelectionModel().getLeadSelectionIndex();
		if (row>=0)
			return imageTable.getModel().getValueAt(imageTable.convertRowIndexToModel(row), column);
		else
			return null;
	}
	
	
	
	
	
	
	private void changeDoublesRelativeMultiplicative(Double dMult, int column) {
		int[] selectedRows = imageTable.getSelectedRows();
		for (int i=0;i<selectedRows.length;i++) {
			Double d = (Double)imageTable.getModel().getValueAt(selectedRows[i], column);
			Double newValue = d*dMult;
			if (column==PresetTableModel.COLUMN_GAIN && newValue<0) newValue=0.0;
			imageTable.getModel().setValueAt(newValue, imageTable.convertRowIndexToModel(selectedRows[i]), column);
		}
	}
	
	
	
	
	
	
	
	private void modifyGainAutomaticallyOfSelectedRows() {
		//int column = PresetTableModel.COLUMN_GAIN;
		
		int n = JOptionPane.showConfirmDialog(this, 
				"Please implement this as a progress task!! Cuz lots of files might take a while!!\n" +
				"Go ahead?", "Go ahead?",
				JOptionPane.YES_NO_OPTION);
		
		if (n==JOptionPane.YES_OPTION) {
		
			int[] selectedRows = imageTable.getSelectedRows();
			for (int i=0;i<selectedRows.length;i++) {
				//Float f = (Float)imageTable.getModel().getValueAt(selectedRows[i],PresetTableModel.COLUMN_GAIN);
				
				String filename = (String)imageTable.getModel().getValueAt(selectedRows[i],PresetTableModel.COLUMN_FILENAME);
				File file = preferences.getFile_ImageArchive(filename);
				PDSObject pdsFile = PDSFileLoader.loadHeader(file);
				float derivedMaximum;
				try {
					derivedMaximum = Float.parseFloat(pdsFile.getValueOfPDSValueOfPDSObject("DERIVED_MAXIMUM","IMAGE"));
					float gain = 255/derivedMaximum;
					imageTable.getModel().setValueAt(new Float(gain), selectedRows[i], PresetTableModel.COLUMN_GAIN);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				} catch (PDSValueNotFoundException e) {
					e.printStackTrace();
				} catch (PDSObjectNotFoundException e) {
					e.printStackTrace();
				}
				
			}
		
		}
	}
	
	
	
	
	
	
	/**
	 * Modifies all Integers in the supplied <i>column</i> to <i>iChange</i> for all selected rows in the {@link #imageTable}.<br>
	 * <UL>
	 * <li>Accepts all values for <i>column</i> = {@link PresetTableModel#COLUMN_LEVEL}<br>
	 * <li>Will change any supplied <i>iChange</i><0 to 0, for <i>column</i> = {@link PresetTableModel#COLUMN_TOP_CUT}
	 * </ul>
	 * @param iChange Supplied integer value.
	 * @param column Column index. use {@link PresetTableModel#COLUMN_TOP_CUT} or {@link PresetTableModel#COLUMN_LEVEL}
	 * @throws RuntimeException if not using one of the requested column indexes.
	 */
	private void changeIntegersRelativeAdditive(Integer iChange, int column) {
		if (column==PresetTableModel.COLUMN_LEVEL || column==PresetTableModel.COLUMN_TOP_CUT) {
			int[] selectedRows = imageTable.getSelectedRows();
			for (int i=0;i<selectedRows.length;i++) {
				Integer integer = (Integer)imageTable.getModel().getValueAt(imageTable.convertRowIndexToModel(selectedRows[i]), column);
				Integer newValue = integer+iChange;
				if (column==PresetTableModel.COLUMN_TOP_CUT && newValue<0) newValue=0;
				imageTable.getModel().setValueAt(newValue, imageTable.convertRowIndexToModel(selectedRows[i]), column);
			}
		} else
			throw new RuntimeException("use PresetTableModel.COLUMN_LEVEL or PresetTableModel.COLUMN_TOP_CUT for column");
	}
	
	
	
	
	
	
	/**
	 * 
	 * PROJECT CLOSE
	 * @return has closed
	 * 
	 */
	
	private boolean projectClose() {
		if (project!=null) {
			Object[] options = {"Save current", "Don't save", "Cancel"};
			int n = JOptionPane.showOptionDialog(
	                this, "Do you want to save the current project before closing?",
	                "Close Project",
	                JOptionPane.YES_NO_CANCEL_OPTION,
	                JOptionPane.QUESTION_MESSAGE,
	                null,
	                options,
	                options[0]);
	        if (n == JOptionPane.YES_OPTION) {
	            projectSave();
	        } else if (n == JOptionPane.NO_OPTION) {
	        	// do nothing
	        } else { // cancel
	            return false;
	        }
		}
        project = null;
        updateGuiFromProject();
        return true;
	}
	
	
	
	
	
	
	/*
	 * 
	 * 
	 * PROJECT LOAD
	 * 
	 * 
	 */
	
	private void projectLoad(File file) {
		if (!file.exists()) return;
		if (!file.isFile()) return;
		projectClose();
		project = Project.load(this, file);//new Project(this);
		//boolean success = project.load(file);
		if (project!=null) {
			preferences.setLastProjectPath(project.getProjectFile());
			//preferences.lastProjectPath = project.getPathToProjectFile();
		} else {
			//project = null;
		}
		//updateGuiFromProject();
	}
	
	
	
	
	
	
	/*
	 * 
	 * 
	 * PROJECT NEW
	 * 
	 * 
	 */
	
	private void projectNew() {
		if (!projectClose()) return;
		ProjectDialogV3 d = new ProjectDialogV3(this,"New project",null);
		if (d.allFieldsValid()) {
			
			if (d.getProjectionType().equals(Projection.Type.STEREOGRAPHIC)) {
				project = new Project(this,
						d.getIndexFile(), d.getModFile(),
						d.getMapWidth(),
						d.getMapHeight(),
						d.getProjectionType(),
						d.getCenterLongitude(),
						d.getCenterLatitude(),
						Float.NaN,
						Float.NaN,
						d.getNorthOrPolarWidth(),
						d.getSouth(),
						d.getPixelScale()
						);
			} else {
				project = new Project(this,
						d.getIndexFile(), d.getModFile(), 
						d.getMapWidth(),
						d.getMapHeight(),
						d.getProjectionType(),
						d.getCenterLongitude(),
						d.getCenterLatitude(),
						d.getWest(),
						d.getEast(),
						d.getNorthOrPolarWidth(),
						d.getSouth(),
						d.getPixelScale());
			}
			
			preferences.setLastIndexFilePath(new File(d.getIndexFile()).getParentFile());
			//preferences.lastProjectIndexFilePath = new File(d.getIndexFile()).getParent();
		} else
			project = null;
		d.dispose();
		d=null;
		updateGuiFromProject();
	}
	
	
	
	
	
	/*
	 * 
	 * 
	 * PROJECT SAVE
	 * 
	 * 
	 */
	
	private void projectSave() {
		
		File projectFile = project.getProjectFile();//getPathToProjectFile();
		if (projectFile==null) projectSaveAs();
		//else if (path.length()==0) projectSaveAs();
		else {
			setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
			project.save();
			statusArea.println("File saved to "+project.getProjectFile().getName());
			preferences.setLastProjectPath(projectFile);
			//preferences.lastProjectPath = project.getPathToProjectFile();
			projectLabel.setText("Project "+projectFile.getName());
			setCursor(Cursor.getDefaultCursor());
		}
	}
	
	
	
	
	
	/*
	 * 
	 * 
	 * PROJECT SAVE AS
	 * 
	 * 
	 */
	
	private void projectSaveAs() {
		JFileChooser chooser = new JFileChooser();
		chooser.setDialogType(JFileChooser.SAVE_DIALOG);
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		chooser.setMultiSelectionEnabled(false);
		chooser.addChoosableFileFilter(new ProjectFileFilter());
		chooser.setAcceptAllFileFilterUsed(false);
		//chooser.setCurrentDirectory(new File(preferences.lastProjectPath).getParentFile());
		chooser.setCurrentDirectory(preferences.getLastProjectPath().getParentFile());
		File projectFile = project.getProjectFile();//new File(project.getPathToProjectFile());
		if (projectFile.exists()) chooser.setSelectedFile(projectFile);
		int returnVal = chooser.showSaveDialog(this);
		if (returnVal==JFileChooser.APPROVE_OPTION) {
			File selectedFile = chooser.getSelectedFile();
			if (!selectedFile.getName().toLowerCase().endsWith(ProjectFileFilter.extension))
				selectedFile = new File(selectedFile.getAbsolutePath()+ProjectFileFilter.extension);
			System.out.println(selectedFile);
			
			boolean saveIt = true;
			if (selectedFile.exists()) {
				int answer = JOptionPane.showConfirmDialog(this, 
						"File exists, overwrite?", "File exists", 
						JOptionPane.YES_NO_OPTION);
				if (answer==JOptionPane.YES_OPTION) {
					saveIt = true;
				} else
					saveIt = false;
			}
			if (saveIt) {
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				project.save(selectedFile);
				statusArea.println("File saved to "+project.getProjectFile().getName());
				//preferences.lastProjectPath = project.getPathToProjectFile();
				preferences.setLastProjectPath(project.getProjectFile());
				//projectLabel.setText("Project "+new File(project.getPathToProjectFile()).getName());
				projectLabel.setText("Project "+project.getProjectFile().getName());
				setCursor(Cursor.getDefaultCursor());
			} else {
				statusArea.println("File not saved");
			}
		}
	}
	
	
	/*
	 * 
	 * Project settings
	 * 
	 */
	private void projectSettings() {
		ProjectDialogV3 d = new ProjectDialogV3(this,"Project settings", project);
		//d.setFieldsFromProject(project);
		if (d.allFieldsValid()) {
				project.modify(d.getMapWidth(), d.getMapHeight(), 
						d.getProjectionType(), 
						d.getCenterLongitude(), d.getCenterLatitude(),
						d.getWest(), d.getEast(), d.getNorthOrPolarWidth(), d.getSouth(),
						d.getPixelScale());
				//project.newMapImage();
				
				//imageFrame.set
				
				imageFrame.removeWindowListener(this);
				imageFrame.dispose();
				//imageFrame=null;
				imageFrame = new ImageFrame(this,
						new String(project.getProjectFile().getName()),
						getLocation().x+getWidth()+10, getLocation().y);
				/*int x = this.getLocation().x + this.getWidth() + 10;
				int y = this.getLocation().y;
				imageFrame.setLocation(x, y);*/
				//imageFrame.addWindowListener(this);
				//imageFrame.updateImage(project.getMapImage());
				
				
		}
		d.dispose();
		d=null;
		/*if (d.ok()) {
			project = new Project(this,
					d.getIndexFile(),
					d.getMapWidth(),
					d.getMapHeight(),
					d.getProjectionType(),
					d.getCenterLongitude(),
					d.getCenterLatitude(),
					d.getPixelScale());
			preferences.LAST_PROJECT_INDEX_FILE_PATH = new File(d.getIndexFile()).getParent();
		} else
			project = null;
		d.dispose();
		d=null;*/
	}
	
	
	
	
	public void propertyChange(PropertyChangeEvent evt) {
		if (evt.getSource().equals(imageTable)) {
			if (evt.getPropertyName().equals("tableCellEditor"))
				// stop editing:
				if (evt.getNewValue()==null)
					renderPreviewImage();
		}
	}
	
	
	
	
		
	/*
	 * 
	 * 
	 * QUIT
	 * 
	 * 
	 */
	
	private void quit() {
		if (projectClose()) {
			Dimension size = imageListPanel.getSize(); //imageListPanel
			System.out.println(size);
			preferences.setImageTableWidth(size.width);
			//preferences.imageTableWidth = String.valueOf(size.width);
			preferences.setImageTableHeight(size.height);
			//preferences.imageTableHeight = String.valueOf(size.height);
			preferences.save();
			System.exit(0);
		}
	}
	
	
	
	private void registerHidableWindow(final JFrame frame) {
		hidableWindows.add(frame);
		JMenuItem mi = new JMenuItem(frame.getTitle());
		windowMenuItems.add(mi);
		windowMenu.add(mi);
		mi.addActionListener(new AbstractAction() {
			private static final long serialVersionUID = 6503212613252879977L;
			public void actionPerformed(ActionEvent arg0) {
				if (!frame.isVisible()) frame.setVisible(true);
				if (frame.getExtendedState()==JFrame.ICONIFIED) 
					frame.setExtendedState(JFrame.NORMAL);
				frame.requestFocus();
			}
		});
		
		frame.addWindowListener(this);
		//windows
	}
	
	
	/**
	 * Renders selected/all objects in the {@link #imageTable}.
	 * <ol>
	 * <li> Sorts the {@link #imageTable} by the column {@link PresetTableModel#COLUMN_LEVEL}
	 * <li> Renders the objects in the new sorting order
	 * <li> Reverts to the previous sorting order
	 * </ol>
	 * @param all (boolean: all or just selected objects)
	 */
	private void renderWithSortKeys(boolean clearMap, boolean all) {
		
		//TODO implement clearMap
		
		if (clearMap) {
			project.clearMapImage();
			project.clearOutlines();
		}
		
		imageFrame.clearUnderMouseOutlines();
		imageFrame.setSelectionVisible(false);
		
		List<? extends SortKey> oldSortKeys = imageTable.getRowSorter().getSortKeys();
		
		//Vector<SortKey> newSortKeys = new Vector<SortKey>();
		//newSortKeys.add(new SortKey(PresetTableModel.COLUMN_LEVEL,SortOrder.ASCENDING));
		imageTable.getRowSorter().setSortKeys(sortKeysUsed);
		
		Vector<PDSObject> obj = getSelectedPDSObjects(all);
		new RenderTaskDialog(this, obj);//, project);
		
		imageTable.getRowSorter().setSortKeys(oldSortKeys);
		imageFrame.setSelectionVisible(true);
	}
	
	
	
	
	
	
	
	
	
	/**
	 * 
	 * 
	 * Renders a preview image
	 * 
	 *
	 */
	private void renderPreviewImage() {
		if (imagePreviewFrame.isVisible() && !(imagePreviewFrame.getState()==JFrame.ICONIFIED)) {
		
			try {
			
			PDSObject modificationObject = getSingleSelectedPDSObject();
			if (modificationObject==null) {
				//System.out.println("none");
				imagePreviewPanel.setBufferedImage(null,null);
				return;
			}
			
			//BufferedImage img = renderImage(modificationObject); //, gainAuto.isSelected()
			String filename = modificationObject.getPDSValue(Element.FILE_NAME.name()).getValue();
			File file = preferences.getFile_ImageArchive(filename);
			PDSObject header = loadCachedHeader(file);
			PDSImage image = loadPDSImage(file,header);
			String filterName = modificationObject.getPDSValue(Element.FILTER_NAME.name()).getValue();
			PDSProcessedImage processedImage = createPDSProcessedImage(image, filterName, modificationObject);
			BufferedImage img = processedImage.createBufferedImageWithProcessorStack();
			//return out;

			//try {
				
				Rectangle filterRectangle = AMIEFilter.getFilterRectangle(filterName);
				imagePreviewPanel.setBufferedImage(img,filterRectangle);
			} catch (PDSValueNotFoundException e) {
				e.printStackTrace();
			}
			
			
			
		
		}
	}
	
	/*public BufferedImage renderImage(PDSObject modificationObject) {
		PDSImage image = loadPDSImage(modificationObject);
		addProcessorStack(image, modificationObject);
		BufferedImage out = image.createBufferedImageWithProcessorStack();
		return out;
	}*/
	
	/*public int[] renderImageToPixelArray(PDSObject modificationObject) {
		PDSImage image = preparePDSImageForRendering(modificationObject);
		int[] out = image.createPixelArrayWithProcessorStack();
		return out;
	}*/
	
	
	
	

	

	/**
	 * @param modificationObject
	 */
	
	public PDSImage loadPDSImage(File file, PDSObject header) { //, boolean autogain
		try {
			//String filename=file.getName();
			PDSImage image = PDSImageLoader.createAndLoad(header, "IMAGE", file);
			return image;
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} 
		return null;
	}
	
	public PDSObject loadCachedHeader(File file) { //, boolean autogain
		try {
			
			String filename = file.getName();
			Object cachedObject = project.getCachedHeaders().get(filename);
			
			PDSObject pdsFile;
			if (cachedObject!=null) {
				pdsFile = (PDSObject)cachedObject;
				//statusArea.println("getting header of "+filename);
			} else {
				pdsFile = PDSFileLoader.loadHeader(file);
				project.getCachedHeaders().put(filename, pdsFile);
				//statusArea.println("storing header of "+filename);
			}
			
			return pdsFile;
			
			} catch (NumberFormatException e1) {
				e1.printStackTrace();
			} 
		return null;
	}
	
	
	public PDSProcessedImage createPDSProcessedImage(PDSImage image, String filterName, PDSObject modificationObject) { //, boolean autogain
		try {
			
			PDSProcessedImage out = new PDSProcessedImage(image);
				
			/*
			 * Add gain to the image processor stack
			 */
			float gain = new Float(modificationObject.getPDSValue(
					DefaultModificationFile.GAIN_VALUE_KEYWORD).getValue());
			PDSImageProcessorGain imageProcessor = new PDSImageProcessorGain(gain);
			out.addPDSImageProcessor(imageProcessor);

			/*
			 * Add topCut and smooth to the image processor stack
			 */
			//String filterName = image.getPDSFile().getPDSValue(PDSDictionary.names[PDSDictionary.FILTER_NAME]).getValue();
			
			//String filterName = modificationObject
			
			int topcut = new Integer(modificationObject.getPDSValue(
					DefaultModificationFile.TOP_CUT_KEYWORD).getValue());
			boolean smoothOn = new Boolean(modificationObject.getPDSValue(
					DefaultModificationFile.EDGE_SMOOTH_KEYWORD).getValue());
			//int SMOOTHPIXELS = 10;
			if (smoothOn)
				out
						.addPDSImageProcessor(new PDSImageProcessorEdgeCutAndSmooth1(
								PDSImageProcessorEdgeCutAndSmooth1.TOP, topcut,
								//PDSImageProcessorEdgeCutAndSmooth.ALL,
								AMIEFilter.getOuterSmoothEdge(filterName),
								SMOOTH_PIXELS));

			else if (topcut > 0)
				out
						.addPDSImageProcessor(new PDSImageProcessorEdgeCutAndSmooth1(
								PDSImageProcessorEdgeCutAndSmooth.TOP, topcut));
			/*
			 * Add trim values to image processor stack
			 */
			out.addPDSImageProcessor(new PDSImageProcessorTrim(0, 255));

			return out;
		} catch (PDSValueNotFoundException e1) {
			e1.printStackTrace();
		} catch (NumberFormatException e1) {
			e1.printStackTrace();
		}
		
		return null;
	}
	
	
	
	/**
	 * Sets the values of all selected rows in the supplied column of the {@link #imageTable} to the supplied data
	 * <ul>
	 * <li><code>column = {@link PresetTableModel#COLUMN_GAIN}</code>: constrains the data to a Double>=0
	 * <li><code>column = {@link PresetTableModel#COLUMN_TOP_CUT}</code>: constrains the data to an Integer>=0
	 * </ul>
	 * @param data
	 * @param column
	 */
	private void changeValuesAbsolute(Object data, int column) {
		int[] selectedRows = imageTable.getSelectedRows();
        for (int i=0;i<selectedRows.length;i++) {
        	if (column==PresetTableModel.COLUMN_GAIN && ((Double)data)<0) data=0.0;
        	if (column==PresetTableModel.COLUMN_TOP_CUT && ((Integer)data)<0) data=0;
        	imageTable.getModel().setValueAt(data,imageTable.convertRowIndexToModel(selectedRows[i]),column);
        }
        renderPreviewImage();
	}

	
	
	
	
	
	/*
	 * Called the TableModel of the modifyTable and the TableModel of the imageTable
	 * (non-Javadoc)
	 * @see javax.swing.event.TableModelListener#tableChanged(javax.swing.event.TableModelEvent)
	 */
	public void tableChanged(TableModelEvent e) {
		/*
		 * Modifying the imageTable from the modifyTable
		 */
		if (e.getSource().equals(modifyTable.getModel())) {
			imageTable.setFocusable(false);
	        int column = e.getColumn();
	        TableModel model = (TableModel)e.getSource();
	        Object data = model.getValueAt(e.getFirstRow(), column);
	        changeValuesAbsolute(data, column);
	        imageTable.setFocusable(true);
		}
	}
	
	
	private int getRowNrOf(String filename) {
		for (int row=0;row<imageTable.getRowCount();row++) {
			String name = (String)imageTable.getValueAt(row, PresetTableModel.COLUMN_FILENAME);
			if (filename.equals(name)) return row;
		}
		throw new RuntimeException("Cannot find "+filename+" in image list");
	}
	
	public void selectRow(String... filename) {
		
		/*
		 * Sort filenames
		 */
		Arrays.sort(filename);
		/*for (int i=0;i<filename.length;i++)
			System.out.println(filename[i]);*/
		
		/*
		 * Sort list
		 */
		List<? extends SortKey> oldSortKeys = imageTable.getRowSorter().getSortKeys();
		ArrayList<SortKey> sort = new ArrayList<SortKey>(1);
		sort.add(new SortKey(PresetTableModel.COLUMN_FILENAME,SortOrder.ASCENDING));
		imageTable.getRowSorter().setSortKeys(sort);
		
		/*
		 * Select
		 */
		ListSelectionModel lsm = imageTable.getSelectionModel();
		//imageTable.setU
		int filenameIndex = 0;
		int lastRow = 0;
		int rows = imageTable.getRowCount();
		//System.out.println(lsm.getValueIsAdjusting());
		
		lsm.setValueIsAdjusting(true);
		for (int row=0;row<rows;row++) {
			String rowValue = (String)imageTable.getValueAt(row, PresetTableModel.COLUMN_FILENAME);
			if (rowValue.equals(filename[filenameIndex])) {
				//System.out.println("selecting "+filename[filenameIndex]);
				if (filenameIndex==0) {
					lsm.setSelectionInterval(row,row);
				}
				else {
					lsm.addSelectionInterval(row, row);
				}
				filenameIndex++;
				if (filenameIndex==filename.length) {
					lastRow = row;
					break;
				}
			}
			//System.out.println(imageTable.getValueAt(row, PresetTableModel.COLUMN_FILENAME));
		}
		imageTable.getRowSorter().setSortKeys(oldSortKeys);
		lsm.setValueIsAdjusting(false);
		//lsm.
		
		//Rectangle rect = imageTable.getCellRect(imageTable.getModel().convertRowIndexToView(lastRow), 0, true);
		//imageTable.scrollRectToVisible(rect);
		/*TableModel model = imageTable.getModel();
		int rows = model.getRowCount();
		for (int row=0;row<rows;row++) {
			System.out.println("row "+row+": "+model.getValueAt(row, PresetTableModel.COLUMN_FILENAME));
		}*/
	}
	
	public void selectRow(String filename) {
		ListSelectionModel lsm = imageTable.getSelectionModel();
		int index = getRowNrOf(filename);
		lsm.setSelectionInterval(index, index);
		Rectangle rect = imageTable.getCellRect(index, 0, true);
		imageTable.scrollRectToVisible(rect);
	}
	
	public void selectAddRow(String filename) {
		ListSelectionModel lsm = imageTable.getSelectionModel();
		int index = getRowNrOf(filename);
		lsm.addSelectionInterval(index, index);
		Rectangle rect = imageTable.getCellRect(index, 0, true);
		imageTable.scrollRectToVisible(rect);
	}
	
	public void selectRemoveRow(String filename) {
		ListSelectionModel lsm = imageTable.getSelectionModel();
		int index = getRowNrOf(filename);
		lsm.removeSelectionInterval(index, index);
		Rectangle rect = imageTable.getCellRect(index, 0, true);
		imageTable.scrollRectToVisible(rect);
	}
	
	/*public void selectRow(String filename, boolean addToSelection) {
		ListSelectionModel lsm = imageTable.getSelectionModel();
		//int index = getRowNrOf(filename);
		
		if (addToSelection) {
			lsm.addSelectionInterval(index, index);
		} else {
			lsm.setSelectionInterval(index, index);
		}
		Rectangle rect = imageTable.getCellRect(index, 0, true);
		imageTable.scrollRectToVisible(rect);
		
	}*/
	
	

	private void updateGuiFromProject() {
		
		
		if (project==null) {
			//imageListPanel.setVisible(false);
			projectLabel.setText("Empty project");
			imageListPanel.setVisible(false);
			//imageTable.setVisible(false);
			//viewSelection.setEnabled(false);
			renderSelectionButtonClear.setEnabled(false);
			renderSelectionButton.setEnabled(false);
			renderSelection.setEnabled(false);
			renderSelectionClear.setEnabled(false);
			renderAllButton.setEnabled(false);
			renderAll.setEnabled(false);
			saveProject.setEnabled(false);
			saveAsProject.setEnabled(false);
			closeProject.setEnabled(false);
			projSettings.setEnabled(false);
			selectAll.setEnabled(false);
			selectNone.setEnabled(false);
			//showFileInfo.setEnabled(false);
			
			if (imageFrame!=null) {
				unregisterHidableWindow(imageFrame);
				//imageFrame.removeWindowListener(this);
				imageFrame.dispose();
				imageFrame=null;
			}
		} else {
			File projectFile = project.getProjectFile();//getPathToProjectFile();
			String msg = "";//projectFile.getName();//new File(projFile).getName();
			if (projectFile==null || !projectFile.exists()) {
				msg = "untitled";
			} else {
				msg = projectFile.getName();
			}
			//if (projFile==null || projFile.length()==0) msg="untitled";
			projectLabel.setText("Project "+msg);
			//imageTable.setVisible(true);
			imageListPanel.setVisible(true);
			//viewSelection.setEnabled(true);
			renderAllButton.setEnabled(true);
			renderAll.setEnabled(true);
			imageTable.getSelectionModel().clearSelection();
			//System.out.println("Implement updateValues !");
			((ImageTableModel)imageTable.getModel()).updateTableFromProject();
			
			saveProject.setEnabled(true);
			saveAsProject.setEnabled(true);
			closeProject.setEnabled(true);
			projSettings.setEnabled(true);
			selectAll.setEnabled(true);
			selectNone.setEnabled(true);
			//pack();
			imageFrame = new ImageFrame(this,
					new String(project.getProjectFile().getName()),
					getLocation().x+getWidth()+10, getLocation().y);
			Point p = getLocationOnScreen();
			p.x += getWidth() + 10;
			imageFrame.setLocation(p);
			registerHidableWindow(imageFrame);
			this.requestFocus();
		}
		
		modifyTableScrollPane.setPreferredSize(new Dimension(imageTableScrollPane.getSize().width,40));
	}

	
	
	private void unregisterHidableWindow(JFrame frame) {
		int index = hidableWindows.indexOf(frame);
		hidableWindows.remove(frame);
		if (index>=0) {
			JMenuItem mi = windowMenuItems.remove(index);
			windowMenu.remove(mi);
		}
		frame.removeWindowListener(this);
	}
	
	
	/*
	 * List selection listener (from imageTable)
	 * (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent e) {
		
		if (e.getValueIsAdjusting()==false) {
			if (imageTable.getSelectedRows().length==0) {
				visibility.setEnabled(false);
				//gain.setEnabled(false);
				gainAbsolute.setEnabled(false);
				gainRelative.setEnabled(false);
				topCut.setEnabled(false);
				edgeSmoothing.setEnabled(false);
				levelSetAbsolute.setEnabled(false);
				levelSetRelative.setEnabled(false);
				levelFront.setEnabled(false);
				levelUp.setEnabled(false);
				levelDown.setEnabled(false);
				levelBack.setEnabled(false);
				modifyTable.clearSelection();
				modifyTable.setVisible(false);
				renderSelectionButtonClear.setEnabled(false);
				renderSelectionButton.setEnabled(false);
				renderSelectionClear.setEnabled(false);
				renderSelection.setEnabled(false);
				viewFileInfoButton.setEnabled(false);
				viewFileInfo.setEnabled(false);
			}
			
			else {
				
				visibility.setEnabled(true);
				//gain.setEnabled(true);
				gainAbsolute.setEnabled(true);
				gainRelative.setEnabled(true);
				topCut.setEnabled(true);
				edgeSmoothing.setEnabled(true);
				levelSetAbsolute.setEnabled(true);
				levelSetRelative.setEnabled(true);
				levelFront.setEnabled(true);
				levelUp.setEnabled(true);
				levelDown.setEnabled(true);
				levelBack.setEnabled(true);
				modifyTable.setVisible(true);
				renderSelectionButtonClear.setEnabled(true);
				renderSelectionButton.setEnabled(true);
				renderSelection.setEnabled(true);
				renderSelectionClear.setEnabled(true);
				viewFileInfoButton.setEnabled(true);
				viewFileInfo.setEnabled(true);
				
				//long mem = Runtime.getRuntime().freeMemory();
				renderPreviewImage();
				//long used = mem - Runtime.getRuntime().freeMemory();
				//System.out.println("memory swallowed: "+used);
				
			}
		} else {
			
			//System.out.println(imageTable.getSelectionModel().getLeadSelectionIndex());
		}
		
	}
	
	
	
	
	

	public void windowActivated(WindowEvent evt) {
	}

	public void windowClosed(WindowEvent evt) {
	}

	public void windowClosing(WindowEvent evt) {
		if (evt.getSource().equals(this)) quit();
	}


	public void windowDeactivated(WindowEvent evt) {
	}

	public void windowDeiconified(WindowEvent evt) {
		if (evt.getSource().equals(imagePreviewFrame)) {
			renderPreviewImage();
		}
	}
	
	public void windowIconified(WindowEvent evt) {
	}





	public void windowOpened(WindowEvent evt) {
	}




	public ImageFrame getImageFrame() {
		return imageFrame;
	}
	


	

	
	
	

}
