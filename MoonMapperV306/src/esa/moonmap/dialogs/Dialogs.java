package esa.moonmap.dialogs;

import java.awt.Frame;

import javax.swing.JOptionPane;

public class Dialogs {

	public static final Double createDoubleDialog(Frame frame, String query, String title) {
		String s = (String)JOptionPane.showInputDialog(
                frame,
                query,
                title,
                JOptionPane.PLAIN_MESSAGE);
		if (s==null) return null;
		try {
			return new Double(s);
		} catch (NumberFormatException e) {
			return null;
		}
	}

	public static Integer createIntegerDialog(Frame frame, String query, String title) {
		String s = (String)JOptionPane.showInputDialog(
                frame,
                query,
                title,
                JOptionPane.PLAIN_MESSAGE);
		if (s==null) return null;
		try {
			return new Integer(s);
		} catch (NumberFormatException e) {
			return null;
		}
	}
	
}
