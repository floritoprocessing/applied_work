package esa.moonmap.dialogs;

import esa.moonmap.Prefs;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class PreferencesDialog extends JDialog implements ActionListener, PropertyChangeListener {

	private static final long serialVersionUID = -2710957879939947164L;

	private JOptionPane optionPane;
	
	private String btnString1 = "Enter";
	private String btnString2 = "Cancel";

	private final JLabel[] label = {
			new JLabel("LUNAR_PHASE_IMAGE_PATH"),
			new JLabel("EXTENDED_MISSION_IMAGE_PATH"),
			new JLabel("LOW_RES_GEOM_PATH")};
	private final JTextField[] field = {
			new JTextField("",30),
			new JTextField("",30),
			new JTextField("",30)
	};
	private File[] file =  { null,null,null };
	private final JButton[] button = {
			new JButton("Browse"),
			new JButton("Browse"),
			new JButton("Browse")
	};
	
	private Prefs preferences;
	
	/** Creates the reusable dialog. */
	public PreferencesDialog(Frame frame, Prefs preferences) {
		super(frame, true);
		setTitle("Preferences");
		
		this.preferences = preferences;

		file[0] = preferences.getLunarPhaseImagePath();
		file[1] = preferences.getExtendedMissionImagePath();
		file[2] = preferences.getLowResGeomPath();
		field[0].setText(file[0].getAbsolutePath());
		field[1].setText(file[1].getAbsolutePath());//extendedMissionImagePath);
		field[2].setText(file[2].getAbsolutePath());//lowResGeomPath);
		
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		for (int i=0;i<3;i++) {
			c.gridx=0;
			c.gridy=i;
			panel.add(label[i],c);
			c.gridx=1;
			panel.add(field[i],c);
			c.gridx=2;
			button[i].addActionListener(this);
			panel.add(button[i],c);
		}
		
		Object[] options = { btnString1, btnString2 };

		optionPane = new JOptionPane(new Object[] {panel}, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.YES_NO_OPTION, null, options, options[0]);

		setContentPane(optionPane);

		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
			}
		});

		optionPane.addPropertyChangeListener(this);
		
		pack();
		setLocationRelativeTo(frame);
        setVisible(true);
	}

	/** This method reacts to state changes in the option pane. */
	public void propertyChange(PropertyChangeEvent e) {
		String prop = e.getPropertyName();

		if (isVisible()
				&& (e.getSource() == optionPane)
				&& (JOptionPane.VALUE_PROPERTY.equals(prop) || JOptionPane.INPUT_VALUE_PROPERTY
						.equals(prop))) {
			Object value = optionPane.getValue();

			if (value == JOptionPane.UNINITIALIZED_VALUE) {
				return;
			}

			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);

			if (btnString1.equals(value)) {
				String[] wrongField = allFieldsAreOk(); 
				if (wrongField.length==0) {
					
					preferences.setLunarPhaseImagePath(file[0]);
					//preferences.lunarPhaseImagePath = field[0].getText();
					preferences.setExtendedMissionImagePath(file[1]);
					//preferences.extendedMissionImagePath = field[1].getText();
					preferences.setLowResGeomPath(file[2]);
					//preferences.lowResGeomPath = field[2].getText();
					
					preferences.save();
					clearAndHide();
				} else {
					String msg1 = wrongField.length==1?"path is invalid:":"paths are invalid:";
					String msg2 = "";
					for (int i=0;i<wrongField.length;i++) {
						msg2 += wrongField[i] + "\n";
					}
					JOptionPane.showMessageDialog(PreferencesDialog.this,
							"Sorry, the following "+msg1+"\n" + msg2,
							"Try again", JOptionPane.ERROR_MESSAGE);
				}
			} else {
				clearAndHide();
			}
		}
	}
	
	private String[] allFieldsAreOk() {
		int wrongCount=0;
		String[] wrongField = new String[3];
		for (int i=0;i<3;i++) {
			if (!new File(field[i].getText()).exists()) {
				wrongField[wrongCount]=label[i].getText();
				wrongCount++;
			}
		}
		String[] out = new String[wrongCount];
		System.arraycopy(wrongField, 0, out, 0, wrongCount);
		return out;
	}

	/** This method clears the dialog and hides it. */
	public void clearAndHide() {
		setVisible(false);
		dispose();
	}

	public void actionPerformed(ActionEvent e) {
		for (int i=0;i<3;i++) {
			if (e.getSource().equals(button[i])) {
				JFileChooser chooser = new JFileChooser();
				chooser.setDialogTitle("Select folder");
				chooser.setDialogType(JFileChooser.OPEN_DIALOG);
				chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				chooser.setCurrentDirectory(new File(field[i].getText()));
				int returnVal = chooser.showOpenDialog(this);
				if (returnVal==JFileChooser.APPROVE_OPTION) {
					file[i] = chooser.getSelectedFile();
					field[i].setText(chooser.getSelectedFile().getAbsolutePath());
				}
			}
		}
		
	}
}
