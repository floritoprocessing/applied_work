package esa.moonmap.dialogs;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import esa.moonmap.MainGui;
import esa.moonmap.project.Project;
import esa.projection.Mercator;
import esa.projection.Projection;
import esa.projection.Rectangular;
import esa.projection.Stereographic;

public class ProjectDialogV3 extends JDialog implements ActionListener, PropertyChangeListener {
	
	
	private static final long serialVersionUID = -522183519601776625L;

	private JOptionPane optionPane;
	
	private final String btnString1;
	private String btnString2 = "Cancel";
	private final Object[] options;
	
	private boolean isDialogForNewProject;

	private JTextField indexFileField = new JTextField(
			"C:\\Documents and Settings\\mgraf\\My Documents\\" +
			"MOON MAP\\S1-L-X-AMIE-3-RDR-CAL-V0.3.1-2 index files\\" +
			"(4) S -30..0 (30 wide) equator\\" +
			"EARTH_ESCAPE_PHASE_and_EXTENDED_MISSION-V1.0_(lon_0_to_30_lat_-30_to_0).csv",30);
	private JButton browseIndexFileButton;
	
	private JTextField modFileField = new JTextField(30);
	private JButton browseModFileButton;
	
	
	private JTextField widthCm, heightCm, dpiField;
	//private JTextField mapHeightField;
	//private JTextField mapWidthField;

	private JTextField longitudeField, latitudeField;
	private String northPole = "North pole";
	private String southPole = "South pole";
	private JComboBox poleComboBox = new JComboBox(new String[] {northPole,southPole});
	//private JTextField eastWestField, northSouthField;
	private final JLabel northLabel = new JLabel("north");
	private final JLabel westLabel = new JLabel("west");
	private final JLabel eastLabel = new JLabel("east");
	private final JLabel southLabel = new JLabel("south");
	private JTextField northOrPolarWidthField, southField, eastField, westField;
	
	private JComboBox projectionComboBox;
	//private JTextField projectionPixelScale;
	private JTextField calcedMapHeight;
	private JTextField calcedMapWidth;
	
	private JPanel centerPanel;
	private JPanel cmPanel;
	private JPanel pixelPanel;
		
	private boolean allFieldsValid = false;
	private MainGui main;
	
	private Projection projection;
	
	public ProjectDialogV3(MainGui main, String title, Project project)  {
		super(main, true);
		isDialogForNewProject = (project==null);
		this.main = main;
		setTitle(title);
		
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent we) {
				optionPane.setValue(new Integer(JOptionPane.CLOSED_OPTION));
			}
		});
		
		JPanel panel = initComponents(isDialogForNewProject);
		
		if (project==null) {
			btnString1 = "Create";
		} else {
			btnString1 = "Modify";
			setFieldsFromProject(project);
		}
		options = new Object[] { btnString1, btnString2 };
		
		
		optionPane = new JOptionPane(new Object[] {panel}, JOptionPane.PLAIN_MESSAGE,
				JOptionPane.YES_NO_OPTION, null, options, options[0]);
		setContentPane(optionPane);
		optionPane.addPropertyChangeListener(this);
		
		cmKeyTyped();
		updateProjection();
		
		updateGuiFromProjection();
		
		//pack();
		setLocationRelativeTo(main);
        setVisible(true);
	}

	public boolean allFieldsValid() {
		return allFieldsValid;
	}
	
	private void updateProjection() {
		projection = null;
		Projection.Type type = (Projection.Type)projectionComboBox.getSelectedItem();
		try {
			if (type==Projection.Type.STEREOGRAPHIC) {
				float centerLon = 0;
				float centerLat = poleComboBox.getSelectedItem().equals(northPole)?90:-90;
				projection = new Stereographic(centerLon,centerLat);
				((Stereographic)projection).setScale(
						getPixelWidthFromFields(), 
						Float.parseFloat(northOrPolarWidthField.getText()));
			} else {
				float centerLon = Float.parseFloat(longitudeField.getText());
				float centerLat = Float.parseFloat(latitudeField.getText());
				float pixelScale = 1;// Float.parseFloat(projectionPixelScale.getText());
				projection = Projection.getInstance((Projection.Type)projectionComboBox.getSelectedItem(), centerLon, centerLat, pixelScale);
			} 
		} catch (NumberFormatException e) {}
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(browseIndexFileButton) 
				|| e.getSource().equals(browseModFileButton)) {
			JFileChooser chooser = new JFileChooser();
			chooser.setDialogType(JFileChooser.OPEN_DIALOG);
			chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			chooser.setMultiSelectionEnabled(false);
			//chooser.setCurrentDirectory(new File(main.getPreferences().lastProjectIndexFilePath));
			chooser.setCurrentDirectory(main.getPreferences().getLastIndexFilePath());//lastProjectIndexFilePath));
			int returnVal = chooser.showOpenDialog(this);
			if (returnVal==JFileChooser.APPROVE_OPTION) {
				if (e.getSource().equals(browseIndexFileButton))
					indexFileField.setText(chooser.getSelectedFile().getAbsolutePath());
				else
					modFileField.setText(chooser.getSelectedFile().getAbsolutePath());
			}
		}
		
		if (e.getSource().equals(projectionComboBox)) {
			updateGuiFromProjection();
			calculateProjectedImageSize();
		}
		
		else if (e.getSource().equals(poleComboBox)) {
			calculateProjectedImageSize();
		}
		
		/*else if (e.getSource().equals(projectionPixelScaleCalcButton)) {
			calcScale();
		}*/
		
	}
	
	private void updateGuiFromProjection() {
		if (getProjectionType().equals(Projection.Type.STEREOGRAPHIC)) {
			northLabel.setText("latitude offset");
			southLabel.setText("pole");
			eastLabel.setVisible(false);
			westLabel.setVisible(false);
			southField.setEnabled(false);
			eastField.setVisible(false);
			westField.setVisible(false);
			longitudeField.setVisible(false);
			latitudeField.setVisible(false);
			poleComboBox.setVisible(true);
		} else {
			northLabel.setText("north");
			southLabel.setText("south");
			eastLabel.setVisible(true);
			westLabel.setVisible(true);
			southField.setEnabled(true);
			eastField.setVisible(true);
			westField.setVisible(true);
			longitudeField.setVisible(true);
			latitudeField.setVisible(true);
			poleComboBox.setVisible(false);
		}
		
		pack();
	}
	
	/*private void calcScale() {
		int width = getMapWidth();
		int height = getMapHeight();
		float cenLon = getCenterLongitude();
		float cenLat = getCenterLatitude();
		Class projectionClass = null;
		try {
			System.out.println(getProjectionType());
			projectionClass = Projection.getProjectionClass(getProjectionType());
		} catch (ProjectionNotFoundError e) {}
		if (projectionClass==null) return;
		
		if (projectionClass.equals(Rectangular.class)) {
			System.out.println("Calculating for rectangular");
		}
		
		if (projectionClass.equals(Mercator.class)) {
			System.out.println("Calculating for mercator");
		}
	}*/

	public void propertyChange(PropertyChangeEvent e) {
		//String prop = e.getPropertyName();
		if (isVisible() && e.getSource().equals(optionPane)) {
			Object value = optionPane.getValue();
			if (value == JOptionPane.UNINITIALIZED_VALUE) return;
			optionPane.setValue(JOptionPane.UNINITIALIZED_VALUE);
			if (value.equals(btnString1)) {
				allFieldsValid = evaluateTextFields();
				if (allFieldsValid) setVisible(false);
			} else {
				setVisible(false);
			}
		}
		
	}
	
	private class Evaluator {
		private String name;
		
		private float number;
		private String message;
		private String newLine = System.getProperty("line.separator");
		
		Evaluator(JTextField field, String name) {
			this(field,name,Float.NEGATIVE_INFINITY,Float.POSITIVE_INFINITY);
		}
		
		Evaluator(JTextField field, String name, float min, float max) {
			this.name = name;
			message="";
			number = Float.NaN;
			try {
				number = Float.parseFloat(field.getText());
				if (!Float.isNaN(number)) {
					if (number<min||number>max) {
						number = Float.NaN;
						message = "* "+name+" not in range (";
						message += min+".."+max;
						message += ")"+newLine;
					}
				}
			} catch (NumberFormatException e) {
				number = Float.NaN;
				message = "* "+name+" is not a number"+newLine;
			}
		}
		
		
		public String getMessage() {
			return message;
		}
		public float getNumber() {
			return number;
		}
	}
	
	private boolean evaluateTextFields() {
		String msg = "";
		Evaluator eval;
		String newLine = System.getProperty("line.separator");
		//boolean allOk = false;
		
		//TODO EVALUATE ALL!
		
		if (isDialogForNewProject) {
			File file = new File(indexFileField.getText());
			if (!file.exists() || file.isDirectory()) {
				msg += "* Index file (does not exist)"+newLine;
			}
			
			if (modFileField.getText().length()>0) {
				file = new File(modFileField.getText());
				if (!file.exists() || file.isDirectory()) {
					msg+= "* Mod file (does not exist)"+newLine;
				}
			}
			
		}
		
		if (projectionComboBox.getSelectedItem().equals(Projection.Type.STEREOGRAPHIC)) {
			
			/*
			 * north
			 */
			
			eval = new Evaluator(northOrPolarWidthField,"North",-90,90);
			float north = eval.getNumber();
			msg += eval.getMessage();
			
		} else {
		
			/*
			 * width
			 */
			
			eval = new Evaluator(widthCm,"Width",1,Float.POSITIVE_INFINITY);
			float widthCm = eval.getNumber();
			msg += eval.getMessage();
					
			/*
			 * dpi
			 */
			
			eval = new Evaluator(dpiField,"Dpi",1,Float.POSITIVE_INFINITY);
			float dpi = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * center lat
			 */
			
			eval = new Evaluator(latitudeField,"Latitude",-90,90);
			float latCenter = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * center lon
			 */
			
			eval = new Evaluator(longitudeField,"Longitude",0,360);
			float lonCenter = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * north
			 */
			
			eval = new Evaluator(northOrPolarWidthField,"North",-90,90);
			float north = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * south
			 */
			
			eval = new Evaluator(southField,"South",-90,90);
			float south = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * north-south
			 */
			
			if (!Float.isNaN(north)&&!Float.isNaN(south)) {
				msg += (north<=south)?("* North/South combination not valid"+newLine):"";
			}
			
			/*
			 * east
			 */
			
			eval = new Evaluator(eastField,"East",0,360);
			float east = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * west
			 */
			
			eval = new Evaluator(westField,"West",0,360);
			float west = eval.getNumber();
			msg += eval.getMessage();
			
			/*
			 * east-west
			 */
			
			if (!Float.isNaN(east)&&!Float.isNaN(west)) {
				msg += (east<=west)?("* West/East combination not valid"+newLine):"";
			}
		
		}
		
		/*
		 * projection
		 */
			
		if (msg.length()==0) {
			calculateProjectedImageSize();
			return true;
		}
		else {
			JOptionPane.showMessageDialog(this, msg, "Illegal field(s)", JOptionPane.ERROR_MESSAGE);
			return false;
		}
	}


	public String getIndexFile() {
		return indexFileField.getText();
	}
	
	public String getModFile() {
		return modFileField.getText();
	}

	/*public int getMapWidth() {
		return new Integer(mapWidthField.getText());
	}

	public int getMapHeight() {
		return new Integer(mapHeightField.getText());
	}*/

	public Projection.Type getProjectionType() {
		return (Projection.Type)projectionComboBox.getSelectedItem();
	}

	public float getCenterLongitude() {
		return projection.getCenterLongitude();
		//return new Float(longitudeField.getText());
	}
	
	public float getEast() {
		return new Float(eastField.getText());
	}
	
	public float getWest() {
		return new Float(westField.getText());
	}
	
	public float getSouth() {
		return new Float(southField.getText());
	}
	
	public float getNorthOrPolarWidth() {
		return new Float(northOrPolarWidthField.getText());
	}

	public float getCenterLatitude() {
		return projection.getCenterLatitude();
		//return new Float(latitudeField.getText());
	}
	
	/*public float getEastWest() {
		return new Float(eastWestField.getText());
	}
	
	public float getNorthSouth() {
		return new Float(northSouthField.getText());
	}*/

	public float getPixelScale() {
		return projection.getScale();
		//return new Float(projectionPixelScale.getText());
	}

	public int getMapWidth() {
		return Integer.parseInt(calcedMapWidth.getText());
	}
	
	public int getMapHeight() {
		return Integer.parseInt(calcedMapHeight.getText());
	}
	
	/**
	 * This method is called from within the constructor to initialize the form.
	 * WARNING: Do NOT modify this code. The content of this method is always
	 * regenerated by the Form Editor.
	 */
	@SuppressWarnings("unchecked")
	// <editor-fold defaultstate="collapsed" desc="Generated Code">
	private JPanel initComponents(boolean newProject) {
		JPanel totalPanel = new JPanel(new BorderLayout());
		
		java.awt.GridBagConstraints gridBagConstraints;
	
		
		
		browseIndexFileButton = new JButton();
		browseModFileButton = new JButton();
		centerPanel = new JPanel();
		JPanel physicalSizePanel = new JPanel();
		cmPanel = new JPanel();
		JLabel lblWidth = new JLabel();
		widthCm = new JTextField();
		heightCm = new JTextField();
		dpiField = new JTextField();
		pixelPanel = new JPanel();
		//mapWidthField = new JTextField();
		//mapHeightField = new JTextField();
		JPanel geoPanel = new JPanel();
		latitudeField = new JTextField();
		longitudeField = new JTextField();
		northOrPolarWidthField = new JTextField();
		southField = new JTextField();
		eastField = new JTextField();
		westField = new JTextField();
		//eastWestField = new JTextField();
		//northSouthField = new JTextField();
		JPanel projectionPanel = new JPanel();
		JPanel projTopPanel = new JPanel();
		projectionComboBox = new JComboBox(Projection.Type.values());
		projectionComboBox.addActionListener(this);
		projectionComboBox.setFont(new Font("Dialog",Font.PLAIN,12));
		//projectionPixelScale = new JTextField();
		JPanel projBottomPanel = new JPanel();
		calcedMapWidth = new JTextField();
		calcedMapHeight = new JTextField();
	
		//setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
		//setAlwaysOnTop(true);
		//setModal(true);
		//setResizable(false);
	
		if (newProject) {
			
			JPanel filePanel = new JPanel();
			filePanel.setLayout(new BoxLayout(filePanel,BoxLayout.Y_AXIS));
			
			JPanel indexFilePanel = new JPanel();
			indexFileField = new JTextField();
			indexFilePanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Index file"));//,
			indexFilePanel.setPreferredSize(new Dimension(50, 50));
			indexFilePanel.setLayout(new BorderLayout());
		
			indexFileField.setToolTipText("the index file");
			indexFilePanel.add(indexFileField, java.awt.BorderLayout.CENTER);
		
			browseIndexFileButton.setText("Browse");
			browseIndexFileButton.addActionListener(this);
			browseIndexFileButton.setToolTipText("browse for an index file");
			indexFilePanel.add(browseIndexFileButton, java.awt.BorderLayout.EAST);
			filePanel.add(indexFilePanel);
			
			JPanel modFilePanel = new JPanel();
			modFileField = new JTextField();
			modFilePanel.setBorder(BorderFactory.createTitledBorder("(Optional) mod file"));
			modFilePanel.setPreferredSize(new Dimension(50,50));
			modFilePanel.setLayout(new BorderLayout());
			
			modFileField.setToolTipText("the (optional) mod file");
			modFilePanel.add(modFileField, BorderLayout.CENTER);
			
			browseModFileButton.setText("Browse");
			browseModFileButton.addActionListener(this);
			browseModFileButton.setToolTipText("browse for a mod file (optional)");
			modFilePanel.add(browseModFileButton, BorderLayout.EAST);
			filePanel.add(modFilePanel);
			
			totalPanel.add(filePanel, java.awt.BorderLayout.NORTH);
		}
	
		centerPanel.setLayout(new javax.swing.BoxLayout(centerPanel,
				javax.swing.BoxLayout.PAGE_AXIS));
	
		physicalSizePanel.setBorder(javax.swing.BorderFactory
				.createTitledBorder("size"));
		physicalSizePanel.setLayout(new java.awt.FlowLayout(
				java.awt.FlowLayout.CENTER, 20, 5));
	
		cmPanel.setLayout(new java.awt.GridLayout(3, 3, 5, 5));
	
		lblWidth.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		lblWidth.setText("width");
		lblWidth.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		cmPanel.add(lblWidth);
	
		widthCm.setColumns(5);
		widthCm.setText("25");
		widthCm.setToolTipText("width of map in cm");
		widthCm.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				cmKeyTyped();
				calculateProjectedImageSize();
			}
		});
		cmPanel.add(widthCm);
	
		cmPanel.add(new JLabel("cm"));
	
		JLabel jLabel3 = new JLabel();
		jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel3.setText("height");
		jLabel3.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		cmPanel.add(jLabel3);
	
		heightCm.setColumns(5);
		//heightCm.setText("25");
		heightCm.setToolTipText("height of map in cm");
		heightCm.setEditable(false);
		heightCm.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				cmKeyTyped();
				calculateProjectedImageSize();
			}
		});
		cmPanel.add(heightCm);
	
		cmPanel.add(new JLabel("cm"));

		JLabel jLabel5 = new JLabel();
		jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel5.setText("@");
		jLabel5.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
		cmPanel.add(jLabel5);
	
		dpiField.setColumns(5);
		dpiField.setText("300");
		dpiField.setToolTipText("dots per inch");
		dpiField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				cmKeyTyped();
				calculateProjectedImageSize();
			}
		});
		cmPanel.add(dpiField);
	
		cmPanel.add(new JLabel("dpi"));
	
		physicalSizePanel.add(cmPanel);
	
		//physicalSizePanel.add(new JLabel("or"));
	
		pixelPanel.setLayout(new java.awt.GridLayout(2, 2, 5, 5));
	
		/*mapWidthField.setColumns(5);
		mapWidthField.setText("3000");
		mapWidthField.setToolTipText("width of map in pixel");
		mapWidthField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				pixelKeyTyped();
				calculateProjectedImageSize();
			}
		});
		pixelPanel.add(mapWidthField);*/
	
		//pixelPanel.add(new JLabel("pixel"));
	
		/*mapHeightField.setColumns(5);
		mapHeightField.setText("3000");
		mapHeightField.setToolTipText("height of map in pixel");
		mapHeightField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				pixelKeyTyped();
				calculateProjectedImageSize();
			}
		});
		pixelPanel.add(mapHeightField);*/
	
		//pixelPanel.add(new JLabel("pixel"));
	
		physicalSizePanel.add(pixelPanel);
	
		centerPanel.add(physicalSizePanel);
	
		geoPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Geometry"));
		geoPanel.setLayout(new java.awt.GridBagLayout());
	
		JLabel jLabel10 = new JLabel();
		jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel10.setText("center");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(jLabel10, gridBagConstraints);
	
		latitudeField.setColumns(5);
		latitudeField.setText("15");
		latitudeField.setToolTipText("map center longitude");
		latitudeField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 0;
		geoPanel.add(latitudeField, gridBagConstraints);
	
		JLabel jLabel11 = new JLabel();
		jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel11.setText("/");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 0;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(jLabel11, gridBagConstraints);
	
		longitudeField.setColumns(5);
		longitudeField.setText("15");
		longitudeField.setToolTipText("map center latitude");
		longitudeField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 0;
		geoPanel.add(longitudeField, gridBagConstraints);
	
		gridBagConstraints = new GridBagConstraints();
		gridBagConstraints.gridx = 4;
		gridBagConstraints.insets = new Insets(5,5,5,5);
		gridBagConstraints.gridy = 0;
		poleComboBox.addActionListener(this);
		geoPanel.add(poleComboBox, gridBagConstraints);
		
		/*JLabel jLabel12 = new JLabel();
		jLabel12.setText("degrees");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 4;
		gridBagConstraints.gridy = 1;
		//gridBagConstraints.gridheight = 2;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(jLabel12, gridBagConstraints);*/
	
		/*JLabel jLabel14 = new JLabel();
		jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		jLabel14.setText("east<->west / north<->south");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 0;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.anchor = java.awt.GridBagConstraints.EAST;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(jLabel14, gridBagConstraints);*/
	
		/*eastWestField.setColumns(5);
		eastWestField.setText("30");
		eastWestField.setToolTipText("map east-west");
		eastWestField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 1;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(eastWestField, gridBagConstraints);*/
	
		/*JLabel jLabel17 = new JLabel();
		jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
		jLabel17.setText("/");
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 2;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(jLabel17, gridBagConstraints);*/
	
		/*northSouthField.setColumns(5);
		northSouthField.setText("30");
		northSouthField.setToolTipText("map north - south");
		northSouthField.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = 3;
		gridBagConstraints.gridy = 1;
		gridBagConstraints.ipadx = 5;
		gridBagConstraints.insets = new java.awt.Insets(5, 5, 5, 5);
		geoPanel.add(northSouthField, gridBagConstraints);*/
	
		centerPanel.add(geoPanel);
		
		
		/*
		 * north south east west
		 */
		JPanel geoPanel2 = new JPanel();
		geoPanel2.setBorder(BorderFactory.createTitledBorder("Geometry2"));
		geoPanel2.setLayout(new FlowLayout());
		geoPanel2.add(northLabel);
		northOrPolarWidthField.setColumns(5);
		northOrPolarWidthField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		geoPanel2.add(northOrPolarWidthField);
		
		geoPanel2.add(southLabel);
		southField.setColumns(5);
		southField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		geoPanel2.add(southField);
		geoPanel2.add(eastLabel);
		eastField.setColumns(5);
		eastField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		geoPanel2.add(eastField);
		geoPanel2.add(westLabel);
		westField.setColumns(5);
		westField.addKeyListener(new KeyAdapter() {
			public void keyReleased(KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		geoPanel2.add(westField);
		centerPanel.add(geoPanel2);
		
		
	
		projectionPanel.setBorder(javax.swing.BorderFactory.createTitledBorder("Projection"));
		projectionPanel.setLayout(new javax.swing.BoxLayout(projectionPanel,
				javax.swing.BoxLayout.PAGE_AXIS));
	
		projTopPanel.add(new JLabel("With projection type"));
	
		//projectionType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
		projTopPanel.add(projectionComboBox);
	
		/*projTopPanel.add(new JLabel("and a map scale of"));
		
		projectionPixelScale.setColumns(10);
		projectionPixelScale.setText("100");
		projectionPixelScale.addKeyListener(new java.awt.event.KeyAdapter() {
			public void keyReleased(java.awt.event.KeyEvent evt) {
				calculateProjectedImageSize();
			}
		});
		projTopPanel.add(projectionPixelScale);*/
	
		projectionPanel.add(projTopPanel);
	
		projBottomPanel.add(new JLabel("the resulting map size will be"));
	
		calcedMapWidth.setColumns(8);
		calcedMapWidth.setEditable(false);
		projBottomPanel.add(calcedMapWidth);
	
		projBottomPanel.add(new JLabel("by"));
	
		calcedMapHeight.setColumns(8);
		calcedMapHeight.setEditable(false);
		projBottomPanel.add(calcedMapHeight);
	
		projBottomPanel.add(new JLabel("pixels in size"));
	
		projectionPanel.add(projBottomPanel);
	
		centerPanel.add(projectionPanel);
	
		//getContentPane()
		totalPanel.add(centerPanel, java.awt.BorderLayout.CENTER);
	
		/*okButton.setText("Ok");
		southPanel.add(okButton);
	
		cancelButton.setText("Cancel");
		southPanel.add(cancelButton);
	
		getContentPane().add(southPanel, java.awt.BorderLayout.SOUTH);*/
	
		//pack();
		
		
		
		for (Component c:getAllComponentsIn(totalPanel, JLabel.class, JTextField.class)) 
			c.setFont(new Font("Dialog",Font.PLAIN,12));
		
		
		return totalPanel;
	}// </editor-fold>
	
	
	private ArrayList<Component> getAllComponentsIn(Container container, Class... componentClasses) {
		class Recurse {
			private ArrayList<Component> list = new ArrayList<Component>();
			private Class[] componentClasses;
			Recurse(Container container, Class... componentClasses) {
				this.componentClasses = componentClasses;
				getComponents(container);
			}
			private void getComponents(Container container) {
				for (Component component:container.getComponents()) {
					if (componentClasses.length==0)
						list.add(component);
					else
						for (Class c:componentClasses) {
							if (component.getClass().equals(c)) {
								list.add(component);
							}
						}
					getComponents((Container)component);
				}
			}
			public ArrayList<Component> getList() {
				return list;
			}
		}
		return new Recurse(container,componentClasses).getList();
	}
	
	
	

	private void cmKeyTyped() {
		try {
			//float wid = Float.parseFloat(widthCm.getText());
			//float hei = Float.parseFloat(heightCm.getText());
			//float dpi = Float.parseFloat(dpiField.getText());
			calculateProjectedImageSize();
			//mapWidthField.setText(String.valueOf((int) (wid / 2.54f * dpi)));
			//mapHeightField.setText(String.valueOf((int) (hei / 2.54f * dpi)));
		} catch (NumberFormatException ignore) {
			//mapWidthField.setText("---");
			//mapHeightField.setText("---");
		}
	}

	/*private void pixelKeyTyped() {
		try {
			float wid = Float.parseFloat(mapWidthField.getText());
			float hei = Float.parseFloat(mapHeightField.getText());
			float dpi = Float.parseFloat(dpiField.getText());
			DecimalFormat df = new DecimalFormat("0.000");
			widthCm.setText(df.format(wid / dpi * 2.54f));
			heightCm.setText(df.format(hei / dpi * 2.54f));
		} catch (NumberFormatException ignore) {
			widthCm.setText("---");
			heightCm.setText("---");
		}
	}*/

	
	private float getPixelWidthFromFields() {
		float widCm = Float.parseFloat(widthCm.getText());
		float dpi = Float.parseFloat(dpiField.getText());
		return widCm/2.54f*dpi;
	}
	
	private void calculateProjectedImageSize() {
		// TODO add your handling code here:
		
		
		
		updateProjection();
		
		if (projection==null) return;
		
		//System.out.println(projection);
		
		// do somin wit da projection
		
		if (projection instanceof Rectangular || projection instanceof Mercator) {
		
			try {
				
				float west = getWest();
				float east = getEast();
				if (west>east) throw new NumberFormatException("west has to be smaller than east");
				
				float north = getNorthOrPolarWidth();
				float south = getSouth();
				if (south>north) throw new NumberFormatException("south has to be smaller than north");

				/*
				 * This happens in Mercator / Rectangular projection:
				 * x = (longitude - lonCenter)*scale;
				 * x is a point in relation to the map center
				 * 
				 * so,
				 * scale = x0/(west - lonCenter)
				 * and 
				 * x0 = -width/2
				 * 
				 * 
				 * is
				 * scale = (-width/2)/(west-lonCenter)
				 * or
				 * scale = (width/2)/(east-lonCenter)
				 */
				
				
				int width = (int)getPixelWidthFromFields();
				
				
				/*
				 * Calc map scale:
				 */
				float lonCenter = projection.getCenterLongitude();
				float mapScale = (width/2)/(east-lonCenter);
				projection.setScale(mapScale);
				calcedMapWidth.setText(String.valueOf(width));
				
				
				/*
				 * Calc map height:
				 */
				
				float maxY = projection.lonLatToXY(projection.getCenterLongitude(), south)[1];
				float minY = projection.lonLatToXY(projection.getCenterLongitude(), north)[1];
				float height = maxY-minY;
				calcedMapHeight.setText(String.valueOf((int)height));
				DecimalFormat df = new DecimalFormat("0.00");
				float dpi = Float.parseFloat(dpiField.getText());
				heightCm.setText(df.format((maxY-minY)/dpi*2.54));
				//float heiCm = Float.parseFloat(heightCm.getText());
				
				
				//System.out.println(mapScale);
				//calcedMapWidth.setText("---");
				//calcedMapHeight.setText("---");
				
			} 
			
			
			/*float wid = Float.parseFloat(eastWestField.getText());
			float hei = Float.parseFloat(northSouthField.getText());
			float lon0 = projection.getCenterLongitude() - wid/2;
			float lon1 = projection.getCenterLongitude() + wid/2;
			float lat0 = projection.getCenterLatitude() - hei/2;
			float lat1 = projection.getCenterLatitude() + hei/2;
			float[][] ll = new float[4][2];
			float[][] cp = new float[4][2];
			ll[0] = new float[] { lon0, lat0 };
			ll[1] = new float[] { lon1, lat0 };
			ll[2] = new float[] { lon1, lat1 };
			ll[3] = new float[] { lon0, lat1 };
			cp[0] = projection.lonLatToXY(lon0, lat0);
			cp[1] = projection.lonLatToXY(lon1, lat0);
			cp[2] = projection.lonLatToXY(lon1, lat1);
			cp[3] = projection.lonLatToXY(lon0, lat1);
			//System.out.println("Four corner points: ");
			for (int i=0;i<4;i++) {
				System.out.print("lonLat: "+ll[i][0]+"/"+ll[i][1]);
				System.out.println("  \t->\t"+cp[i][0]+"/"+cp[i][1]);
			}
			
			float minX = Math.min(Math.min(cp[0][0],cp[1][0]),Math.min(cp[2][0],cp[3][0]));
			float maxX = Math.max(Math.max(cp[0][0],cp[1][0]),Math.max(cp[2][0],cp[3][0]));
			
			float minY = Math.min(Math.min(cp[0][1],cp[1][1]),Math.min(cp[2][1],cp[3][1]));
			float maxY = Math.max(Math.max(cp[0][1],cp[1][1]),Math.max(cp[2][1],cp[3][1]));*/
			
			//calcedMapWidth.setText(String.valueOf(maxX-minX));
			//calcedMapHeight.setText(String.valueOf(maxY-minY));
		  catch (NumberFormatException e) {
			calcedMapWidth.setText("---");
			calcedMapHeight.setText("---");
		  }
		  	
		} else {
			//System.out.println("Projection "+projection+" pixel scale calculation not yet implemented!");
			
			
			
			southField.setText(poleComboBox.getSelectedItem().equals(northPole)?"90":"-90");
			
			try {
				float latRange = Float.parseFloat(northOrPolarWidthField.getText());
				float y = projection.lonLatToXY(0, latRange)[1];
				//float x = projection.lonLatToXY((float)(Math.PI), latRange)[1];
				//System.out.println(-y*2+"\t"+x*2);
				calcedMapWidth.setText(String.valueOf((int)(-y*2)));
				calcedMapHeight.setText(String.valueOf((int)(-y*2)));
			} catch (NumberFormatException e) {
				calcedMapWidth.setText("---");
				calcedMapHeight.setText("---");
			}
			
		}
		
		
		//projection.lonLatToXY(longitude, latitude)
	
		
	}

	public void setFieldsFromProject(Project project) {
		//System.out.println(mapWidthField.getText());
		//indexFileField.setText(project.INDEX_FILE);
		//mapWidthField.setText(project.getMapWidth());
		//mapHeightField.setText(project.getMapHeight());
		dpiField.setText("300");
		//pixelKeyTyped();
		longitudeField.setText(project.getProjectionLongitude());
		latitudeField.setText(project.getProjectionLatitude());
		/*eastWestField.setText(project.getProjectionEastWest());
		northSouthField.setText(project.getProjectionNorthSouth());
		projectionPixelScale.setText(project.getProjectionPixelScale());*/
		eastField.setText(project.getMapEast());
		westField.setText(project.getMapWest());
		northOrPolarWidthField.setText(project.getMapNorth());
		southField.setText(project.getMapSouth());
		//System.out.println("Set selected item: "+project.PROJECTION_TYPE);
		
		for (int i=0;i<projectionComboBox.getItemCount();i++) {
			if (((Projection.Type)projectionComboBox.getItemAt(i)).getName().equals(project.getProjectionType()))
			projectionComboBox.setSelectedIndex(i);
		}
		
		calculateProjectedImageSize();
	}

	
	
}
