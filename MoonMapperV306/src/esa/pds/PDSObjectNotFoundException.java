package esa.pds;

public class PDSObjectNotFoundException extends Exception {

	private static final long serialVersionUID = 8201171733344166157L;

	public PDSObjectNotFoundException(String msg) {
		super(msg);
	}
	
}
