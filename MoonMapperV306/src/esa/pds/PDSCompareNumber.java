package esa.pds;

public class PDSCompareNumber implements PDSCompare {

	private String variableName;
	private double valueMin, valueMax;
	
	public PDSCompareNumber(String variableName, double valueMin, double valueMax) {
		this.variableName = variableName;
		if (valueMin>valueMax) {
			double temp=valueMin;
			valueMin=valueMax;
			valueMax=temp;
		}
		this.valueMin = valueMin;
		this.valueMax = valueMax;
	}
	
	public boolean compare(Object valueToCompare) {
		try {
			double doubleToCompare = (new Double((String)valueToCompare)).doubleValue();
			return (valueMin<=doubleToCompare && doubleToCompare<=valueMax);
		} catch (NumberFormatException e) {
			// for the case of N/A settingsFile
			return false;
		}
		
	}

	public String getVariableName() {
		return variableName;
	}

	public boolean isBooleanCompare() {
		return false;
	}

	public String[] toStrings() {
		return new String[] {"new PDSCompareNumber(\""+variableName+"\","+valueMin+","+valueMax+")"};
	}
	
	public String toString() {
		return "\""+variableName+"\" FROM "+valueMin+" TO "+valueMax;
	}

}
