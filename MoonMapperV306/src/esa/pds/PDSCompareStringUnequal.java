package esa.pds;

public class PDSCompareStringUnequal implements PDSCompare {
	
	private String variableName;
	private String value;
	
	public PDSCompareStringUnequal(String variableName, String value) {
		this.variableName = variableName;
		this.value = value;
	}

	public String getVariableName() {
		return variableName;
	}
	
	public boolean isBooleanCompare() {
		return false;
	}

	public boolean compare(Object valueToCompare) {
		return !value.equals((String)valueToCompare);
	}

	public String[] toStrings() {
		return new String[] {"new PDSCompareStringUnequal(\""+variableName+"\",\""+value+"\")"};
	}

	public String toString() {
		return "\""+variableName+"\" NOT EQUAL \""+value+"\"";
	}


	
}
