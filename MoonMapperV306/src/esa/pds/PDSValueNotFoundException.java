package esa.pds;

public class PDSValueNotFoundException extends Exception {

	private static final long serialVersionUID = 7538316726543937372L;

	public PDSValueNotFoundException(String msg) {
		super(msg);
	}
	
}
