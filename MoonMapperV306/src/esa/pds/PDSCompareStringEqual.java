package esa.pds;

public class PDSCompareStringEqual implements PDSCompare {
	
	private String variableName;
	private String value;
	
	public PDSCompareStringEqual(String variableName, String value) {
		this.variableName = variableName;
		this.value = value;
	}

	public String getVariableName() {
		return variableName;
	}
	
	public boolean isBooleanCompare() {
		return false;
	}

	public boolean compare(Object valueToCompare) {
		return value.equals((String)valueToCompare);
	}

	public String[] toStrings() {
		return new String[] {"new PDSCompareStringEqual(\""+variableName+"\",\""+value+"\")"};
	}

	public String toString() {
		return "\""+variableName+"\" EQUALS \""+value+"\"";
	}


	
}
