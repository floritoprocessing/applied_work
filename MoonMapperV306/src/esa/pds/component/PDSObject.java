package esa.pds.component;

import esa.pds.PDSObjectNotFoundException;
import esa.pds.PDSUnStringableException;
import esa.pds.PDSValueNotFoundException;
import esa.pds.dictionary.Element;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

public class PDSObject extends PDSComponent implements Cloneable {

	private Element element = Element.EMPTY;
	protected ArrayList<PDSComponent> pdsComponent = new ArrayList<PDSComponent>(10);

	public PDSObject() {
	}

	public PDSObject(String name) {
		//this.name = name;
		element = Element.valueOf(name);
	}

	public PDSObject(PDSValue[] input) {
		addNewValues(input);
	}

	public void addNewComment(String commentString) {
		pdsComponent.add(new PDSComment(commentString));
	}

	public void addNewValue(PDSValue input) {
		if (input!=null) {
			pdsComponent.add((PDSValue)input.clone());
		}
	}


	public void addNewValues(PDSValue[] input) {
		for (int i=0;i<input.length;i++)
			if (input[i]!=null) pdsComponent.add((PDSValue)input[i].clone());
	}

	public String toCSVStrings(boolean isHeader) throws PDSUnStringableException {
		String out = "";
		for (int i=0;i<pdsComponent.size();i++) {
			if (!pdsComponent.get(i).isPDSValue()) throw new PDSUnStringableException("Cannot CSV String PDSObjects with PDSObjects. Only PDSValues permitted!");
			if (isHeader) {
				out += pdsComponent.get(i).getName() + (i<pdsComponent.size()-1?",":"");
			} else {
				out += ((PDSValue)pdsComponent.get(i)).getValue() + (i<pdsComponent.size()-1?",":"");
			}
		}
		return out;
	}


	public void addNewObject(PDSObject input) {
		if (input!=null) {
			pdsComponent.add((PDSObject)input.clone());
		}
	}
	public int size() {
		return pdsComponent.size();
	}

	public PDSComponent getComponentAt(int i) {
		return pdsComponent.get(i);
	}
	
	public PDSValue getPDSValue(String name) throws PDSValueNotFoundException {
		for (int i=0;i<pdsComponent.size();i++)
			if (pdsComponent.get(i).isPDSValue()){
				if (pdsComponent.get(i).getName().equals(name)) return (PDSValue)pdsComponent.get(i);
			}
		throw new PDSValueNotFoundException("PDSValue named "+name+" not found");
	}
	
	public PDSObject getPDSObject(String name) throws PDSObjectNotFoundException {
		for (int i=0;i<pdsComponent.size();i++)
			if (pdsComponent.get(i).isPDSObject())
				if (pdsComponent.get(i).getName().equals(name)) return (PDSObject)pdsComponent.get(i);
		throw new PDSObjectNotFoundException("PDSObject named "+name+" not found");
	}

	public PDSValue[] getOnlyPDSValues() {
		Vector<PDSValue> vec = new Vector<PDSValue>();
		for (int i=0;i<pdsComponent.size();i++) 
			if (pdsComponent.get(i).isPDSValue()) vec.add((PDSValue)pdsComponent.get(i));
		PDSValue[] out = new PDSValue[vec.size()];
		for (int i=0;i<out.length;i++) out[i]=vec.elementAt(i);
		return out;
	}
	
	public PDSValue getPDSValueOfPDSObject(String valueName, String objectName) throws PDSValueNotFoundException, PDSObjectNotFoundException {
		return getPDSObject(objectName).getPDSValue(valueName);
	}

	public String getValueOfPDSValueOfPDSObject(String valueName, String objectName) throws PDSValueNotFoundException, PDSObjectNotFoundException {
		return getPDSValueOfPDSObject(valueName, objectName).getValue();
	}

	public String[] getPDSValuesAsStrings(String[] variableNames) throws PDSValueNotFoundException {
		String[] valuesAsStrings = new String[variableNames.length];
		for (int i=0;i<variableNames.length;i++) {
			valuesAsStrings[i] = getPDSValue(variableNames[i]).getValue();
		}
		return valuesAsStrings;
	}
	
	public String[] getAllPDSValueNames() {
		Vector<PDSValue> pdsValues = new Vector<PDSValue>();

		for (int i=0;i<pdsComponent.size();i++) 
			if (pdsComponent.get(i).isPDSValue())
				pdsValues.add((PDSValue)pdsComponent.get(i));

		String[] out = new String[pdsValues.size()];
		for (int i=0;i<out.length;i++)
			out[i] = pdsValues.elementAt(i).getName();
		return out;
	}

	public String getValueOfPDSValue(String name) throws PDSValueNotFoundException {
		return getPDSValue(name).getValue();
	}

	public void setValueOfPDSValue(String name, String value) throws PDSValueNotFoundException {
		getPDSValue(name).setValue(value);
	}

	public String getPointerValue(String name) throws PDSValueNotFoundException {
		PDSValue val = getPDSValue(name);
		if (val.isPointer()) {
			return val.getValue();
		}
		throw new RuntimeException("this object has no pointer "+name);
		//return getValueOfPDSValue("^"+name);
	}

	public String getPointerUnits(String name) throws PDSValueNotFoundException {
		PDSValue val = getPDSValue(name);
		if (val.isPointer()) {
			return val.getUnits();
		}
		throw new RuntimeException("this object has no pointer "+name);
		//return getPDSValue("^"+name).getUnits();
	}

	/*public void setName(String name) {
		this.name= name;
	}*/

	public String getName() {
		if (element==Element.EMPTY) return "";
		else return element.name();
		//return name;
	}

	public String toPDSString() {
		return toString("   ");
	}

	
	private String toString(String indent) {

		String out="";
		boolean hasName = (element!=Element.EMPTY);
		//boolean hasName = !element.name().equals("");
		if (hasName)
			out+="OBJECT = "+element.name()+"\r\n";

		for (int i=0;i<pdsComponent.size();i++) {
			if (pdsComponent.get(i).isPDSValue()) {
				PDSValue pdsValue = (PDSValue)pdsComponent.get(i);
				out += (hasName?indent:"")+pdsValue.toPDSString() + "\r\n";
			} else if (pdsComponent.get(i).isPDSObject()) {
				PDSObject pdsObject = (PDSObject)pdsComponent.get(i);
				String[] pdsObjectsAsStrings = pdsObject.toPDSString().split("\r\n");
				for (int j=0;j<pdsObjectsAsStrings.length;j++)
					out += (hasName?indent:"")+pdsObjectsAsStrings[j] + "\r\n";
			} else if (pdsComponent.get(i).isPDSComment()){
				PDSComment pdsComment = (PDSComment)pdsComponent.get(i);
				out += (hasName?indent:"")+pdsComment.toPDSString() + "\r\n";
			}
		}

		if (hasName)
			out += "END_OBJECT = "+element.name();

		return out;
	}
	
	public Object clone() {
		PDSObject out = new PDSObject();
		out.element = element;
		out.pdsComponent = new ArrayList<PDSComponent>(pdsComponent.size());
		for (int i=0;i<pdsComponent.size();i++) {
			if (pdsComponent.get(i).isPDSValue()) {
				out.pdsComponent.add( (PDSValue)((PDSValue)pdsComponent.get(i)).clone() );
			} else if (pdsComponent.get(i).isPDSComment()) { 
				out.pdsComponent.add( (PDSComment)((PDSComment)pdsComponent.get(i)).clone() );
			} else {
				out.pdsComponent.add( (PDSObject)((PDSObject)pdsComponent.get(i)).clone() );
			}
		}
		return out;
	}

	public boolean isPDSObject() {
		return true;
	}

	public boolean isPDSValue() {
		return false;
	}

	public boolean isPDSComment() {
		return false;
	}


	public static boolean saveCSVFile(PDSObject[] objects, String fileName) {

		/*
		 * Check if all objects have the same variables
		 */ 

		PDSObject object1 = objects[0];
		for (int i=1;i<objects.length;i++) {
			for (int v=0;v<object1.size();v++) {
				String valName = object1.getComponentAt(v).getName();
				try {
					objects[i].getPDSValue(valName);
				} catch (PDSValueNotFoundException e) {
					e.printStackTrace();
					return false;
				}
			}
		}

		/*
		 * Get object names
		 */
		String[] valueNames = object1.getAllPDSValueNames();

		/*
		 * Write file
		 */
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));
			String line = "";

			for (int col=0;col<valueNames.length;col++)
				line += valueNames[col] + (col<valueNames.length-1?",":"");
			writer.write(line);
			writer.newLine();

			for (int i=0;i<objects.length;i++) {
				line = "";
				for (int col=0;col<valueNames.length;col++)
					try {
						line += objects[i].getValueOfPDSValue(valueNames[col]) + (col<valueNames.length-1?",":"");
					} catch (PDSValueNotFoundException e) {
						e.printStackTrace();
						writer.flush();
						writer.close();
						File file = new File(fileName);
						file.delete();
						return false;
					}
					writer.write(line);
					writer.newLine();
			}

			writer.flush();
			writer.close();
			return true;

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

	}

	public static PDSObject[] loadCSVFile(String fileName) throws FileNotFoundException, IOException {
		BufferedReader reader = new BufferedReader(new FileReader(fileName));
		String line="";
		boolean comment = false;
		boolean firstLine = true;
		Vector<PDSObject> out = new Vector<PDSObject>();
		String[] variables = new String[0];
		while ( (line=reader.readLine())!=null) {
			if (line.startsWith("/*")) comment = true;

			if (!comment) {
				String[] columns = line.split(",");
				if (variables.length==0) variables = new String[columns.length];
				for (int i=0;i<columns.length;i++) {
					columns[i] = columns[i].replaceAll("^\\s+", ""); // remove leading white;
					columns[i] = columns[i].replaceAll("\\s+$", ""); // remove trailing white;
				}
				if (firstLine) {
					firstLine = false;
					for (int i=0;i<columns.length;i++) variables[i] = columns[i];
				} else {
					PDSValue[] pdsValue = new PDSValue[columns.length];
					for (int i=0;i<columns.length;i++) pdsValue[i] = new PDSValue(variables[i],columns[i]);
					out.add(new PDSObject(pdsValue));
				}
			}

			if (line.contains("*/")) comment = false;
		}

		PDSObject[] outArray = new PDSObject[out.size()];
		for (int i=0;i<out.size();i++)
			outArray[i] = out.elementAt(i);
		return outArray;


	}

	public static PDSObject getPDSObjectWith(String pdsValueName, String value, PDSObject[] objectList) {
		for (int i=0;i<objectList.length;i++) {
			try {
				if (objectList[i].getValueOfPDSValue(pdsValueName).equals(value)) return objectList[i];
			} catch (PDSValueNotFoundException e) {
				e.printStackTrace();
			}
		}
		return null;
	}
	
	public boolean removeLastValueNamed(String name) {
		boolean removed = false;
		for (int i=pdsComponent.size()-1;i>=0;i--) {
			if (pdsComponent.get(i).isPDSValue())
				if (pdsComponent.get(i).getName().equals(name)) {
					pdsComponent.remove(i);
					removed=true;
					i=-1;
				}
		}
		return removed;
	}

}
