package esa.pds.component;

public class PDSComment extends PDSComponent implements Cloneable {

	private String comment="";
	
	public PDSComment() {
	}
	
	public PDSComment(String comment) {
		this.comment = comment;
	}
	
	public Object clone() {
		PDSComment out = new PDSComment(this.comment);
		return out;
	}
	
	public String getName() {
		return "";
	}

	public boolean isPDSComment() {
		return true;
	}

	public boolean isPDSObject() {
		return false;
	}

	public boolean isPDSValue() {
		return false;
	}

	public String toPDSString() {
		return comment;
	}

}
