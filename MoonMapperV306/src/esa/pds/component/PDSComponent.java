package esa.pds.component;

import java.io.Serializable;

public abstract class PDSComponent implements Serializable {

	public abstract String getName();
	public abstract boolean isPDSValue();
	public abstract boolean isPDSObject();
	public abstract boolean isPDSComment();
	public abstract String toPDSString();
	
}
