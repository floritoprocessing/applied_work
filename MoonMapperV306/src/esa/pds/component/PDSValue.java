package esa.pds.component;

import esa.pds.dictionary.Element;

public class PDSValue extends PDSComponent implements Cloneable {

	protected Element element;
	private boolean isPointer = false;
	private String value;
	private String units="";
	private boolean valueHasQuotes = false;

	public PDSValue(String name,String value) {
		name = name.replaceAll(":", "__");
		if (name.charAt(0)=='^') {
			isPointer=true;
			element = Element.valueOf(name.substring(1));
		} else {
			element = Element.valueOf(name);
		}
		this.value = value;
	}
	
	public PDSValue(String name, String value, String units) {
		this(name,value);
		this.units = units;
	}
	
	PDSValue(String name, String value, boolean valueHasQuotes) {
		this(name,value);
		this.valueHasQuotes = valueHasQuotes;
	}
	
	PDSValue(String name, String value, String units, boolean valueHasQuotes) {
		this(name,value,units);
		this.valueHasQuotes = valueHasQuotes;
	}
	
	private PDSValue(String name, String value, String units, boolean valueHasQuotes, boolean isPointer) {
		this(name,value,units,valueHasQuotes);
		this.isPointer = isPointer;
	}
	
	public boolean isPointer() {
		return isPointer;
	}

	public PDSValue(String name, int value) {
		this(name,""+value);
	}

	public PDSValue(String name, double value) {
		this(name,""+value);
	}
	
	public String toPDSString() {
		String out = "";
		if (isPointer) {
			out="^";
		}
		if (valueHasQuotes) {
			return out+element.name()+" = "+"\""+value+"\""+(units.length()>0?(" <"+units+">"):"");//+" "+units;
		} else
			return out+element.name()+" = "+value+(units.length()>0?(" <"+units+">"):"");//+" "+units;
	}

	public String getName() {
		return element.name();
	}

	public String getValue() {
		return value;
	}
	
	public String getUnits() {
		return units;
	}
	
	public void setValue(String value) {
		this.value = value;
	}

	public Object clone() {
		return new PDSValue(element.name(),value,units,valueHasQuotes,isPointer);
	}

	public boolean isPDSObject() {
		return false;
	}

	public boolean isPDSValue() {
		return true;
	}

	public boolean isPDSComment() {
		return false;
	}

	@Override
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((element == null) ? 0 : element.hashCode());
		result = PRIME * result + (isPointer ? 1231 : 1237);
		result = PRIME * result + ((units == null) ? 0 : units.hashCode());
		result = PRIME * result + ((value == null) ? 0 : value.hashCode());
		result = PRIME * result + (valueHasQuotes ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		final PDSValue other = (PDSValue) obj;
		if (element == null) {
			if (other.element != null)
				return false;
		} else if (!element.equals(other.element))
			return false;
		if (isPointer != other.isPointer)
			return false;
		if (units == null) {
			if (other.units != null)
				return false;
		} else if (!units.equals(other.units))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		if (valueHasQuotes != other.valueHasQuotes)
			return false;
		return true;
	}


}
