package esa.pds.component;

public class PDSValueCreator {

	public static PDSValue createFromPDSString(String pdsString) {
		boolean valueHasQuotes = false;
		String[] valNameSplit = pdsString.split("=");
		if (valNameSplit.length!=2) throw new RuntimeException("cannot parse "+pdsString);
		else {
			String name = valNameSplit[0].replaceAll("^\\s+","");
			name = name.replaceAll("\\s+$","");
			
			String value = valNameSplit[1].replaceAll("^\\s+","");
			value = value.replaceAll("\\s+$","");
			
			int q1 = value.indexOf("\"");
			int q2 = value.lastIndexOf("\"");
			if (!value.startsWith("\"")) q1=-1;
			if (!value.endsWith("\"")) q2=-1;
			int c1 = value.indexOf("<");
			int c2 = value.indexOf(">");
	
			if (q1>=0&&q2>=0) {
				valueHasQuotes = true;
				if (q1!=q2) {
					value = value.substring(q1+1,q2);
				}
			} 
			
			if (c1>=0&&c2>=0) {
				String units = value.substring(c1+1,c2);
				value = value.substring(0,c1);
				value = value.replaceAll("\\s+$","");
				/*PDSValue out = null;
				try {
					out = new PDSValue(name,value,units,valueHasQuotes);
				} catch (IllegalArgumentException e) {
					System.err.println(e.getLocalizedMessage());
				}
				return out;*/
				return new PDSValue(name,value,units,valueHasQuotes);
			}
			
			/*PDSValue out = null;
			try {
				out = new PDSValue(name,value,valueHasQuotes);
			} catch (IllegalArgumentException e) {
				System.err.println(e.getLocalizedMessage());
			}
			return out;*/
			return new PDSValue(name,value,valueHasQuotes);//out;//new PDSValue(name,value,valueHasQuotes));
		}
		
	}

}
