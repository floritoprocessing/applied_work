package esa.pds;

public class PDSUnStringableException extends Exception {

	private static final long serialVersionUID = -376179011721057848L;

	public PDSUnStringableException(String msg) {
		super(msg);
	}
}
