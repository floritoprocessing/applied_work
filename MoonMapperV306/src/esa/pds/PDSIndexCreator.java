package esa.pds;

import java.io.*;

/**
 * Stream class for browsing directories and writing the supplied settingsFile to a seperate index file
 * @author mgraf
 *
 */
public class PDSIndexCreator {

	private BufferedWriter writer;
	private File root;
	private String extension;
	private String[] variablesToIndex;
	private int filesToRead;
	private PDSFileConstrain constrain;

	private String[] lines;
	private int writeLine;
	
	public class Counter {
		public int val=0;
		public Counter() {
			val=0;
		}
	}

	public PDSIndexCreator(BufferedWriter writer, File root, String extension, 
			String[] variablesToIndex, int filesToRead, PDSFileConstrain constrain) {
		this.writer = writer;
		this.root = root;
		this.extension = extension;
		this.variablesToIndex = variablesToIndex;
		this.filesToRead = filesToRead;
		lines = new String[filesToRead];
		writeLine = 0;
		this.constrain = constrain;
	}

	/**
	 * Creates a new CSVIndexFile. From a root directory and its containing PDS files.
	 * @param rootPath (String) - the root path for browsing subdirectories
	 * @param extension (String) - file extension of the PDS files
	 * @param outputFilename (String) - filename of the indexFile to write
	 * @param variablesToIndex (String[]) - array of variable names to extract
	 * @param filesToRead (int) - number of files to read before writing to output file
	 */
	public static void createCSVIndexFileFromFiles(String rootPath, String extension, String outputFilename, boolean overwriteFile, String[] variablesToIndex, PDSFileConstrain constrain, int filesToRead) {
		File outputFile = new File(outputFilename); 
		if (!outputFile.exists() || overwriteFile) {
		File root = new File(rootPath);
			try {
				BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile));
				PDSIndexCreator indexCreator = new PDSIndexCreator(writer,root,extension,variablesToIndex,filesToRead,constrain);
				String[] commentLines = constrain.toStrings();
				for (int i=0;i<commentLines.length;i++) {
					writer.write(commentLines[i]);
					writer.newLine();
				}
				writer.flush();
				String firstLine="";//"Filename, ";
				for (int i=0;i<variablesToIndex.length-1;i++) firstLine += (variablesToIndex[i]+",");
				firstLine += variablesToIndex[variablesToIndex.length-1];
				writer.write(firstLine);
				writer.newLine();
				indexCreator.recurseRoot(false, null);
				//indexCreator.recurse(root,false,null);
				indexCreator.finish();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public static void reduceCSVFile(String inputFilename, String outputFilename, PDSFileConstrain constrain, int entriesToRead) {
		File outputFile = new File(outputFilename);
		if (outputFile.exists()) {
			System.out.println("will not overwrite existing file "+outputFilename);
			System.exit(0);
		}
		CSVFileWriter writer = new CSVFileWriter(outputFilename);
		CSVFileLoader loader = new CSVFileLoader(inputFilename);
		
		if ( !writer.processAndSave(loader,constrain,entriesToRead) )
			throw new RuntimeException("Problem creating "+outputFilename);
		
		//PDSFile.loadAndWriteCSVFile(inputFilename, filesToRead, writer);
		//System.out.println(pdsFile.length+" pdsFiles loaded");
		//File outputFile = new File(outputFilename);
		
	}
	
	public static void createCSVIndexFile(String rootPath, String extension, String outputFilename, boolean overwriteFile, String[] variablesToIndex, int filesToRead) {
		createCSVIndexFileFromFiles(rootPath, extension, outputFilename, overwriteFile, variablesToIndex, null, filesToRead);
	}
	
	private void recurseRoot(boolean countFiles, Counter count) {
		recurse(root,countFiles,count);
	}

	private void recurse(File path, boolean countFiles, Counter count) {
		System.out.println(path.getName());
		File files[] = path.listFiles();
		for(int i=0;i<files.length;i++) {
			if(files[i].isDirectory()) {
				recurse(files[i], countFiles, count);
			} else if(files[i].isFile()) {
				if (!countFiles) {
					if (files[i].getName().endsWith(extension))
						addContentOfFile(files[i]);
				} else {
					count.val++;
				}
			}
		}
	}

	private void addContentOfFile(File file) {

		boolean addFile = true;
		String[] values;
		try {
			values = PDSFileProcessor.getValuesOfFile(file, variablesToIndex);
			if (constrain!=null)
				addFile = constrain.withinRange(variablesToIndex, values);

			if (addFile) {
				//String line = file.getName()+", "; 
				String line = "";
				if (values!=null) {
					for (int i=0;i<values.length-1;i++) line+=(values[i]+", ");
					line+=values[values.length-1];
				} else {
					line+=" one or more illegal variables!";
				}
				appendLine(line,false);
			}
		} catch (PDSValueNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private void appendLine(String line, boolean forceWrite) {
		if (writeLine<filesToRead && !forceWrite) {
			lines[writeLine] = line;
			writeLine++;
		} else {

			System.out.println("writing "+writeLine+" lines");
			try {
				for (int i=0;i<(forceWrite?writeLine:lines.length);i++) {
					writer.write(lines[i]);
					writer.newLine();
				}
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
			writeLine=0;

		}

	}

	private void finish() {
		appendLine("",true);
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("finished writing file");
	}

	

}
