package esa.pds.image.processing;

import esa.pds.image.ImageData;

public class PDSImageProcessorTrim implements PDSImageProcessorGeneric {

	private float min, max;
	
	public PDSImageProcessorTrim(float minValue, float maxValue) {
		min = minValue;
		max = maxValue;
	}
	
	public void processFloatData(ImageData floatData, int col, int row, int width, int height) {
		floatData.setPixelValue(Math.max(min, Math.min(max, floatData.getPixelValue())));
	}

	public void prepare(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	/*public void processIntData(IntegerImageData intData, int col, int row, int width, int height) {
		intData.setPixelValue((int)Math.max(min, Math.min(max, intData.getPixelValue())));
	}*/

	

}
