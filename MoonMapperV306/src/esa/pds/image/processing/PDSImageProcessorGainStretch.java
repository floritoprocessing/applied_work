package esa.pds.image.processing;

import esa.pds.image.ImageData;

public class PDSImageProcessorGainStretch implements PDSImageProcessorGeneric {

	//public float PIXEL_GAIN = 1.0f;
	
	private float range;
	private float vMin;
	private float dv;
	private float stretch;
	
	public PDSImageProcessorGainStretch(float targetRange, float vMin, float vMax) {
		this.range = targetRange;
		this.vMin = vMin;
		dv = vMax-vMin;
		stretch = range/dv;
	}
	
	public void processFloatData(ImageData floatData, int col, int row, int width, int height) {
		floatData.setPixelValue((floatData.getPixelValue()-vMin)*stretch);///dv*range);
	}

	public void prepare(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	/*public void processIntData(IntegerImageData intData, int col, int row, int width, int height) {
		intData.setPixelValue((int)((intData.getPixelValue()-vMin)/dv*range));
	}*/

	

}
