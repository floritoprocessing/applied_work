package esa.pds.image.processing;

import esa.pds.image.ImageData;

public interface PDSImageProcessorGeneric {

	//public void processIntData(IntegerImageData intData, int col, int row, int width, int height);
	public void processFloatData(ImageData floatData, int col, int row, int width, int height);
	
	public void prepare(int width, int height);
	
}
