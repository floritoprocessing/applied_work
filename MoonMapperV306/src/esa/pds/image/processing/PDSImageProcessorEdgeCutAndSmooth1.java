package esa.pds.image.processing;

import esa.pds.image.ImageData;

public class PDSImageProcessorEdgeCutAndSmooth1 implements PDSImageProcessorGeneric {

	public static final int TOP = 1<<0;
	public static final int LEFT = 1<<1;
	public static final int BOTTOM = 1<<2;
	public static final int RIGHT = 1<<3;
	public static final int TOP_AND_BOTTOM = TOP+BOTTOM;
	public static final int LEFT_AND_RIGHT = LEFT+RIGHT;
	public static final int ALL = TOP+LEFT+BOTTOM+RIGHT;
	
	int cutTarget, cutAmount;
	int smoothTarget=0, smoothAmount=0;
	
	public PDSImageProcessorEdgeCutAndSmooth1(int cutTarget, int cutAmount) {
		this.cutTarget = cutTarget;
		this.cutAmount = cutAmount;
	}
	
	public PDSImageProcessorEdgeCutAndSmooth1(int cutTarget, int cutAmount, int smoothTarget, int smoothAmount) {
		this(cutTarget,cutAmount);
		this.smoothTarget = smoothTarget;
		this.smoothAmount = smoothAmount;
	}
	
	private float[] topSmooth, leftSmooth, bottomSmooth, rightSmooth;
	
	public void prepare(int width, int height) {
		
		//System.out.println("preparing "+width+"/"+height);
		
		topSmooth = new float[height];
		leftSmooth = new float[width];
		bottomSmooth = new float[height];
		rightSmooth = new float[width];
		
		boolean cutIt, smoothIt;
		int cutMax=0, smooth=0;
		if (cutTarget>0 || smoothTarget>0) {
			int s=0;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//TOP: if (row<cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?cutAmount:0;
				smooth = cutMax + (smoothIt?smoothAmount:0);
				for (int row=0;row<height;row++) {
					if (row<cutMax) 
						topSmooth[row]=0;
					else if (row<smooth)
						topSmooth[row]=((float)row-cutMax)/smoothAmount;
				}
			}
			s=1;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//LEFT: if (col<cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?cutAmount:0;
				smooth = cutMax + (smoothIt?smoothAmount:0);
				for (int col=0;col<width;col++) {
					if (col<cutMax) 
						leftSmooth[col]=0;
					else if (col<smooth) 
						leftSmooth[col] = ((float)col-cutMax)/smoothAmount;
				}
			}
			s=2;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//BOTTOM: if (row>=height-cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?(height-cutAmount):height;
				smooth = cutMax - (smoothIt?smoothAmount:0);
				for (int row=0;row<height;row++) {
					if (row>=smooth && row<cutMax) 
						bottomSmooth[row] = 1-((float)(row-smooth)/smoothAmount);
					else if (row>=cutMax) 
						bottomSmooth[row]=0;
				}
			}
			s=3;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//RIGHT if (col>=width-cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?(width-cutAmount):width;
				smooth = cutMax - (smoothIt?smoothAmount:0);
				for (int col=0;col<width;col++) {
					if (col>=smooth && col<cutMax) 
						rightSmooth[col] = 1-((float)(col-smooth)/smoothAmount);
					else if (col>=cutMax) 
						rightSmooth[col]=0;
				}
			}
		}
	}

	public void processFloatData(ImageData floatData, int col, int row, int width, int height) {
		//System.out.println(width+" "+height);
		boolean cutIt, smoothIt;
		int cutMax=0, smooth=0;
		if (cutTarget>0 || smoothTarget>0) {
			int s=0;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//TOP: if (row<cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?cutAmount:0;
				smooth = cutMax + (smoothIt?smoothAmount:0);
				if (row<cutMax) 
					floatData.setAlpha(0);//topSmooth[row]=0;
				else if (row<smooth)
					floatData.setAlpha(floatData.getAlpha()*topSmooth[row]);
			}
			s=1;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//LEFT: if (col<cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?cutAmount:0;
				smooth = cutMax + (smoothIt?smoothAmount:0);
				if (col<cutMax) 
					floatData.setAlpha(0);//leftSmooth[col]=0;
				else if (col<smooth) 
					floatData.setAlpha(floatData.getAlpha()*leftSmooth[col]);
					//leftSmooth[col] = ((float)col-cutMax)/smoothAmount;
			}
			s=2;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//BOTTOM: if (row>=height-cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?(height-cutAmount):height;
				smooth = cutMax - (smoothIt?smoothAmount:0);
				if (row>=smooth && row<cutMax) 
					floatData.setAlpha(floatData.getAlpha()*bottomSmooth[row]);// = 1-((float)(row-smooth)/smoothAmount);
				else if (row>=cutMax) 
					floatData.setAlpha(0);//bottomSmooth[row]=0;
			}
			s=3;
			cutIt = (cutTarget>>s&0x01)==1;
			smoothIt = (smoothTarget>>s&0x01)==1;
			if (cutIt || smoothIt) {
				//RIGHT if (col>=width-cutAmount) floatData.setAlpha(0); break;
				cutMax = cutIt?(width-cutAmount):width;
				smooth = cutMax - (smoothIt?smoothAmount:0);
				if (col>=smooth && col<cutMax) 
					floatData.setAlpha(floatData.getAlpha()*rightSmooth[col]);// = 1-((float)(col-smooth)/smoothAmount);
				else if (col>=cutMax) 
					floatData.setAlpha(0);//rightSmooth[col]=0;
			}
		}
	}
	
	/*public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				doit();
			}
		});
	}
	
	public static void doit() {
		int w=200;
		int h=200;
		
		BufferedImage img1 = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
		BufferedImage img2 = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
		
		ImageData[][] data1 = new ImageData[w][h];
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) data1[x][y] = new ImageData(1,1);
		
		ImageData[][] data2 = new ImageData[w][h];
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) data2[x][y] = new ImageData(1,1);
		
		PDSImageProcessorEdgeCutAndSmooth proc = new PDSImageProcessorEdgeCutAndSmooth(ALL, 5, ALL, 5);
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) proc.processFloatData(data2[x][y],x,y,w,h);
		
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) {
			img1.setRGB(x, y, floatAndAlphaToARGB(data1[x][y]));
			img2.setRGB(x, y, floatAndAlphaToARGB(data2[x][y]));
		}
		
		JLabel lbl1 = new JLabel(new ImageIcon(img1));
		JLabel lbl2 = new JLabel(new ImageIcon(img2));
		JFrame frame = new JFrame("test");
		frame.getContentPane().setBackground(Color.BLACK);
		frame.getContentPane().add(BorderLayout.NORTH,lbl1);
		frame.getContentPane().add(BorderLayout.SOUTH,lbl2);
		frame.pack();
		frame.setVisible(true);
	}
	
	private static int floatAndAlphaToARGB(ImageData data) {
		int px = (int)(data.getPixelValue()*255);
		int al = (int)(data.getAlpha()*255);
		return al<<24|px<<16|px<<8|px;
	}*/

	/*public void processIntData(IntegerImageData intData, int col, int row, int width, int height) {
		if (cutTarget>0) {
			for (int s=0;s<4;s++) {
				boolean sideCut = (cutTarget>>s&0x01)==1;
				if (sideCut) {
					switch (s) {
						case 0: if (row<cutAmount) intData.setAlpha(0); break;
						case 1: if (col<cutAmount) intData.setAlpha(0); break;
						case 2: if (row>=height-cutAmount) intData.setAlpha(0); break;
						case 3: if (col>=width-cutAmount) intData.setAlpha(0); break;
						default: break;
					}
				}
			}
		}
	}*/

	

}
