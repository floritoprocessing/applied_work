package esa.pds.image.processing;

import esa.pds.image.ImageData;

public class PDSImageProcessorGain implements PDSImageProcessorGeneric {

	public float PIXEL_GAIN = 1.0f;
	
	public PDSImageProcessorGain(float gainValue) {
		PIXEL_GAIN = gainValue;
	}
	
	public void setGain(float gainValue) {
		PIXEL_GAIN = gainValue;
	}
	
	public float getGain() {
		return PIXEL_GAIN;
	}
	
	public void processFloatData(ImageData floatData, int col, int row, int width, int height) {
		floatData.setPixelValue(floatData.getPixelValue()*PIXEL_GAIN);
	}

	public void prepare(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	/*public void processIntData(IntegerImageData intData, int col, int row, int width, int height) {
		intData.setPixelValue((int)(intData.getPixelValue()*PIXEL_GAIN));
	}*/

	

}
