package esa.pds.image.processing;

import esa.pds.image.ImageData;

import java.util.Vector;

public class ProcessorStack {
	
	private Vector<PDSImageProcessorGeneric> stack;
	
	public ProcessorStack() {
		stack = new Vector<PDSImageProcessorGeneric>();
	}
	
	public void add(PDSImageProcessorGeneric processor) {
		stack.add(processor);
	}
	
	
	/*public IntegerImageData processIntData(IntegerImageData intData, int col, int row, int width, int height) {
		if (stack.size()==0) return intData;
		else {
			for (int i=0;i<stack.size();i++) {
				stack.elementAt(i).processIntData(intData, col, row, width, height);
			}
			return intData;
		}
	}*/

	//ImageData
	public void processFloatData(ImageData floatData, int col, int row, int width, int height) {
		if (stack.size()==0) return;// floatData;
		else {
			for (int i=0;i<stack.size();i++) {
				stack.elementAt(i).processFloatData(floatData, col, row, width, height);//, processor.elementAt(i));
			}
		}
	}

	/*public PDSImageProcessorGain getProcessorGain() {
		for (int i=0;i<stack.size();i++)
			if (stack.elementAt(i).getClass().equals(PDSImageProcessorGain.class)) return (PDSImageProcessorGain)stack.elementAt(i);
		return null;
	}*/

	public void prepare(int width, int height) {
		for (int i=0;i<stack.size();i++)
			stack.elementAt(i).prepare(width,height);
	}
	
	

}
