package esa.pds.image.processing;

import esa.pds.image.ImageData;

public class PDSImageProcessorEdgeCutAndSmooth implements PDSImageProcessorGeneric {

	public static final int TOP = 1<<0;
	public static final int LEFT = 1<<1;
	public static final int BOTTOM = 1<<2;
	public static final int RIGHT = 1<<3;
	public static final int TOP_AND_BOTTOM = TOP+BOTTOM;
	public static final int LEFT_AND_RIGHT = LEFT+RIGHT;
	public static final int ALL = TOP+LEFT+BOTTOM+RIGHT;
	
	int cutTarget, cutAmount;
	int smoothTarget=0, smoothAmount=0;
	
	public PDSImageProcessorEdgeCutAndSmooth(int cutTarget, int cutAmount) {
		this.cutTarget = cutTarget;
		this.cutAmount = cutAmount;
	}
	
	public PDSImageProcessorEdgeCutAndSmooth(int cutTarget, int cutAmount, int smoothTarget, int smoothAmount) {
		this(cutTarget,cutAmount);
		this.smoothTarget = smoothTarget;
		this.smoothAmount = smoothAmount;
	}

	public void processFloatData(ImageData floatData, int col, int row, int width, int height) {
		//System.out.println(width+" "+height);
		boolean cutIt, smoothIt;
		int cutMax=0, smooth=0;
		if (cutTarget>0 || smoothTarget>0) {
			for (int s=0;s<4;s++) {
				cutIt = (cutTarget>>s&0x01)==1;
				smoothIt = (smoothTarget>>s&0x01)==1;
				if (cutIt || smoothIt) {
					//cutMax=0;
					//smooth=0;
					switch (s) {
						case 0: //TOP: if (row<cutAmount) floatData.setAlpha(0); break;
							cutMax = cutIt?cutAmount:0;
							smooth = cutMax + (smoothIt?smoothAmount:0);
							if (row<cutMax) 
								floatData.setAlpha(0);
							else if (row<smooth) 
								floatData.setAlpha(
										floatData.getAlpha() * ((float)row-cutMax)/smoothAmount);
							break;
						case 1: //LEFT: if (col<cutAmount) floatData.setAlpha(0); break;
							cutMax = cutIt?cutAmount:0;
							smooth = cutMax + (smoothIt?smoothAmount:0);
							if (col<cutMax) 
								floatData.setAlpha(0);
							else if (col<smooth) 
								floatData.setAlpha(
										floatData.getAlpha() * ((float)col-cutMax)/smoothAmount);
							break;
						case 2: //BOTTOM: if (row>=height-cutAmount) floatData.setAlpha(0); break;
							cutMax = cutIt?(height-cutAmount):height;
							smooth = cutMax - (smoothIt?smoothAmount:0);
							if (row>=smooth && row<cutMax) 
								floatData.setAlpha(
										floatData.getAlpha() * (1-((float)(row-smooth)/smoothAmount)));
							else if (row>=cutMax) 
								floatData.setAlpha(0);
							break;
						case 3: //RIGHT if (col>=width-cutAmount) floatData.setAlpha(0); break;
							cutMax = cutIt?(width-cutAmount):width;
							smooth = cutMax - (smoothIt?smoothAmount:0);
							if (col>=smooth && col<cutMax) 
								floatData.setAlpha(
										floatData.getAlpha() * (1-((float)(col-smooth)/smoothAmount)));
							else if (col>=cutMax) 
								floatData.setAlpha(0);
							break;
						default: break;
					}
				}
			}
		}
	}

	public void prepare(int width, int height) {
		// TODO Auto-generated method stub
		
	}
	
	/*public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				doit();
			}
		});
	}
	
	public static void doit() {
		int w=200;
		int h=200;
		
		BufferedImage img1 = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
		BufferedImage img2 = new BufferedImage(w,h,BufferedImage.TYPE_INT_ARGB);
		
		ImageData[][] data1 = new ImageData[w][h];
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) data1[x][y] = new ImageData(1,1);
		
		ImageData[][] data2 = new ImageData[w][h];
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) data2[x][y] = new ImageData(1,1);
		
		PDSImageProcessorEdgeCutAndSmooth proc = new PDSImageProcessorEdgeCutAndSmooth(ALL, 5, ALL, 5);
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) proc.processFloatData(data2[x][y],x,y,w,h);
		
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) {
			img1.setRGB(x, y, floatAndAlphaToARGB(data1[x][y]));
			img2.setRGB(x, y, floatAndAlphaToARGB(data2[x][y]));
		}
		
		JLabel lbl1 = new JLabel(new ImageIcon(img1));
		JLabel lbl2 = new JLabel(new ImageIcon(img2));
		JFrame frame = new JFrame("test");
		frame.getContentPane().setBackground(Color.BLACK);
		frame.getContentPane().add(BorderLayout.NORTH,lbl1);
		frame.getContentPane().add(BorderLayout.SOUTH,lbl2);
		frame.pack();
		frame.setVisible(true);
	}
	
	private static int floatAndAlphaToARGB(ImageData data) {
		int px = (int)(data.getPixelValue()*255);
		int al = (int)(data.getAlpha()*255);
		return al<<24|px<<16|px<<8|px;
	}*/

	/*public void processIntData(IntegerImageData intData, int col, int row, int width, int height) {
		if (cutTarget>0) {
			for (int s=0;s<4;s++) {
				boolean sideCut = (cutTarget>>s&0x01)==1;
				if (sideCut) {
					switch (s) {
						case 0: if (row<cutAmount) intData.setAlpha(0); break;
						case 1: if (col<cutAmount) intData.setAlpha(0); break;
						case 2: if (row>=height-cutAmount) intData.setAlpha(0); break;
						case 3: if (col>=width-cutAmount) intData.setAlpha(0); break;
						default: break;
					}
				}
			}
		}
	}*/

	

}
