package esa.pds.image;

import esa.pds.dictionary.SampleType;

public class PDSImage {

	protected int width;
	protected int height;
	//protected int sampleType;
	protected SampleType sampleType;
	protected int sampleBits;
	protected ImageData[] dataAsFloat;
	
	protected PDSImage() {
	}
	
	public PDSImage(int width, int height, SampleType sampleType, int sampleBits, int[] data) {
		this.width = width;
		this.height = height;
		this.sampleType = sampleType;
		this.sampleBits = sampleBits;
		dataAsFloat = new ImageData[data.length];
		for (int i=0;i<data.length;i++)
			dataAsFloat[i] = new ImageData(data[i]);
	}

	public PDSImage(int width, int height, SampleType sampleType, int sampleBits, float[] data) {
		this.width = width;
		this.height = height;
		this.sampleType = sampleType;
		this.sampleBits = sampleBits;
		dataAsFloat = new ImageData[data.length];
		for (int i=0;i<data.length;i++)
			dataAsFloat[i] = new ImageData(data[i]);
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}

	public ImageData[] getImageDataAsFloat() {
		return dataAsFloat;
	}

}
