package esa.pds.image;

public class ImageData implements Cloneable {

	float pixelValue = 0;
	float alpha = 1;
	
	public ImageData(float pixelValue) {
		this.pixelValue = pixelValue;
	}

	public ImageData(ImageData source) {
		pixelValue = source.pixelValue;
		alpha = source.alpha;
	}

	public ImageData(float pixelValue, float alpha) {
		this.pixelValue = pixelValue;
		this.alpha = alpha;
	}

	public float getPixelValue() {
		return pixelValue;
	}
	
	public void setPixelValue(float pixelValue) {
		this.pixelValue = pixelValue;
	}
	
	public Object clone() {
		return new ImageData(this);
	}

	public void setAlpha(float alpha) {
		this.alpha = alpha;
	}
	
	/**
	 * Alpha (0..1)
	 * @return
	 */
	public float getAlpha() {
		return alpha;
	}
	

}
