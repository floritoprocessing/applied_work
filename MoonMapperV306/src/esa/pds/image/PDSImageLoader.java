package esa.pds.image;

import java.io.File;

import esa.file.tools.AsciiFileReader;
import esa.file.tools.BinaryFileReader;
import esa.pds.PDSObjectNotFoundException;
import esa.pds.PDSValueNotFoundException;
import esa.pds.component.PDSObject;
import esa.pds.dictionary.ByteParser;
import esa.pds.dictionary.SampleType;

public class PDSImageLoader {

	public static final int BINARY_IMAGE = 0;
	public static final int BINARY_CORNERS = 1;
	public static final int BINARY_IMAGE_AND_CORNERS = 2;
	
	/**
	 * Get Corner data (per 8x8 pixels 4 corner points)
	 * this data starts at the "top left" corner and is sorted by columns (x increases slower than y)
	 * Convert to cornerData[x][y][lonLat]
	 * 
	 * for the low res images: corner data longitude gets converted from -180..180 to 0..360
	 */
	//float[] cornerData = binaryFile.get4ByteFloats(8,129*129*2); -> offset=8,
	//float[] dataAsFloat = binaryFile.get4ByteFloats(8+129*129*2*4+8+8, 128*128); offset =	
	//public static PDSImage createAndLoadBinary(File[] fileLowResGeom, int loadWhat) {
	public static float[] createAndLoadBinary(File[] fileLowResGeom, int loadWhat) {
		// TODO Auto-generated method stub
	
		AsciiFileReader descrReader = new AsciiFileReader(fileLowResGeom[0]);
		descrReader.read();
		BinaryFileReader binaryReader = new BinaryFileReader(fileLowResGeom[1]);
		binaryReader.read();
	
		int offsetShape=-1, offsetImage=-1;
		int amountShape=-1, amountImage=-1;
		int width=128, height=128;
	
		//
		// Find data byteoffset and amount of 4byteFloats for shape and image,
		// Find image size
		//
	
		if (loadWhat==BINARY_IMAGE || loadWhat==BINARY_CORNERS ) {//|| loadWhat==BINARY_IMAGE_AND_CORNERS) {
			String[] descr = descrReader.getStrings(0,new char[] {'\n','\r'} );
			for (int i=0;i<descr.length;i++) {
				descr[i] = descr[i].replaceAll("^\\s+","");
	
				// find shape and image data
				if (descr[i].startsWith("object 3") || descr[i].startsWith("object 2")) { // object 2 for shapes
					int whatData = descr[i].startsWith("object 2")?BINARY_CORNERS:BINARY_IMAGE;
					if (whatData==BINARY_IMAGE)
						offsetImage = Integer.parseInt(descr[i].split(",")[1].replaceAll("^\\s+", ""));
					else
						offsetShape = Integer.parseInt(descr[i].split(",")[1].replaceAll("^\\s+", ""));
	
					// find amount of data
					String[] colString = descr[i].split(" ");
					for (int col=0;col<colString.length;col++) {
						if (colString[col].equals("items"))
							if (whatData==BINARY_IMAGE)
								amountImage = Integer.parseInt(colString[col+1]);
							else
								amountShape = 2 * Integer.parseInt(colString[col+1]); // lon lat
					}
				}
	
			}
		} else {
			throw new RuntimeException(" dunno what to load ");
		}
	
		if (offsetShape<0||offsetImage<0||amountShape<0||amountImage<0)
			throw new RuntimeException("could not find data positions of objects");
	
		if (loadWhat==BINARY_IMAGE) {
			float[] imageData = binaryReader.get4ByteFloats(offsetImage, amountImage);
			float[] swappedImageData = new float[imageData.length];
			//	TODO swap x/y image data 
			int si=0;
			for (int sy=0;sy<height;sy++)
				for (int sx=0;sx<width;sx++) {
					int ti=sx*width+sy;
					swappedImageData[ti]=imageData[si++];
				}
			return swappedImageData;
		}
		
		else {
			float[] cornerDataYX = binaryReader.get4ByteFloats(offsetShape, amountShape);
			//
			// Swap x/y corner data
			// reformat corner data
			// make longitudes 0..360 instead of -180..180
			// set binaryImageCrossesMedian flag
			//
			
			float[] cornerDataXY = new float[cornerDataYX.length];
			int si=0;
			int ti=0;
			int jump=-129*129*2+2;
			for (int sx=0;sx<129;sx++) {
				//si = sx*129*2;
				//ti = sx*2;
				for (int sy=0;sy<129;sy++)  {
					cornerDataXY[ti]=cornerDataYX[si];
					cornerDataXY[ti+1]=cornerDataYX[si+1];
					si+=2;
					ti+=258;
				}
				ti+=jump;
			}
			
			return cornerDataXY;
			
			
			/*float[][][] cornerDataFormatted = new float[129][129][2];
			int ix=0;
			
			for (int x=0;x<129;x++) { 
				for (int y=0;y<129;y++)  {
					cornerDataFormatted[x][y][0] = cornerDataYX[ix++];
					if (cornerDataFormatted[x][y][0]<0) {
						cornerDataFormatted[x][y][0]+=360;
					}
					cornerDataFormatted[x][y][1] = cornerDataYX[ix++];
				}
			}
			
			float[] cornerDataNew = new float[129*129*2];
			int i=0;
			for (int y=0;y<129;y++) {
				for (int x=0;x<129;x++) {
					cornerDataNew[i++] = cornerDataFormatted[x][y][0];
					cornerDataNew[i++] = cornerDataFormatted[x][y][1];
				}
			}
			
			return cornerDataNew;*/
		}
		
	}
	
	
	/**
	 * @param pdsFile
	 * @param imageObjectName
	 * @param file
	 * @return
	 */
	public static PDSImage createAndLoad(PDSObject pdsFile, String imageObjectName, File file) {
		if (pdsFile==null) throw new RuntimeException("Cannot create BufferedImage from null");
	
		//this.pdsFile = pdsFile;
		
		/*
		 * Get data pointer (^) to image data
		 */
		String dataPointerUnits;
		int dataPointer;
		try {
			dataPointerUnits = pdsFile.getPointerUnits(imageObjectName);
			if (!dataPointerUnits.equals("BYTES")) throw new RuntimeException("Units ("+dataPointerUnits+") unknown for data pointer ^"+imageObjectName);
			dataPointer = Integer.parseInt(pdsFile.getPointerValue(imageObjectName));
			dataPointer--;
		} catch (PDSValueNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
	
		/*
		 * Get and check SAMPLE_TYPE of image
		 */
		PDSObject imageDescription;
		try {
			imageDescription = pdsFile.getPDSObject(imageObjectName);
		} catch (PDSObjectNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
		SampleType sampleType;
		//int sampleType;
		try {
			//sampleType = PDSValue_SAMPLE_TYPE.getType(imageDescription.getValueOfPDSValue("SAMPLE_TYPE"));
			sampleType = SampleType.valueOf(imageDescription.getValueOfPDSValue("SAMPLE_TYPE"));
		} catch (PDSValueNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		
	
		/*
		 * Get image dimensions and create empty image
		 */
		int width, height, sampleBits;
		try {
			width = Integer.parseInt(imageDescription.getValueOfPDSValue("LINE_SAMPLES"));
			height = Integer.parseInt(imageDescription.getValueOfPDSValue("LINES"));
			sampleBits = Integer.parseInt(imageDescription.getValueOfPDSValue("SAMPLE_BITS"));
		} catch (NumberFormatException e) {
			e.printStackTrace();
			return null;
		} catch (PDSValueNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	
		ByteParser byteParser = new ByteParser(width,height,sampleType,sampleBits);
	
		/*
		 * Read file and create settingsFile
		 */
	
		byteParser.readFileToData(file,dataPointer);
	
		PDSImage out = byteParser.toPDSImage();
		//out.setPDSFile(pdsFile);
		return out;
	}

}
