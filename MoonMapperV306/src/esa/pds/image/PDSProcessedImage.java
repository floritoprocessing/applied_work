package esa.pds.image;

import java.awt.image.BufferedImage;

import esa.pds.dictionary.SampleType;
import esa.pds.image.processing.PDSImageProcessorGeneric;
import esa.pds.image.processing.ProcessorStack;

public class PDSProcessedImage extends PDSImage {

	private ProcessorStack processorStack = new ProcessorStack();
	
	public PDSProcessedImage(int width, int height, SampleType sampleType, int sampleBits, float[] data) {
		super(width, height, sampleType, sampleBits, data);
	}

	public PDSProcessedImage(int width, int height, SampleType sampleType, int sampleBits, int[] data) {
		super(width, height, sampleType, sampleBits, data);
	}
	
	/**
	 * Creates a new PDSProcessedImage based on a PDSImage, by just referencing data
	 * Data is NOT copied!
	 * @param image
	 */
	public PDSProcessedImage(PDSImage image) {
		super();
		width = image.width;
		height = image.height;
		sampleType = image.sampleType;
		sampleBits = image.sampleBits;
		dataAsFloat = image.dataAsFloat;//new ImageData[image.dataAsFloat.length];
		//System.arraycopy(image.dataAsFloat, 0, dataAsFloat, 0, image.dataAsFloat.length);
		//pdsFile = image.pdsFile;//(PDSObject)image.pdsFile.clone();
	}

	public BufferedImage createBufferedImageWithProcessorStack() {
		//long ms = System.currentTimeMillis();
		BufferedImage out = new BufferedImage(width,height,BufferedImage.TYPE_INT_ARGB);
		int i=0, pixel, alpha;
		processorStack.prepare(width,height);
		for (int y=0;y<height;y++)
			for (int x=0;x<width;x++) {
				if (sampleType==SampleType.MSB_UNSIGNED_INTEGER && sampleBits==8) {
					//dataAsFloat[i] = 
					processorStack.processFloatData(dataAsFloat[i],x,y,width,height);//,processor);
					//if (dataAsInteger[i]!=null) {
						pixel = (int)(dataAsFloat[i].getPixelValue());
						alpha = (int)(dataAsFloat[i].getAlpha()*255);
						out.setRGB(x, y, alpha<<24|pixel<<16|pixel<<8|pixel);
					/*}
					else
						out.setRGB(x, y, 0x00000000);*/
				}
				else if (sampleType==SampleType.PC_REAL && sampleBits==32) {
					//dataAsFloat[i] = 
					processorStack.processFloatData(dataAsFloat[i],x,y,width,height);//,processor);
					//if (dataAsFloat[i]!=null) {
						pixel = (int)dataAsFloat[i].getPixelValue();
						alpha = (int)(dataAsFloat[i].getAlpha()*255);
						out.setRGB(x, y, alpha<<24|pixel<<16|pixel<<8|pixel);
					/*} else
						out.setRGB(x, y, 0x00000000);*/
					
				}
				i++;
			}
		//System.out.println(System.currentTimeMillis()-ms);
		return out;
	}
	
	public int[] createPixelArrayWithProcessorStack() {
		int[] out = new int[width*height];
		int i=0, pixel, alpha;
		processorStack.prepare(width,height);
		for (int y=0;y<height;y++)
			for (int x=0;x<width;x++) {
				processorStack.processFloatData(dataAsFloat[i],x,y,width,height);//,processor);
				alpha = (int)(dataAsFloat[i].getAlpha()*255);
				pixel = (int)(dataAsFloat[i].getPixelValue());
				out[i] = alpha<<24|pixel<<16|pixel<<8|pixel;
				i++;
			}
		return out;
	}

	public ProcessorStack getProcessorStack() {
		return processorStack;
	}
	
	public void addPDSImageProcessor(PDSImageProcessorGeneric imageProcessor) {
		processorStack.add(imageProcessor);
	}
	
}
