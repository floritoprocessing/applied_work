package esa.pds;

import java.io.*;

public class PDSFileProcessor {

	public static String[][] getValuesOfFiles(File[] files, String[] variablesToIndex) throws PDSValueNotFoundException {
		String[][] out = new String[files.length][variablesToIndex.length];
		for (int i=0;i<files.length;i++) {
			if (i%50==0) System.out.println("reading "+files[i].getName()+" ("+i+"/"+files.length+")");
			out[i] = getValuesOfFile(files[i],variablesToIndex);
		}
		return out;
	}
	
	/**
	 * Returns all PDS settingsFile of the supplied variables to Index as array of Strings. 
	 * Returns null if at least one variable was not found
	 * @param file
	 * @param variablesToIndex
	 * @return
	 */
	public static String[] getValuesOfFile(File file, String[] variablesToIndex) throws PDSValueNotFoundException {
		if (variablesToIndex==null) return null;
		if (variablesToIndex.length==0) return null;
		String[] out = new String[variablesToIndex.length];
		boolean[] found = new boolean[variablesToIndex.length];
		for (int i=0;i<found.length;i++) found[i]=false;

		FileReader fileReader;
		try {
			fileReader = new FileReader(file);
			BufferedReader reader = new BufferedReader(fileReader);
			String line;

			boolean eof=false;
			while (!eof) {
				try {
					if ((line=reader.readLine())==null) 
						eof=true;
					else if (line.startsWith("END ")){
						eof=true;
					} else {
						String tmp = line.replaceAll("^\\s+", ""); // remove leading white
						for (int i=0;i<variablesToIndex.length;i++) {
							if (!found[i])
								if (tmp.startsWith(variablesToIndex[i])) {
									found[i]=true;
									out[i]=tmp.split("=")[1].replaceAll("^\\s+", "").replaceAll("\\s+$", "").split(" ")[0].replaceAll("\"", "");
									boolean allFound = true;
									for (int j=0;j<variablesToIndex.length;j++)
										allFound = allFound & found[j];
									if (allFound) {
										//reader.
										reader.close();
										reader = null;
										fileReader.close();
										fileReader = null;
										return out;
									}
								}
						}
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			throw new PDSValueNotFoundException("One or more variables not found!");
			//return null;
			//throw new RuntimeException("One or more variables not found!");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		return null;
	}

}
