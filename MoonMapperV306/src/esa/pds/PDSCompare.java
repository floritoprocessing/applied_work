package esa.pds;

public interface PDSCompare {

	public String getVariableName();
	public boolean isBooleanCompare();
	public boolean compare(Object valueToCompare);
	public String[] toStrings();
}
