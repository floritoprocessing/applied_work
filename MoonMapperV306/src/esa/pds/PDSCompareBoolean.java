package esa.pds;

import java.util.Vector;

public class PDSCompareBoolean implements PDSCompare {

	private PDSCompare[] compares;
	public static final int TYPE_AND = 0;
	public static final int TYPE_OR = 1;
	private int type;

	public PDSCompareBoolean(int type, PDSCompare[] compares) {
		if (type!=TYPE_AND&&type!=TYPE_OR) throw new RuntimeException("Illegal type");
		this.type = type;
		this.compares = compares;
	}

	public String getVariableName() {
		return null;
	}

	public boolean isBooleanCompare() {
		return true;
	}

	public boolean compare(Object valueToCompare) {
		return false;
	}

	public boolean compare(String[] variableNames, String[] values) {
		boolean out = true;
		if (type==TYPE_AND) out = true;
		else if (type==TYPE_OR) out = false;
		for (int i=0;i<compares.length;i++) {
			if (compares[i].isBooleanCompare()) {
				boolean compareResult = ((PDSCompareBoolean)compares[i]).compare(variableNames, values);
				if (type==TYPE_AND && compareResult==false) return false;
					//if (compareResult==false) return false;
					//else out = out && compareResult;
				else if (type==TYPE_OR && compareResult==true) return true;
					//if (compareResult==true) return true;
					//out = out || compareResult;
			} else
				for (int j=0;j<variableNames.length;j++) {
					if (compares[i].getVariableName().equals(variableNames[j])) {
						boolean compareResult = compares[i].compare(values[j]);
						if (type==TYPE_AND && compareResult==false) return false;
							//if (compareResult==false) return false;
							//else out = out && compareResult;
						else if (type==TYPE_OR && compareResult==true) return true;
							//if (compareResult==true) return true;
							//else out = out || compareResult;//
					}
				}
		}
		return out;
	}

	public String[] toStrings() {
		Vector<String> out = new Vector<String>();
		out.add("new PDSCompareBoolean(PDSCompareBoolean.TYPE_"+(type==TYPE_AND?"AND":"OR")+", new PDSCompare[] {");
		for (int i=0;i<compares.length;i++) {
			String[] strings = compares[i].toStrings();
			for (int j=0;j<strings.length;j++)
				out.add("\t"+strings[j] +
						(i<compares.length-1&&j==strings.length-1?",":""));
		}
		out.add("})");
		String[] output = new String[out.size()];
		for (int i=0;i<output.length;i++) output[i]=out.elementAt(i);
		return output;
	}
	
	public String toString() {
		return toString("");
	}
	
	public String toString(String indent) {
		String boolWord = type==TYPE_AND?" AND ":" OR ";
		String out = indent+"";
		for (int i=0;i<compares.length;i++) {
			if (compares[i].isBooleanCompare())
				out += "\r\n"+indent+"\t("+((PDSCompareBoolean)compares[i]).toString(indent+"\t")+")\r\n" 
					+ indent + "\t"+(i<compares.length-1?boolWord:"");
			else
				out += "\r\n"+indent+"\t("+compares[i].toString()+")\r\n" 
					+ indent + "\t"+(i<compares.length-1?boolWord:"");
		}
		out += "\r\n"+indent+"";
		return out;
	}

}
