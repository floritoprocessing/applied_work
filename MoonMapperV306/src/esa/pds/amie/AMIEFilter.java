package esa.pds.amie;

import java.awt.Rectangle;
import static esa.pds.image.processing.PDSImageProcessorEdgeCutAndSmooth.*;

public class AMIEFilter {

	//public static final int HORIZONTAL_LO_RES_PIXELS = 128;	// horizontal corners: 129
	//public static final int VERTICAL_LO_RES_PIXELS = 128;   // vertical corners: 129
	
	public static final int LO_RES_WIDTH = 129;
	public static final int CORNER_INDEX_TO_LO_RES_CORNER_INDEX = 32;
	public static final int LO_RES_SIZE = 8;
	
	private static final int[] outerSmoothEdge = {
		LEFT+BOTTOM,
		LEFT,
		RIGHT+BOTTOM,
		LEFT,
		LEFT+TOP,
		TOP,
		TOP+RIGHT,
		TOP
	};
	
	private static final Rectangle[] filterRectangle = {
		new Rectangle(0,3,2,1),
		new Rectangle(0,2,2,1),
		new Rectangle(2,2,2,2),
		new Rectangle(0,1,2,1),
		new Rectangle(0,0,1,1),
		new Rectangle(2,0,1,2),
		new Rectangle(3,0,1,2),
		new Rectangle(1,0,1,2)
	};
	
	/*
	 *   0      1      2      3      4
	 * 0 -----------------------------
	 *   |      |      |      |      |
	 *   | LE 5 |  L   |  L   |  L   |
  	 *   |      |  E   |  E   |  E   |
	 * 1 -------|. . . |      |      |
	 *   |      .  8   |  6   |  7   |     
	 *   |   L E. 4    |      |      |
	 *   |      .      |      |      |
	 * 2 -----------------------------
	 *   |             |             |
	 *   |   L E  2    |             |
	 *   |             |             |
	 * 3 --------------|   L E  3    |
	 *   |             |             |
	 *   |   L E  1    |             |
	 *   |             |             |
	 * 4 -----------------------------
	 * 
	 */
	
	/*public static int[][] getClockwiseCornerIndex(int filterNumber) {
		return cornerIndex[filterNumber-1];
	}*/
	
	public static final int FILTER_1_VIS_Y = 0;
	public static final int FILTER_2_FeL_Y = 1;
	public static final int FILTER_3_NONE = 2;
	public static final int FILTER_4_FeH_Y = 3;
	public static final int FILTER_5_LASER = 4;
	public static final int FILTER_6_FeL_X = 5;
	public static final int FILTER_7_VIS_X = 6;
	public static final int FILTER_8_FeH_X = 7;
	
	public static final String[] names = new String[] {
		"VIS_Y",
		"FeL_Y",
		"NONE",
		"FeH_Y",
		"LASER",
		"FeL_X",
		"VIS_X",
		"FeH_X"
	};
	
	/*private static float[][] createAllNormalizedTexturePoints() {
		float[][] 
		return normalizedTexturePointsOfQuads;
	}*/
	
	private static final float[][] normalizedTexturePointsOfQuads;// = createAllNormalizedTexturePoints();
	static {
		normalizedTexturePointsOfQuads = new float[filterRectangle.length][];
		for (int filterIndex=0;filterIndex<filterRectangle.length;filterIndex++) {
			int coGridWidth = filterRectangle[filterIndex].width*CORNER_INDEX_TO_LO_RES_CORNER_INDEX;
			int coGridHeight = filterRectangle[filterIndex].height*CORNER_INDEX_TO_LO_RES_CORNER_INDEX;
			normalizedTexturePointsOfQuads[filterIndex] = new float[coGridWidth * coGridHeight * 4 * 2];
			int index1 = 0;
			for (int yq = 0; yq < coGridHeight; yq++) {
				for (int xq = 0; xq < coGridWidth; xq++) {		
					float u0 = (float)xq / coGridWidth;
					float u1 = (float)(xq + 1) / coGridWidth;
					float v0 = (float)yq / coGridHeight;
					float v1 = (float)(yq + 1) / coGridHeight;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = u0;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = v0;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = u1;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = v0;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = u1;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = v1;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = u0;
					normalizedTexturePointsOfQuads[filterIndex][index1++] = v1;
				}
			}
		}
	}
	
	
	public static Rectangle getFilterRectangle(String filterName) {
		for (int i=0;i<names.length;i++) {
			if (filterName.equals(names[i])) return getFilterRectangle(i);
		}
		return null;
	}
	
	public static int getOuterSmoothEdge(String filterName) {
		for (int i=0;i<outerSmoothEdge.length;i++) {
			if (filterName.equals(names[i])) return outerSmoothEdge[i];
		}
		return 0;
	}
	
	public static int getFilterIndex(String filterName) {
		for (int i=0;i<names.length;i++)
			if (filterName.equals(names[i])) return i;
		throw new RuntimeException("Unknown filter named "+filterName);
	}
	
	public static Rectangle getFilterRectangle(int filterIndex) {
		return filterRectangle[filterIndex];
	}

	public static float[] getNormalizedTexturePointsOfFilter(String filterName) {
		return getNormalizedTexturePointsOfFilter(getFilterIndex(filterName));
	}
	
	private static float[] getNormalizedTexturePointsOfFilter(int filterIndex) {
		return normalizedTexturePointsOfQuads[filterIndex];
	}
	
	

	
}
