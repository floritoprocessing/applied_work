package esa.pds;

import esa.pds.component.PDSObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Vector;

import javax.swing.SwingWorker;

public class CSVFileWriter {
	
	public static final String PROPERTY_LINES_PARSED = "linesParsed";

	private BufferedWriter writer;

	public CSVFileWriter(String filename) {
		try {
			writer = new BufferedWriter(new FileWriter(filename));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public CSVFileWriter(File file) {
		try {
			writer = new BufferedWriter(new FileWriter(file));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public boolean processAndSave(CSVFileLoader loader, PDSFileConstrain constrain, int entriesToRead) {
		return processAndSave(loader, constrain, entriesToRead, null);
	}
	
	public boolean processAndSave(CSVFileLoader loader, PDSFileConstrain constrain, int entriesToRead, SwingWorker propertyEventTarget) {

		int totalcount = 0;
		
		boolean variablesSet = false;
		Vector<PDSObject> pdsFiles = new Vector<PDSObject>();
		boolean finished = false;

		String[] variableNames = new String[0];

		while (!finished) {

			pdsFiles = new Vector<PDSObject>();
			finished = loader.load(pdsFiles,entriesToRead);

			if (!variablesSet) {
				variableNames = pdsFiles.elementAt(0).getAllPDSValueNames();

				try {
					for (int i=0;i<variableNames.length;i++)
						writer.write(variableNames[i]+(i<variableNames.length-1?", ":""));
					writer.newLine();
				} catch (IOException e) {
					e.printStackTrace();
				}


				variablesSet = true;
			}

			//System.out.println("loaded "+pdsFiles.size()+" lines");
			if (propertyEventTarget!=null) propertyEventTarget.firePropertyChange(PROPERTY_LINES_PARSED, totalcount-1, totalcount);
			
			for (int i=0;i<pdsFiles.size();i++) {
				
				totalcount++;
				String[] values = null;
				try {
					values = pdsFiles.elementAt(i).getPDSValuesAsStrings(variableNames);
				} catch (PDSValueNotFoundException e1) {
					e1.printStackTrace();
				}

				if (values!=null) {

					boolean addOutput = constrain.withinRange(variableNames, values);
					if (addOutput) {
						try {
							for (int c=0;c<values.length;c++)
								writer.write(values[c]+(c<values.length-1?", ":""));
							writer.newLine();
						}
						catch (IOException e) {
							e.printStackTrace();
						}
						//System.out.println(((PDSFile)pdsFiles.elementAt(i)).toString());
					}
				}
			}
			
			try {
				writer.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}


			if (finished) {
				try {
					writer.flush();
					writer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}

				return true;
			}
		}


		return false;
	}



}
