package esa.pds;

import esa.pds.component.PDSObject;
import esa.pds.component.PDSValue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class CSVFileLoader {

	private BufferedReader reader;
	
	private String[] variableNames = new String[0];

	public CSVFileLoader(String filename) {
		try {
			reader = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public CSVFileLoader(File file) {
		try {
			reader = new BufferedReader(new FileReader(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public boolean load(Vector<PDSObject> pdsFiles, int totalEntriesToRead) {

		boolean thisEnd = false; 
		boolean comment = false;
		String line = "";
		int entriesToRead = totalEntriesToRead;

		try {
			while ( ((line=reader.readLine())!=null) && !(entriesToRead==0) ) {

				if (line.startsWith("/*")) comment = true;
				if (!comment) {
					String[] columns = line.split(",");
					for (int i=0;i<columns.length;i++) {
						columns[i] = columns[i].replaceAll("^\\s+", ""); // remove leading white;
						columns[i] = columns[i].replaceAll("\\s+$", ""); // remove trailing white;
					}
					if (variableNames.length==0) {
						variableNames = new String[columns.length];
						for (int i=0;i<columns.length;i++) {
							variableNames[i] = columns[i];
						}
					} else {
						PDSValue[] pdsValue = new PDSValue[columns.length];
						for (int i=0;i<columns.length;i++) pdsValue[i] = new PDSValue(variableNames[i],columns[i]);
						PDSObject entry = new PDSObject(pdsValue);
						pdsFiles.add(entry);
						entriesToRead--;
					}
				}
				thisEnd = (entriesToRead==0);
				if (line.contains("*/")) comment = false;
			}
			
			if (!thisEnd) {
				try {
					reader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				return true;
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return false;
	}

}
