package esa.pds;

//import esa.pds.component.PDSFile;
import esa.pds.component.PDSObject;
import esa.pds.component.PDSValue;
import esa.pds.component.PDSValueCreator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class PDSFileLoader {

	/**
	 * Creates a PDSFile with the header portion only!
	 */
	public static PDSObject loadHeader(File file) {

		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = "";
			/*
			 * Load Header
			 */
			boolean headerLoaded = false;
			boolean comment=false;
			boolean object=false, objectStart=false, objectFirstLineDone = false, objectFinalize=false;

			PDSObject out = new PDSObject();

			PDSObject pdsObject = null;
			String inputString = "";
			
			while ( (line=reader.readLine()) !=null && !headerLoaded) {

				if (line.startsWith("END ")) {
					headerLoaded=true;
				} else {

					String noSpace = line.replaceAll("\\s+", "");
					if (noSpace.length()>0) {
						String noBeginSpace = line.replaceAll("^\\s+","");
						
						if (noBeginSpace.startsWith("/*")) {
							String noEndSpace = noBeginSpace.replaceAll("\\s+$", "");
							if (noEndSpace.endsWith("*/")) {
								comment = true;
							} else {
								throw new RuntimeException("Multiline comment not implemented!");
							}
						}
						
						if (comment) {
							out.addNewComment(noBeginSpace);
							comment = false;
						}

						else {

							if (noBeginSpace.startsWith("OBJECT")) {
								object = true;
								objectStart = true;
							}
							if (noBeginSpace.startsWith("END_OBJECT")) {
								objectFinalize = true;
							}
							

							String[] split = line.split("=");
							
							
							// find new definition line like "a = 33 <deg>"
							
							if (split.length==2) {
								// finalize last pds Value / object
								
								if (inputString.length()>0) {
									
									
									if (!object){
										PDSValue pdsValue = PDSValueCreator.createFromPDSString(inputString);
										out.addNewValue(pdsValue);
										//System.out.println("-> add "+pdsValue.toString());
									}
									
									else {
										if (objectStart) {
											PDSValue tVal = PDSValueCreator.createFromPDSString(line);
											pdsObject = new PDSObject(tVal.getValue());
											objectStart = false;
											objectFirstLineDone = true;
										} else if (objectFirstLineDone) {
											objectFirstLineDone = false;
											//System.out.println("START new PDSObject "+pdsValue.getName());
										} else if (!objectFinalize) {
											//System.out.println("ADD "+pdsValue.toString());
											PDSValue pdsValue = PDSValueCreator.createFromPDSString(inputString);
											pdsObject.addNewValue(pdsValue);
										} else {
											//System.out.println("-> add PDSObject "+pdsObject.toString());
											PDSValue pdsValue = PDSValueCreator.createFromPDSString(inputString);
											pdsObject.addNewValue(pdsValue);
											out.addNewObject(pdsObject);
											object=false;
											objectFinalize=false;
										}
									}
									
								}
								
								// start definition
								line = line.replaceAll("\\s+$", "");
								inputString = line.replaceAll("^\\s+", "");
							} else {
								// continue last definition and finalize
								line = line.replaceAll("\\s+$", "");
								inputString += line.replaceAll("^\\s+", "");
							}

						}

					}
				}
			}
			
			return out;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

}
