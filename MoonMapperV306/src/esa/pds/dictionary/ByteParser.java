package esa.pds.dictionary;

import java.io.File;

import esa.pds.image.PDSImage;
import esa.file.tools.BinaryFileReader;

public class ByteParser {

	private int width, height, pixelAmount, sampleBits;
	private SampleType sampleType;
	private int bytesToRead;

	int[] inArray;
	int[] intArray;// = new int[width*height];
	float[] floatArray;// = new double[width*height];
	int addIndex=0;

	//PDSValue_SAMPLE_TYPE
	//public ByteParser(int width, int height, int sampleType, int sampleBits) {
	public ByteParser(int width, int height, SampleType sampleType, int sampleBits) {
		this.width = width;
		this.height= height;
		pixelAmount = width*height;
		this.sampleType = sampleType;
		this.sampleBits = sampleBits;
		if (sampleType==SampleType.MSB_UNSIGNED_INTEGER && sampleBits==8) {
			bytesToRead = width * height;
			inArray = new int[bytesToRead];
			intArray = new int[bytesToRead];

		}
		else if (sampleType==SampleType.PC_REAL && sampleBits==32) {
			bytesToRead = 4 * width * height;
			inArray = new int[bytesToRead];
			floatArray = new float[width*height];
		}
		else 
			throw new RuntimeException("Cannot create parser, unknown sampleType/sampleBits combination ");
	}

	public int getBytesToRead() {
		return bytesToRead;
	}

	public int getPixelAmount() {
		return width*height;
	}
	
	public void readFileToData(File file, int dataPointer) {
		BinaryFileReader reader = new BinaryFileReader(file);
		reader.read();
		if (sampleType==SampleType.MSB_UNSIGNED_INTEGER && sampleBits==8) {
			intArray = reader.getByteInt(dataPointer, pixelAmount);
		}
		else if (sampleType==SampleType.PC_REAL && sampleBits==32) {
			floatArray = reader.get4ByteFloats(dataPointer, pixelAmount);
		}
	}
	
	/*public void parseData() {
		int byteCount=0;
		int[] bytesBy4 = new int[4];
		int index=0;
		float vMin=Float.MAX_VALUE, vMax=Float.MIN_VALUE;
		for (int i=0;i<bytesToRead;i++) {
			if (sampleType==PDSValue_SAMPLE_TYPE.MSB_UNSIGNED_INTEGER && sampleBits==8) {
				intArray[i]=inArray[i];
			}
			else if (sampleType==PDSValue_SAMPLE_TYPE.PC_REAL && sampleBits==32) {
				
				bytesBy4[byteCount] = inArray[i];
				//System.out.println(bytesBy4[byteCount]);
				
				byteCount++;
				if (byteCount==4) {
					floatArray[index] = byte4ToFloat(bytesBy4,0);
					if (floatArray[index]<vMin) vMin=floatArray[index];
					if (floatArray[index]>vMax) vMax=floatArray[index];
					byteCount=0;
					index++;
				}
			}
		}
		System.out.println(vMin+".."+vMax);
	}*/


	/*public void setFloatArray(float[] input) {
		this.floatArray = new float[input.length];//]
		System.arraycopy(input, 0, floatArray, 0, input.length);
	}*/
	
	public PDSImage toPDSImage() {
		if (sampleType==SampleType.MSB_UNSIGNED_INTEGER && sampleBits==8) {
			return new PDSImage(width,height,sampleType,sampleBits,intArray);
		}
		else if (sampleType==SampleType.PC_REAL && sampleBits==32) {
			return new PDSImage(width,height,sampleType,sampleBits,floatArray);
		}
		return null;
	}

	/*private static float byte4ToFloat(int[] arr, int start) {
		int i = 0;
		int len = 4;
		int cnt = 0;
		int[] tmp = new int[len]; // was byte
		for (i = start; i < (start + len); i++) {
			tmp[cnt] = arr[i];
			cnt++;
		}
		int accum = 0;
		i = 0;
		for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 ) {
			accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
			i++;
		}
		return Float.intBitsToFloat(accum);
	}*/

	/*public void add(int in) {
		//System.out.println("|"+in+"|");
		inArray[addIndex]=in;//&0xff;
		addIndex++;
	}*/
}
