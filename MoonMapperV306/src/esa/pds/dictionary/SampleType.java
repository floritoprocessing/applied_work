package esa.pds.dictionary;

public enum SampleType {

	IEEE_REAL,
	LSB_INTEGER,
	LSB_UNSIGNED_INTEGER,
	MSB_INTEGER,
	MSB_UNSIGNED_INTEGER,
	PC_REAL,
	UNSIGNED_INTEGER,
	VAX_REAL;
	
}