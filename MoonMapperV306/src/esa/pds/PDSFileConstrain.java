package esa.pds;

import java.util.Vector;

public class PDSFileConstrain {

	Vector<PDSCompare> constrain;

	public PDSFileConstrain() {
		constrain = new Vector<PDSCompare>();
	}

	public void addPDSCompareConstrain(PDSCompare compare) {
		if (compare!=null) constrain.add(compare);
	}

	public boolean withinRange(String[] variableNames, String[] values) {
		boolean out = true;
		for (int i=0;i<constrain.size();i++) {

			PDSCompare compare = constrain.elementAt(i);

			boolean compareResult=false;
			if (!compare.isBooleanCompare()) {
				for (int j=0;j<variableNames.length;j++)
					if (compare.getVariableName().equals(variableNames[j]))
						compareResult = compare.compare(values[j]);
			} else {
				//out = out & 
				compareResult = ((PDSCompareBoolean)compare).compare(variableNames,values);
			}
			if (compareResult==false) return false;
			//out = out & compareResult;

			//out = out & (PDSCompare)constrain.elementAt(i)
		}
		return out;
	}
	
	public String[] getPDSCompareVariableNames() {
		if (constrain==null || constrain.size()==0) return null;
		String[] out = new String[constrain.size()];
		for (int i=0;i<out.length;i++)
			out[i] = constrain.elementAt(i).getVariableName();
		return out;
	}

	public String[] toStrings() {
		Vector<String> strings = new Vector<String>();
		strings.add("/*");
		strings.add("variableConstrain = new PDSFileConstrain();");
		for (int i=0;i<constrain.size();i++) {
			strings.add("variableConstrain.addPDSCompareConstrain(");
			String[] constrainStrings = constrain.elementAt(i).toStrings();
			for (int j=0;j<constrainStrings.length;j++)
				strings.add("\t"+constrainStrings[j]);
			strings.add(");");
		}
		strings.add("*/");
		String[] out = new String[strings.size()];
		for (int i=0;i<strings.size();i++) out[i]=(String)strings.elementAt(i);
		return out;
	}

	public String toString() {
		String out = "";
		String[] lines = toStrings();
		for (int i=0;i<lines.length;i++) out += lines[i]+(i<lines.length-1?"\r\n":"");
		return out;
	}

}
