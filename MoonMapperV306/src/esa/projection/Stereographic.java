package esa.projection;

public class Stereographic extends Projection {

	//private double k;
	
	private double lonRad, latRad;
	//private double R = 1;
	
	public Stereographic(float longitudeCenter, float latitudeCenter) {
		lonDeg = longitudeCenter;
		latDeg = latitudeCenter;
		
		lonRad = lonDeg*Math.PI/180.0;
		latRad = latDeg*Math.PI/180.0;
		
		//double R = 1;//sphereRadius;
		//double div = 1 + 
		//	Math.sin(latRad)
	}
	
	public Stereographic(float longitudeCenter, float latitudeCenter, float degreeToPixelScale) {
		this(longitudeCenter,latitudeCenter);
		if (degreeToPixelScale==0) throw new RuntimeException("degreeToPixelScale can not be zero!");
		this.scale = degreeToPixelScale;
	}
	
	public void setScale(float imageWidthInPixels, float imageWidthInDegree) {
		float d = imageWidthInPixels/2;
		double fi = imageWidthInDegree*Math.PI/180.0;
		scale = (float)(d/Math.cos(fi));
		//System.out.println("Radius is "+R);
	}

	@Override
	public Type getType() {
		return Type.STEREOGRAPHIC;
	}
	/*
	 * 
	 * has been tested: this works!
	 * double[] projectPoleFast(double LON, double LAT, double R) {
	 *   double fac = -2 * R * Math.cos(LAT) / (1+Math.sin(Math.abs(LAT)));
	 *   double x = fac * Math.sin(LON);
	 *   double z = fac * Math.cos(LON);
	 *   return new double[] { x,z };
	 * }
	 * 
	 * (non-Javadoc)
	 * @see esa.projection.Projection#lonLatToXY(float, float)
	 */

	@Override
	public float[] lonLatToXY(float longitude, float latitude)
			throws ProjectionInputException {
		if (latitude<-90||latitude>90) throw new ProjectionInputException("Illegal latitude ("+latitude+"), please stay within -90..90");

		double lat = latitude*Math.PI/180.0;
		double lon = longitude*Math.PI/180.0;
		
		//double R = 1;// sphere of radius R
		
		double div = 1
			+ Math.sin(latRad)*Math.sin(lat)
			+ Math.cos(latRad)*Math.cos(lat)*Math.cos(lon-lonRad);
		
		double k = 2 * scale / div;
		
		double x = k * Math.cos(lat) * Math.sin(lon-lonRad);
		double y = k * (Math.cos(latRad)*Math.sin(lat)-Math.sin(latRad)*Math.cos(lat)*Math.cos(lon-lonRad));
		
		return new float[] {(float)x, (float)y};
	}

}
