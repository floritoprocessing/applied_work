package esa.projection;

public class Mercator extends Projection {

	//private double lonRad;
	private final double yOffset;
	
	public Mercator(float longitudeCenter, float latitudeCenter) {
		lonDeg = longitudeCenter;
		latDeg = latitudeCenter;
		yOffset = latRadiansToY(Math.toRadians(latitudeCenter));
		//lonRad = lonDeg*Math.PI/180.0;
	}
	
	public Mercator(float longitudeCenter, float latitudeCenter, float degreeToPixelScale) {
		this(longitudeCenter,latitudeCenter);
		if (degreeToPixelScale==0) throw new RuntimeException("degreeToPixelScale can not be zero!");
		this.scale = degreeToPixelScale;
	}
	
	@Override
	public Type getType() {
		return Type.MERCATOR;
	}
	
	public float[] lonLatToXY(float longitude, float latitude) throws ProjectionInputException {
		if (latitude<-90||latitude>90) throw new ProjectionInputException("Illegal latitude ("+latitude+"), please stay within -90..90");
		//double lon_rad = Math.toRadians(longitude);//*Math.PI/180.0;
		double lat_rad = Math.toRadians(latitude);//latitude*Math.PI/180.0;
		//double delta_lon_rad = lon_rad - lonRad;
		//double x = delta_lon_rad*180.0/Math.PI*scale;
		float x = (longitude - lonDeg)*scale;
		// y-value with latitudeCenter at equator
		double y = (latRadiansToY(lat_rad) - yOffset)*scale;
		return new float[] { x, (float)y };
	}
	
	private double latRadiansToY(double latRadians) {
		return -Math.log( Math.tan(0.25*Math.PI+0.5*latRadians)  )*180/Math.PI;
	}
}
	
