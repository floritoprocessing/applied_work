package esa.projection;

public class Rectangular extends Projection {

	public Rectangular(float longitudeCenter, float latitudeCenter) {
		lonDeg = longitudeCenter;
		latDeg = latitudeCenter;
		//lonRad = (float)Math.toRadians(lonDeg);
		//latRad = (float)Math.toRadians(latDeg);
	}
	
	public Rectangular(float longitudeCenter, float latitudeCenter, float degreeToPixelScale) {
		this(longitudeCenter,latitudeCenter);
		if (degreeToPixelScale==0) throw new RuntimeException("degreeToPixelScale can not be zero!");
		this.scale = degreeToPixelScale;
	}
	
	@Override
	public Type getType() {
		return Type.RECTANGULAR;
	}
	
	public float[] lonLatToXY(float longitude, float latitude) throws ProjectionInputException {
		if (latitude<-90||latitude>90) throw new ProjectionInputException("Illegal latitude ("+latitude+"), please stay within -90..90");
		
		//while (longitude<0) longitude+=360;
		//while (longitude>360) longitude-=360;
		
		float lonToX = (longitude-lonDeg)*scale;
		float latToY = - (latitude-latDeg)*scale;
		
		return new float[] { lonToX, latToY };
	}

}
