package esa.projection;

public class ProjectionInputException extends IllegalArgumentException {

	private static final long serialVersionUID = -1159566239693065936L;

	public ProjectionInputException(String msg) {
		super(msg);
	}

}
