package esa.projection;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

public abstract class Projection {

	public enum Type {
		RECTANGULAR ("Rectangular"),
		MERCATOR ("Mercator"),
		STEREOGRAPHIC ("Stereographic");
		private final String name;
		Type(String name) {
			this.name = name;
		}
		public String getName() {
			return name;
		}
		@Override
		public String toString() {
			return name;
		}
	}

	protected float scale= 0f;
	protected float lonDeg = 0;
	protected float latDeg = 0;
	//protected float lonRad = 0;
	//protected float latRad = 0;
	
	public abstract Type getType();
	
	public abstract float[] lonLatToXY(float longitude, float latitude) throws ProjectionInputException;
		
	/**
	 * Returns center longitude in degree
	 * @return
	 */
	public float getCenterLongitude() {
		return lonDeg;
	}
	
	/**
	 * Returns center latitude in degree
	 * @return
	 */
	public float getCenterLatitude() {
		return latDeg;
	}
	
	public float getScale() {
		return scale;
	}
	
	public void setScale(float scale) {
		this.scale = scale;
	}
	
	private static Class<? extends Projection> getProjectionClass(Type type) {
		switch (type) {
		case RECTANGULAR: return Rectangular.class;
		case MERCATOR: return Mercator.class;
		case STEREOGRAPHIC: return Stereographic.class;
		default: return null;
		}
	}
	
	public static Projection getInstance(Type type, float centerLon, float centerLat, float pixelScale) {
		try {
			Class<? extends Projection> projectionClass = Projection.getProjectionClass(type);
			Constructor<? extends Projection> constructor = projectionClass.getConstructor(float.class,float.class,float.class);
			Projection projection = constructor.newInstance(centerLon,centerLat,pixelScale);
			return projection;
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
		throw new RuntimeException("Something wrong in getInstance() of Projection. Invoked with: "+type+", "+centerLon+", "+centerLat+", "+pixelScale);
	}
	
	public String toString() {
		return "Projection "+getType()+" centered at "+lonDeg+" / "+latDeg+" degrees, with scale "+scale;
	}
	
	
}
