package esa.projection;

import processing.core.PApplet;
import processing.core.PGraphics;

public class ProjectionRenderer {

	public static float[][] projectQuad(float[][] coordinates, Projection projection) {
		float[][] out = new float[coordinates.length][2];
		for (int i=0;i<coordinates.length;i++) {
			out[i] = projection.lonLatToXY(coordinates[i][0], coordinates[i][1]);
		}
		return out;
	}
	
	
	/**
	 * 
	 * @param sourceHeight 
	 * @param sourceWidth 
	 * @param cornerData array x / y / lonLat
	 * @param imageDataAsFloat array xy (with y slow increasing)
	 * @param projection
	 * @param projected
	 */
	public static void project(
			int sourceWidth, int sourceHeight, 
			float[][][] cornerData, 
			Float[] imageDataAsFloat, 
			float gain,
			Projection projection, 
			PGraphics projected) {
		
		projected.beginDraw();
		projected.background(255);
		projected.endDraw();
		
		int projectedXcenter = projected.width/2;
		int projectedYcenter = projected.height/2;
		
		//TODO: Convert corners of pixels to x/y settingsFile
		
		int index=0;
		for (int y=0;y<sourceHeight;y++) {
			for (int x=0;x<sourceWidth;x++) {

				int pixelValue = Math.max(0 , Math.min(255, (int)(imageDataAsFloat[index].floatValue() * gain)));

				//TODO: get corners of pixels (x/y settingsFile)
				float[] ll1 = cornerData[x][y];
				float[] ll2 = cornerData[x+1][y];
				float[] ll3 = cornerData[x+1][y+1];
				float[] ll4 = cornerData[x][y+1];

				//TODO: convert into xy
				//try {
					float[] p1 = projection.lonLatToXY(ll1[0], ll1[1]);
					float[] p2 = projection.lonLatToXY(ll2[0], ll2[1]);
					float[] p3 = projection.lonLatToXY(ll3[0], ll3[1]);
					float[] p4 = projection.lonLatToXY(ll4[0], ll4[1]);
					
					//TODO: draw into projected
					projected.beginDraw();
					projected.noStroke();
					projected.fill(0xFF<<24 | pixelValue<<16 | pixelValue<<8 | pixelValue);
					projected.beginShape(PApplet.QUADS);
					projected.vertex(projectedXcenter + p1[0], projectedYcenter - p1[1]);
					projected.vertex(projectedXcenter + p2[0], projectedYcenter - p2[1]);
					projected.vertex(projectedXcenter + p3[0], projectedYcenter - p3[1]);
					projected.vertex(projectedXcenter + p4[0], projectedYcenter - p4[1]);
					projected.endShape();
					projected.endDraw();
					
					
				/*} catch (ProjectionInputException e) {
					e.printStackTrace();
				}*/

				index++;
			}
		}
		

	}

}
