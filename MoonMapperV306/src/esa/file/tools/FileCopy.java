package esa.file.tools;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileCopy {

	public static void copyWithTempFile(File inputFile, File outputFile) throws IOException {
		File tempFile = File.createTempFile("moonMapperTemp", "moon");
		
		copy(inputFile,tempFile);
		copy(tempFile,outputFile);
		tempFile.delete();
		
	}
	
	public static void copy(File inputFile, File outputFile) throws IOException {

		FileReader in = new FileReader(inputFile);
		FileWriter out = new FileWriter(outputFile);
		
		
		int c;

		while ((c = in.read()) != -1)
			out.write(c);

		in.close();
		out.close();
	}
	
}
