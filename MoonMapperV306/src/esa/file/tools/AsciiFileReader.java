package esa.file.tools;

import java.io.File;
import java.util.Vector;

public class AsciiFileReader extends GenericFileReader {
	
	public AsciiFileReader(String filename) {
		super(filename);
	}
	
	public AsciiFileReader(File file) {
		super(file);
	}

	public String[] getStrings(int offset, char[] splitChars) {
		Vector<String> words = new Vector<String>();
		boolean lastCharIsSplit = true;
		int wordStart=0;
		for (int i=0;i<bytes.length;i++) {
			char c = (char)bytes[i];
			boolean charIsSplit = false;//splitChars;//(c==' ' || c=='\t' || c=='\n' || c=='\r');
			for (int ci=0;ci<splitChars.length;ci++)
				charIsSplit = charIsSplit || (c==splitChars[ci]);
			if (lastCharIsSplit&&!charIsSplit) {
				wordStart=i;
			}
			if (!lastCharIsSplit&&charIsSplit) {
				byte[] tmp = new byte[i-wordStart];
				for (int j=0;j<tmp.length;j++) tmp[j]=bytes[wordStart+j];
				String s = new String(tmp);
				if (offset==0)
					words.add(s);
				else
					offset--;
			}
			
			lastCharIsSplit = charIsSplit;
		}
		String[] out = new String[words.size()];
		for (int i=0;i<out.length;i++)
			out[i] = words.elementAt(i);
		return out;
	}
	
	/**
	 * Converts the read byte array into an array of strings.<br>
	 * Please use read() first. <br>
	 * Converts the byte array into Strings and splits them by space, tab, new line and carriage return characters.<br>
	 * Returns these strings as an array of strings.
	 * @param offset amount of bytes to skip (for i.e. headers)
	 * @return the array of strings
	 */
	public String[] getStrings(int offset) {
		return getStrings(offset,new char[] {' ','\t','\n','\r'});
	}
	
	public String[] getStrings(int offset, char splitChar) {
		return getStrings(offset,new char[] {splitChar});
	}

}
