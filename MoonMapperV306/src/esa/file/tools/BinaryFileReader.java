package esa.file.tools;

import java.io.File;

public class BinaryFileReader extends GenericFileReader {

	public BinaryFileReader(String filename) {
		super(filename);
	}
	
	public BinaryFileReader(File file) {
		super(file);
	}

	public static float arr2float(byte[] arr, int start) {
		int i = 0;
		int len = 4;
		int cnt = 0;
		byte[] tmp = new byte[len];
		for (i = start; i < (start + len); i++) {
			tmp[cnt] = arr[i];
			cnt++;
		}
		int accum = 0;
		i = 0;
		for ( int shiftBy = 0; shiftBy < 32; shiftBy += 8 ) {
			accum |= ( (long)( tmp[i] & 0xff ) ) << shiftBy;
			i++;
		}
		return Float.intBitsToFloat(accum);
	}
	
	/**
	 * Create an array of (4-byte) floats based on this file <BR>
	 * Please use read() first. <br>
	 * You can specify a byte offset (for i.e. headers) and an amount of bytes to read.<br>
	 * If the amount you specify is beyond the limits of the protected byte array, an error is thrown
	 * @param offset offset in amount of bytes
	 * @param amount amount 4-byte floats to create (uses 4-bytes per float)
	 * @return the float array
	 */
	public float[] get4ByteFloats(int offset, int amount) {
		float[] out = new float[amount];
		int index=0;
		int endI = offset+amount*4;
		for (int i=offset;i<endI;i+=4) {
			out[index] = arr2float(bytes,i);
			index++;
		}
		return out;
	}
	
	public int[] getByteInt(int offset, int amount) {
		int[] out = new int[amount];
		int index = 0;
		for (int i=offset;i<offset+amount;i++) {
			out[index] = (int)bytes[i];
			if (out[index]<0) out[index]+=255;
			index++;
		}
		return out;
	}
	
	

}
