package esa.file.tools;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

public class GenericFileReader {

	private String filename;
	private File file;
	protected byte[] bytes;
	
	/**
	 * Create a new GenericFileReader
	 * @param filename
	 */
	public GenericFileReader(String filename) {
		this.filename = filename;
	}
	
	/**
	 * Create a new GenericFileReader
	 * @param file
	 */
	public GenericFileReader(File file) {
		this.file = file;
	}

	/**
	 * Reads the entire file into the protected array <i>bytes</i>
	 */
	/*public void readV0() {
		try {
			if (file==null)
				file = new File(filename);
			InputStream is = new FileInputStream(file);
			DataInputStream dis = new DataInputStream( is );
			
			long length = file.length();
			if (length > Integer.MAX_VALUE) {
				throw new IOException("File is too large");
			} else {
				bytes = new byte[(int)length];
				int offset = 0;
				int numRead = 0;
				while (offset < bytes.length && 
						(numRead = is.read(bytes, offset, bytes.length-offset) ) >= 0) {
					offset += numRead;
				}
				if (offset < bytes.length) {
					throw new IOException("Could not completely read file "+file.getName());
				}
				dis.close();
				is.close();
				//System.out.println(file.getName()+" nr of bytes = "+offset);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}*/
	
	/**
	 * Reads the entire file into the protected array <i>bytes</i>
	 */
	public void read() {
		if (file==null) file=new File(filename);
		try {
			BufferedInputStream stream = new BufferedInputStream(new FileInputStream(file));
			long length = file.length();
			
			try {
				if (length > Integer.MAX_VALUE) {
					throw new RuntimeException("Cannot create byte array of length "+length);
				} else {
					bytes = new byte[(int)length];
					int offset=0;
					int numRead=0;
					while (offset< bytes.length &&
							(numRead = stream.read(bytes, offset, bytes.length-offset) ) >= 0) {
						offset += numRead;
					}
					if (offset<bytes.length) {
						throw new RuntimeException("Could not completely read the file "+file.getName());
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

}
