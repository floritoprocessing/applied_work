package esa.tools;

public class Timer {

	private boolean systemOut = false;
	private String message = "";
	private long start = 0;
	private long lastTime = 0;
	
	private double averageTime = 0;
	private double smooth = 0.95;
	
	public Timer() {
		start("");
	}
	
	public Timer(boolean systemOut) {
		this();
		this.systemOut = systemOut;
	}
	
	public Timer(boolean systemOut, double smooth) {
		this(systemOut);
		setSmooth(smooth);
	}
	
	
	
	public void start(String message) {
		this.message = message;
		start = System.nanoTime();
	}
	
	public void start() {
		start("");
	}
	
	public float stop() {
		lastTime = System.nanoTime()-start;
		averageTime = smooth*averageTime + (1-smooth)*lastTime;
		float out = ((int)(lastTime/100000.0))/10.0f;
		if (systemOut) System.out.println(message+" "+out);
		return out;
	}
	
	public int getLastTime() {
		return (int)(lastTime/1000000);
	}
	
	public void setSmooth(double smooth) {
		this.smooth = Math.min(0.999,Math.max(0,smooth));
	}
	
	public double getAverageTime() {
		return (averageTime/1000000);
	}
}
