package math;

/**
 * Vec class for 3-dimensional vectors.
 * Class is good for scaling, adding, subtracting, dividing, rotating vectors
 * @author mgraf
 *
 */
public class Vec {
	
	private double X=0, Y=0, Z=0;
	
	/**
	 * Creates the nullvector
	 */
	public Vec() {
	}
	
	/**
	 * Creates a new vector with value x, y, z
	 * @param x
	 * @param y
	 * @param z
	 */
	public Vec(double x, double y, double z) {
		X = x;
		Y = y;
		Z = z;
	}
	
	/**
	 * Creates a new vector with the array of doubles as input
	 * @param xyz array of double
	 */
	public Vec(double[] xyz) {
		X = xyz[0];
		Y = xyz[1];
		Z = xyz[2];
	}
	
	/**
	 * Creates a new vector based on the input vector v
	 * @param v the input vector
	 */
	public Vec(Vec v) {
		X = v.getX();
		Y = v.getY();
		Z = v.getZ();
	}
	
	/**
	 * Returns the x value of the vector
	 * @return double
	 */
	public double getX() {
		return X;
	}
	
	/**
	 * Returns the y value of the vector
	 * @return double
	 */
	public double getY() {
		return Y;
	}
	
	/**
	 * Returns the z value of the vector
	 * @return double
	 */
	public double getZ() {
		return Z;
	}
	
	/**
	 * Adds the Vector v to the current vector
	 * @param v the vector to add
	 */
	public void add(Vec v) {
		X += v.X;
		Y += v.Y;
		Z += v.Z;
	}
	
	/**
	 * scales the vector with n
	 * @param n scale factor
	 */
	public void mul(double n) {
		X *= n;
		Y *= n;
		Z *= n;
	}
	
	/**
	 * rotates the vector around the X-axis
	 * @param rd rotation in radians
	 */
	public void rotX(double rd) {
		double SIN=Math.sin(rd); 
		double COS=Math.cos(rd);
		double yn=Y*COS-Z*SIN;
		double zn=Z*COS+Y*SIN;
		Y=yn;
		Z=zn;
	}
	
	/**
	 * rotates the vector around the Y-axis
	 * @param rd rotation in radians
	 */
	public void rotY(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=X*COS-Z*SIN; 
		double zn=Z*COS+X*SIN;
		X=xn;
		Z=zn;
	}

	/**
	 * rotates the vector around the Z-axis
	 * @param rd rotation in radians
	 */
	public void rotZ(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=X*COS-Y*SIN; 
		double yn=Y*COS+X*SIN;
		X=xn;
		Y=yn;
	}
	
	/**
	 * Returns a string representation of the vector
	 * @return string
	 */
	public String toString() {
		return (X+"\t"+Y+"\t"+Z);
	}
	
	/**
	 * Returns the length of the vector
	 * @return length the length of the vector
	 */
	public double length() {
		return Math.sqrt(X*X+Y*Y+Z*Z);
	}
	
	/**
	 * Returns true if the vector is the nullvector
	 * @return boolean
	 */
	public boolean isNull() {
		return (X==0&&Y==0&Z==0);
	}

	public static Vec add(Vec a, Vec b) {
		return new Vec(a.X+b.X,a.Y+b.Y,a.Z+b.Z);
	}

	
}
