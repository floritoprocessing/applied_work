package math;

/**
 * Class Matrix, used for Matrix addition, multiplication etc.
 * 
 */
final public class Matrix {
	
    private final int M;             // number of rows
    private final int N;             // number of columns
    private final double[][] data;   // M-by-N array

    /**
     * Create new M-by-N Matrix
     * @param M number of rows
     * @param N number of columns
     */
    public Matrix(int M, int N) {
        this.M = M;
        this.N = N;
        data = new double[M][N];
    }

    /**
     * Create new Matrix based on the 2-dimensional array data
     * @param data the array
     */
    public Matrix(double[][] data) {
        M = data.length;
        N = data[0].length;
        this.data = new double[M][N];
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                    this.data[i][j] = data[i][j];
    }

    /*// copy constructor
    private Matrix(Matrix A) { this(A.data); }*/
    
    /**
     * Creates and returns a new Matrix based on a 3-dimensional Vec
     * @param v the Vec
     * @return Matrix
     */
    public static Matrix fromVec(Vec v) {
    	return new Matrix(new double[][] {{v.getX(),v.getY(),v.getZ()}});
    }
    
    /**
     * Creates and returns a new M-by-N Matrix with random values
     * @param M number of rows 
     * @param N number of columns
     * @return Matrix
     */
    public static Matrix random(int M, int N) {
        Matrix A = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[i][j] = Math.random();
        return A;
    }

    /**
     * Creates and returns the N-by-N identity Matrix
     * @param N number of rows/columns
     * @return Matrix
     */
    public static Matrix identity(int N) {
        Matrix I = new Matrix(N, N);
        for (int i = 0; i < N; i++)
            I.data[i][i] = 1;
        return I;
    }

    /*private void swap(int i, int j) {
        double[] temp = data[i];
        data[i] = data[j];
        data[j] = temp;
    }*/

    /**
     * Creates and returns the transpose of the invoking matrix
     * @return A Matrix
     */
    public Matrix transpose() {
        Matrix A = new Matrix(N, M);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                A.data[j][i] = this.data[i][j];
        return A;
    }

    /**
     * Returns the current Matrix plus Matrix B
     * @param B the Matrix to be added
     * @return Matrix
     */
    public Matrix plus(Matrix B) {
        Matrix A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        Matrix C = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j] = A.data[i][j] + B.data[i][j];
        return C;
    }

    /**
     * Returns the current Matrix minux Matrix B
     * @param B the Matrix to be subtracted
     * @return Matrix
     */
    public Matrix minus(Matrix B) {
        Matrix A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        Matrix C = new Matrix(M, N);
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                C.data[i][j] = A.data[i][j] - B.data[i][j];
        return C;
    }

    /**
     * Returns true of the current Matrix equals Matrix B
     * @param B the Matrix to be compared with
     * @return boolean
     */
    public boolean eq(Matrix B) {
        Matrix A = this;
        if (B.M != A.M || B.N != A.N) throw new RuntimeException("Illegal matrix dimensions.");
        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                if (A.data[i][j] != B.data[i][j]) return false;
        return true;
    }

    /**
     * Returns the current Matrix multiplied by the Matrix B
     * @param B the matrix to be multiplied with
     * @return Matrix
     */
    public Matrix times(Matrix B) {
        Matrix A = this;
        if (A.N != B.M) throw new RuntimeException("Illegal matrix dimensions: A:"+A.M+"x"+A.N+" times B:"+B.N+"x"+B.M);
        Matrix C = new Matrix(A.M, B.N);
        for (int i = 0; i < C.M; i++)
            for (int j = 0; j < C.N; j++)
                for (int k = 0; k < A.N; k++)
                    C.data[i][j] += (A.data[i][k] * B.data[k][j]);
        return C;
    }

    
    public static final int ROTATION_X = 0;
    public static final int ROTATION_Y = 1;
    public static final int ROTATION_Z = 2;
    
    /**
     * Creates a new rotation Matrix based on type and angle
     * @param type use ROTATION_X, ROTATION_Y or ROTATION_Z
     * @param rd the angle of rotation in radians
     * @return Matrix
     */
    public static final Matrix rotation(int type, double rd) {
    	double SIN = Math.sin(rd);
    	double COS = Math.cos(rd);
    	if (type==ROTATION_X) {
    		return new Matrix(new double[][] { {1,0,0},{0,COS,-SIN},{0,SIN,COS} });
    	}
    	else if (type==ROTATION_Y) {
    		return new Matrix(new double[][] { {COS,0,SIN},{0,1,0},{-SIN,0,COS} });
    	}
    	else if (type==ROTATION_Z) {
    		return new Matrix(new double[][] { {COS,-SIN,0},{SIN,COS,0},{0,0,1} });
    	} else
    		throw new RuntimeException("Illegal rotation type, use rotation(ROTATION_X/Y/Z,angle)");
    }
    
    /**
     * Returns the data array representing the Matrix
     * @return data array
     */
    public double[][] getData() {
    	return data;
    }
    
    // return x = A^-1 b, assuming A is square and has full rank
    /*public Matrix solve(Matrix rhs) {
        if (M != N || rhs.M != N || rhs.N != 1)
            throw new RuntimeException("Illegal matrix dimensions.");

        // create copies of the data
        Matrix A = new Matrix(this);
        Matrix b = new Matrix(rhs);

        // Gaussian elimination with partial pivoting
        for (int i = 0; i < N; i++) {

            // find pivot row and swap
            int max = i;
            for (int j = i + 1; j < N; j++)
                if (Math.abs(A.data[j][i]) > Math.abs(A.data[max][i]))
                    max = j;
            A.swap(i, max);
            b.swap(i, max);

            // singular
            if (A.data[i][i] == 0.0) throw new RuntimeException("Matrix is singular.");

            // pivot within b
            for (int j = i + 1; j < N; j++)
                b.data[j][0] -= b.data[i][0] * A.data[j][i] / A.data[i][i];

            // pivot within A
            for (int j = i + 1; j < N; j++) {
                double m = A.data[j][i] / A.data[i][i];
                for (int k = i+1; k < N; k++) {
                    A.data[j][k] -= A.data[i][k] * m;
                }
                A.data[j][i] = 0.0;
            }
        }

        // back substitution
        Matrix x = new Matrix(N, 1);
        for (int j = N - 1; j >= 0; j--) {
            double t = 0.0;
            for (int k = j + 1; k < N; k++)
                t += A.data[j][k] * x.data[k][0];
            x.data[j][0] = (b.data[j][0] - t) / A.data[j][j];
        }
        return x;
   
    }*/

    // print matrix to standard output
   /* public void show() {
        for (int i = 0; i < M; i++) {
            for (int j = 0; j < N; j++) 
                System.out.printf("%9.4f ", data[i][j]);
            System.out.println();
        }
    }*/
    
    
}