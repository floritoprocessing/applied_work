package visual;

import math.Vec;

public class Sphere {

	Vec[][] point;
	Line[][] hLine, vLine;

	double lonStep = 10;
	double latStep = 10;
	double radius = 100;

	public Sphere() {

		int lonSteps = (int)(360/lonStep);
		int latSteps = (int)(180/latStep);
		point = new Vec[lonSteps][latSteps+1];

		for (int i=0;i<lonSteps;i++) {
			double ra = Math.PI*i*lonStep/180;			
			for (int j=0;j<=latSteps;j++) {
				double de = -Math.PI*.5 + Math.PI*j*latStep/180;
				if (i==0)System.out.println(de);
				point[i][j] = new Vec(0,0,radius);
				point[i][j].rotX(de);
				point[i][j].rotY(ra);
			}
		}

		hLine = new Line[lonSteps][latSteps+1];
		for (int j=0;j<=latSteps;j++) {
			for (int i=0;i<lonSteps;i++) {
				if (i!=0) {
					hLine[i][j] = new Line(point[i-1][j],point[i][j]);
				} else {
					hLine[i][j] = new Line(point[point.length-1][j],point[i][j]);
				}
			}
		}
		
		vLine = new Line[lonSteps][latSteps];
		for (int i=0;i<lonSteps;i++) {
			for (int j=0;j<latSteps;j++) {
				vLine[i][j] = new Line(point[i][j],point[i][j+1]);
			}
		}

	}

	public Vec[] getPoints() {
		Vec[] out = new Vec[point.length*point[0].length];
		for (int i=0;i<point.length;i++) for (int j=0;j<point[0].length;j++)
			out[i*point[0].length+j] = point[i][j];
		return out;
	}

	public Line[] getHLines() {
		Line[] out = new Line[hLine.length*hLine[0].length];
		for (int i=0;i<hLine.length;i++) for (int j=0;j<hLine[0].length;j++)
			out[i*hLine[0].length+j] = hLine[i][j];
		return out;
	}
	
	public Line[] getVLines() {
		Line[] out = new Line[vLine.length*vLine[0].length];
		for (int i=0;i<vLine.length;i++)
			for (int j=0;j<vLine[0].length;j++)
				out[i*vLine[0].length+j]=vLine[i][j];
		return out;
	}
}
