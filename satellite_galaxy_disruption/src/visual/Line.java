package visual;

import math.Vec;

public class Line {
	Vec[] point;
	
	Line(Vec a, Vec b) {
		point = new Vec[2];
		point[0] = a;
		point[1] = b;
	}

	public Vec[] getPoints() {
		return point;
	}
}
