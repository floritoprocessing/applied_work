package visual.aa;

import processing.core.PApplet;

public class Drawing {
	
	public static final int BLEND_MODE_NORMAL = 0;
	public static final int BLEND_MODE_ADD = 1;
	private int blendMode = BLEND_MODE_NORMAL;
	
	public Drawing() {
	}
	
	public void setBlendMode(int m) {
		blendMode = m;
	}
	
	public void set(PApplet pa, double x, double y, int c) {
		set(pa,x,y,c,1.0);
	}

	public void set(PApplet pa, double x, double y, int c, double p) {
		if (x<0||x>=pa.width-1||y<0||y>=pa.height-1) return;
		int x0=(int)x, x1 = x0+1;
		int y0=(int)y, y1 = y0+1;
		double px1 = x-x0, px0=1-px1;
		double py1 = y-y0, py0=1-py1;
		double p00 = p*px0*py0;
		double p01 = p*px0*py1;
		double p10 = p*px1*py0;
		double p11 = p - p00-p01-p10;
		set(pa,x0,y0,c,p00);
		set(pa,x0,y1,c,p01);
		set(pa,x1,y0,c,p10);
		set(pa,x1,y1,c,p11);
	}
	
	public void set(PApplet pa, int x, int y, int c, double p) {
		int cb = pa.get(x,y);
		pa.set(x,y,colorBlend(c,cb,p));
	}

	private int colorBlend(int c, int cb, double p) {
		if (blendMode==BLEND_MODE_NORMAL) {
			double pb=1-p;
			return color(red(c)*p+red(cb)*pb,green(c)*p+green(cb)*pb,blue(c)*p+blue(cb)*pb);
		} else if (blendMode==BLEND_MODE_ADD) {
			return color(constrainTop(red(c)*p+red(cb)),constrainTop(green(c)*p+green(cb)),constrainTop(blue(c)*p+blue(cb)));
		} else
			throw new RuntimeException("Blend mode unknown");
		
	}

	private double constrainTop(double v) {
		return v<255?v:255;
	}

	private int color(double r, double g, double b) {
		int R = (int)r;
		int G = (int)g;
		int B = (int)b;
		return 0xFF000000|R<<16|G<<8|B;
	}

	private int red(int c) {
		return c>>16&0xFF;
	}

	private int blue(int c) {
		return c>>8&0xFF;
	}

	private int green(int c) {
		return c&0xFF;
	}

}
