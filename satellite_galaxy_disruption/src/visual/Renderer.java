package visual;

import math.Matrix;
import math.Vec;
import processing.core.PApplet;
import visual.aa.Drawing;

public class Renderer {
	
	private double FOV_HORIZONTAL_DEG = 45;
	private double DISTANCE_TO_VIEWER;
	
	private double cameraRa = 0, cameraDe = 0;
	private double cameraRaMouse = 0, cameraDeMouse = 0;
	private Matrix worldRotationMatrix;
	
	private PApplet pa;
	private Drawing drawing;
	
	private int mouseDownX, mouseDownY;
	
	private int drawColor = 0xFFFFFF;
	private double opacity = 1.0;
	
	public Renderer(PApplet pa) {
		this.pa = pa;
		drawing = new Drawing();
		drawing.setBlendMode(Drawing.BLEND_MODE_ADD);
		DISTANCE_TO_VIEWER = pa.width/Math.tan(FOV_HORIZONTAL_DEG*Math.PI/180);
		updateWorldRotationMatrix();
	}
	
	private Vec makeScreenPoint(Vec pos) {
		Matrix posMatrix = Matrix.fromVec(pos);
		posMatrix = posMatrix.times(getWorldRotationMatrix());
		double[] xyz = posMatrix.getData()[0];
		double screenX = pa.width/2.0 + (xyz[0])*DISTANCE_TO_VIEWER/(xyz[2]+100);
		double screenY = pa.height/2.0 + (xyz[1])*DISTANCE_TO_VIEWER/(xyz[2]+100);
		return new Vec(screenX,screenY,0);
	}
	
	private Vec[] makeScreenPoint(Vec[] pos) {
		Vec[] out = new Vec[pos.length];
		for (int i=0;i<pos.length;i++)
			out[i] = makeScreenPoint(pos[i]);
		return out;
	}
	
	public void setOpacity(double p) {
		opacity = p;
	}
	
	public void setDrawColor(int c) {
		drawColor = c;
	}
	
	public void setPoint(Vec pos) {
		Vec sPoint = makeScreenPoint(pos);
		drawing.set(pa,sPoint.getX(),sPoint.getY(),drawColor,opacity);
		//pa.point((float)sPoint.getX(),(float)sPoint.getY());
	}
	
	public void setLine(Line l) {
		Vec[] point = makeScreenPoint(l.getPoints());
		pa.line((float)point[0].getX(), (float)point[0].getY(), (float)point[1].getX(), (float)point[1].getY());
	}
	
	public void drawBlobAt(Blob blob, Vec pos) {
		Vec[] point = blob.getPoints();
		for (int i=0;i<point.length;i++)
			setPoint(Vec.add(point[i],pos));
	}
	
	private void updateWorldRotationMatrix() {
		worldRotationMatrix = Matrix.rotation(Matrix.ROTATION_Y,-Math.PI*(cameraRa+cameraRaMouse)/180).times(Matrix.rotation(Matrix.ROTATION_X,Math.PI*(cameraDe+cameraDeMouse)/180));
	}
	
	/**
	 * Returns the world rotation matrix
	 * @return Matrix
	 */
	public Matrix getWorldRotationMatrix() {
		return worldRotationMatrix;
	}

	public void setMouseDownPos(int mouseX, int mouseY) {
		mouseDownX = mouseX;
		mouseDownY = mouseY;
	}

	public void updateWorldFrom(int mouseX, int mouseY) {
		int dx = mouseX-mouseDownX, dy = mouseY-mouseDownY;
		cameraRaMouse = -dx*0.08;
		cameraDeMouse = -dy*0.08;
		updateWorldRotationMatrix();
		//System.out.println(cameraRa+cameraRaMouse+"\t"+cameraDe+cameraDeMouse);
	}

	public void finalizeWorld() {
		cameraRa += cameraRaMouse;
		cameraDe += cameraDeMouse;
		cameraRaMouse=0;
		cameraDeMouse=0;
		updateWorldRotationMatrix();
	}

	public void draw(Sphere sphere) {
		/*Vec[] points = sphere.getPoints();
		for (int i=0;i<points.length;i++)
			setPoint(points[i]);*/
		
		Line[] lines = sphere.getHLines();
		for (int i=0;i<lines.length;i++) setLine(lines[i]);
		lines = sphere.getVLines();
		for (int i=0;i<lines.length;i++) setLine(lines[i]);
	}

	public void setCameraRa(double ra) {
		cameraRa = ra;
		updateWorldRotationMatrix();
	}
	
	public void setCameraDe(double de) {
		cameraDe = de;
		updateWorldRotationMatrix();
	}
	
	public void setCameraRaDe(double ra, double de) {
		cameraRa = ra;
		cameraDe = de;
		updateWorldRotationMatrix();
	}


	


}
