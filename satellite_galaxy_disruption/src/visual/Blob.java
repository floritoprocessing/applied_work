package visual;

import math.Vec;

public class Blob {
	
	Vec[] point;
	
	public Blob(double size, int amountOfParticles) {
		point = new Vec[amountOfParticles];
		for (int i=0;i<point.length;i++) {
			double radius=size*Math.pow(Math.random(),.5);
			double ra=2*Math.PI*Math.random();
			double de=-.5*Math.PI + Math.PI*Math.random();
			point[i] = new Vec(0,0,radius);
			point[i].rotX(de);
			point[i].rotY(ra);
		}
	}

	public Vec[] getPoints() {
		return point;
	}
}
