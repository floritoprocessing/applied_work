import math.Vec;
import data.Data;
import data.DataReader;
import processing.core.*;
import visual.Blob;
import visual.Renderer;
import visual.Sphere;

public class GalaxyDisruption extends PApplet {
	
	private static final long serialVersionUID = -2683461714874909048L;

	public static final void main(String[] args) {
		PApplet.main(new String[] {"GalaxyDisruption"});
	}
	
	private String dataFolder = "";
	
	private static final int MODE_LOAD=0;
	private static final int MODE_DISPLAY=1;
	private int MODE = MODE_LOAD;
	
	private PFont font;
	
	private Data data;
	private DataReader dataReader;
	private Sphere sphere;
	private Blob[] blobs;
	
	private Renderer renderer;
	
	private int frame = 0;
	private int startFrame = 0;
	private boolean resetOnNextFrame=false;
	
	public void settings() {
		size(800,600);
	}
	public void setup() {
		
		background(0);
		frameRate(25);
		
		font = loadFont(dataFolder+"Verdana-12.vlw");
		textFont(font,12);
		
		data = new Data();
		dataReader = new DataReader(this,dataPath("Halo1.dat"));
		
		sphere = new Sphere();
		
		renderer = new Renderer(this);
	}
	
	public void mousePressed() {
		if (mouseButton==LEFT) renderer.setMouseDownPos(mouseX,mouseY);
		else if (mouseButton==RIGHT) {
			if (MODE==MODE_DISPLAY) resetOnNextFrame();
		}
	}
	
	private void resetOnNextFrame() {
		resetOnNextFrame=true;
	}

	public void mouseReleased() {
		if (mouseButton==LEFT) renderer.finalizeWorld();
	}
	
	public void draw() {
		
		
		if (MODE==MODE_LOAD) {
			background(0);
			fill(255,255,255,128);
			if (dataReader.loadInto(data)) {
				blobs = new Blob[data.getStarAmount()];
				for (int i=0;i<blobs.length;i++) blobs[i] = new Blob(0.1,10);
				
				data.scale(2.0);
				MODE=MODE_DISPLAY;
				startFrame = frameCount+1;
			}
		}
		
		if (MODE==MODE_DISPLAY) {
			background(0);
			fill(255,255,255,128);
			text("Frame: "+frame,5,15);
			text("Drag left mouse button to change view",5,30);
			text("Click right mouse button to restart animation",5,45);
			
			renderer.setCameraRaDe((frameCount-startFrame)*1,45*Math.sin(2*Math.PI*(frameCount-startFrame)/500.0));
			
			stroke(255);
			fill(255);
			if (resetOnNextFrame) {
				resetOnNextFrame=false;
				frame=0;
			}
			if (mousePressed&&mouseButton==LEFT) renderer.updateWorldFrom(mouseX,mouseY);
			
			renderer.setDrawColor(0xFFAB70);
			renderer.setOpacity(0.7);
			for (int star=0;star<data.getStarAmount();star++) {
				Vec pos = data.getStarOfFrame(star, frame);
				renderer.setPoint(pos);
				//renderer.drawBlobAt(blobs[star],pos);
			}
			
			stroke(128,128,255,32);
			renderer.draw(sphere);
			//renderer.setPoint(new Vec(-20,-20,100));
			
			frame++;
			if (frame==data.getFrameAmount()) frame=0;
		}
	}
}
