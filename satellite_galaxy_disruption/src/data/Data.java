package data;

import math.Vec;

public class Data {

	int frameAmount = 600;
	int starAmount = 2000;
	int frame=0, star=0;
	
	private Vec[][] pos;
	
	public Data() {
		pos = new Vec[frameAmount][starAmount];
	}
	
	public int getFrameAmount() {
		return frameAmount;
	}
	
	public int getStarAmount() {
		return starAmount;
	}
	
	public void addLine(double[] values) {
		pos[frame][star] = new Vec(values);
		star++;
		if (star==starAmount) {
			star=0;
			frame++;
		}
	}
	
	public Vec[] getFrame(int frameIndex) {
		if (frameIndex<0||frameIndex>=frameAmount) throw new RuntimeException("Frame index out of bounds");
		return pos[frameIndex];
	}
	
	public Vec getStarOfFrame(int starIndex, int frameIndex) {
		Vec[] frame = getFrame(frameIndex);
		if (starIndex<0||starIndex>=starAmount) throw new RuntimeException("Star index out of bounds");
		return frame[starIndex];
	}

	public void scale(double d) {
		for (int i=0;i<pos.length;i++) for (int j=0;j<pos[0].length;j++)
			pos[i][j].mul(d);
	}

}
