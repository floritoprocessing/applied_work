package data;

public class DataParser {

	public static double[] getValues(String inline) {
		String[] values = inline.split(",");
		double[] out = new double[3];
		for (int i=0;i<values.length;i++)
			out[i]=Double.parseDouble(values[i]);
		return out;
	}

}
