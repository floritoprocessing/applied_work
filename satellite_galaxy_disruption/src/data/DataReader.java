package data;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import processing.core.PApplet;

public class DataReader {
	/**
	 * Milliseconds per call to load
	 */
	public static final int MILLISECONDS_PER_CALL_TO_LOAD = 40;
	
	private PApplet pa;
	private BufferedReader in;
	private int linesRead=0;
	
	public DataReader(PApplet pa, String filename) {
		this.pa = pa;
		try {
			in = new BufferedReader(new FileReader(filename));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public boolean loadInto(Data data) {
		boolean finished = false;
		long startTime = System.currentTimeMillis();
		long endTime = startTime+MILLISECONDS_PER_CALL_TO_LOAD;
		String inline = "";
		while(System.currentTimeMillis()<endTime) {
			try {
				inline = in.readLine();
				if (inline!=null)
					data.addLine(DataParser.getValues(inline));
				else finished = true;
			} catch (IOException e) {
				e.printStackTrace();
			}
			linesRead++;
		}
		if (!finished) pa.text("Loading animation data, line: "+linesRead+": "+inline,5,15);
		//else System.out.println(stars.getAmount()+" created");
		return finished;
	}
	
}
