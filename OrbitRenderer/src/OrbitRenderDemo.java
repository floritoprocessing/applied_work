import java.io.File;
import java.net.URISyntaxException;

import processing.core.*;
import graf.esa.processing.orbitrenderer.*;
import graf.esa.jpl.horizons.*;

public class OrbitRenderDemo extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"OrbitRenderDemo"});
	}
	
	String path="";// = "C:\\Documents and Settings\\mgraf\\My Documents\\Processing Eclipse\\OrbitRenderer\\src\\";
	HorizonsTextFile earthData, marsData, rosettaData;
	OrbitRenderer earthOrbit, marsOrbit, rosettaOrbit;
	int row = 0;
	
	public void settings() {
		size(640,480,P3D);
	}
	
	
	public void setup() {
		
		
//		try {
//			String rootPath = getClass().getResource("").toURI().getPath();
//			path =  (new File(rootPath)).getParentFile().getAbsolutePath()+"\\src\\";
//			println(path);
//			//f = new File(f.getParentFile().getAbsolutePath()+"\\src\\");
//			
//		} catch (URISyntaxException e) {
//			e.printStackTrace();
//		}
		
		earthData = new HorizonsTextFile(loadStrings(path+"earth20062008.txt"));
		marsData = new HorizonsTextFile(loadStrings(path+"mars20062008.txt"));
		rosettaData = new HorizonsTextFile(loadStrings(path+"rosetta20062008.txt"));
		
		earthOrbit = new OrbitRenderer(this);
		marsOrbit = new OrbitRenderer(this);
		rosettaOrbit = new OrbitRenderer(this);
		
		earthOrbit.setXYZData(earthData.getValues(new String[] {"X","Y","Z"}));
		marsOrbit.setXYZData(marsData.getValues(new String[] {"X","Y","Z"}));
		rosettaOrbit.setXYZData(rosettaData.getValues(new String[] {"X","Y","Z"}));
		
		earthOrbit.scaleData(100);
		marsOrbit.scaleData(100);
		rosettaOrbit.scaleData(100);
		//earthOrbit.offsetData(width/2,height/2);
		rosettaOrbit.glow();
		rosettaOrbit.setGlowSize(24);
		rosettaOrbit.setGlowOpacity(255);
		rosettaOrbit.noLine();
	}
	
	public void draw() {
		background(0);
		translate(width/2,height/2);
		rotateX(PI);//+HALF_PI*0.9f);
		
		earthOrbit.renderOrbit(0x20ff20);
		marsOrbit.renderOrbit(0xff2020);
		//rosettaOrbit.setGlow(-PI-HALF_PI*0.9f);
		rosettaOrbit.renderOrbit(0x8000FF,0,row);
		
		earthOrbit.renderPlanet(0x20ff20,2,row);
		marsOrbit.renderPlanet(0xff2020,2,row);
		rosettaOrbit.renderPlanet(0x8000FF,2,row);
		
//		saveFrame("output/OrbitRenderer_preview_###.jpg");
		row++;
		if (row==earthOrbit.getNumberOfRows()) {
			row=0;
			exit();
		}
	}
	
}
