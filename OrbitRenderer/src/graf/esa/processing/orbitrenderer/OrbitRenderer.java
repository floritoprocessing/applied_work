package graf.esa.processing.orbitrenderer;

import processing.core.*;

public class OrbitRenderer {

	private PApplet pa;
	private static final int DIM2 = 2;
	private static final int DIM3 = 3;
	
	private int dimensions;
	private double[] x, y, z;
	
	private boolean line = true;
	
	private boolean glow = false;
	private int glowOpacity = 255;
	private int glowSize = 4;
		
	public OrbitRenderer(PApplet pa) {
		this.pa = pa;
	}

	public void setXYZData(double[][] xyz) {
		if (xyz.length<2||xyz.length>3) throw new RuntimeException("Please supply a 2- or 3-dimensional array of numbers");
		if (xyz.length==2) dimensions=DIM2;
		else dimensions=DIM3;
		x = new double[xyz[0].length];
		y = new double[xyz[0].length];
		if (dimensions==DIM3) z = new double[xyz[0].length];
		for (int i=0;i<xyz[0].length;i++) {
			x[i] = xyz[0][i];
			y[i] = xyz[1][i];
			if (dimensions==DIM3) z[i] = xyz[2][i];
		}
	}
	
	public int getNumberOfRows() {
		return x.length;
	}

	public void scaleData(double fac) {
		for (int i=0;i<getNumberOfRows();i++) {
			x[i]*=fac;
			y[i]*=fac;
			if (dimensions==DIM3) z[i]*=fac;
		}
	}

	public void offsetData(double xo, double yo, double zo) {
		for (int i=0;i<getNumberOfRows();i++) {
			x[i]+=xo;
			y[i]+=yo;
			if (dimensions==DIM3) z[i]+=zo;
		}
	}
	
	public void offsetData(double xo, double yo) {
		offsetData(xo,yo,0);
	}

	public void renderOrbit(int color) {
		renderOrbit(color,0,getNumberOfRows());
	}
	
	public void renderOrbit(int color, int startRow, int endRow) {
		if (line) {
			pa.stroke(0xFF<<24|color);
			pa.noFill();
			pa.beginShape();
			for (int i=startRow;i<endRow;i++) {
				if (dimensions==DIM2) 
					pa.vertex((float)x[i],(float)y[i]);
				else
					pa.vertex((float)x[i],(float)y[i],(float)z[i]);
			}
			pa.endShape();
		}
		
		
		if (glow) {
			PImage img = new PImage(glowSize,glowSize);
			img.format = PApplet.ARGB;
			img.loadPixels();
			int px=0;
			double cen = (img.width)/2.0;
			double maxd = Math.sqrt(img.width+img.width);
			for (int y=0;y<img.height;y++) {
				double dy = y-cen;
				double dy2 = dy*dy;
				for (int x=0;x<img.width;x++) {
					double dx = x-cen;
					double dx2 = dx*dx;
					//double d = 1 - Math.sqrt(dx2+dy2) / maxd;
					double d = 1 - Math.pow(Math.sqrt(dx2+dy2) / maxd,0.1);
					int opac = (int)(glowOpacity*d);
					if (opac<0) opac=0; else if (opac>glowOpacity) opac=glowOpacity;
					img.pixels[px] = opac<<24|color;
					px++;
				}
			}
			img.updatePixels();
			for (int i=startRow;i<endRow;i++) {
				pa.pushMatrix();
				if (dimensions==DIM2) 
					pa.translate((float)x[i],(float)y[i]);
				else
					pa.translate((float)x[i],(float)y[i],(float)z[i]);
				pa.image(img, -img.width/2, -img.height/2);
				pa.popMatrix();
			}
			
			pa.image(img, 0, 0, 20, 20);
		}
	}

	public void renderPlanet(int color, float planetSize, int row) {
		pa.noStroke();
		pa.fill(0xFF<<24|color);
		pa.pushMatrix();
		if (dimensions==DIM2) 
			pa.translate((float)x[row],(float)y[row]);
		else
			pa.translate((float)x[row],(float)y[row],(float)z[row]);
		
		pa.sphere(planetSize);
		pa.popMatrix();
	}

	public void glow() {
		glow = true;
	}
	
	public void noGlow() {
		glow = false;
	}

	public void setGlowOpacity(int opacity) {
		glowOpacity = opacity;
	}
	
	public void setGlowSize(int glowSize) {
		this.glowSize = glowSize;
	}

	
	public void line() {
		line = true;
	}
	
	public void noLine() {
		line = false;
	}
	
	
	

	
}
