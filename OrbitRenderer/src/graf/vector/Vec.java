package graf.vector;

public class Vec {
	
	public double x=0,y=0,z=0;
	
	public Vec() {
	}
	
	public Vec(double x, double y) {
		this.x=x;
		this.y=y;
	}
	
	public Vec(double x, double y, double z) {
		this.x=x;
		this.y=y;
		this.z=z;
	}
	
	public Vec(double[] xyz) {
		if (xyz.length>0) {
			this.x=xyz[0];
			if (xyz.length>1) {
				this.y=xyz[1];
				if (xyz.length>2) {
					this.z=xyz[2];
				}
			}
		}
	}
	
	public Vec(Vec v) {
		this.x=v.x;
		this.y=v.y;
		this.z=v.z;
	}

	public String toString() {
		return x+", "+y+", "+z;
	}
	
	public void mul(double n) {
		x*=n;
		y*=n;
		z*=n;
	}

	public void div(double n) {
		if (n==0) throw new RuntimeException("Division by zero!");
		x/=n;
		y/=n;
		z/=n;
	}

	public void add(Vec v) {
		x+=v.x;
		y+=v.y;
		z+=v.z;
	}

	public void sub(Vec v) {
		x-=v.x;
		y-=v.y;
		z-=v.z;
	}
}
