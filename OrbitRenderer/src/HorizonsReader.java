

import graf.esa.jpl.horizons.HorizonsTextFile;
import graf.vector.Vec;
import processing.core.*;

public class HorizonsReader extends PApplet {

	public static void main(String[] args) {
		PApplet.main(new String[] {"HorizonsReader"});
	}
	
	HorizonsTextFile file;
	
	double[] jd;
	Vec[] rosettaPos;
	Vec[] steinsPos;
	
	Vec averageCenter;
	
	public void setup() {
		size(600,400,P3D);
		background(224);
		
		file = new HorizonsTextFile(loadStrings(
				"C:\\Documents and Settings\\mgraf\\My Documents\\Processing Eclipse\\HorizonsReader\\files\\" +
				"2008_09_05_1600-2100_2m_km_steins.txt"));
		
		//System.out.println(file.variableNamesToString());
		jd = new double[file.getNumberOfRows()];
		for (int i=0;i<jd.length;i++)
			jd[i] = file.getValueAtRow("JDCT", i);
		
		steinsPos = new Vec[file.getNumberOfRows()];
		for (int i=0;i<steinsPos.length;i++)
			steinsPos[i] = new Vec(file.getValuesAtRow(new String[] {"X","Y","Z"}, i));
		
		file = new HorizonsTextFile(loadStrings(
				"C:\\Documents and Settings\\mgraf\\My Documents\\Processing Eclipse\\HorizonsReader\\files\\" +
				"2008_09_05_1600-2100_2m_km_rosetta.txt"));
		
		rosettaPos = new Vec[file.getNumberOfRows()];
		for (int i=0;i<rosettaPos.length;i++)
			rosettaPos[i] = new Vec(file.getValuesAtRow(new String[] {"X","Y","Z"}, i));
		
		
		averageCenter = new Vec();
		for (int i=0;i<steinsPos.length;i++) averageCenter.add(steinsPos[i]);
		for (int i=0;i<rosettaPos.length;i++) averageCenter.add(rosettaPos[i]);
		averageCenter.div(steinsPos.length+rosettaPos.length);
		
		//System.out.println(averageCenter.toString());
	}
	
	float endPoint=0;
	double scale = 0.001;
	float rotZ=0;
	
	public void draw() {
		System.out.println(averageCenter.z*scale);
		
		background(0);
		
		noFill();
		translate(width/2,height/2);
		
		rotateX(HALF_PI*0.8f);// + (float)Math.sin(frameCount/200.0)*0.2f);
		stroke(128,0,255);
		
		rotateZ(HALF_PI + (rotZ+=0.006f));
		
		beginShape();
		for (int i=0;i<endPoint;i++) {
			Vec pos = new Vec(rosettaPos[i]);
			pos.sub(averageCenter);
			pos.mul(scale);
			vertex(-(float)pos.x,(float)pos.y,(float)pos.z);
		}
		endShape();
		
		stroke(128,0,255,128);
		for (int i=0;i<endPoint;i+=5) {
			Vec pos = new Vec(rosettaPos[i]);
			pos.sub(averageCenter);
			pos.mul(scale);
			//line(-(float)pos.x,(float)pos.y,(float)pos.z,-(float)pos.x,(float)pos.y,0);
		}
		
		stroke(96,96,96);
		beginShape();
		for (int i=0;i<endPoint;i++) {
			Vec pos = new Vec(steinsPos[i]);
			pos.sub(averageCenter);
			pos.mul(scale);
			vertex(-(float)pos.x,(float)pos.y,(float)pos.z);
		}
		endShape();
		
		stroke(96,96,96,128);
		for (int i=0;i<endPoint;i+=5) {
			Vec pos = new Vec(steinsPos[i]);
			pos.sub(averageCenter);
			pos.mul(scale);
			//line(-(float)pos.x,(float)pos.y,(float)pos.z,-(float)pos.x,(float)pos.y,0);
		}
		
		//box(200,200,200);
		//box(200,200,1);
		stroke(40,40,80);
		fill(20,20,40,200);
		//rect(-100,-100,200,200);
		//rect(-200,-200,400,400);
		
		endPoint+=0.1f;
		if (endPoint>=rosettaPos.length) endPoint--;
	}
}
