package graf.projection;

public class IllegalLatitudeException extends Exception {

	public IllegalLatitudeException(double latitude) {
		super("Illegal latitude "+latitude);
	}
}
