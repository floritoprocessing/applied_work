package graf.projection;

import java.awt.geom.Point2D;

public class Rectangular extends Projection {
	
	double lonCenter;
	
	public Rectangular(
			double lonMin, double lonMax, 
			double latMin, double latMax,
			double mapWidth, double mapHeight) throws IllegalLatitudeException {		
		
		validateAndSetEdges(lonMin,lonMax,latMin,latMax);
		
		this.mapWidth = mapWidth;
		this.mapHeight = mapHeight;
		lonCenter = lonMin + lonRange*0.5;
		
		scaleX = mapWidth/lonRange;
		scaleY = -mapHeight/latRange;
	}

	@Override
	public Type getProjectionType() {
		return Type.RECTANGULAR;
	}

	@Override
	protected Point2D.Double legalLonLatToXY(double lonDeg, double latDeg)
			throws IllegalLatitudeException {
		
		Point2D.Double out = new Point2D.Double();
		
		if (lonDeg-lonCenter>180) lonDeg-=360;
		else if (lonDeg-lonCenter<-180) lonDeg+=360;
		out.x = (lonDeg-lonMin) * scaleX;
		//System.out.println(latDeg);
		out.y = (latDeg-latMin) * scaleY;
		
		return out;
	}
}
	