package graf.projection;

import java.awt.geom.Point2D;

public abstract class Projection {

	public enum Type {
		RECTANGULAR("Rectangular"),
		MERCATOR("Mercator"),
		STEREOGRAPHIC("Sterographic");
		private String typeName;
		Type(String typeName) {
			this.typeName = typeName;
		}
		public String getName() {
			return typeName;
		}
		public String toString() {
			return getName();
		}
	}
	/**
	 * NORTH edge = 90
	 */
	protected static final double LAT_MAX_INC = 90;
	/**
	 * SOUTH edge = -90
	 */
	protected static final double LAT_MIN_INC = -90;
	/**
	 * WEST edge = 0
	 */
	protected static final double LON_MIN_INC = 0;
	/**
	 * EAST edge = 360 (exclusive)
	 */
	protected static final double LON_MAX_EX = 360;
	
	protected double mapScale=1;
	protected double lonMin, lonRange;
	protected double latMin, latRange;
	//protected double lonCenter, latCenter;
	protected double mapWidth, mapHeight;
	protected double scaleX, scaleY;
	
	/**
	 * Returns the enum Projection Type
	 * @return
	 */
	public abstract Type getProjectionType();
	
	/**
	 * Projects longitude / latitude (degrees) to x/y (left->right, top->bottom)
	 * This routine will always constrain longitudes between 0(inclusive) and 360(exclusive)
	 * @throws IllegalArgumentException when latitudes are outside -90 and 90
	 * @param lonDeg 0..360 (east,west)
	 * @param latDeg -90..90 (south,north)
	 * @return the Point2D.Double (xy in map scale)
	 */
	public Point2D.Double lonLatToXY(double lonDeg, double latDeg) throws IllegalLatitudeException {
		lonDeg = validateLongitudeAndLatitude(lonDeg, latDeg);
		return legalLonLatToXY(lonDeg, latDeg);
	}
	
	/**
	 * Implementation of the lonLat to xy routine.
	 * This routine is called by the lonLatToXY method.
	 * This routine only gets called with values that are within range (0..360) and (-90..90)
	 * @see #lonLatToXY(double, double)
	 * @param lonDeg
	 * @param latDeg
	 * @return Point2D.Double (xy in map scale)
	 */
	protected abstract Point2D.Double legalLonLatToXY(double lonDeg, double latDeg) throws IllegalLatitudeException;
		
	/**
	 * Sets lonMin, lonRange, latMin and latRange
	 * @param lonMin inc
	 * @param lonMax ex
	 * @param latMin inc
	 * @param latMax inc
	 * @throws IllegalLatitudeException
	 */
	protected void validateAndSetEdges(double lonMin, double lonMax, double latMin, double latMax) 
	throws IllegalLatitudeException {
		if (lonMin>=lonMax) throw new IllegalArgumentException("lonMin has to be smaller than lonMax");
		if (lonMin<LON_MIN_INC||lonMin>=LON_MAX_EX) throw new IllegalArgumentException("lonMin not valid");
		if (lonMax<LON_MIN_INC||lonMax>LON_MAX_EX) throw new IllegalArgumentException("lonMax not valid");
		if (latMin>=latMax) throw new IllegalArgumentException("latMin has to be smaller than latMax");
		
		validateLatitude(latMin);
		validateLatitude(latMax);
		this.latMin = latMin;
		this.latRange = latMax-latMin;
		
		this.lonMin = lonMin;
		this.lonRange = lonMax-lonMin;
	}
	
	/**
	 * Validates the latitude.
	 * If the latitude is out of range, throws an {@link IllegalLatitudeException}
	 * @param latDeg
	 * @throws IllegalLatitudeException
	 */
	protected void validateLatitude(double latDeg) throws IllegalLatitudeException {
		if (latDeg<LAT_MIN_INC || latDeg>LAT_MAX_INC) 
			throw new IllegalLatitudeException(latDeg);
	}
	
	/**
	 * Validates the longitude and returns it. Creates values between 0(inc) and 360(ex)
	 * @param lonDeg
	 * @return
	 */
	protected double validateLongitude(double lonDeg) {
		if (lonDeg>=LON_MAX_EX) lonDeg%=LON_MAX_EX;
		else while(lonDeg<LON_MIN_INC) lonDeg+=LON_MAX_EX;
		return lonDeg;
	}
	
	/**
	 * Validates longitude and latitude. Returns the new longitude
	 * @see #validateLongitude(double)
	 * @see #validateLatitude(double)
	 * @param lonDeg
	 * @param latDeg
	 * @return the new longitude
	 * @throws IllegalLatitudeException
	 */
	protected double validateLongitudeAndLatitude(double lonDeg, double latDeg) throws IllegalLatitudeException {
		validateLatitude(latDeg);
		return validateLongitude(lonDeg);
	}
}
