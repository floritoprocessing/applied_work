package projectiontest;

import graf.projection.IllegalLatitudeException;
import graf.projection.Projection;
import graf.projection.Rectangular;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.geom.Point2D;
import java.awt.image.BufferedImage;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;

import processing.core.PApplet;
import processing.core.PGraphics;
import processing.core.PImage;

public class ProjectionTestPanel extends JPanel {

	private static final long serialVersionUID = -6727726424236599128L;

	private final Projection projection;
	
	private final PGraphics map;
	private final int mapW=0, mapE=360, mapN=90, mapS=-90;
	
	private final int loResDeg = 10;
	private final int loResWidth, loResHeight;
	private final Point2D.Double[] loResProjected;
	
	private int projectedWidth = 600, projectedHeight = 300;
	private final BufferedImage projectedImage 
	= new BufferedImage(projectedWidth, projectedHeight, BufferedImage.TYPE_INT_RGB);;
	
	public ProjectionTestPanel(PApplet pa) throws IllegalLatitudeException {
		super();
		
		/*
		 * Load map
		 */
		URL location = getClass().getResource("./FakeMap.jpg");
		System.out.println(location);
		ImageIcon mapIcon = new ImageIcon(location);
		Image image = mapIcon.getImage();
		int mapWidth = image.getWidth(null);
		int mapHeight = image.getHeight(null);
		BufferedImage tmp = new BufferedImage(mapWidth,mapHeight,BufferedImage.TYPE_INT_RGB);
		tmp.getGraphics().drawImage(image,0,0,null);
		
		/*
		 * Convert to PGraphics
		 */
		map = pa.createGraphics(mapWidth, mapHeight, PApplet.P3D);
		map.loadPixels();
		map.pixels = tmp.getRGB(0, 0, mapWidth, mapHeight, null, 0, mapWidth);
		map.updatePixels();
		pa.image(map, 0, 0, pa.width, pa.height);
		
		
		projection = new Rectangular(mapW,mapE,mapS,mapN,400,200);
		
		int w=0, h=0;
		for (int lon=mapW;lon<=mapE;lon+=loResDeg) w++;
		loResWidth = w;
		for (int lat=mapS;lat<=mapN;lat+=loResDeg) h++;
		loResHeight = h;
		loResProjected = new Point2D.Double[loResHeight*loResWidth];
		
		/*
		 * Project corner points as one array
		 */
		int i=0;
		for (int lat=mapS;lat<=mapN;lat+=loResDeg) {
			for (int lon=mapW;lon<=mapE;lon+=loResDeg) {
				loResProjected[i++] = projection.lonLatToXY(lon, lat);
				//System.out.println(loResProjected[i-1]);
			}
		}
		
		/*
		 * Create quads
		 */
		i=0;
		int i00, i01, i10, i11;
		int ti=0;
		Point2D.Double[] loResQuads = new Point2D.Double[(loResWidth-1)*(loResHeight-1)*4];
		for (int y=0;y<loResHeight-1;y++) {
			for (int x=0;x<loResWidth-1;x++) {
				i00 = i;
				i01 = i + loResWidth;
				i10 = i + 1;
				i11 = i + loResWidth + 1;
				loResQuads[ti++] = (Point2D.Double)loResProjected[i00].clone();
				loResQuads[ti++] = (Point2D.Double)loResProjected[i01].clone();
				loResQuads[ti++] = (Point2D.Double)loResProjected[i11].clone();
				loResQuads[ti++] = (Point2D.Double)loResProjected[i10].clone();
				//System.out.println(loResQuads[ti-1]);
				i++;
			}
			i++;
		}
		
		/*
		 * draw
		 */
		PGraphics projected = pa.createGraphics(projectedWidth, projectedHeight, PApplet.P3D);
		projected.beginDraw();
		projected.background(0);
		
		projected.noFill();
		projected.stroke(255,0,0);
		projected.beginShape(PApplet.QUADS);
		for (i=0;i<loResQuads.length;i++) {
			projected.vertex((float)loResQuads[i].x,(float)loResQuads[i].y);
		}
		projected.endShape();
		
		projected.endDraw();
		projected.loadPixels();
		/*
		 * convert to buffered image
		 */
		projected.updatePixels();
//		System.out.println(projectedImage);
//		System.out.println(projected.pixels);
//		projectedImage = new BufferedImage(projectedWidth, projectedHeight, BufferedImage.TYPE_INT_ARGB);
		projectedImage.setRGB(0, 0, projectedWidth, projectedHeight, projected.pixels, 0, projectedWidth);
	}
	
	public void paintComponent(Graphics g) {
		g.drawImage(projectedImage,10,10,null);
	}
	
	/*
	 * (non-Javadoc)
	 * @see javax.swing.JComponent#getPreferredSize()
	 */
	public Dimension getPreferredSize() {
		return new Dimension(projectedImage.getWidth()+20,projectedImage.getHeight()+20);
	}
	
	public static void createAndShowGui(PApplet pa) {
		ProjectionTestPanel content=null;
		try {
			content = new ProjectionTestPanel(pa);
		} catch (IllegalLatitudeException e) {
			e.printStackTrace();
		}
		if (content!=null) {
			JFrame frame = new JFrame("ProjectionTest (V2)");
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setContentPane(content);
			frame.pack();
			frame.setVisible(true);
		}
	}
	
	/**
	 * @param args
	 */
	/*public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGui();
			}
		});
	}*/

}
