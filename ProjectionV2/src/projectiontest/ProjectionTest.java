package projectiontest;

import processing.core.PApplet;

public class ProjectionTest extends PApplet {

	
	/* (non-Javadoc)
	 * @see processing.core.PApplet#settings()
	 */
	@Override
	public void settings() {
		size(120,120,P3D);
	}
	/*
	 * (non-Javadoc)
	 * @see processing.core.PApplet#setup()
	 */
	public void setup() {
		
		background(0);
		noLoop();
	}
	
	/*
	 * (non-Javadoc)
	 * @see processing.core.PApplet#draw()
	 */
	public void draw() {
		ProjectionTestPanel.createAndShowGui(this);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PApplet.main(new String[] {"--location=0,0","projectiontest.ProjectionTest"});
	}

}
