import java.util.Vector;

import javax.media.opengl.GL;

import peasy.PeasyCam;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PVector;
import processing.opengl.PGraphicsOpenGL;


public class TextualLandscape extends PApplet {

	/*String txt = "Writing on top of a landscape can be fun, now can't it? " +
			"I think this kind of stuff could be very useful for the quick brown fox that jumped over the lazy dog. " +
			"Andrea is really cute and she will do amazing stuff during the Book and Digital Media Studies. " +
			"I believe this kind of text could look very awesome done like this. " +
			"I have no fucking clue what to write anymore and I think it's about time to feed Perla. ";*/
	
	String txt = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc ut ipsum vel dui molestie convallis eu id quam. In fermentum suscipit dolor ut accumsan. Phasellus felis arcu, adipiscing nec sagittis at, aliquet et lacus. Curabitur eget ultrices quam. Curabitur lobortis elementum enim, non gravida orci vestibulum ac. Fusce dictum ante vel purus sagittis tempus id nec quam. Proin condimentum lobortis sapien ac sollicitudin. Pellentesque ornare erat eu justo rhoncus nec vulputate leo dictum. Pellentesque ornare odio in lectus iaculis ut vehicula sem dapibus. Quisque cursus, est a luctus sollicitudin, lacus justo pulvinar felis, non euismod eros urna eu sapien. Curabitur aliquam lorem eget felis adipiscing a tincidunt mi tempus. Nam ultricies porta sodales. Aenean dictum magna quis metus consequat ut auctor nisi accumsan. Duis luctus metus nec mi consequat mollis." +
			"Nulla risus nisl, porta eget auctor in, faucibus vitae ipsum. Aenean arcu felis, fringilla quis varius sit amet, vulputate quis erat. Sed consectetur dapibus orci ac rhoncus. Aliquam at lorem vel felis varius dictum. Duis varius arcu eu odio dapibus tincidunt. Mauris viverra odio vitae diam imperdiet lacinia in rutrum mi. Quisque in tortor metus. In vitae malesuada nibh. Nam id dui a mi ultrices pharetra eu sit amet metus. Vestibulum tristique sem imperdiet dui faucibus vel feugiat arcu tincidunt. Vivamus in felis magna. Duis sapien nulla, suscipit eu vestibulum vitae, pellentesque et est." +
			"Curabitur pretium faucibus eros, ut pretium erat euismod in. Sed placerat lacus in risus sodales vel aliquet magna rhoncus. Pellentesque aliquet nisi sit amet ante luctus convallis. Quisque molestie dui libero, sit amet faucibus dui. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam id volutpat sem. Ut vel nibh a tortor facilisis eleifend vitae ultricies felis. Nunc at purus quam. Duis eu risus leo, a iaculis justo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas." +
			"Sed vitae ante ante. Mauris ultrices ipsum eget ipsum auctor id interdum turpis semper. Integer consequat vulputate fermentum. Nunc ac mauris in est luctus gravida. Nulla facilisi. Nulla vitae turpis risus, non dictum est. Ut mollis porttitor odio in elementum. Duis orci sem, mollis eget egestas sit amet, aliquam nec sapien. Cras at ipsum massa. Sed convallis cursus risus, a dignissim augue posuere quis. Cras sed diam felis, ut rhoncus dolor." +
			"Aliquam rhoncus lorem pharetra nibh fermentum ultrices. Vivamus elit enim, tincidunt bibendum fringilla a, pulvinar vitae ligula. Integer lacinia, magna et consectetur consectetur, augue tellus tempus turpis, eget sagittis leo quam vel mauris. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed lacus nunc, molestie vitae tristique vel, rhoncus non erat. Donec commodo facilisis tincidunt. Integer non placerat augue. Praesent id nulla neque. Proin placerat euismod lorem, sed consectetur elit viverra blandit. Sed neque arcu, bibendum sed consectetur sed, sagittis vitae nibh. Nunc et fermentum libero. ";
	
	String fontName = "Times New Roman";//"Verdana Bold";
	float fontSize = 20;
	float lineSpacing = 26;
	
	float noiseFactor = 0.005f;
	float landscapeHeight = 150;
	
	PFont font;
	
	PeasyCam cam;
	
	PVector[][] landscape;
	
	Spiral spiral;
	Vector<float[]> spiralLine = new Vector<>();
	Vector<Character> spiralLetters = new Vector<>();
	
	String keyString = txt + txt + txt;//"";
	
	public void setup() {
		size(1280, 800, OPENGL);
		smooth();
		
		font = createFont(fontName,fontSize);
		textFont(font);
		textAlign(CENTER, TOP);
		
		cam = new PeasyCam(this, 500);
		
		int step = 20;
		int xSize = (2*width)/step;
		int ySize = (2*height)/step;
		landscape = new PVector[ySize][xSize];
		int yIndex = 0;
		for (int y=-height;y<height;y+=step) {
			int xIndex = 0;
			for (int x=-width;x<width;x+=step) {
				float[] xyz = xyToXyz(new float[]{x,y});
				landscape[yIndex][xIndex] = new PVector(xyz[0],xyz[1],xyz[2]-10);
				xIndex++;
			}
			yIndex++;
		}
		
		spiral = new Spiral(lineSpacing);
		spiralLine.add(xyToXyz(spiral.getCurrentPoint()));
	}
	
	
	private float[] xyToXyz(float[] xy) {
		float x = xy[0] - width/2;
		float y = xy[1] - height/2;
		float z = landscapeHeight * noise(x*noiseFactor, y*noiseFactor);
		return new float[] {xy[0],xy[1],z};
	}

	
	public void keyPressed() {
		keyString += key;
	}
	
	public void draw() {
		
		char[] keys = keyString.toCharArray();
		for (char c:keys) {
			spiralLetters.add(c);
			
			float step = textWidth(c);
			spiral.step(step);
			
			float[] position =xyToXyz(spiral.getCurrentPoint());
			spiralLine.add(position);
						
		}
		keyString = "";
		
		
		
		//beginRecord(PDF, "test.pdf");
		background(255);
		
		PGraphicsOpenGL pgl = (PGraphicsOpenGL)g;
		GL gl = pgl.beginGL();
		gl.glDisable(GL.GL_DEPTH_TEST);
		pgl.endGL();
		
		lights();
		
		/*strokeWeight(1);
		line(0,0,-100,0,0,100);*/
		
		
		boolean drawLines = true;
		if (drawLines) {
			strokeWeight(1);
			stroke(192);
			noFill();
			int ySize = landscape.length;
			int xSize = landscape[0].length;
			beginShape(QUADS);
			for (int y=0;y<ySize-1;y++) {
				for (int x=0;x<xSize-1;x++) {
					PVector p0 = landscape[y][x];
					PVector p1 = landscape[y][x+1];
					PVector p2 = landscape[y+1][x+1];
					PVector p3 = landscape[y+1][x];
					vertex(p0.x,p0.y,p0.z);
					vertex(p1.x,p1.y,p1.z);
					vertex(p2.x,p2.y,p2.z);
					vertex(p3.x,p3.y,p3.z);
				}
			}
			endShape();
		}
		
		stroke(0);
		strokeWeight(1);
		beginShape(POINTS);
		float[] p = spiralLine.get(spiralLine.size()-1);
		vertex(p[0],p[1],p[2]);
		endShape();
		
		strokeWeight(1);
		fill(0); stroke(0);
		for (int i=0;i<spiralLetters.size();i++) {
			char c = spiralLetters.get(i);
			float[] prevPos = spiralLine.get(i);
			float[] nextPos = spiralLine.get(i+1);
			float orientation = (float)Math.atan2(nextPos[1]-prevPos[1], nextPos[0]-prevPos[0]);
			float[] avPos = average(prevPos,nextPos);
			pushMatrix();
			translate(avPos[0], avPos[1], avPos[2]);
			rotateZ(orientation);
			text(c,0,0);
			popMatrix();
		}
		
		
		
		
		/*for (int i=0;i<10;i++) {
			spiral.step(10);
			spiralLine.add(xyToXyz(spiral.getCurrentPoint()));
		}*/
		
		
		//endRecord();
		
		
	}

	private float[] average(float[] a, float[] b) {
		int size = Math.min(a.length,b.length);
		float[] out = new float[size];
		for (int i=0;i<size;i++) {
			out[i] = (a[i]+b[i])/2.0f;
		}
		return out;
	}


	public static void main(String[] args) {
		PApplet.main(new String[] { "TextualLandscape" });
	}

}
