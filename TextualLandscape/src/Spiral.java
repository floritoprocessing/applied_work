
public class Spiral {
	
	private static float TWO_PI = (float)(2*Math.PI);
	
	private float lineSpacing;
	
	private float currentRadius;
	private float currentRadian = 0.75f * TWO_PI;

	public Spiral(float lineSpacing) {
		this.lineSpacing = lineSpacing;
		currentRadius = lineSpacing;
	}
	
	public float getCurrentRadian() {
		return currentRadian;
	}
	
	public float[] getCurrentPoint() {
		return new float[] {
				currentRadius*(float)Math.cos(currentRadian),
				-currentRadius*(float)Math.sin(currentRadian)
		};
	}
	
	public void step(float onCircumference) {
		
		float fullCircumference = TWO_PI * (currentRadius+currentRadius+lineSpacing)/2 ;
		float circlePercentage = onCircumference / fullCircumference;
				
		float deltaRadians = TWO_PI * circlePercentage;
		float deltaRadius = lineSpacing * circlePercentage;
		
		currentRadian += deltaRadians;
		currentRadius += deltaRadius;
	}

}
