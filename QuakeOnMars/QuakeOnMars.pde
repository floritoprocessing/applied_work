/*****************************
* 
* QuakeOnMars
*
*****************************/

boolean STEREO_MODE=false;
PImage buf1, buf2;

PImage DTM;
String DTM_NAME="h0563_dtm_small.jpg";
int DTM_PIXEL_PER_VERTEX=20;
int DTM_WIDTH, DTM_HEIGHT;
int[][] DTM_MAP;
float DTM_MAP_SCALE=15.0;
float DTM_HEIGHT_SCALE=2.0;

PImage[] IMG=new PImage[3];
String IMG_NAME="h0563_0000_nd2 small.jpg";

QuakeCam quakeCam;

float rotX=1.1;
float rotY=0.0;
float zTrans=0.0;

void setup() {
  size(640,480,P3D);
  buf1=new PImage(width,height);
  buf2=new PImage(width,height);
  noStroke();
  fill(255);
  
  DTM=loadImage(DTM_NAME);
  IMG[0]=loadImage(IMG_NAME);
  IMG[1]=new PImage(IMG[0].width,IMG[0].height);
  IMG[2]=new PImage(IMG[0].width,IMG[0].height);
  for (int i=0;i<IMG[0].pixels.length;i++) {
    IMG[1].pixels[i]=color(brightness(IMG[0].pixels[i]),0,0);
    IMG[2].pixels[i]=color(0,brightness(IMG[0].pixels[i]),0);
  }
  
  // CREAT DTM MAP
  DTM_WIDTH=DTM.width/DTM_PIXEL_PER_VERTEX;
  DTM_HEIGHT=DTM.height/DTM_PIXEL_PER_VERTEX;
  DTM_MAP=new int[DTM_WIDTH][DTM_HEIGHT];
  for (int x=0;x<DTM_WIDTH;x++) {
    for (int y=0;y<DTM_HEIGHT;y++) {
      DTM_MAP[x][y]=(int)brightness(DTM.pixels[DTM.width*y*DTM_PIXEL_PER_VERTEX+x*DTM_PIXEL_PER_VERTEX]);
    }
  }
  
  // CREATE CAMERA WITH BOUNDARIES
  quakeCam=new QuakeCam(0,0,DTM_WIDTH-1,DTM_HEIGHT-1,DTM_MAP_SCALE,DTM_HEIGHT_SCALE);
  quakeCam.setPosition(DTM_WIDTH/2.0+5,DTM_HEIGHT/2.0+5);
  
  //Image cursorImage = Toolkit.getDefaultToolkit().getImage("xparent.gif");
  //Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(cursorImage, new Point( 0, 0), "" );
  //setCursor( blankCursor );
}

void keyPressed() {
  if (keyCode==TAB) quakeCam.changeMode();
  if (key=='1') STEREO_MODE=false;
  if (key=='2') STEREO_MODE=true;
}
  
void draw() {
  background(0);
  quakeCam.update(DTM_MAP);
  quakeCam.show(STEREO_MODE?-1:0);
  drawScene(STEREO_MODE?1:0);

  if (STEREO_MODE) {
    loadPixels();
    for (int i=0;i<pixels.length;i++) buf1.pixels[i]=pixels[i];
    background(0);
    quakeCam.show(1);
    drawScene(2);
    for (int i=0;i<pixels.length;i++) buf2.pixels[i]=pixels[i];
    background(0);
    camera(width/2.0, height/2.0, height*0.9 , width/2.0, height/2.0, 0, 0, 1, 0);
    buf1.blend(buf2,0,0,width,height,0,0,width,height,ADD);
    image(buf1,0,0);
  }
}

void drawScene(int chan) {
  textureMode(NORMAL);
  for (int x=0;x<DTM_WIDTH-1;x++) {
    for (int y=0;y<DTM_HEIGHT-1;y++) {
      beginShape(QUADS);
      texture(IMG[chan]);
      int x1=x, x2=x+1;
      int y1=y, y2=y+1;
      float u1=x1/float(DTM_WIDTH);
      float u2=x2/float(DTM_WIDTH);
      float v1=y1/float(DTM_HEIGHT);
      float v2=y2/float(DTM_HEIGHT);
      vertex(x1*DTM_MAP_SCALE,-DTM_MAP[x1][y1]*DTM_HEIGHT_SCALE,y1*DTM_MAP_SCALE,u1,v1);
      vertex(x2*DTM_MAP_SCALE,-DTM_MAP[x2][y1]*DTM_HEIGHT_SCALE,y1*DTM_MAP_SCALE,u2,v1);
      vertex(x2*DTM_MAP_SCALE,-DTM_MAP[x2][y2]*DTM_HEIGHT_SCALE,y2*DTM_MAP_SCALE,u2,v2);
      vertex(x1*DTM_MAP_SCALE,-DTM_MAP[x1][y2]*DTM_HEIGHT_SCALE,y2*DTM_MAP_SCALE,u1,v2);
      endShape();
    }
  }
}
