class QuakeCam {
  Vec position=new Vec();
  Vec look=new Vec();
  Vec target=new Vec();
  Vec upaxis=new Vec(0,1,0);
  
  int MODE=1;  //0: walk cam .. 1: fly cam
  float STEREO_EYE_DISTANCE=20;
  
  float lookSpeed=0.02;
  float horizLook=PI*0.5;
  float vertiLook=-PI/4.5;
  float maxVertiLook=PI/3.0;
  float manHeight=20;
  float flyHeight=200;
  float walkSpeed=5.0;
  float mapScale=0;    // whateva
  float heightScale=0; // whateva
  float xMin=0, xMax=0, zMin=0, zMax=0;  // whateva
  
  // stepping:
  float maxStepHeight=2;
  float stepHeight=0;
  float stepAdd=0.2;
  float stepDir=1.0;
  
  QuakeCam(float x1, float z1, float x2, float z2, float mscl, float hscl) {
    mapScale=mscl;
    heightScale=hscl;
    xMin=x1*mapScale;
    xMax=x2*mapScale;
    zMin=z1*mapScale;
    zMax=z2*mapScale;
  }
  
  void setPosition(float dtmX, float dtmZ) {
    position.x=dtmX*mapScale;
    position.z=dtmZ*mapScale;
  }
  
  void changeMode() {
    MODE=1-MODE;
  }
  
  void update(int[][] heightMap) {
    // CHANGE LOOKING DIRECTION
    if (mousePressed) {
      float tHorizLook=horizLook+lookSpeed*(mouseX-pmouseX);
      float tVertiLook=vertiLook+lookSpeed*0.25*(pmouseY-mouseY);
      horizLook=(2*horizLook+tHorizLook)/3.0;
      vertiLook=(2*vertiLook+tVertiLook)/3.0;
      vertiLook=constrain(vertiLook,-maxVertiLook,maxVertiLook);
    }
    look.setVec(0,0,1);
    look.rotX(vertiLook);
    look.rotY(horizLook);
    
    // CHANGE POSITION
    if (keyPressed) {
      if (key=='w'||key=='W') position.add(walkSpeed*look.x,0,walkSpeed*look.z);
      if (key=='s'||key=='S') position.sub(walkSpeed*look.x,0,walkSpeed*look.z);
      if (key=='a'||key=='A') {
        Vec slook=new Vec(look);
        slook.rotY(-PI/2.0);
        position.add(walkSpeed*slook.x,0,walkSpeed*slook.z);
      }
      if (key=='d'||key=='D') {
        Vec slook=new Vec(look);
        slook.rotY(PI/2.0);
        position.add(walkSpeed*slook.x,0,walkSpeed*slook.z);
      }
      // stepping:
      if (key=='w'||key=='W'||key=='s'||key=='S') {
        stepHeight+=stepDir*stepAdd;
        stepHeight=constrain(stepHeight,0,1);
        if (stepHeight>=1.0||stepHeight<=0) stepDir*=-1;
      }
    }
    
    float tPos=(MODE==1?-flyHeight:-manHeight-sqrt(stepHeight)*maxStepHeight-heightMap[int((float)position.x/mapScale)][int((float)position.z/mapScale)]*heightScale);
    position.y=(position.y+tPos)/2.0;
    
    // CONSTRAIN POSITION:
    if (position.x<xMin) position.x=xMin;
    if (position.x>xMax) position.x=xMax;
    if (position.z<zMin) position.z=zMin;
    if (position.z>zMax) position.z=zMax;  
  }
  
  void show(int LeftCenRight) {
    if (LeftCenRight!=0) {
      Vec slook=new Vec(look);
      slook.mul(STEREO_EYE_DISTANCE*(MODE==0?0.1:1.0));
      slook.rotY(LeftCenRight*PI/2.0);
      Vec tPos=new Vec(vecAdd(position,slook));
      target=vecAdd(tPos,look);
      vecCamera(tPos,target,upaxis);
    } else {  
      target=vecAdd(position,look);
      vecCamera(position,target,upaxis);
    }
  }
  
}


