

import java.io.File;
import processing.core.*;
import graphics.HighQualityImage;
import graphics.Bmp;

public class StarsMain extends PApplet {

	private static final long serialVersionUID = 1L;

	public Logger logger;
	private SetupScreen setupScreen;
	HipparcosData hipparcosData;
	Stars stars;
	Animation animation;
	HighQualityImage screenImage, screenImageLeft, screenImageRight;

	public Settings settings;

	private static final int RENDER_MODE_OFFLINE = 0;
	private static final int RENDER_MODE_ONLINE = 1;
	private int RENDER_MODE = RENDER_MODE_OFFLINE;

	private boolean RENDER_MODE_INIT_OFFLINE = false;
	private boolean RENDER_MODE_INIT_ONLINE = false;

	private static final int MODE_WARN_TO_LOAD = 0;
	private static final int MODE_LOAD_DATA = 1;
	private static final int MODE_LOAD_RADIAL_VELOCITIES = 2;
	private static final int MODE_ADJUST_DATA = 3;
	private static final int MODE_CREATE_STAR_IMAGES = 4;
	private static final int MODE_SETUP_SCREEN = 5;
	private static final int MODE_RENDER = 6;
	//private static final int MODE_SETUP = 7;
	private int MODE = MODE_WARN_TO_LOAD;

	private boolean SHOW_IMAGE_ON_SCREEN = true; // you can not show, to improve speed

	//private boolean NEXT_FRAME_START_ANIMATION = false;

	public float EPOCH = 1991.25f;

	public PFont font;//, font2, font3;
	private int bg = 0xFF000000;
	boolean saveOnNextFrame = false;

	static public void main(String args[]) {   
		PApplet.main(new String[] { "StarsMain" });
	}



	public void settings() {
		settings = new Settings(this);
		setupScreen = new SetupScreen(this);
		setupScreen.toggleActive();
		logger = new Logger(settings,settings.PROJECT_PATH+settings.PROJECT_NAME+".log",true);
		screenSetup();
	}
	
	
	public void setup() {
		loadPixels();
		font = loadFont("ArialMT-11.vlw"); //C:\\Documents and Settings\\mgraf\\My Documents\\Micheal Perryman\\ProcessingEclipse\\StarsV18\\
		textFont(font,11);
		hipparcosData = new HipparcosData();  
		stars = new Stars(settings,EPOCH); //RENDER_MODE_PER_PIXEL
	}


	public void screenSetup() {
		if (settings.RENDER_MODE == Settings.RENDER_MODE_3D_CROSS_EYED || settings.RENDER_MODE == Settings.RENDER_MODE_3D_PARALLEL) {
			size(settings.SCREEN_WIDTH*2,settings.SCREEN_HEIGHT);
			screenImage = new HighQualityImage(settings.SCREEN_WIDTH*2,settings.SCREEN_HEIGHT);
		} 
		else {
			size(settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT);
			screenImage = new HighQualityImage(settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT);
		}
		screenImageLeft = new HighQualityImage(settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT);
		screenImageRight = new HighQualityImage(settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT);
//		loadPixels();
	}

	public void changeRenderModeTo(int m) {
		settings.setRenderMode(m);
		screenSetup();
	}
	
	public void createStarImages() {
		stars.clearStarImages();
		MODE=MODE_CREATE_STAR_IMAGES;
	}


	public void switchToOfflineMode() {
		RENDER_MODE_INIT_OFFLINE=true;//RENDER_MODE = RENDER_MODE_OFFLINE;
	}

	public void switchToOnlineMode() {
		RENDER_MODE_INIT_ONLINE=true;
		//NEXT_FRAME_START_ANIMATION = true;
	}


	public void switchToLoading() {
		MODE = MODE_WARN_TO_LOAD;
	}



	public void keyPressed() {
		if (key=='s'&&RENDER_MODE==RENDER_MODE_OFFLINE) setupScreen.toggleActive();

		//if (key=='S') saveOnNextFrame = true;

		if (key=='d') SHOW_IMAGE_ON_SCREEN = !SHOW_IMAGE_ON_SCREEN;

		if (MODE==MODE_RENDER) {


			if (RENDER_MODE==RENDER_MODE_OFFLINE) {
				if (key==' '&&!setupScreen.active()) switchToOnlineMode();
				if (key==CODED) {
					if (keyCode==UP) settings.increaseRealtimeModeSpeed(10);
					else if (keyCode==DOWN) settings.increaseRealtimeModeSpeed(-10);
				}
			}

			else if (RENDER_MODE==RENDER_MODE_ONLINE) {
				if (key==' ') switchToOfflineMode();
			}
		}
		
		/*
		if (key=='/') {
			String[] settingsAsString = settings.asString();
			settings.setFromString(settingsAsString);
			println(settings.asString());
		}*/

		setupScreen.tellKeyPressed(key,keyCode);
	}



	public void draw() {

		if (RENDER_MODE_INIT_ONLINE) {
			RENDER_MODE_INIT_ONLINE = false;
			RENDER_MODE = RENDER_MODE_ONLINE;
			float deltaT = settings.ANIMATION_START - EPOCH;
			stars.setTimeInRelationToEpoch(deltaT,EPOCH);
			animation = new Animation(this,settings.SCREEN_WIDTH,settings.SCREEN_HEIGHT,settings.ANIMATION_START,settings.ANIMATION_STOP,settings.ANIMATION_STEP,stars);
		}

		else if (RENDER_MODE_INIT_OFFLINE) {
			RENDER_MODE_INIT_OFFLINE = false;
			RENDER_MODE = RENDER_MODE_OFFLINE;
			stars.setTimeInRelationToEpoch(0,EPOCH);
		}



		//
		//
		//  WARNING: WE ARE GOING TO LOAD
		//
		//

		if (MODE==MODE_WARN_TO_LOAD) {
			background(bg);
			fill(255,255,255);
			text("Loading new stars from Vizier...",width/2,height/2);
			MODE = MODE_LOAD_DATA;
		} 





		//
		//
		// LOADING STARS
		//
		//

		else if (MODE==MODE_LOAD_DATA) {
			stars.clear();
			if (settings.LOAD_HIPPARCOS) hipparcosData.loadFromVizier(this,VizierFile.CATALOGUE_HIPPARCOS,stars,settings.LOAD_DATA_FROM_DISK);
			if (settings.LOAD_TYCHO1) hipparcosData.loadFromVizier(this,VizierFile.CATALOGUE_TYCHO1,stars,settings.LOAD_DATA_FROM_DISK);
			if (settings.LOAD_TYCHO2) hipparcosData.loadFromVizier(this,VizierFile.CATALOGUE_TYCHO2,stars,settings.LOAD_DATA_FROM_DISK);
			//sim.EPOCH = EPOCH;
			MODE = MODE_LOAD_RADIAL_VELOCITIES;
		}





		//
		//
		// LOADING RADIAL VELOCITIES
		//
		//

		else if (MODE==MODE_LOAD_RADIAL_VELOCITIES) {
			background(bg);
			if (settings.LOAD_RADIAL_VELOCITIES) {
				if (stars.loadRadialVelocities(this,hipparcosData,settings.LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME,settings.LOAD_DATA_FROM_DISK)) {
					MODE = MODE_ADJUST_DATA;
				}
			}
			else
				MODE = MODE_ADJUST_DATA;
		}





		//
		//
		//  SET WORLD PARAMETERS AND ADJUST PARALLAX FOR UNKNOWN PARALLAX
		//
		//

		else if (MODE==MODE_ADJUST_DATA) {
			
			//float avDistance = stars.getAverageDistanceOfStarsWithParallax();
			//stars.removeStarsWithoutParallax();
			//stars.setStarsWithoutParallaxToInfinitiy(this);
			//stars.deleteAllHipBut(new int[] { 27204, 24575, 26241 });
//			stars.removeStarsWithZeroRadialVelocity();
//			stars.setZeroParallaxTo(randomDistance);
			
			//stars.createRelativeStarPositionsPc();
			//stars.saveStarPositions(this);
			
			
			if (setupScreen.active()) setupScreen.show();
			MODE=MODE_CREATE_STAR_IMAGES;
		} 





		//
		//
		//  CREATING INDIVIDUAL STAR IMAGES
		//
		//

		else if (MODE==MODE_CREATE_STAR_IMAGES) {
			//stars.
			if (stars.createStarImagesInTimeAndReturnReady(this,50)) {
				MODE = MODE_SETUP_SCREEN;
				//MODE = MODE_RENDER;
				background(bg);
			}
			HighQualityImage img = stars.getLastCreatedImage();
			if (img!=null) {
				image(img,0,0);
				noFill();
				stroke(255,0,0);
				rectMode(CORNER);
				rect(0,0,img.width,img.height);
			}
			if (setupScreen.active()) setupScreen.show();
		} 





		//
		//
		//	SETUP SCREEN
		//
		//

		else if (MODE==MODE_SETUP_SCREEN) {
			screenSetup();
			if (setupScreen.active()) setupScreen.show();
			MODE = MODE_RENDER;
		}






		//
		//
		//  SIMULATION / RENDER MODE
		//
		//

		else if (MODE==MODE_RENDER) {

			if (setupScreen.active()) {
				setupScreen.show();
			} else {


				if (settings.RENDER_MODE == Settings.RENDER_MODE_2D_NORMAL) {
					// 2D
					screenImage.background(0x0000,0x0000,0x0000);
					stars.drawAllStars(settings,World3D.EYE_LEFT,screenImage,Stars.TWO_D);
					screenImage.updatePImage();
				} 

				else {
					// 3D
					screenImageLeft.background(0x0000,0x0000,0x0000);
					screenImageRight.background(0x0000,0x0000,0x0000);
					//int w = screenImageLeft.width;

					// make Left Eye
					if (settings.RENDER_MODE != Settings.RENDER_MODE_3D_RIGHT) {
						stars.drawAllStars(settings,World3D.EYE_LEFT,screenImageLeft,Stars.THREE_D);
						screenImageLeft.updatePImage();
					}

					// make Right Eye
					if (settings.RENDER_MODE != Settings.RENDER_MODE_3D_LEFT) {
						stars.drawAllStars(settings,World3D.EYE_RIGHT,screenImageRight,Stars.THREE_D);
						screenImageRight.updatePImage();
					}


					// COMPILE TOTAL IMAGE:
					// --------------------
					if (settings.RENDER_MODE == Settings.RENDER_MODE_3D_LEFT) {
						screenImageLeft.copyInto(screenImage);
					}

					else if (settings.RENDER_MODE == Settings.RENDER_MODE_3D_RIGHT) {
						screenImageRight.copyInto(screenImage);
					}

					else if (settings.RENDER_MODE == Settings.RENDER_MODE_3D_PARALLEL) {
						screenImageLeft.copyInto(screenImage,0,0);
						screenImageRight.copyInto(screenImage,settings.SCREEN_WIDTH,0);
					}

					else if (settings.RENDER_MODE == Settings.RENDER_MODE_3D_CROSS_EYED) {
						screenImageLeft.copyInto(screenImage,settings.SCREEN_WIDTH,0);
						screenImageRight.copyInto(screenImage,0,0);
					}

					else if (settings.RENDER_MODE == Settings.RENDER_MODE_3D_ANAGLYPH) {
						for (int x=0;x<screenImage.width;x++) for (int y=0;y<screenImage.height;y++) {
							int bL = (int)(settings.RENDER_ANAGLYPH_INTENSITY_LEFT*screenImageLeft.brightness(x,y));
							int bR = (int)(settings.RENDER_ANAGLYPH_INTENSITY_RIGHT*screenImageRight.brightness(x,y));
							screenImage.set(x,y,bL,bR,bR);
						}
						screenImage.updatePImage();
					}

				}


				//loadPixels();
				//screenImage.updatePImage();
				//image(screenImage,0,0);
				//updatePixels();
				//set()

				//loadPixels();
				if (SHOW_IMAGE_ON_SCREEN) {
					for (int i=0;i<pixels.length;i++) {
						pixels[i]=screenImage.pixels[i];
					}
					updatePixels();
				} else {
					background(bg);
				}

				//for (int x=0;x<screenImage.width;x++) for (int y=0;y<screenImage.height;y++) {
				//	set(x,y,screenImage.get(x,y));
				//}

				//fill(255,255,255);
				//stroke(255,0,0);
				//for (int t=0;t<1000;t++) set((int)random(width),(int)random(height),0xFFFF0000);
				//redraw();

				fill(255,255,255);
				text("Epoch: "+nf(stars.getEpoch(),1,2),10,20);
				if (RENDER_MODE==RENDER_MODE_OFFLINE) {
					text("[S] to go into setup screen",10,40);
					text("[UP]/[DOWN] to change speed",10,55);
				}

				fill(255,255,255);
				text("Press [D] to toggle showing image on screen (to improve render time)",10,70);

				if (RENDER_MODE==RENDER_MODE_ONLINE) animation.storeFrame(screenImage);

				
				// ADVANCE TIME
				
				if (RENDER_MODE==RENDER_MODE_OFFLINE)
					stars.advanceTime(settings.REALTIME_YEARS_PER_FRAME);
				else 
					stars.advanceTime(settings.ANIMATION_STEP);

			}




			if (saveOnNextFrame) {
				saveOnNextFrame = false;
				File folder = new File(settings.PROJECT_PATH);
				if (!folder.exists()) folder.mkdirs();
				Bmp bmp=new Bmp(this,screenImage,24);
				bmp.capture24Bit(screenImage);
				bmp.saveAs(settings.PROJECT_PATH+"_"+StringTools.numberFormat(frameCount,8));
				println("file saved to "+settings.PROJECT_PATH);
			}
		}

	}

	public long getFreeMemory() {
		return Runtime.getRuntime().freeMemory()/1024;
	}

	public long getTotalMemory() {
		return Runtime.getRuntime().totalMemory()/1024;
	}

}
