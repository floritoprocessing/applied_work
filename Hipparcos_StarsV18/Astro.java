

public class Astro {

	public static final float I_GAIN = 2.5f*255.0f;         // 2.5 normal, 1.0 normal
	//public static final float MOFFAT_RADIUS = 0.7f * Settings.SCREEN_WIDTH / (100.0f*Settings.SIMULATION_RA_VIEW_RANGE) ;
	public static final float MOFFAT_BETA = 2.5f;

	public static final int RED_CHANNEL = 0;
	public static final int GREEN_CHANNEL = 1;
	public static final int BLUE_CHANNEL = 2;

	public static final float RED_ADJUST = 1.0f;//0.7;
	public static final float GREEN_ADJUST = 1.0f;//1.5;
	public static final float BLUE_ADJUST = 1.0f;//1.5;
	
	public static final float INFINITE_PARALLAX = 0.000001f;

	public static double milliArcSecondToDegree(double mas) {
		double as = mas/1000.0;
		double am = as/60.0;
		double deg = am/60.0;
		return deg;
	}
	
	public static int[] Color_at_R(float R, float maxR, float Vmag, float Vmag_MAX, float BV, boolean faded, boolean addNoise, float noiseLevel, float moffatRadius) {
		//int c = 0;
		float I_R = 0, I_G = 0, I_B = 0;

		I_R = Astro.RED_ADJUST*Astro.I_GAIN*I_at_R_JOS(R,Vmag,Vmag_MAX,BV,Astro.RED_CHANNEL,moffatRadius);
		I_G = Astro.GREEN_ADJUST*Astro.I_GAIN*I_at_R_JOS(R,Vmag,Vmag_MAX,BV,Astro.GREEN_CHANNEL,moffatRadius);
		I_B = Astro.BLUE_ADJUST*Astro.I_GAIN*I_at_R_JOS(R,Vmag,Vmag_MAX,BV,Astro.BLUE_CHANNEL,moffatRadius);

		if (addNoise) {
			I_R += noiseLevel * (-.5+Math.random()) * Math.sqrt(I_R);
			I_G += noiseLevel * (-.5+Math.random()) * Math.sqrt(I_G);
			I_B += noiseLevel * (-.5+Math.random()) * Math.sqrt(I_B);
		}

		if (faded) {
			float p = (float)(1.0-Math.abs(R)/maxR);
			if (p>1.0) p=1.0f;
			if (p<0.0) p=0.0f;
			float BELL = (float)Math.pow(p,.1);
			I_R *= BELL;
			I_G *= BELL;
			I_B *= BELL;
		}

		I_R = Math.min(Math.max(I_R,0),65535);
		I_G = Math.min(Math.max(I_G,0),65535);
		I_B = Math.min(Math.max(I_B,0),65535);

		int[] rgb = new int[3];
		rgb[0] = (int)I_R;
		rgb[1] = (int)I_G;
		rgb[2] = (int)I_B;

		return rgb;
	}



	public static float I_at_R_JOS(float R, float Vmag, float Vmag_MAX, float BV, int chan, float moffatRadius) {
		float out = 0;
		if (chan==Astro.RED_CHANNEL) {
			out = (float)(Math.pow(10,-0.4*(Vmag-BV-Vmag_MAX))*I_moffat(R,moffatRadius));
		} 
		else if (chan==Astro.GREEN_CHANNEL) {
			out = (float)(Math.pow(10,-0.4*(Vmag-Vmag_MAX))*I_moffat(R,moffatRadius));
		} 
		else if (chan==Astro.BLUE_CHANNEL) {
			out = (float)(Math.pow(10,-0.4*(Vmag+BV-Vmag_MAX))*I_moffat(R,moffatRadius));
		}
		return out;
	}


	public static float I_moffat(float R,float moffatRadius) {
		double rda = R/moffatRadius;
		double rsq = rda*rda;
		float I = (float)Math.pow(( 1 + (Math.pow(2,1/Astro.MOFFAT_BETA)-1)*rsq ) , -Astro.MOFFAT_BETA);
		return I;
	}
}








