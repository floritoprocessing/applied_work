


public class HipparcosData {

	HipparcosData() {
	}


	//
	//
	//  LOAD HIPPARCOS INPUT CATALOGUE
	//
	//

	public void loadHipparcosInputFromVizier(StarsMain pa, String hipparcosNumberList, int startNr, int endNr, Stars stars, boolean loadFromDisk) {
		// link Hipparcos input catalogue:
		// http://vizier.u-strasbg.fr/viz-bin/VizieR?-source=I%2F196%2Fmain&HIC=19617,19641,19651&RV=!=0&-out=RV&-out=HIC&-sort=RV&-out.max=u
		// limited&-out.form=Tab-Separated-Values
		Settings settings = pa.settings;
		pa.logger.log("");
		pa.logger.log("loading Radial velocities: "+startNr+".."+endNr);
		VizierFile file = new VizierFile(pa);
		if (!loadFromDisk) {
			String LINK = file.makeURLHipparcosInput(hipparcosNumberList);
			pa.logger.log("Loading from web: "+LINK);
			//pa.text("Loading from web",pa.width/2,pa.height/2+15);

			if (startNr==0) {
				file.load(LINK);
				file.saveToFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsHipparcosInput.tab");
			} else {
				file.loadFromFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsHipparcosInput.tab");        
				VizierFile addon = new VizierFile(pa);
				addon.load(LINK);
				addon.removeComments();
				addon.removeRows(0,2);
				for (int i=0;i<addon.size();i++) file.add(addon.elementAt(i));
				file.saveToFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsHipparcosInput.tab");
			}
		} 
		else {
			pa.logger.log("Loading from disk");
			//pa.text("Loading from disk",pa.width/2,pa.height/2+15);
			file.loadFromFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsHipparcosInput.tab");
		}

		file.removeComments();
		file.removeRows(0,2);
		file.parseToStars(pa,VizierFile.CATALOGUE_HIPPARCOS_INPUT,stars);

	}





	//
	//
	//  LOAD HIPPARCOS / TYCHO2 STAR INFORMATION (RA, DEC, MAGNITUDE ETC)
	//
	//

	public void loadFromVizier(StarsMain pa, int vizFileCatIndex, Stars stars, boolean loadFromDisk) {
		
		Settings settings = pa.settings;
		
		float ra_min = settings.SIMULATION_RA_CEN-settings.SIMULATION_RA_CALC_RANGE/2.0f;
		float ra_max = settings.SIMULATION_RA_CEN+settings.SIMULATION_RA_CALC_RANGE/2.0f;
		float dec_min = settings.SIMULATION_DEC_CEN-settings.SIMULATION_DEC_CALC_RANGE/2.0f;
		float dec_max = settings.SIMULATION_DEC_CEN+settings.SIMULATION_DEC_CALC_RANGE/2.0f;
		float mag_min = settings.SIMULATION_MAGNITUDE_MIN;
		float mag_max = settings.SIMULATION_MAGNITUDE_MAX;  

		VizierFile file = new VizierFile(pa);
		pa.logger.log("");
		pa.logger.log("------------------------------------------------------");
		pa.logger.log(file.CATALOGUE_NAMES[vizFileCatIndex]);
		pa.logger.log("------------------------------------------------------");
		if (!loadFromDisk) {
			String LINK = file.makeURL(vizFileCatIndex,ra_min,ra_max,dec_min,dec_max,mag_min,mag_max);
			pa.logger.log("Loading from web: "+LINK);
			//pa.text("Loading from web",pa.width/2,pa.height/2+15);
			file.load(LINK);
			if (vizFileCatIndex == VizierFile.CATALOGUE_HIPPARCOS) {
				file.saveToFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsHipparcos.tab");
			} 
			else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO1) {
				file.saveToFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsTycho1.tab");
			} 
			else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO2) {
				file.saveToFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsTycho2.tab");
			}
		} 
		else {
			pa.logger.log("Loading from disk");
			//pa.text("Loading from disk",pa.width/2,pa.height/2+15);
			if (vizFileCatIndex == VizierFile.CATALOGUE_HIPPARCOS) {
				file.loadFromFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsHipparcos.tab");
			} 
			else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO1) {
				file.loadFromFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsTycho1.tab");
			} 
			else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO2) {
				file.loadFromFile(pa,settings.PROJECT_PATH+settings.PROJECT_NAME+"_DownloadedStarsTycho2.tab");
			}
		}

		file.removeComments();
		file.removeRows(0,2);
		file.parseToStars(pa,vizFileCatIndex,stars);
	}



}
