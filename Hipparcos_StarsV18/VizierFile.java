
import processing.core.*;

import java.util.Vector;

import java.net.URL;
import java.net.URLConnection;
import java.net.MalformedURLException;

//import java.io.DataInputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;


public class VizierFile extends Vector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final int CATALOGUE_HIPPARCOS = 0;
	public static final int CATALOGUE_TYCHO1 = 1;
	public static final int CATALOGUE_TYCHO2 = 2;
	public static final int CATALOGUE_HIPPARCOS_INPUT = 3;

	public String[] CATALOGUE_NAMES = { 
			"Hipparcos", "Tycho1", "Tycho2", "Hipparcos input catalogue"     };

	//Star(String name, int hip, float ra, float de, float pmra, float pmde, float vmag, float bv, float plx) {
	private String COLUMNS_HIPPARCOS[] = {  
			"HIP", "RA(ICRS)", "DE(ICRS)", "pmRA", "pmDE", "Vmag", "B-V", "Plx"                   };
	private String COLUMNS_TYCHO1[] = {  
			"HIP", "RA(ICRS)", "DE(ICRS)", "pmRA", "pmDE", "Vmag", "B-V", "Plx", "TYC"                   };
	private String COLUMNS_TYCHO2[] = {
			"HIP", "RA(ICRS)", "DE(ICRS)", "pmRA", "pmDE", "VTmag", "BTmag", "TYC1" , "TYC2", "TYC3"      };
	//private String COLUMNS_HIPPARCOS_INPUT[] = {
			//"HIC", "RV"   };



	//private int CATALOGUE;
	
	private PApplet processing;

	VizierFile(PApplet pa) {
		super();
		pa = processing;
	}

	public void load(String LINK) {
		try {
			URL url = new URL(LINK);
			try {
				URLConnection connection = url.openConnection();
				//DataInputStream in = new DataInputStream(connection.getInputStream());
				BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));

				String line = "";
				while (line!=null) {
					line = in.readLine();
					if (line!=null) add(line);
				}
			} 
			catch (IOException e) {
				System.out.println("IOEXception: "+e);
			}
		} 
		catch (MalformedURLException e) {
			System.out.println("MalformedURLException :"+e);
		}
	}

	public void removeComments() {
		for (int i=size()-1;i>=0;i--) {
			String line = ((String)elementAt(i));
			if (line.length()>0) {
				if (line.charAt(0)=='#') {
					this.removeElementAt(i);
				}
			}
		}
	}

	public void removeRows(int i0, int i1) {
		if (this.size()==0) return;
		int amount = i1-i0+1;
		for (int i=0;i<amount;i++) {
			if (this.size()>i0-1) this.removeElementAt(i0);
		}
	}

	/*
	private String[] splitIncludingEmpty(String line) {
		Vector cols = new Vector();
		int startIndex = 0;    
		for (int i=0;i<line.length();i++) {
			if (line.charAt(i)=='\t'||i==line.length()-1) {
				String col = line.substring(startIndex,i);
				cols.add(col);
				startIndex = i+1;
			}
		}
		String[] out = new String[cols.size()];
		for (int i=0;i<cols.size();i++) out[i] = (String)cols.elementAt(i);
		return out;
	}
	*/

	public void parseRadialVelocitiesToStars(Vector stars) {
		for (int i=0;i<size();i++) {
		}
	}

	public String makeURLHipparcosInput(String hipparcosNumberList) {
		String type = "asu-tsv";
		String catalogue = "-source=I%2F196%2Fmain";
		String LINK = "http://vizier.u-strasbg.fr/viz-bin/"+type+"?"+catalogue;
		LINK += "&HIC="+hipparcosNumberList;
		LINK += "&RV=!=0";  // only get the ones with radial velocities
		LINK += "&-out=HIC"; // output hipparcos catalogue number
		LINK += "&-out=RV"; // show radial velocities
		LINK += "&-out.max=unlimited&-out.form=Tab-Separated-Values";
		return LINK;
	}



	public String makeURL(int vizFileCatIndex,float ra_min,float ra_max,float dec_min,float dec_max,float mag_min,float mag_max) {
		String type = "asu-tsv"; // asu-tsv or VizieR-4
		String catalogue = "";
		if (vizFileCatIndex == VizierFile.CATALOGUE_HIPPARCOS)
			catalogue = "-source=I%2F239%2Fhip_main";
		else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO1)
			//catalogue = "-source=I%2F250%2Fcatalog";
			catalogue = "-source=I%2F239%2Ftyc_main";
		else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO2)
			catalogue = "-source=I%2F259%2Ftyc2";

		String LINK = "http://vizier.u-strasbg.fr/viz-bin/"+type+"?"+catalogue;

		// RA
		if (ra_min<0&&ra_max<0) {
			ra_min+=360;
			ra_max+=360;
		}
		if (ra_max>360)
			LINK += "&RA(ICRS)="+ra_min+"..360,0.."+(ra_max-360);
		else if (ra_min<0)
			LINK += "&RA(ICRS)="+(ra_min+360)+"..360,0.."+ra_max;
		else
			LINK += "&RA(ICRS)="+ra_min+".."+ra_max;

		// DEC:
		LINK += "&DE(ICRS)="+dec_min+".."+dec_max;    

		if (vizFileCatIndex == VizierFile.CATALOGUE_HIPPARCOS) {
			LINK += "&Vmag="+mag_min+".."+mag_max;
			for (int i=0;i<COLUMNS_HIPPARCOS.length;i++)
				LINK += "&-out="+COLUMNS_HIPPARCOS[i];
		} 

		else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO1) {
			LINK += "&Vmag="+mag_min+".."+mag_max;
			for (int i=0;i<COLUMNS_TYCHO1.length;i++)
				LINK += "&-out="+COLUMNS_TYCHO1[i];
		} 

		else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO2) {
			LINK += "&VTmag="+mag_min+".."+mag_max; // questionable
			for (int i=0;i<COLUMNS_TYCHO2.length;i++)
				LINK += "&-out="+COLUMNS_TYCHO2[i];
		}

		//    else if (vizFileCatIndex == VizierFile.CATALOGUE_TYCHO2) {
		//      LINK += "&VTmag="+mag_min+".."+mag_max;
		//    }

		LINK += "&-out.max=unlimited"; // unlimited
		LINK += "&-out.form=Tab-Separated-Values";

		return LINK;
	}



	private boolean hipAlreadyLoaded(Stars stars, int hip) {
		if (hip==0) return false;
		//    println(hip);
		for (int s=0;s<stars.size();s++) 
			if ( ((Star)stars.elementAt(s)).getNAME().substring(0,3).equals("HIP") )
				if (hip==((Star)stars.elementAt(s)).getHIP()) return true;
		return false;
	}





	public void parseToStars(StarsMain pa, int vizFileCatIndex, Stars stars) {

		// check the first line that looks like this:
		// ------	------------	------------	--------	--------	-----	------	-------
		// to establish column widths

		if (size()<2) return;
		String firstLine = ((String)elementAt(0));
		//println(firstLine);

		int[] columnStart = new int[0];
		int[] columnWidth = new int[0];
		int fullWidth = firstLine.length();

		char lastChar='.';
		for (int i=0;i<fullWidth;i++) {
			char ch = firstLine.charAt(i);
			if (ch=='-') {
				if (lastChar!=ch) {
					columnStart = PApplet.append(columnStart,i);
					columnWidth = PApplet.append(columnWidth,1);
				}
				else columnWidth[columnWidth.length-1]++;
			}
			lastChar = ch;
		}

		//
		// Now, for each star:
		//

		int starsAdded = 0;
		pa.logger.log("data for "+(size()-1)+" stars loaded");

		for (int i=1;i<size();i++) {


			String line = (String)elementAt(i);
			//      println(line);
			if (line.length()>1) {

				// split each (non-empty) line into columns
				// ------------------------
				String cs[] = new String[0];
				for (int c=0;c<columnStart.length;c++) 
					cs = PApplet.append(cs,line.substring(columnStart[c],columnStart[c]+columnWidth[c]));


				// HIPPARCOS:
				if (vizFileCatIndex==CATALOGUE_HIPPARCOS) {
					if (cs.length>=8) {
						//           Star(name        ,hip  , ra   , de   , spmra,  pmde, vmag , bv   , plx  ) 
						Star s = new Star(pa,"HIP "+cs[0],cs[0], cs[1], cs[2], cs[3], cs[4], cs[5], cs[6], cs[7]);
						stars.add(s);
						starsAdded++;
						//print(cs[7]+"\t");
					}
				} 


				// TYCHO 1:
				else if (vizFileCatIndex==CATALOGUE_TYCHO1) {  
					if (cs.length>=9) {
						int hipNumber = 0;
						boolean hasNoHipNumber = false;
						try {
							hipNumber = (int)Float.parseFloat(cs[0]);
						} catch (NumberFormatException e) {
							hasNoHipNumber = true;
						}
						
						boolean hipIsAlreadyLoaded = false;
						if (!hasNoHipNumber) hipIsAlreadyLoaded = hipAlreadyLoaded(stars,hipNumber);
						
						if (hasNoHipNumber || !hipIsAlreadyLoaded) {
						//if (!hipAlreadyLoaded(stars,(int)Float.parseFloat(cs[0]))) {
							//           Star(name         ,hip  , ra   , de   , spmra,  pmde, vmag , bv   , plx  ) 
							Star s = new Star(pa,"TYC1 "+cs[8],"", cs[1], cs[2], cs[3], cs[4], cs[5], cs[6], cs[7]);
							// TODO: CHECK IF THIS SPLIT WORKS!
							
							// split a String like "1261  1502 1" into 1261 1502 1
							String[] tycho123 = new String[0];
							
							boolean lastCharIsSpace = true;
							int beginIndex = 0, endIndex = 0 ;
							for (int c=0;c<cs[8].length();c++) {
								char currentChar = cs[8].charAt(c);
								boolean currentCharIsSpace = (currentChar==' ');
								
								if (lastCharIsSpace&&!currentCharIsSpace) {
									// beginning of a new number:
									beginIndex = c;
								}
								else if (!lastCharIsSpace&&currentCharIsSpace) {
									endIndex = c; // excluding
									tycho123 = PApplet.append(tycho123,cs[8].substring(beginIndex,endIndex));
									// end of word
								}
								if (c==cs[8].length()-1) {
									tycho123 = PApplet.append(tycho123,cs[8].substring(beginIndex));
								}
								
								lastCharIsSpace = currentCharIsSpace;
							}
							
							//System.out.println(cs[8]+":");
							//for (int c=0;c<tycho123.length;c++) System.out.println("\t"+tycho123[c]);
							
							
							s.setTycho123( (int)Float.parseFloat(tycho123[0]) , (int)Float.parseFloat(tycho123[1]) , (int)Float.parseFloat(tycho123[2]) );
							stars.add(s);
							starsAdded++;
							//print(cs[7]+"\t");
						} 
						else {
							//println("Star TYC1 "+cs[8]+" already loaded as HIP "+cs[0]+" thus skipping");
						}
					}
				}


				// TYCHO 2:
				// "HIP", "RA(ICRS)", "DE(ICRS)", "pmRA", "pmDE", "VTmag", "BTmag", "TYC1" , "TYC2", "TYC3"
				else if (vizFileCatIndex==CATALOGUE_TYCHO2) {
					if (cs.length>=9) {
						
						
						int hipNumber = 0;
						boolean hasNoHipNumber = false;
						try {
							hipNumber = (int)Float.parseFloat(cs[0]);
						} catch (NumberFormatException e) {
							hasNoHipNumber = true;
						}
						boolean hipIsAlreadyLoaded = false;
						if (!hasNoHipNumber) hipIsAlreadyLoaded = hipAlreadyLoaded(stars,hipNumber);
						if (hasNoHipNumber || !hipIsAlreadyLoaded) {
						
						//if (!hipAlreadyLoaded(stars,(int)Float.parseFloat(cs[0]))) { //int(float(cs[0])))) {
							int t1 = (int)Float.parseFloat(cs[7]);
							int t2 = (int)Float.parseFloat(cs[8]);
							int t3 = (int)Float.parseFloat(cs[9]);

							Star star = new Star();
							star = stars.getTychoStar(t1,t2,t3);
							if (star!=null) {
								//println("Star "+star.NAME+" already present, only adjusting proper motion");
							} 
							else {
								//println("Creating a new star without parallax, converting VT BT to V BV");
								//V = VT -0.090*(BT-VT) 
								//B-V = 0.850*(BT-VT) Consult Sect 1.3 of Vol 1 of "The Hipparcos and Tycho Catalogues", ESA SP-1200, 1997, for details. 
								float VT = Float.parseFloat(cs[5]);
								float BT = 0;
								try {
									BT = Float.parseFloat(cs[6]);
								} catch (NumberFormatException e) {
									pa.logger.log("Tycho2 loading: "+t1+" "+t2+" "+t3+" has no BT, setting to zero");
								}
								float V = VT -0.090f*(BT-VT);
								float BV = 0.850f*(BT-VT);
								//         Star(name                    ,hip  , ra   , de   , spmra,  pmde, vmag  , bv      , plx  ) 
								star = new Star(pa,"TYC2 "+t1+" "+t2+" "+t3,cs[0], cs[1], cs[2], cs[3], cs[4], ""+V, ""+BV , "0" );
								star.setTycho123(t1,t2,t3);
								stars.add(star);
								starsAdded++;
							}


						}
					}
				}


				else if (vizFileCatIndex==CATALOGUE_HIPPARCOS_INPUT) {
					if (cs.length>=2) {
						int hip = -999999;
						try {
							hip=(int)Float.parseFloat(cs[0]);
						} catch (NumberFormatException e) {
							
						}
						
						if (hip!=-999999) {
							//int hip = (int)Float.parseFloat(cs[0]);
							Star star = stars.getHipparcosStar(hip);
							if (star!=null) {
								if (star.getRV()==0) {
									pa.logger.log("Setting radial velocity for star "+star.getNAME()+" to "+Float.parseFloat(cs[1]));
									star.setRV(Float.parseFloat(cs[1]));
								}
							}
						}
					}
				}

			} 
		}

		pa.logger.log(starsAdded+" stars added");
	}

	public void saveToFile(StarsMain pa,String s) {
		String[] outFile = new String[size()];
		for (int i=0;i<size();i++) {
			outFile[i] = (String)elementAt(i);
		}
		pa.saveStrings(s,outFile);
		pa.logger.log(s+" saved");
	}

	public void loadFromFile(StarsMain pa,String s) {
		pa.logger.log("loading "+s);
		String[] inFile = pa.loadStrings(s);
		for (int i=0;i<inFile.length;i++) {
			add(inFile[i]);
		}
	}

}
