


public class World3D {

	public static final int EYE_LEFT = 0;
	public static final int EYE_RIGHT = 1;

	//private float WORLD3D_FACTOR;
	
	private Settings settings;

	World3D(Settings _settings) {
		settings = _settings;
		//pixX = settings.SCREEN_WIDTH;
		//showValues();
		//recalc3DFactor();
	}

	/*
	public void showValues() {
		System.out.println("[Q]/[A]\treference distance: "+StringTools.numberFormat(settings.WORLD3D_REFERENCE_DISTANCE,1,1)+" parsec");
		System.out.println("[W]/[S]\tseparation of images: "+StringTools.numberFormat(sepScreen,1,1));
		System.out.println("[E]/[D]\tscreen width: "+StringTools.numberFormat(xScreen,1,1));
	}
	*/



	public float screen2DToScreen3DX(int eye, float x, float z) {
		
		/*
		 * 		(1) I'd like to see the basic formula that you are using to calculate 
		 * lateral separations. Jos used:
		 * $fact = (1-($dRef/$dStar)) * $sepScreen * $pixX / $xScreen;
		 * $xcoordR = $xcoordL + $fact;
		 * where:
		 * sepscreen = Maximum separation of image on screen (mm)
		 * xScreen  = X dimension of screen in meters (default 6 m)
		 * dRef = Reference distance (pc)
		 * pixX = Number of pixels in X (default 800)
		 * 
		 */
		

		float fact = (1-(settings.WORLD3D_REFERENCE_DISTANCE/z)) * settings.WORLD3D_FACTOR;
		float out = x + eye*fact;
		return out;
	}

	public float screen2DToScreen3DY(float y, float z) {
		return y;
	}

}
