
import graphics.HighQualityImage;


public class Star {

	private String NAME;
	private int HIP;
	private int TYC1, TYC2, TYC3;
	private float RA;
	private float DE;
	private float Plx;
	private float pmRA;
	private float pmDE;
	private Vec posPc;
	private float Vmag;
	private float BV;
	private float RV = 0;            // radial velocity in km/s WHICH DIRECTION??

	private float RAti;
	private float DEti;
	private float distance;	// parsec
	private float distanceTi; // distance changing over time

	public static final double PARSEC_IN_KILOMETERS = 3.08568025 * Math.pow(10,13) ;
	public static final double SIDEREAL_YEAR_IN_SECONDS = 365*24*3600 + 6*3600 + 9*60 + 9;// s365.256363051;

	private HighQualityImage img;

	//private static final float Vmag_MIN = -2;
	private static final float Vmag_MAX = 15;

	private float x,y;
	private float CENTER_OF_IMAGE;
	
	private Settings settings;

	Star() {
	}

	Star(StarsMain pa, String name, String hip, String ra, String de, String pmra, String pmde, String vmag, String bv, String plx) {
		settings = pa.settings;
		NAME = name;
		//String logMessage = NAME+" created";
		//pa.logger.log; //hip
		
		//int hipNumber = 0;
		try {
			HIP = (int)Float.parseFloat(hip);
			//logMessage += "\nhip number: "+hip;
		} catch (NumberFormatException e) {
			//logMessage += "\nno hip number";
		}
		
		//HIP = (int)Float.parseFloat(hip);//int(float(hip));
		RA = Float.parseFloat(ra);
		DE = Float.parseFloat(de);//float(de);
		try {
			Plx = Float.parseFloat(plx);	
			//logMessage += "\nparallax: "+plx;
		} catch (NumberFormatException e) {
			//logMessage += "\nno parallax, setting to infinity ("+Astro.INFINITE_PARALLAX+")";
			Plx = Astro.INFINITE_PARALLAX;
		};
		
		if (Plx==0) {
			//pa.logger.log("parallax is zero , setting to infinity ("+Astro.INFINITE_PARALLAX+")",Logger.TO_STOUT);
			Plx = Astro.INFINITE_PARALLAX;
		} else if (Plx<0) {
			pa.logger.log("parallax less than zero, setting to infinity ("+Astro.INFINITE_PARALLAX+")",Logger.TO_STOUT);
			Plx = Astro.INFINITE_PARALLAX;
		}
		
		distance = 1000 / Plx;
		
		try {
			pmRA = Float.parseFloat(pmra);
			//logMessage += "\nproper motion RA: "+pmRA;
		} catch (NumberFormatException e) {
			//logMessage += "\nno proper motion RA, setting to zero";
			pmRA = 0;
		}
		
		try {
			pmDE = Float.parseFloat(pmde);
			//logMessage += "\nproper motion DE: "+pmDE;
		}	catch (NumberFormatException e) {
			//logMessage += "\nno properMotion DE, setting to zero";
			pmDE = 0;
		}

		Vmag = Float.parseFloat(vmag);//float(vmag);
		
		float bvFloat = 0;
		boolean bvIsNumber = true;
		try {
			bvFloat = Float.parseFloat(bv);
		} catch (NumberFormatException e) {
			//e.printStackTrace();
			bvIsNumber = false;
		}
		
		if (!bvIsNumber) {
			//logMessage += "\nno BV available, setting to 0";
			BV = 0;
		} else {
			//logMessage += "\nBV: "+bvFloat;
			BV = bvFloat;
		}
		
		RAti = RA;
		DEti = DE;
		
		distanceTi = distance;
		//pa.logger.log(logMessage);
		
	}
	
	public void createRelativeStarPositionPc(Settings settings) {
		posPc = new Vec(0,0,distance);
		posPc.rotX(2*Math.PI*(DE-settings.SIMULATION_DEC_CEN)/360.0);
		posPc.rotY(-2*Math.PI*(RA-settings.SIMULATION_RA_CEN)/360.0);
	}
	
	public String getNAME() {
		return NAME;
	}

	void setTycho123(int t1, int t2, int t3) {
		TYC1 = t1;
		TYC2 = t2;
		TYC3 = t3;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	public Vec getPosPc() {
		return posPc;
	}

	public float getVmag() {
		return Vmag;
	}

	public float getBV() {
		return BV;
	}

	public void setRV(float rv) {
		RV = rv;
	}

	public float getRV() {
		return RV;
	}
	
	public void setParallax(float p) {
		Plx = p;
	}
	public float getParallax() {
		return Plx;
	}
	
	
	public void setDistanceInParsec(float d) {
		distance = d;
		//distanceTi = d;
	}
	public float getDistanceInParsec() {
		return distance;
	}
	
	public int getTYC1() {
		return TYC1;
	}

	public int getTYC2() {
		return TYC2;
	}

	public int getTYC3() {
		return TYC3;
	}
	
	public int getHIP() {
		return HIP;
	}

	public HighQualityImage getImage() {
		return img;
	}






	public void setTimeInRelationToEpoch(float deltaT) {
		//float deltaT = t - EPOCH;
		RAti = (float) (RA + Astro.milliArcSecondToDegree(deltaT*pmRA));
		while (RAti<0) RAti+=360;
		while (RAti>=360) RAti-=360;
		DEti = (float) (DE + Astro.milliArcSecondToDegree(deltaT*pmDE));

		distanceTi = distance + (float)(deltaT*RV*SIDEREAL_YEAR_IN_SECONDS/PARSEC_IN_KILOMETERS);

	}
	
	/**
	 * Changes position of star according to proper motion and radial velocity
	 * according to the change of time
     *
     * @param      t   Advance <code>double</code> years
     */
	
	public void advanceTime(double t) {
		RAti += Astro.milliArcSecondToDegree(t*pmRA);
		if (RAti<0) RAti+=360;
		if (RAti>=360) RAti-=360;
		DEti += Astro.milliArcSecondToDegree(t*pmDE);

		distanceTi += (float)(t*RV*SIDEREAL_YEAR_IN_SECONDS/PARSEC_IN_KILOMETERS);
	}



	public void makePos() {

		float raCen = settings.SIMULATION_RA_CEN;
		float deCen = settings.SIMULATION_DEC_CEN;
		float raRange = settings.SIMULATION_RA_VIEW_RANGE;
		float deRange = settings.SIMULATION_DEC_VIEW_RANGE; 
		float raMin = raCen-raRange/2.0f, raMax = raCen+raRange/2.0f;
		float deMin = deCen-deRange/2.0f;

		// WRAP AROUND RA/DEC!!!
		x = settings.SCREEN_WIDTH - settings.SCREEN_WIDTH * (float)((RAti-raMin)/raRange);
		if (raMin<0&&RAti>raMax) x = settings.SCREEN_WIDTH - settings.SCREEN_WIDTH * (float)(((RAti-360)-raMin)/raRange);
		if (raMax>360&&RAti<raMin) x = settings.SCREEN_WIDTH - settings.SCREEN_WIDTH * (float)(((RAti+360)-raMin)/raRange);
		y = settings.SCREEN_HEIGHT - settings.SCREEN_HEIGHT * (float)((DEti-deMin)/deRange);

	}



	public void drawEllipse(HighQualityImage screenImage) {

		float radius = 12;
		float TWO_PI = (float)(2*Math.PI);
		float C = TWO_PI*radius;

		for (float p=0;p<1;p+=1.0/C) {
			float xo = (float)(radius*Math.cos(TWO_PI*p));
			float yo = (float)(radius*Math.sin(TWO_PI*p));
			screenImage.set(x+xo,y+yo,0xFFFF,0x0000,0x0000,0.15f);
		}
	}
	
	
	
	public void drawImageAt(HighQualityImage screenImage, float x, float y) {
		float xo = x-CENTER_OF_IMAGE;
		float yo = y-CENTER_OF_IMAGE;
		if (xo>settings.SCREEN_WIDTH) return;
		if (yo>settings.SCREEN_HEIGHT) return;
		if (x+CENTER_OF_IMAGE<0) return;
		if (y+CENTER_OF_IMAGE<0) return;
		
		/*
		int xoInt = (int)Math.floor(xo);
		int yoInt = (int)Math.floor(yo);
		float xoResidue = xo-xoInt;
		float yoResidue = yo-yoInt;
		HighQualityImage imageToDraw = new HighQualityImage(settings.STAR_SIZE+1,settings.STAR_SIZE+1);
		imageToDraw.background(0x0000,0x0000,0x0000);
		for (int xi=0;xi<settings.STAR_SIZE;xi++) for (int yi=0;yi<settings.STAR_SIZE;yi++) {
			imageToDraw.setAdd(xi+xoResidue,yi+yoResidue,img.getRGB(xi,yi));
		}
		for (int xi=0;xi<imageToDraw.width;xi++) for (int yi=0;yi<imageToDraw.height;yi++) {
			screenImage.setAdd(xi+xoInt,yi+yoInt,imageToDraw.getRGB(xi,yi));
		}
		*/
		
		for (int xi=0;xi<settings.STAR_SIZE;xi++) for (int yi=0;yi<settings.STAR_SIZE;yi++) {
			screenImage.setAdd(xi+xo,yi+yo,img.getRGB(xi,yi));
		}
	}

	// BASED ON FULL IMAGE
	public void drawImage(HighQualityImage screenImage) {
		drawImageAt(screenImage,x,y);
	}




	// FULL IMAGE:
	public void createStarImage() {
		float moffatRadius = settings.STAR_MOFFAT_RADIUS_EXAGGERATION * 0.7f * settings.SCREEN_WIDTH / (100.0f*settings.SIMULATION_RA_VIEW_RANGE) ;
		img = new HighQualityImage(settings.STAR_SIZE,settings.STAR_SIZE);  //10
		CENTER_OF_IMAGE = (settings.STAR_SIZE-1)/2.0f;
		float maxR = (float)(CENTER_OF_IMAGE*Math.sqrt(2));  // 4.5 * 1.4 = 
		for (int x=0;x<settings.STAR_SIZE;x++) for (int y=0;y<settings.STAR_SIZE;y++) {
			float dx = x-CENTER_OF_IMAGE;
			float dy = y-CENTER_OF_IMAGE;
			float R = (float)Math.sqrt(dx*dx+dy*dy);
			int[] rgb = Astro.Color_at_R(R,maxR,Vmag,Vmag_MAX,BV,settings.STAR_FADE_EDGES,settings.STAR_NOISE,settings.STAR_NOISE_LEVEL,moffatRadius);
			img.set(x,y,rgb[0],rgb[1],rgb[2]);
		}
		img.updatePImage();
	}





}
