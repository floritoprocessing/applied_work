
import processing.core.*;

public class ScreenText {
	
	private static final int COLOR_NORMAL = 0xFF000000;
	private static final int COLOR_CHANGED = 0xFF000000;
	private static final int COLOR_UNCHANGED = 0xFF808080;
	private static final int COLOR_CHANGED_OVER = 0xFF800000;
	private static final int COLOR_UNCHANGED_OVER = 0xFFFF8080;
	
	public static void makeAndChange(PApplet pa, Settings settings, int settingsVar, char dec, char inc, String text, float x, float y) {
		char DEC = String.valueOf(dec).toUpperCase().charAt(0);
		char INC = String.valueOf(inc).toUpperCase().charAt(0);
		boolean mouseOver = false;
		int varInc = 0;
		String outText = "";
		if (DEC==INC) outText = "["+DEC+"] "+text+": "+Settings.parseVariable(settings.getNewVariable(settingsVar),settingsVar)+" ("+Settings.parseVariable(settings.getVariable(settingsVar),settingsVar)+")";
		else outText = "["+DEC+"]/["+INC+"] "+text+": "+Settings.parseVariable(settings.getNewVariable(settingsVar),settingsVar)+" ("+Settings.parseVariable(settings.getVariable(settingsVar),settingsVar)+")";
		
		// increase/decrease by keyboard
		boolean changeValue = false;
		if (pa.keyPressed) {
			if (pa.key==dec) varInc=-1; else if (pa.key==inc) varInc=1;
			else if (pa.key==DEC) varInc=-10; 
			else if (pa.key==INC) varInc=10;
		}
		
		// increase/decrease by mouse
		if (pa.mouseX>=x&&pa.mouseX<=x+pa.textWidth(outText)) {
			if (pa.mouseY>=y-pa.textAscent()&&pa.mouseY<=y+pa.textDescent()) {
				mouseOver = true;
				if (pa.mousePressed) {
					if (pa.mouseButton==PApplet.LEFT) {
						varInc = -1;
					} else if (pa.mouseButton==PApplet.RIGHT) {
						varInc = 1;
					}
					if (pa.keyPressed) if (pa.key==PApplet.CODED) if (pa.keyCode==PApplet.SHIFT) varInc*=10;
				}
			}
		}
		
		if (varInc!=0) settings.changeNewVariable(settingsVar,varInc);
		
		if (settings.variableHasChanged(settingsVar)) {
			if (mouseOver) pa.fill(COLOR_CHANGED_OVER);
			else pa.fill(COLOR_CHANGED); 
		} else  {
			if (mouseOver) pa.fill(COLOR_UNCHANGED_OVER);
			else pa.fill(COLOR_UNCHANGED);
		}
		pa.text(outText,x,y);
		
	}
	
	public static void makeAndChange(PApplet pa, Settings settings, int settingsVar, char dec, String text, float x, float y) {
		makeAndChange(pa, settings, settingsVar, dec, dec, text, x, y);
	}

	public static void make(PApplet pa, String text, float x, float y) {
		pa.fill(COLOR_NORMAL);
		pa.text(text,x,y);
	}
	
}
