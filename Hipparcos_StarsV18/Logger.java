
//import processing.core.*;
import java.io.FileWriter;
import java.io.IOException;

public class Logger {

	public static final int TO_FILE = 1;
	public static final int TO_STOUT = 2;
	public static final int TO_ALL = 3;
	
	private String filename;
	private boolean logToDisk = true;

	public Logger(Settings settings,String _filename,boolean _logToDisk) {
		logToDisk = _logToDisk;
		filename = _filename;
		if (logToDisk) {
			try {
				FileWriter fw = new FileWriter(filename,false);
				fw.write("Project log "+settings.PROJECT_NAME);
				fw.close();
			} catch (IOException ioe) {
				System.err.println("IOException: " + ioe.getMessage());
			}
		}
		//log = new String[0];
	}

	public void log(String s) {
		log(s,TO_ALL);
	}
	
	public void log(String s, int target) {
		boolean toFile = (target&0x01)==1;
		boolean toStout = (target>>1&0x01)==1;
		if (toStout) System.out.println(s);
		
		if (logToDisk&&toFile) {
			try {
				FileWriter fw = new FileWriter(filename,true);
				fw.write(s+"\n");//appends the string to the file
				fw.close();
			} catch (IOException ioe) {
				System.err.println("IOException: " + ioe.getMessage());
			}
		}
	}

	public void save(String filename) {
		//File file = new File(filename);
		//PApplet.saveStrings(file, log);
	}


}
