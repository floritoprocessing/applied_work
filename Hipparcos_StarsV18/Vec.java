
/*****************************
 *  Vec class and functions  *
 *****************************/

public class Vec {
	double x=0,y=0,z=0;

	public Vec() {
	}

	public Vec(double _x, double _y, double _z) {
		x=_x;
		y=_y;
		z=_z;
	}

	public Vec(Vec _v) {
		x=_v.x; 
		y=_v.y; 
		z=_v.z;
	}

	public void setVec(Vec _v) {
		x=_v.x; 
		y=_v.y; 
		z=_v.z;
	}

	public void setVec(double _x, double _y, double _z) {
		x=_x;
		y=_y;
		z=_z;
	}

	public double len() {
		return Math.sqrt(x*x+y*y+z*z);
	}

	public double lenSQ() {
		return (x*x+y*y+z*z);
	}

	public void add(Vec _v) {
		x+=_v.x;
		y+=_v.y;
		z+=_v.z;
	}

	public void add(double _x, double _y, double _z) {
		x+=_x;
		y+=_y;
		z+=_z;
	}

	public void sub(Vec _v) {
		x-=_v.x;
		y-=_v.y;
		z-=_v.z;
	}

	public void mul(double p) {
		x*=p;
		y*=p;
		z*=p;
	}

	public void div(double p) {
		x/=p;
		y/=p;
		z/=p;
	}

	public void negX() {
		x=-x;
	}

	public void negY() {
		y=-y;
	}

	public void negZ() {
		z=-z;
	}

	public void neg() {
		negX();
		negY();
		negZ();
	}

	public void normalize() {
		double l=len();
		if (l!=0) { // if not nullvector
			div(l);
		} 
		else {
			x=0;
			y=0;
			z=0;
		}
	}

	public Vec getNormalized() {
		double l=len();
		if (l!=0) {
			Vec out=new Vec(this);
			out.div(l);
			return out;
		} 
		else {
			return new Vec();
		}
	}

	public void setLen(double ml) {
		//    Vec out=new Vec(this);
		double l=len();
		if (l!=0) {
			double fac=ml/l;
			x*=fac;
			y*=fac;
			z*=fac;
		}
	}

	public void rotX(double rd) {
		double SIN=Math.sin(rd); 
		double COS=Math.cos(rd);
		double yn=y*COS-z*SIN;
		double zn=z*COS+y*SIN;
		y=yn;
		z=zn;
	}

	public void rotY(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-z*SIN; 
		double zn=z*COS+x*SIN;
		x=xn;
		z=zn;
	}

	public void rotZ(double rd) {
		double SIN=Math.sin(rd);
		double COS=Math.cos(rd); 
		double xn=x*COS-y*SIN; 
		double yn=y*COS+x*SIN;
		x=xn;
		y=yn;
	}

	public void rot(double xrd, double yrd, double zrd) {
		rotX(xrd);
		rotY(yrd);
		rotZ(zrd);
	}

	public boolean isNullVec() {
		if (x==0&&y==0&&z==0) {
			return true;
		} 
		else {
			return false;
		}
	}

	public void clip(Vec c) {
		while (x<0) x+=c.x;
		while (x>=c.x) x-=c.x;
		while (y<0) y+=c.y;
		while (y>=c.y) y-=c.y;
		while (z<0) z+=c.z;
		while (z>=c.z) z-=c.z;
	}

	public void constrain(Vec mi, Vec ma) {
		if (x<mi.x) x=mi.x;
		if (x>ma.x) x=ma.x;
		if (y<mi.y) y=mi.y;
		if (y>ma.y) y=ma.y;
		if (z<mi.z) z=mi.z;
		if (z>ma.z) z=ma.z;
	}

	public double getLongitude() {
		return Math.atan2(z,x);
	}

	public double getLatitude() {
		Vec tmp=new Vec(this);
		tmp.rotY(-getLongitude());
		return Math.atan2(tmp.y,tmp.x);
	}

	public double[] getLonLat() {
		double[] lonlat=new double[2];
		lonlat[0]=Math.atan2(z,x);
		Vec tmp=new Vec(this);
		tmp.rotY(-lonlat[0]);
		lonlat[1]=Math.atan2(tmp.y,tmp.x);
		return lonlat;
	}

	public String toString() {
		return (x+"\t"+y+"\t"+z);
	}
	
	

	public static Vec vecAdd(Vec a, Vec b) {
		Vec out=new Vec(a);
		out.add(b);
		return out;
	}

	public static Vec vecSub(Vec a, Vec b) {
		Vec out=new Vec(a);
		out.sub(b);
		return out;
	}

	public static Vec vecMul(Vec a, double b) {
		Vec out=new Vec(a);
		out.mul(b);
		return out;
	}

	public static Vec vecComponentMul(Vec a, Vec b) {
		Vec out=new Vec(a);
		out.x*=b.x;
		out.y*=b.y;
		out.z*=b.z;
		return out;
	}

	public static double vecMulSkalar(Vec a, Vec b) {
		double out=a.x*b.x+a.y*b.y+a.z*b.z;
		return out;
	}

	public static Vec vecDiv(Vec a, double b) {
		Vec out=new Vec(a);
		out.div(b);
		return out;
	}

	public static double vecLen(Vec a) {
		return a.len();
	}

	public static Vec vecRotY(Vec a, double rd) {
		Vec out=new Vec(a);
		out.rotY(rd);
		return out;
	}

	public static void vecSwap(Vec v1, Vec v2) {
		Vec t=new Vec(v1);
		v1.setVec(v2);
		v2.setVec(t);
	}

	public static double vecDistance(Vec v1, Vec v2) {
		double dx = v2.x-v1.x;
		double dy = v2.y-v1.y;
		double dz = v2.z-v1.z;
		return Math.sqrt(dx*dx+dy*dy+dz*dz);
	}

	public static double vecAngleBetween(Vec v1, Vec v2) {
		double out=Math.acos(vecMulSkalar(v1,v2)/(vecLen(v1)*vecLen(v2)));
		return out;
	}
}





