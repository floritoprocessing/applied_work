

import processing.core.*;
import java.text.DecimalFormat;
import java.io.File;
import graphics.HighQualityImage;
import graphics.Bmp;

public class Animation {

	//public float YEARS_PER_FRAME;
	private int renderStep=0;
	//private int showStep=0;
	private int steps;

	public boolean RENDERED = false;

	private PImage img[];

	private StarPositionFile starPosFile;
	private Stars stars;
	HighQualityImage frameBlendingImage[];
	
	StarsMain mainApplet;
	Settings settings;
	
	private int startMillis;
	private float avTotalSeconds = 0;

	Animation(StarsMain pa, int w, int h, float e0, float e1, float es, Stars _stars) {
		mainApplet = pa;
		settings = mainApplet.settings;
		//YEARS_PER_FRAME = es;
		steps = (int)((e1-e0)/es);
		img = new PImage[steps];
		for (int i=0;i<img.length;i++) img[i]=null;
		pa.logger.log("Starting animation with "+steps+" frames.");
		stars = _stars;
		if (settings.ANIMATION_SAVE_STAR_POSITIONS) starPosFile = new StarPositionFile(steps,stars.size());
		frameBlendingImage = new HighQualityImage[settings.ANIMATION_FRAME_BLENDING_FRAMES];
		
		if (settings.ANIMATION_SAVE_SINGLE_FRAME) {
			File folder = new File(settings.ANIMATION_DISK_PATH_NORMAL);
			if (!folder.exists()) folder.mkdirs();
		}
		
		if (settings.ANIMATION_SAVE_FRAME_BLENDING) {
			File folder = new File(settings.ANIMATION_DISK_PATH_FRAME_BLENDING);
			if (!folder.exists()) folder.mkdirs();
		}
		
		if (settings.ANIMATION_SAVE_STAR_POSITIONS) {
			File folder = new File(settings.ANIMATION_DISK_PATH_STAR_POSITIONS);
			if (!folder.exists()) folder.mkdirs();
		}
		
		String[] s = settings.asString();
		String settingsName = settings.PROJECT_PATH + settings.PROJECT_NAME + "_settings.txt";
		pa.logger.log("Saving settings to txt file: "+settingsName);
		pa.saveStrings(settingsName, s);
		
		startMillis = mainApplet.millis();
	}

	void storeFrame(HighQualityImage img) {

		mainApplet.logger.log("Image created: "+renderStep+" / "+steps);
		if (settings.ANIMATION_SAVE_SINGLE_FRAME) {
			String name = settings.PROJECT_NAME+"_"+Settings.RENDER_MODE_NAME[settings.RENDER_MODE]+"_"+(new DecimalFormat("0000")).format(renderStep);//+".tga";
			mainApplet.logger.log("saving to disk: "+name);
			name = settings.ANIMATION_DISK_PATH_NORMAL+name;
			Bmp bmp = new Bmp(mainApplet,img.width,img.height,24);
			bmp.capture24Bit(img);
			bmp.saveAs(name);
		}
		
		mainApplet.fill(255);
		mainApplet.text("Rendered image: "+renderStep+"/"+steps+" ("+(settings.ANIMATION_SAVE_SINGLE_FRAME?"saving":"not saving")+")",10,mainApplet.height-20);

		// store frame blending
		if (settings.ANIMATION_SAVE_FRAME_BLENDING) {
			// store frame
			frameBlendingImage[renderStep%settings.ANIMATION_FRAME_BLENDING_FRAMES] = new HighQualityImage(img);

			// compile to one image:
			if ((renderStep+1)%settings.ANIMATION_FRAME_BLENDING_FRAMES==0) {
				HighQualityImage blendImage = new HighQualityImage(img.width,img.height);
				for (int x=0;x<img.width;x++) for (int y=0;y<img.height;y++) {
					int R=0, G=0, B=0;
					for (int i=0;i<settings.ANIMATION_FRAME_BLENDING_FRAMES;i++) {
						R += frameBlendingImage[i].R[x][y];
						G += frameBlendingImage[i].G[x][y];
						B += frameBlendingImage[i].B[x][y];
					}
					R /= settings.ANIMATION_FRAME_BLENDING_FRAMES;
					G /= settings.ANIMATION_FRAME_BLENDING_FRAMES;
					B /= settings.ANIMATION_FRAME_BLENDING_FRAMES;
					blendImage.set(x,y,R,G,B);
				}
				blendImage.updatePImage();
				String name = settings.PROJECT_NAME+"_"+Settings.RENDER_MODE_NAME[settings.RENDER_MODE]+"_frameBlending_"+(new DecimalFormat("0000")).format(renderStep/settings.ANIMATION_FRAME_BLENDING_FRAMES);//+".tga";
				mainApplet.logger.log("Saving blendImage to disk: "+name);
				mainApplet.text("Frame blending image created: "+(renderStep/settings.ANIMATION_FRAME_BLENDING_FRAMES),10,mainApplet.height-5);
				name = settings.ANIMATION_DISK_PATH_FRAME_BLENDING+name;
				Bmp bmp = new Bmp(mainApplet,img.width,img.height,24);
				bmp.capture24Bit(blendImage);
				bmp.saveAs(name);
			} 
		}



		if (settings.ANIMATION_SAVE_STAR_POSITIONS) {
			int i=0;
			for (int s=0;s<stars.size();s++) {
				if (renderStep==0) starPosFile.setStarMagnitude(s,((Star)stars.elementAt(s)).getVmag());
				starPosFile.setStarPosition(renderStep,s,((Star)stars.elementAt(s)).getX(),((Star)stars.elementAt(s)).getY());
				i++;
			}
			mainApplet.logger.log(i+" star positions added");
		}


		renderStep++;
		if (renderStep==steps) {
			if (settings.ANIMATION_SAVE_STAR_POSITIONS) {
				mainApplet.logger.log("Creating star position file...");
				starPosFile.createFile();
				mainApplet.logger.log("Saving star position file...");
				starPosFile.saveFile(mainApplet,settings.ANIMATION_DISK_PATH_STAR_POSITIONS+settings.PROJECT_NAME);
				mainApplet.logger.log("Done.");
			}
			//RENDERED = true;
			mainApplet.switchToOfflineMode();
		}
		
		float secondsSinceStart = (mainApplet.millis() - startMillis)/1000;
		float averageSecondsPerFrame = (float)secondsSinceStart/renderStep;
		float totalSeconds = averageSecondsPerFrame * steps;
		avTotalSeconds = 0.9f*avTotalSeconds + 0.1f*totalSeconds;
		float timeRemaining = totalSeconds-secondsSinceStart;
		StringTools.secondsToHMS(secondsSinceStart);
		String timeMsg = "Time rendered: "+StringTools.secondsToHMS(secondsSinceStart);
		timeMsg += " of "+StringTools.secondsToHMS(avTotalSeconds);
		timeMsg += " (remaining: "+StringTools.secondsToHMS(timeRemaining)+")";
		mainApplet.text(timeMsg,10,mainApplet.height-40);
		mainApplet.logger.log(timeMsg);
	}
	
	/*
	PImage getCurrentFrame() {
		return img[showStep];
	}
	*/
	
	/*
	public void advance() {
		if (showStep<steps-1) showStep++;
		//if (showStep==steps) showStep=0;
	}
	*/

}
