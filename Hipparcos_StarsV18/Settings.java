import processing.core.*;

public class Settings {

	// -------------------------------------------------------------------
	// DISK PATHS:
	// -------------------------------------------------------------------

	public String PROJECT_NAME="IC2602_offCenter_2m";
	public String PROJECT_PATH = "V:\\Hipparcos Renderings\\";

	public String ANIMATION_DISK_PATH_NORMAL=PROJECT_PATH+PROJECT_NAME+"\\";
	public String ANIMATION_DISK_PATH_FRAME_BLENDING=PROJECT_PATH+PROJECT_NAME+"_FrameBlending\\";
	public String ANIMATION_DISK_PATH_STAR_POSITIONS=PROJECT_PATH+PROJECT_NAME+"_StarPositions\\";

	// -------------------------------------------------------------------
	// CATALOGUE SELECTION SETTINGS
	// -------------------------------------------------------------------

	public boolean LOAD_DATA_FROM_DISK = true;

	public boolean LOAD_HIPPARCOS = true;
	public boolean LOAD_TYCHO1 = false;
	public boolean LOAD_TYCHO2 = false;
	public boolean LOAD_RADIAL_VELOCITIES = false;

	public int LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME = 50;

	//-------------------------------------------------------------------
	// SKYFIELD SETTINGS
	// -------------------------------------------------------------------

	// MAKE SURE THAT THE ASPECT RATIO IS THE SAME AS THE RA/DEC_VIEW_RANGE ASPECT RATIO
	// IC 2391
	// public float SIMULATION_RA_CEN  = 130.07f;  
	// public float SIMULATION_DEC_CEN = -53.02f;
	// Alpha Per
	//public float SIMULATION_RA_CEN  = 51.08f;  
	//public float SIMULATION_DEC_CEN = 49.86f;
	// Praesepe
	//public float SIMULATION_RA_CEN  = 130.09f;  
	//public float SIMULATION_DEC_CEN = 19.67f;
	// Coma Cluster
	//public float SIMULATION_RA_CEN  = 185.62f;  
	//public float SIMULATION_DEC_CEN = 25.85f;
	// Hyades:
	//public float SIMULATION_RA_CEN  = 66.82f;// + 1.5f;  
	//public float SIMULATION_DEC_CEN = 15.79f;// - 1;
	// Pleiades: (dRef: 100)
	//public float SIMULATION_RA_CEN  = 56.7f + 1;  
	//public float SIMULATION_DEC_CEN = 24.2f - 2;
	// IC 2602: (dRef-150pc)
	public float SIMULATION_RA_CEN  = 160.7f - .5f;  
	public float SIMULATION_DEC_CEN = -64.4f + .5f;
	// Trapezium: (dRef: ?)
	//public float SIMULATION_RA_CEN  = 84f;  
	//public float SIMULATION_DEC_CEN = -5.5f;
	public float SIMULATION_RA_VIEW_RANGE  = 8;	// usually 8
	public float SIMULATION_DEC_VIEW_RANGE = 6; // usually 6
	public float SIMULATION_RA_CALC_RANGE  = 16;
	public float SIMULATION_DEC_CALC_RANGE = 12;
	public float SIMULATION_MAGNITUDE_MIN  = -1;	// usually -1
	public float SIMULATION_MAGNITUDE_MAX  = 15;	// usually 15, 7 for trapezium

	// -------------------------------------------------------------------
	// SIMULATION_MODE: ANIMATION SETTINGS
	// -------------------------------------------------------------------

	public float ANIMATION_START = 1991.25f;
	public float ANIMATION_STOP  = 150000;
	public float ANIMATION_STEP  = 300;

	public boolean ANIMATION_SAVE_SINGLE_FRAME = true;
	public boolean ANIMATION_SAVE_FRAME_BLENDING = false;
	public int ANIMATION_FRAME_BLENDING_FRAMES = 10;

	public boolean ANIMATION_SAVE_STAR_POSITIONS = false;

	// -------------------------------------------------------------------
	// SIMULATION_MODE: REALTIME SETTINGS
	// -------------------------------------------------------------------

	public float REALTIME_YEARS_PER_FRAME = 0;

	// -------------------------------------------------------------------
	// RENDER SETTINGS: MAIN
	// -------------------------------------------------------------------

	// MAKE SURE THAT THE ASPECT RATIO IS THE SAME AS THE RA/DEC_VIEW_RANGE ASPECT RATIO
	//	 changing SCREEN_WIDTH at runtime (which you can't) and you will have to call recalcWorld3DFactor()
	public int SCREEN_WIDTH = 1024;
	public int SCREEN_HEIGHT = 768;

	public static final int RENDER_MODE_2D_NORMAL = 0;
	public static final int RENDER_MODE_3D_LEFT = 1;
	public static final int RENDER_MODE_3D_RIGHT = 2;
	public static final int RENDER_MODE_3D_ANAGLYPH = 3;
	public static final int RENDER_MODE_3D_PARALLEL = 4;
	public static final int RENDER_MODE_3D_CROSS_EYED = 5;
	public static final String[] RENDER_MODE_NAME = { "2D", "left eye", "right eye", "anaglyph", "parallel", "cross-eyed" };

	public int RENDER_MODE = RENDER_MODE_2D_NORMAL;//3D_ANAGLYPH;

	private float RENDER_ANAGLYPH_INTENSITY_LR_RATIO = 1.0f/.196f*.157f;
	public float RENDER_ANAGLYPH_INTENSITY_LEFT = 1.0f;
	public float RENDER_ANAGLYPH_INTENSITY_RIGHT = RENDER_ANAGLYPH_INTENSITY_LEFT * RENDER_ANAGLYPH_INTENSITY_LR_RATIO;

	/*
	 * Hyades: 30
	 * Pleiades: 100
	 * IC: 150
	 */

	// -------------------------------------------------------------------
	// WORLD3D SETTINGS
	// -------------------------------------------------------------------

	public float WORLD3D_REFERENCE_DISTANCE = 150; 
	public float WORLD3D_SEPARATION_OF_EYES = 50;//50;
	public float WORLD3D_IMAGE_WIDTH = 2000; //300
	public float WORLD3D_FACTOR = WORLD3D_SEPARATION_OF_EYES * SCREEN_WIDTH / WORLD3D_IMAGE_WIDTH;

	// -------------------------------------------------------------------
	// RENDER SETTINGS: STAR
	// -------------------------------------------------------------------

	public int STAR_SIZE = 20; //pixels 60 is good, 80 is safe for 1024
	public boolean STAR_NOISE     = true;
	public float STAR_NOISE_LEVEL = 30.0f;
	public boolean STAR_FADE_EDGES = true;
	public float STAR_MOFFAT_RADIUS_EXAGGERATION = 1.0f; // usually 1.0, 2.5 for Trapezium huge

	
	
	
	
	
	
	
	
	// -------------------------------------------------------------------
	// MAIN CONSTRUCTOR Settings()
	// -------------------------------------------------------------------
	
	public static final int VAR_ANIMATION_START = 0;
	public static final int VAR_ANIMATION_STOP = 1;
	public static final int VAR_ANIMATION_STEP = 2;
	public static final int VAR_STAR_SIZE = 3;
	public static final int VAR_STAR_NOISE = 4;
	public static final int VAR_STAR_NOISE_LEVEL = 5;
	public static final int VAR_STAR_FADE_EDGES = 6;	
	public static final int VAR_WORLD3D_REFERENCE_DISTANCE = 7;
	public static final int VAR_WORLD3D_SEPARATION_OF_EYES = 8;
	public static final int VAR_WORLD3D_IMAGE_WIDTH = 9;

	private float NEW_ANIMATION_START, NEW_ANIMATION_STOP, NEW_ANIMATION_STEP;
	private float NEW_STAR_SIZE, NEW_STAR_NOISE, NEW_STAR_NOISE_LEVEL, NEW_STAR_FADE_EDGES;
	private float NEW_WORLD3D_REFERENCE_DISTANCE, NEW_WORLD3D_SEPARATION_OF_EYES, NEW_WORLD3D_IMAGE_WIDTH;
	private StarsMain main;
	private long lastTimeChanged;
	
	public Settings(StarsMain _main) {
		main = _main;
		lastTimeChanged = main.millis();
		resetNewVariables();
	}

	public void resetNewVariables() {
		NEW_ANIMATION_START = ANIMATION_START;
		NEW_ANIMATION_STOP = ANIMATION_STOP;
		NEW_ANIMATION_STEP = ANIMATION_STEP;
		NEW_STAR_SIZE = STAR_SIZE;
		NEW_STAR_NOISE = STAR_NOISE?1:0;
		NEW_STAR_NOISE_LEVEL = STAR_NOISE_LEVEL;
		NEW_STAR_FADE_EDGES = STAR_FADE_EDGES?1:0;
		NEW_WORLD3D_REFERENCE_DISTANCE = WORLD3D_REFERENCE_DISTANCE;
		NEW_WORLD3D_SEPARATION_OF_EYES = WORLD3D_SEPARATION_OF_EYES;
		NEW_WORLD3D_IMAGE_WIDTH = WORLD3D_IMAGE_WIDTH;
	}

	public boolean variableHasChanged(int variableIndex) {
		return getVariable(variableIndex)!=getNewVariable(variableIndex);
	}

	public float getVariable(int variableIndex) {
		switch (variableIndex) {
		case VAR_ANIMATION_START: return ANIMATION_START;
		case VAR_ANIMATION_STOP: return ANIMATION_STOP;
		case VAR_ANIMATION_STEP: return ANIMATION_STEP;
		case VAR_STAR_SIZE: return STAR_SIZE;
		case VAR_STAR_NOISE: return STAR_NOISE?1:0;
		case VAR_STAR_NOISE_LEVEL: return STAR_NOISE_LEVEL;
		case VAR_STAR_FADE_EDGES: return STAR_FADE_EDGES?1:0;
		case VAR_WORLD3D_REFERENCE_DISTANCE: return WORLD3D_REFERENCE_DISTANCE;
		case VAR_WORLD3D_SEPARATION_OF_EYES: return WORLD3D_SEPARATION_OF_EYES;
		case VAR_WORLD3D_IMAGE_WIDTH: return WORLD3D_IMAGE_WIDTH;
		default: break;
		}
		return 0;
	}

	public float getNewVariable(int variableIndex) {
		switch (variableIndex) {
		case VAR_ANIMATION_START: return NEW_ANIMATION_START;
		case VAR_ANIMATION_STOP: return NEW_ANIMATION_STOP;
		case VAR_ANIMATION_STEP: return NEW_ANIMATION_STEP;
		case VAR_STAR_SIZE: return NEW_STAR_SIZE;
		case VAR_STAR_NOISE: return NEW_STAR_NOISE;
		case VAR_STAR_NOISE_LEVEL: return NEW_STAR_NOISE_LEVEL;
		case VAR_STAR_FADE_EDGES: return NEW_STAR_FADE_EDGES;
		case VAR_WORLD3D_REFERENCE_DISTANCE: return NEW_WORLD3D_REFERENCE_DISTANCE;
		case VAR_WORLD3D_SEPARATION_OF_EYES: return NEW_WORLD3D_SEPARATION_OF_EYES;
		case VAR_WORLD3D_IMAGE_WIDTH: return NEW_WORLD3D_IMAGE_WIDTH;
		default: break;
		}
		return 0;
	}

	public void changeNewVariable(int variableIndex, float inc) {
		switch (variableIndex) {
		case VAR_ANIMATION_START: NEW_ANIMATION_START += 1000*inc; break;
		case VAR_ANIMATION_STOP: NEW_ANIMATION_STOP += 1000*inc; break;
		case VAR_ANIMATION_STEP: NEW_ANIMATION_STEP += 5*inc; break;
		case VAR_STAR_SIZE: NEW_STAR_SIZE += 1*inc; break;
		case VAR_STAR_NOISE: if (lastTimeChanged<main.millis()-150) NEW_STAR_NOISE = (inc==0)?NEW_STAR_NOISE:(1-NEW_STAR_NOISE); break;
		case VAR_STAR_NOISE_LEVEL: NEW_STAR_NOISE_LEVEL += .5f*inc; break;
		case VAR_STAR_FADE_EDGES: if (lastTimeChanged<main.millis()-150) NEW_STAR_FADE_EDGES = (inc==0)?NEW_STAR_FADE_EDGES:(1-NEW_STAR_FADE_EDGES); break;
		case VAR_WORLD3D_REFERENCE_DISTANCE: NEW_WORLD3D_REFERENCE_DISTANCE += 1*inc; break;
		case VAR_WORLD3D_SEPARATION_OF_EYES: NEW_WORLD3D_SEPARATION_OF_EYES += 1*inc; break;
		case VAR_WORLD3D_IMAGE_WIDTH: NEW_WORLD3D_IMAGE_WIDTH += 10*inc; break;
		default: break;
		}
		lastTimeChanged = main.millis();
		if (NEW_STAR_SIZE<1) NEW_STAR_SIZE=1;
		if (NEW_WORLD3D_REFERENCE_DISTANCE<0) NEW_WORLD3D_REFERENCE_DISTANCE=0;
	}

	public boolean starChanged() {
		return (int)NEW_STAR_SIZE!=STAR_SIZE
		|| NEW_STAR_NOISE!=(STAR_NOISE?1:0)
		|| NEW_STAR_NOISE_LEVEL!=STAR_NOISE_LEVEL
		|| NEW_STAR_FADE_EDGES!=(STAR_FADE_EDGES?1:0);
	}

	public void applyChangesToStar() {
		STAR_SIZE = (int)NEW_STAR_SIZE;
		STAR_NOISE = (NEW_STAR_NOISE==1);
		STAR_NOISE_LEVEL = NEW_STAR_NOISE_LEVEL;
		STAR_FADE_EDGES = (NEW_STAR_FADE_EDGES==1);
		main.createStarImages();
	}

	public boolean animationChanged() {
		return NEW_ANIMATION_START!=ANIMATION_START
		|| NEW_ANIMATION_STOP!=ANIMATION_STOP
		|| NEW_ANIMATION_STEP!=ANIMATION_STEP;
	}

	public void applyChangesToAnimation() {
		ANIMATION_START = NEW_ANIMATION_START;
		ANIMATION_STOP = NEW_ANIMATION_STOP;
		ANIMATION_STEP = NEW_ANIMATION_STEP;
	}

	public boolean world3DChanged() {
		return NEW_WORLD3D_REFERENCE_DISTANCE!=WORLD3D_REFERENCE_DISTANCE
		|| NEW_WORLD3D_SEPARATION_OF_EYES!=WORLD3D_SEPARATION_OF_EYES
		|| NEW_WORLD3D_IMAGE_WIDTH!=WORLD3D_IMAGE_WIDTH;
	}

	public void applyChangesToWorld3D() {
		WORLD3D_REFERENCE_DISTANCE = NEW_WORLD3D_REFERENCE_DISTANCE;
		WORLD3D_SEPARATION_OF_EYES = NEW_WORLD3D_SEPARATION_OF_EYES;
		WORLD3D_IMAGE_WIDTH = NEW_WORLD3D_IMAGE_WIDTH;
		WORLD3D_FACTOR = WORLD3D_SEPARATION_OF_EYES * SCREEN_WIDTH / WORLD3D_IMAGE_WIDTH;
	}



	public void increaseRealtimeModeSpeed(float f) {
		REALTIME_YEARS_PER_FRAME += f;
	}

	public void setRenderMode(int m) {
		RENDER_MODE = m;
	}

	public String[] asString() {
		String out[] = new String[0];
		out = PApplet.append(out,"## PATHS");
		out = PApplet.append(out,"PROJECT_NAME\t\t"+PROJECT_NAME);
		out = PApplet.append(out,"PROJECT_PATH\t\t"+PROJECT_PATH);
		out = PApplet.append(out,"ANIMATION_DISK_PATH_NORMAL\t\t"+ANIMATION_DISK_PATH_NORMAL);
		out = PApplet.append(out,"ANIMATION_DISK_PATH_FRAME_BLENDING\t"+ANIMATION_DISK_PATH_FRAME_BLENDING);
		out = PApplet.append(out,"ANIMATION_DISK_PATH_STAR_POSITIONS\t"+ANIMATION_DISK_PATH_STAR_POSITIONS);
		out = PApplet.append(out,"");
		out = PApplet.append(out,"## CATALOGUE SELECTION SETTINGS");
		out = PApplet.append(out,"LOAD_DATA_FROM_DISK\t"+LOAD_DATA_FROM_DISK);
		out = PApplet.append(out,"LOAD_HIPPARCOS\t\t"+LOAD_HIPPARCOS);
		out = PApplet.append(out,"LOAD_TYCHO1\t\t"+LOAD_TYCHO1);
		out = PApplet.append(out,"LOAD_TYCHO2\t\t"+LOAD_TYCHO2);
		out = PApplet.append(out,"LOAD_RADIAL_VELOCITIES\t"+LOAD_RADIAL_VELOCITIES);
		out = PApplet.append(out,"LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME\t"+LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME);
		out = PApplet.append(out,"");
		out = PApplet.append(out,"## SKYFIELD SETTINGS");
		out = PApplet.append(out,"SIMULATION_RA_CEN\t\t"+SIMULATION_RA_CEN);
		out = PApplet.append(out,"SIMULATION_DEC_CEN\t\t"+SIMULATION_DEC_CEN);
		out = PApplet.append(out,"SIMULATION_RA_VIEW_RANGE\t"+SIMULATION_RA_VIEW_RANGE);
		out = PApplet.append(out,"SIMULATION_DEC_VIEW_RANGE\t"+SIMULATION_DEC_VIEW_RANGE);
		out = PApplet.append(out,"SIMULATION_RA_CALC_RANGE\t"+SIMULATION_RA_CALC_RANGE);
		out = PApplet.append(out,"SIMULATION_DEC_CALC_RANGE\t"+SIMULATION_DEC_CALC_RANGE);
		out = PApplet.append(out,"SIMULATION_MAGNITUDE_MIN\t"+SIMULATION_MAGNITUDE_MIN);
		out = PApplet.append(out,"SIMULATION_MAGNITUDE_MAX\t"+SIMULATION_MAGNITUDE_MAX);
		out = PApplet.append(out,"");
		out = PApplet.append(out,"## SIMULATION_MODE: ANIMATION SETTINGS");
		out = PApplet.append(out,"ANIMATION_START\t\t\t"+ANIMATION_START);
		out = PApplet.append(out,"ANIMATION_STOP\t\t\t"+ANIMATION_STOP);
		out = PApplet.append(out,"ANIMATION_STEP\t\t\t"+ANIMATION_STEP);
		out = PApplet.append(out,"ANIMATION_SAVE_SINGLE_FRAME\t"+ANIMATION_SAVE_SINGLE_FRAME);
		out = PApplet.append(out,"ANIMATION_SAVE_FRAME_BLENDING\t"+ANIMATION_SAVE_FRAME_BLENDING);
		out = PApplet.append(out,"ANIMATION_FRAME_BLENDING_FRAMES\t"+ANIMATION_FRAME_BLENDING_FRAMES);
		out = PApplet.append(out,"ANIMATION_SAVE_STAR_POSITIONS\t"+ANIMATION_SAVE_STAR_POSITIONS);
		out = PApplet.append(out,"");
		out = PApplet.append(out,"## RENDER SETTINGS: MAIN");
		out = PApplet.append(out,"SCREEN_WIDTH\t"+SCREEN_WIDTH);
		out = PApplet.append(out,"SCREEN_HEIGHT\t"+SCREEN_HEIGHT);
		out = PApplet.append(out,"RENDER_MODE\t"+RENDER_MODE);
		out = PApplet.append(out,"");
		out = PApplet.append(out,"## WORLD3D SETTINGS");
		out = PApplet.append(out,"WORLD3D_REFERENCE_DISTANCE\t"+WORLD3D_REFERENCE_DISTANCE);
		out = PApplet.append(out,"WORLD3D_SEPARATION_OF_EYES\t"+WORLD3D_SEPARATION_OF_EYES);
		out = PApplet.append(out,"WORLD3D_IMAGE_WIDTH\t\t"+WORLD3D_IMAGE_WIDTH);
		out = PApplet.append(out,"");
		out = PApplet.append(out,"## RENDER SETTINGS: STAR");
		out = PApplet.append(out,"STAR_SIZE\t\t"+STAR_SIZE);
		out = PApplet.append(out,"STAR_NOISE\t\t"+STAR_NOISE);
		out = PApplet.append(out,"STAR_NOISE_LEVEL\t"+STAR_NOISE_LEVEL);
		out = PApplet.append(out,"STAR_FADE_EDGES\t\t"+STAR_FADE_EDGES);
		out = PApplet.append(out,"STAR_MOFFAT_RADIUS_EXAGGERATION\t"+STAR_MOFFAT_RADIUS_EXAGGERATION);
		return out;
	}

	public static String parseVariable(float variable, int settingsVar) {
		switch (settingsVar) {
		case VAR_ANIMATION_START: return String.valueOf(variable);
		case VAR_ANIMATION_STOP: return String.valueOf(variable);
		case VAR_ANIMATION_STEP: return String.valueOf(variable);
		case VAR_STAR_SIZE: return String.valueOf((int)variable);
		case VAR_STAR_NOISE: return String.valueOf(variable==1);
		case VAR_STAR_NOISE_LEVEL: return String.valueOf(variable);
		case VAR_STAR_FADE_EDGES: return String.valueOf(variable==1);
		case VAR_WORLD3D_REFERENCE_DISTANCE: return String.valueOf(variable);
		case VAR_WORLD3D_SEPARATION_OF_EYES: return String.valueOf(variable);
		case VAR_WORLD3D_IMAGE_WIDTH: return String.valueOf(variable);
		default: return "";
		}
	}

	/*
	 * public void setFromString(String[] in) {
		for (int i=0;i<in.length;i++) {
			// disk paths
			if (StringTools.contains(in[i],"PROJECT_NAME"))
				PROJECT_NAME = StringTools.stringAfterLastTab(in[i]);
			if (StringTools.contains(in[i],"PROJECT_PATH"))
				PROJECT_PATH = StringTools.stringAfterLastTab(in[i]);
			else if (StringTools.contains(in[i],"ANIMATION_DISK_PATH_NORMAL"))
				ANIMATION_DISK_PATH_NORMAL = StringTools.stringAfterLastTab(in[i]);
			else if (StringTools.contains(in[i],"ANIMATION_DISK_PATH_FRAME_BLENDING"))
				ANIMATION_DISK_PATH_FRAME_BLENDING = StringTools.stringAfterLastTab(in[i]);
			else if (StringTools.contains(in[i],"ANIMATION_DISK_PATH_STAR_POSITIONS"))
				ANIMATION_DISK_PATH_STAR_POSITIONS = StringTools.stringAfterLastTab(in[i]);
			// CATALOGUE SELECTION SETTINGS
			else if (StringTools.contains(in[i],"LOAD_DATA_FROM_DISK"))
				LOAD_DATA_FROM_DISK = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"LOAD_HIPPARCOS"))
				LOAD_HIPPARCOS = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"LOAD_TYCHO1"))
				LOAD_TYCHO1 = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"LOAD_TYCHO2"))
				LOAD_TYCHO2 = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"LOAD_RADIAL_VELOCITIES")&&!StringTools.contains(in[i],"LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME"))
				LOAD_RADIAL_VELOCITIES = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME"))
				LOAD_RADIAL_VELOCITIES_AMOUNT_PER_FRAME = Integer.parseInt(StringTools.stringAfterLastTab(in[i]));
			// SKYFIELD SETTINGS
			else if (StringTools.contains(in[i],"SIMULATION_RA_CEN"))
				SIMULATION_RA_CEN = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_DEC_CEN"))
				SIMULATION_DEC_CEN = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_RA_VIEW_RANGE"))
				SIMULATION_RA_VIEW_RANGE = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_DEC_VIEW_RANGE"))
				SIMULATION_DEC_VIEW_RANGE = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_RA_CALC_RANGE"))
				SIMULATION_RA_CALC_RANGE = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_DEC_CALC_RANGE"))
				SIMULATION_DEC_CALC_RANGE = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_MAGNITUDE_MIN"))
				SIMULATION_MAGNITUDE_MIN = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SIMULATION_MAGNITUDE_MAX"))
				SIMULATION_MAGNITUDE_MAX = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			// SIMULATION_MODE: ANIMATION SETTINGS
			else if (StringTools.contains(in[i],"ANIMATION_START"))
				ANIMATION_START = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"ANIMATION_STOP"))
				ANIMATION_STOP = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"ANIMATION_STEP"))
				ANIMATION_STEP = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"ANIMATION_SAVE_SINGLE_FRAME"))
				ANIMATION_SAVE_SINGLE_FRAME = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"ANIMATION_SAVE_FRAME_BLENDING"))
				ANIMATION_SAVE_FRAME_BLENDING = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"ANIMATION_FRAME_BLENDING_FRAMES"))
				ANIMATION_FRAME_BLENDING_FRAMES = Integer.parseInt(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"ANIMATION_SAVE_STAR_POSITIONS"))
				ANIMATION_SAVE_STAR_POSITIONS = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			// RENDER SETTINGS: MAIN
			else if (StringTools.contains(in[i],"SCREEN_WIDTH"))
				SCREEN_WIDTH = Integer.parseInt(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"SCREEN_HEIGHT"))
				SCREEN_HEIGHT = Integer.parseInt(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"RENDER_MODE"))
				RENDER_MODE = Integer.parseInt(StringTools.stringAfterLastTab(in[i]));
			// WORLD3D SETTINGS
			else if (StringTools.contains(in[i],"WORLD3D_REFERENCE_DISTANCE"))
				WORLD3D_REFERENCE_DISTANCE = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"WORLD3D_SEPARATION_OF_EYES"))
				WORLD3D_SEPARATION_OF_EYES = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"WORLD3D_IMAGE_WIDTH"))
				WORLD3D_IMAGE_WIDTH = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			// RENDER SETTINGS: STAR
			else if (StringTools.contains(in[i],"STAR_SIZE"))
				STAR_SIZE = Integer.parseInt(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"STAR_NOISE")&&!StringTools.contains(in[i],"STAR_NOISE_LEVEL"))
				STAR_NOISE = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"STAR_NOISE_LEVEL"))
				STAR_NOISE_LEVEL = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"STAR_FADE_EDGES"))
				STAR_FADE_EDGES = Boolean.parseBoolean(StringTools.stringAfterLastTab(in[i]));
			else if (StringTools.contains(in[i],"STAR_MOFFAT_RADIUS_EXAGGERATION"))
				STAR_MOFFAT_RADIUS_EXAGGERATION = Float.parseFloat(StringTools.stringAfterLastTab(in[i]));
		}
		resetNewVariables();

		recalcWorld3DFactor();

	}*/





}
