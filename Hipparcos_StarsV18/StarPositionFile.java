
import processing.core.*;

public class StarPositionFile {

	private int STAR_AMOUNT;
	private int FRAMES;

	private float[][] X, Y;
	private float[] MAGNITUDE;

	String[] outputFile;

	StarPositionFile(int nrOfFrames, int nrOfStars) {
		
		FRAMES = nrOfFrames;
		STAR_AMOUNT = nrOfStars;
		X = new float[FRAMES][STAR_AMOUNT];
		Y = new float[FRAMES][STAR_AMOUNT];
		MAGNITUDE = new float[STAR_AMOUNT];
		outputFile = new String[0];
	}

	void setStarMagnitude(int star, float m) {
		if (star<STAR_AMOUNT) MAGNITUDE[star]=m;
	}

	void setStarPosition(int frame, int star, float x, float y) {
		if (frame<FRAMES&&star<STAR_AMOUNT) {
			X[frame][star] = x;
			Y[frame][star] = y;
		}
	}

	void createFile() {
		outputFile = PApplet.append(outputFile,"");
		outputFile = PApplet.append(outputFile,"var nrOfFrames = "+FRAMES);
		outputFile = PApplet.append(outputFile,"var nrOfStars = "+STAR_AMOUNT);
		outputFile = PApplet.append(outputFile,"");
		outputFile = PApplet.append(outputFile,"var starX = new Array();");
		outputFile = PApplet.append(outputFile,"var starY = new Array();");
		outputFile = PApplet.append(outputFile,"var starMagnitude = new Array();");
		outputFile = PApplet.append(outputFile,"");
		outputFile = PApplet.append(outputFile,"for (var i=0;i<nrOfFrames;i++) {");
		outputFile = PApplet.append(outputFile,"\tstarX[i] = new Array();");
		outputFile = PApplet.append(outputFile,"\tstarY[i] = new Array();");
		outputFile = PApplet.append(outputFile,"};");
		outputFile = PApplet.append(outputFile,"");
		for (int star=0;star<STAR_AMOUNT;star++) 
			outputFile = PApplet.append(outputFile,"starMagnitude["+star+"] = "+PApplet.nf(MAGNITUDE[star],0,1)+";");

		for (int frame=0;frame<FRAMES;frame++) {
			for (int star=0;star<STAR_AMOUNT;star++) {
				outputFile = PApplet.append(outputFile,"starX["+frame+"]["+star+"] = "+PApplet.nf(X[frame][star],0,1)+";");
				outputFile = PApplet.append(outputFile,"starY["+frame+"]["+star+"] = "+PApplet.nf(Y[frame][star],0,1)+";");
			}
		}

	}

	void saveFile(PApplet pa,String path) {
		pa.saveStrings(path+"_positions.jsx",outputFile);
	}

}
