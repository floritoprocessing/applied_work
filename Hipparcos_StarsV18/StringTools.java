

import java.text.DecimalFormat;

public class StringTools {
	

	public static String numberFormat(int n, int digits) {
		String regex = "";
		for (int i=0;i<digits;i++) regex+="0";
		DecimalFormat df = new DecimalFormat(regex); // "00"
		return df.format(n);
	}
	
	public static String numberFormat(double f, int dig0, int dig1) {
		String regex = "";
		for (int i=0;i<dig0;i++) regex+="0";
		regex += ".";
		for (int i=0;i<dig1;i++) regex+="0";
		DecimalFormat df = new DecimalFormat(regex); // "0.00"
		return df.format(f);
	}
	
	public static String secondsToHMS(float seconds) {
		int time = (int)seconds;
		int hh = time/3600;
		time -= hh*3600;
		int mm = time/60;
		time -= mm*60;
		int ss = time;
		return numberFormat(hh,2)+":"+numberFormat(mm,2)+":"+numberFormat(ss,2);
	}
	
	public static boolean contains(String main, String sub) {
		int mainLen = main.length();
		int subLen = sub.length();
		for (int i=0;i<mainLen-subLen;i++) {
			if (main.substring(0,subLen).equals(sub)) return true;
		}
		return false;
	}

	public static String stringAfterLastTab(String string) {
		int index = 0;
		for (int i=0;i<string.length();i++) {
			if (string.charAt(i)=='\t') index=i+1;
		}
		if (index==0) return null; else return string.substring(index);
	}

}
