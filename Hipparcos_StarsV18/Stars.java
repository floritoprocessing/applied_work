

import processing.core.*;
import java.util.Vector;
import graphics.HighQualityImage;

public class Stars extends Vector {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public static final int TWO_D = 0;
	public static final int THREE_D = 1;

	private int lastStarCreatedIndex;
	private int lastStarRadialVelocityLoaded;

	private World3D world3D;
	private Settings settings;
	private float epoch;

	Stars(Settings _settings, float _epoch) {
		super();
		settings = _settings;
		epoch = _epoch;
		lastStarCreatedIndex = 0;
		lastStarRadialVelocityLoaded = 0;
		world3D = new World3D(settings);
	}

	public float getEpoch() {
		return epoch;
	}

	public void clear() {
		super.clear();
		lastStarCreatedIndex = 0;
		lastStarRadialVelocityLoaded = 0;
	}

	public void clearStarImages() {
		lastStarCreatedIndex = 0;
	}


	public float getClosestDistance() {
		float out = 999999999;
		for (int i=0;i<size();i++) {
			float d = ((Star)elementAt(i)).getDistanceInParsec();//DistanceInLy;
			//print(((Star)elementAt(i)).Plx+"="+d+"\t");
			if (d<out) out = d;
		}
		//println();
		return out;
	}

	public float getFurthestDistance() {
		float out = -999999999;
		for (int i=0;i<size();i++) {
			float d = ((Star)elementAt(i)).getDistanceInParsec();//DistanceInLy;
			//print(((Star)elementAt(i)).Plx+"="+d+"\t");
			if (d>out) out = d;
		}
		//println();
		return out;
	}

	public float getAverageDistanceOfStarsWithParallax() {
		float out = 0;
		float n = 0;
		for (int i=0;i<size();i++) {
			if (((Star)elementAt(i)).getParallax()!=0) {
				out += ((Star)elementAt(i)).getDistanceInParsec();
				n++;
			}
		}
		return out/n;
	}



	public Star getTychoStar(int t1,int t2,int t3) {
		for (int i=0;i<size();i++) {
			Star s = ((Star)elementAt(i));
			if (s.getTYC1()==t1&&s.getTYC2()==t2&&s.getTYC3()==t3) return s;
		}
		return null;
	}

	public Star getHipparcosStar(int hip) {
		for (int i=0;i<size();i++) {
			Star s = ((Star)elementAt(i));
			if (s.getHIP()==hip) return s;
		}
		return null;
	}



	public void setZeroParallaxTo(StarsMain pa, float dly) {
		int c=0;
		for (int i=0;i<size();i++)
			if (((Star)elementAt(i)).getParallax()==0) c++;
		pa.logger.log("adjusting "+c+" stars with parallax zero");
		for (int i=0;i<size();i++)
			if (((Star)elementAt(i)).getParallax()==0) ((Star)elementAt(i)).setDistanceInParsec(dly);
	}

	public void removeStarsWithoutParallax(StarsMain pa) {
		pa.logger.log("removing stars without parallax");
		int c=0;
		for (int i=0;i<size();i++) {
			if (((Star)elementAt(i)).getParallax()==0||Float.isNaN(((Star)elementAt(i)).getParallax())) {
				removeElementAt(i);
				i--;
				c++;
			}
		}
		pa.logger.log(c+" stars removed");
	}

	public void setStarsWithoutParallaxToInfinitiy(StarsMain pa) {
		pa.logger.log("setting stars without or with negative parallax to infinity");
		int c=0;
		for (int i=0;i<size();i++) {
			if (((Star)elementAt(i)).getParallax()<=0||Float.isNaN(((Star)elementAt(i)).getParallax()))
				((Star)elementAt(i)).setParallax(Astro.INFINITE_PARALLAX);
		}
		pa.logger.log(c+" stars adjusted");
	}

	public void removeStarsWithZeroRadialVelocity(StarsMain pa) {
		pa.logger.log("removing stars with zero radial velocity");
		int c=0;
		for (int i=0;i<size();i++) {
			if (((Star)elementAt(i)).getRV()==0) {
				removeElementAt(i);
				i--;
				c++;
			}
		}
		pa.logger.log(c+" stars removed");
	}

	public void deleteAllHipBut(int[] hiplist) {
		for (int i=0;i<size();i++) {
			boolean delete=true;
			Star s = (Star)elementAt(i);
			for (int h=0;h<hiplist.length;h++) if (s.getHIP()==hiplist[h]) delete=false;
			if (delete) {
				removeElementAt(i);
				i--;
			}
		}
	}






	public boolean loadRadialVelocities(StarsMain pa, HipparcosData hipparcosData, int amount, boolean loadFromDisk) {
		pa.logger.log("Loading radial velocities..."+lastStarRadialVelocityLoaded+"/"+size());//,pa.width/2,pa.height/2);

		if (!loadFromDisk) {
			pa.logger.log("Loading from web");//,pa.width/2,pa.height/2+30);
			int startNr = lastStarRadialVelocityLoaded;
			int endNr = 0;

			// MAKE A LIST OF <amount> HIP NUMBERS
			int[] hipList = new int[0];
			int c=0;
			while (c<amount&&lastStarRadialVelocityLoaded<size()) {
				int hip = ((Star)elementAt(lastStarRadialVelocityLoaded)).getHIP();
				if (hip!=0) hipList = PApplet.append(hipList,hip);
				endNr = lastStarRadialVelocityLoaded;
				c++;
				lastStarRadialVelocityLoaded++;
			}


			// CONVERT TO STRING AND LOAD
			if (hipList.length>0) {
				String listOfHipNumbers="";
				for (int i=0;i<hipList.length;i++) listOfHipNumbers += ""+(hipList[i]) + (i<hipList.length-1?",":"");
				pa.logger.log("Hip numbers: "+listOfHipNumbers);//,pa.width/2,pa.height/2+15);
				hipparcosData.loadHipparcosInputFromVizier(pa,listOfHipNumbers,startNr,endNr,this,loadFromDisk);
			} 
			else {
				pa.logger.log("no hip number - not loading data");//,pa.width/2,pa.height/2+15);
			}
		} 


		else {
			pa.logger.log("Loading from disk");//,pa.width/2,pa.height/2+30);
			hipparcosData.loadHipparcosInputFromVizier(pa,"",0,0,this,loadFromDisk);
		}

		// RETURN TRUE WHEN ALL STARS HAVE BEEN CHECKED
		if (loadFromDisk)
			return true;
		else if (lastStarRadialVelocityLoaded<size())
			return false;
		else
			return true;
	}





	public boolean createStarImagesInTimeAndReturnReady(StarsMain pa, int ms) {
		//    if (RENDER_MODE == RENDER_MODE_IMAGE_PER_STAR) {
		int startTime = pa.millis();


		while (startTime+ms>pa.millis()&&lastStarCreatedIndex<size()) {
			((Star)elementAt(lastStarCreatedIndex)).createStarImage();
			lastStarCreatedIndex++;
		}

		pa.background(0);
		pa.fill(255,255,255);
		pa.text("Creating star images... "+lastStarCreatedIndex+"/"+size(),pa.width/2,pa.height/2);
		if (lastStarCreatedIndex==0) pa.logger.log("Creating star images");

		if (lastStarCreatedIndex==size()) {
			pa.logger.log("done.");
			return true;
		}
		else return false;

	}

	public HighQualityImage getLastCreatedImage() {
		int i=lastStarCreatedIndex-1;
		if (i>=size()) i=size()-1;
		if (i<0) return null;
		return ((Star)elementAt(i)).getImage();
	}


	public void drawAllStars(Settings settings, int eye, HighQualityImage img, int mode2d3d) {
		for (int i=0;i<size();i++) {
			((Star)elementAt(i)).makePos();
			if (mode2d3d==TWO_D)
				((Star)elementAt(i)).drawImage(img);
			else if (mode2d3d==THREE_D)  {
				Star s = ((Star)elementAt(i));
				float x3d = world3D.screen2DToScreen3DX(eye,s.getX(),s.getDistanceInParsec());
				float y3d = world3D.screen2DToScreen3DY(s.getY(),s.getDistanceInParsec());
				((Star)elementAt(i)).drawImageAt(img,x3d,y3d);
			}
		}
	}

	public void setTimeInRelationToEpoch(float dt, float _epoch) {
		epoch = _epoch - dt;
		for (int i=0;i<size();i++) ((Star)elementAt(i)).setTimeInRelationToEpoch(dt);
		//System.out.println("time: "+epoch);
	}

	public void advanceTime(double t) {
		epoch += t;
		for (int i=0;i<size();i++) ((Star)elementAt(i)).advanceTime(t);
		//System.out.println("time: "+epoch);
	}

	public void createRelativeStarPositionsPc() {
		for (int i=0;i<size();i++) ((Star)elementAt(i)).createRelativeStarPositionPc(settings);
	}

	public void saveStarPositions(StarsMain pa) {
		String out[] = new String[0];
		for (int i=0;i<size();i++) {
			String line = ((Star)elementAt(i)).getNAME();
			line += "\t";
			line += ((Star)elementAt(i)).getPosPc().toString();
			out = PApplet.append(out,line);
		}
		pa.logger.log("Saving star positions");
		pa.saveStrings(settings.PROJECT_PATH+settings.PROJECT_NAME+"_starPositions.tab",out);
	}

	/*
	public void changeWorld3DSettingsFromKeyboard(PApplet pa) {
		world3D.changeValuesFromKeyboard(pa);
	}
	 */

}
