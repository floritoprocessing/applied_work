

import processing.core.*;

public class SetupScreen  {

	private StarsMain main;
	private Settings settings;
	private boolean active = false;
	private int X = 30;
	private int Y = 50;
	private char key;
	//private int keyCode;
	
	private int NEW_RENDER_MODE;
	
	public SetupScreen(StarsMain _main) {
		main = _main;
		settings = main.settings;
	}

	public void toggleActive() {
		if (!active) init();
		active = !active;
	}
	
	public boolean active() {
		return active;
	}
	
	public void tellKeyPressed(char _key, int _keyCode) {
		if (active) {
			key =_key;
			//keyCode = _keyCode;
		}
	}
	
	public void init() {
		NEW_RENDER_MODE = settings.RENDER_MODE;
		settings.resetNewVariables();
	}

	public void show() {
		if (active) {
			// RECTANGLE
			main.rectMode(PApplet.CORNER);
			main.stroke(128);
			main.fill(240);
			main.rect(X,Y,700,300);

			// Title
			main.fill(0);
			main.text("SETUP",X+10,Y+20);

			
			// RENDER MODES
			if (key>='1'&&key<='6') NEW_RENDER_MODE = (int)key - 49;
			boolean renderModeChange = (NEW_RENDER_MODE != settings.RENDER_MODE);
			main.text("RENDER MODES:",X+10,Y+50);
			for (int i=0;i<Settings.RENDER_MODE_NAME.length;i++) {
				if (i==NEW_RENDER_MODE) main.fill(0); else main.fill(128);
				main.text("["+(i+1)+"] "+Settings.RENDER_MODE_NAME[i],X+10,Y+70+i*20);
			}
			
			// ANIMATION SETUP
			ScreenText.make(main,"ANIMATION SETUP:",X+150,Y+50);
			ScreenText.makeAndChange(main,settings,Settings.VAR_ANIMATION_START,'d','e',"ANIMATION START",X+150,Y+70);
			ScreenText.makeAndChange(main,settings,Settings.VAR_ANIMATION_STOP,'f','r',"ANIMATION STOP",X+150,Y+90);
			ScreenText.makeAndChange(main,settings,Settings.VAR_ANIMATION_STEP,'g','t',"ANIMATION STEP",X+150,Y+110);
			float nrOfFrames = (settings.getNewVariable(Settings.VAR_ANIMATION_STOP)-settings.getNewVariable(Settings.VAR_ANIMATION_START))/settings.getNewVariable(Settings.VAR_ANIMATION_STEP);
			main.text("-> nr of frames: "+StringTools.numberFormat((int)nrOfFrames,1)+((nrOfFrames<0)?" NEGATIVE FRAMES!":""),X+150,Y+130);
			boolean animationSetupChanged = settings.animationChanged();			
			
			// STAR SETUP
			ScreenText.make(main,"STAR GRAPHIC SETUP:",X+150,Y+170);
			ScreenText.makeAndChange(main,settings,Settings.VAR_STAR_SIZE,'c','v',"SIZE",X+150,Y+190);
			ScreenText.makeAndChange(main,settings,Settings.VAR_STAR_NOISE,'b',"NOISE",X+150,Y+210);
			ScreenText.makeAndChange(main,settings,Settings.VAR_STAR_NOISE_LEVEL,'n','m',"NOISE LEVEL",X+150,Y+230);
			ScreenText.makeAndChange(main,settings,Settings.VAR_STAR_FADE_EDGES,',',"FADE EDGES",X+150,Y+250);
			boolean starSetupChanged = settings.starChanged();
			
			// WORLD3D SETUP
			ScreenText.make(main,"3D SETUP:",X+390,Y+50);
			ScreenText.makeAndChange(main,settings,Settings.VAR_WORLD3D_REFERENCE_DISTANCE,'h','y',"REFERENCE DISTANCE",X+390,Y+70);
			ScreenText.makeAndChange(main,settings,Settings.VAR_WORLD3D_SEPARATION_OF_EYES,'k','i',"IMAGE WIDTH",X+390,Y+90);
			ScreenText.makeAndChange(main,settings,Settings.VAR_WORLD3D_IMAGE_WIDTH,'k','i',"IMAGE WIDTH",X+390,Y+110);
			boolean world3DChanged = settings.world3DChanged();
			
			if (renderModeChange||animationSetupChanged||world3DChanged||starSetupChanged) {
				main.fill(0);
				String msg ="[A] Apply changes to";
				msg += (renderModeChange?" - [Render Mode]":"");
				msg += (animationSetupChanged?" - [Animation Setup]":"");
				msg += (starSetupChanged?" - [Star Setup]":"");
				msg += (world3DChanged?" - [3D Setup]":"");
				main.text(msg,X+10,Y+270);
				
				if (key=='a') {
					if (animationSetupChanged) settings.applyChangesToAnimation();
					if (world3DChanged) settings.applyChangesToWorld3D();
					if (renderModeChange) main.changeRenderModeTo(NEW_RENDER_MODE);
					if (starSetupChanged) settings.applyChangesToStar();
					main.background(0);
					active=false;
				}
			}
			
			
				
			
			
			

		}
		
		key=' ';
		//keyCode=0;
	}

}
