package graphics;

public class Noise {

	private int WIDTH,HEIGHT;
	private float[][] rnd;

	Noise(int w, int h, float nLev) {
		WIDTH = w;
		HEIGHT = h ;
		rnd = new float[w][h];
		for (int x=0;x<w;x++) for (int y=0;y<h;y++) rnd[x][y] = (float)(nLev*(-.5+Math.random()));
	}

	void modify(HighQualityImage img, int xTargetOffset) {
		for (int y=0;y<HEIGHT;y++) for (int x=0;x<WIDTH;x++) {
			int xTarget = x+xTargetOffset;
			img.R[xTarget][y] += (int)(rnd[x][y]*Math.sqrt((float)img.R[xTarget][y]));
			img.G[xTarget][y] += (int)(rnd[x][y]*Math.sqrt((float)img.G[xTarget][y]));
			img.B[xTarget][y] += (int)(rnd[x][y]*Math.sqrt((float)img.B[xTarget][y]));
			img.limit(x,y);
		}
		img.updatePImage();
	}

}