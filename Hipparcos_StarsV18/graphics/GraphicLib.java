package graphics;

import processing.core.*;

public class GraphicLib {

	public static int copyColors(int c0, int c1) {
		int r = (c0>>16&0xFF)+(c1>>16&0xFF);
		if (r>255) r=255;
		int g = (c0>>8&0xFF)+(c1>>8&0xFF);
		if (g>255) g=255;
		int b = (c0&0xFF)+(c1&0xFF);
		if (b>255) b=255;
		return (r<<16|g<<8|b);
	}

	public static int copyColors(int c0, int c1, float p1) {
		float p0 = 1.0f - p1;
		int r = (int)(p0*(c0>>16&0xFF)+p1*(c1>>16&0xFF));
		if (r>255) r=255;
		int g = (int)(p0*(c0>>8&0xFF)+p1*(c1>>8&0xFF));
		if (g>255) g=255;
		int b = (int)(p0*(c0&0xFF)+p1*(c1&0xFF));
		if (b>255) b=255;
		return (r<<16|g<<8|b);
	}

	public static int addColors(int c0, int c1) {
		int r = (c0>>16&0xFF)+(c1>>16&0xFF);
		if (r>255) r=255;
		int g = (c0>>8&0xFF)+(c1>>8&0xFF);
		if (g>255) g=255;
		int b = (c0&0xFF)+(c1&0xFF);
		if (b>255) b=255;
		return (r<<16|g<<8|b);
	}

	public static int addColors(int c0, int c1, float p1) {
		int r = (int)((c0>>16&0xFF)+p1*(c1>>16&0xFF));
		if (r>255) r=255;
		int g = (int)((c0>>8&0xFF)+p1*(c1>>8&0xFF));
		if (g>255) g=255;
		int b = (int)((c0&0xFF)+p1*(c1&0xFF));
		if (b>255) b=255;
		return (r<<16|g<<8|b);
	}

	public static void setAdd(PApplet pa, float x, float y, int c1) {
		int x0 = (int)x, x1 = x0 + 1;
		int y0 = (int)y, y1 = y0 + 1;
		float px1 = x-x0, px0 = 1.0f - px1;
		float py1 = y-y0, py0 = 1.0f - py1;
		float p00 = px0*py0;
		float p01 = px0*py1;
		float p10 = px1*py0;
		float p11 = 1.0f-p00-p01-p10;//px1*py1;
		setAdd(pa,x0,y0,c1,p00);
		setAdd(pa,x0,y1,c1,p01);
		setAdd(pa,x1,y0,c1,p10);
		setAdd(pa,x1,y1,c1,p11);
	}

	public static void setAdd(PApplet pa, int sx, int sy, int c1, float p) {
		int c = addColors(pa.get(sx,sy),c1,p);
		pa.set(sx,sy,c);
	}

	public static void setAdd(PApplet pa, int sx, int sy, int c1) {
		int c = addColors(pa.get(sx,sy),c1);
		pa.set(sx,sy,c);
	}

	public static void setCopy(PApplet pa,float x, float y, int c1, float p) {
		int x0 = (int)x, x1 = x0 + 1;
		int y0 = (int)y, y1 = y0 + 1;
		float px1 = x-x0, px0 = 1 - px1;
		float py1 = y-y0, py0 = 1 - py1;
		float p00 = p*px0*py0;
		float p01 = p*px0*py1;
		float p10 = p*px1*py0;
		float p11 = p*(1-p00-p01-p10);//px1*py1;
		setCopy(pa,x0,y0,c1,p00);
		setCopy(pa,x0,y1,c1,p01);
		setCopy(pa,x1,y0,c1,p10);
		setCopy(pa,x1,y1,c1,p11);
	}

	public static void setCopy(PApplet pa, int sx, int sy, int c1, float p) {
		int c = copyColors(pa.get(sx,sy),c1,p);
		pa.set(sx,sy,c);
	}

	public static void setCopy(PApplet pa, int sx, int sy, int c1) {
		int c = copyColors(pa.get(sx,sy),c1);
		pa.set(sx,sy,c);
	}

}
