/*

 format always lllsb, llsb, lsb, msb

 0    2    signature, must be 4D42 hex -> 0x42, 0x4D
 2    4    size of BMP file in bytes (unreliable) = 640 x 480 x 3(24bit) + 54 -> lllsb, llsb, lsb, msb
 6    2    reserved, must be zero
 8    2    reserved, must be zero
 10   4    offset to start of image data in bytes
 14   4    size of BITMAPINFOHEADER structure, must be 40
 18   4    image width in pixels
 22   4    image height in pixels
 26   2    number of planes in the image, must be 1
 28   2    number of bits per pixel (1, 4, 8, or 24)
 30   4    compression type (0=none, 1=RLE-8, 2=RLE-4)
 34   4    size of image data in bytes (including padding) 640x480x3+2
 38   4    horizontal resolution in pixels per meter (unreliable) (72dpi preset)
 42   4    vertical resolution in pixels per meter (unreliable) (72dpi preset)
 46   4    number of colors in image, or zero
 50   4    number of important colors, or zero 
 */

package graphics;

import processing.core.*;

public class Bmp {

	private byte[] header = { 
			0x42, 0x4D,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00,
			0x00, 0x00,
			0x36, 0x00, 0x00, 0x00,
			0x28, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x01, 0x00,
			0x18, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x12, 0x0B, 0x00, 0x00,
			0x12, 0x0B, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00,
			0x00, 0x00, 0x00, 0x00   };

	private byte[] imageData;
	private int WIDTH = 1, HEIGHT = 1;
	private int BITS_PER_PIXEL = 24; // 8, 16, 24

	private PApplet processing;

	public Bmp(PApplet pa, int w, int h, int bpp) {
		processing = pa;
		WIDTH = w;
		HEIGHT = h;
		BITS_PER_PIXEL = bpp;

		setHeaderSizeOfBMP(WIDTH*HEIGHT*(BITS_PER_PIXEL/8)+54);
		setHeaderImageWidth(WIDTH);
		setHeaderImageHeight(HEIGHT);
		setHeaderBitsPerPixel(BITS_PER_PIXEL);
		setHeaderImageDataInBytes(WIDTH*HEIGHT*(BITS_PER_PIXEL/8)+2);

		imageData = new byte[WIDTH*HEIGHT*(BITS_PER_PIXEL/8)+54];
		for (int i=0;i<54;i++) imageData[i] = header[i];
	}

	public Bmp(PApplet pa, HighQualityImage img, int bpp) {
		this(pa,img.width,img.height,bpp);
		processing = pa;
	}


	public void saveAs(String filename) {
		if (filename.length()<4) {
			processing.saveBytes(filename+".bmp",imageData);
		} 
		else {
			String ext=filename.substring(filename.length()-4,filename.length());
			ext = ext.toLowerCase();
			if (!ext.equals(".bmp")) {
				processing.saveBytes(filename+".bmp",imageData);
			} else {
				processing.saveBytes(filename,imageData);
			}
		}
	}



	// CAPTURES ONLY 24 BIT PER PIXEL!!!
	public void capture24Bit() {
		int i=54;
		for (int y=processing.height-1;y>=0;y--) {
			for (int x=0;x<processing.width;x++) {
				imageData[i+2] = (byte)((processing.get(x,y)>>16)&0xFF);
				imageData[i+1] = (byte)((processing.get(x,y)>>8)&0xFF);
				imageData[i] = (byte)(processing.get(x,y)&0xFF);
				i+=3;
			}
		}
	}

	public void capture24Bit(HighQualityImage img) {
		int i=54;
		for (int y=img.height-1;y>=0;y--) {
			for (int x=0;x<img.width;x++) {
				imageData[i+2] = (byte)((img.get(x,y)>>16)&0xFF);
				imageData[i+1] = (byte)((img.get(x,y)>>8)&0xFF);
				imageData[i] = (byte)(img.get(x,y)&0xFF);
				i+=3;
			}
		}
	}




	private byte[] intToHex(int i) {
		byte[] out = new byte[4];
		out[0] = (byte)(i&0xFF);
		out[1] = (byte)((i>>8)&0xFF);
		out[2] = (byte)((i>>16)&0xFF);
		out[3] = (byte)((i>>24)&0xFF);
		return out;
	}

	private void setHeader(int offset, int bytes, byte[] h) {
		for (int i=0;i<bytes;i++) header[offset+i] = h[i];
	}

	//  2    4    size of BMP file in bytes (unreliable) = 640 x 480 x 3(24bit) + 54 -> lllsb, llsb, lsb, msb  
	private void setHeaderSizeOfBMP(int s) {
		setHeader(2,4,intToHex(s));
	}

	private void setHeaderImageWidth(int s) {
		setHeader(18,4,intToHex(s));
	}

	//  22   4    image height in pixels
	private void setHeaderImageHeight(int s) {
		setHeader(22,4,intToHex(s));
	}

	//  28   2    number of bits per pixel (1, 4, 8, or 24)
	private void setHeaderBitsPerPixel(int s) {
		setHeader(28,2,intToHex(s));
	}

	//  34   4    size of image data in bytes (including padding) 640x480x3+2
	private void setHeaderImageDataInBytes(int s) {
		setHeader(34,4,intToHex(s));
	}

}
