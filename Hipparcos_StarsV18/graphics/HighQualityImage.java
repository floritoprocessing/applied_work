package graphics;

import processing.core.*;

public class HighQualityImage extends PImage {

	public int[][] R, G, B;

	public HighQualityImage(int w, int h) {
		super(w,h,RGB);
		R = new int[w][h];
		G = new int[w][h];
		B = new int[w][h];
		background(0x0000,0x0000,0x0000);
	}

	public HighQualityImage(HighQualityImage img) {
		this(img.width,img.height);
		for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
			R[x][y] = img.R[x][y];
			G[x][y] = img.G[x][y];
			B[x][y] = img.B[x][y];
			set(x,y,img.get(x,y));
		}
	}

	public HighQualityImage copy() {
		HighQualityImage out = new HighQualityImage(width,height);
		for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
			out.R[x][y] = R[x][y];
			out.G[x][y] = G[x][y];
			out.B[x][y] = B[x][y];
			out.set(x,y,get(x,y));
		}
		return out;
	}

	public int brightness(int x, int y) {
		if (x<0||x>this.width-1||y<0||y>this.height-1) return 0;
		return (R[x][y]+G[x][y]+B[x][y])/3;
	}

	public void background(int r, int g, int b) {
		for (int x=0;x<this.width;x++) for (int y=0;y<this.height;y++) set(x,y,r,g,b);
		updatePImage();
	}
	
	public void set(int x, int y, int[] rgb) {
		set(x,y,rgb[0],rgb[1],rgb[2]);
	}
	
	public void set(float x, float y, int[] rgb) {
		set(x,y,rgb[0],rgb[1],rgb[2],1.0f);
	}

	public void set(float x, float y, int r, int g, int b) {
		set(x,y,r,g,b,1.0f);
	}

	public void set(float x, float y, int r, int g, int b, float p) {
		int x0 = (int)x, x1 = x0 + 1;
		int y0 = (int)y, y1 = y0 + 1;
		float px1 = x-x0, px0 = 1.0f - px1;
		float py1 = y-y0, py0 = 1.0f - py1;
		float p00 = p*px0*py0;
		float p01 = p*px0*py1;
		float p10 = p*px1*py0;
		float p11 = p*(1.0f-p00-p01-p10);//px1*py1;
		set(x0,y0,r,g,b,p00);
		set(x0,y1,r,g,b,p01);
		set(x1,y0,r,g,b,p10);
		set(x1,y1,r,g,b,p11);
	}

	public void set(int x, int y, int r, int g, int b, float pColor) {
		if (x<0||x>this.width-1||y<0||y>this.height-1) return;
		float pBackground = 1.0f - pColor;
		R[x][y] = (int)(pColor*r + pBackground*R[x][y]);
		G[x][y] = (int)(pColor*g + pBackground*G[x][y]);
		B[x][y] = (int)(pColor*b + pBackground*B[x][y]);
		limit(x,y);
	}

	public void set(int x, int y, int r, int g, int b) {
		if (x<0||x>this.width-1||y<0||y>this.height-1) return;
		R[x][y]=r>0xFFFF?0xFFFF:r;
		G[x][y]=g>0xFFFF?0xFFFF:g;
		B[x][y]=b>0xFFFF?0xFFFF:b;
		limit(x,y);
	}

	public void setAdd(int x, int y, int[] rgb) {
		setAdd(x,y,rgb[0],rgb[1],rgb[2]);
	}
	
	public void setAdd(float x, float y, int[] rgb) {
		setAdd(x,y,rgb[0],rgb[1],rgb[2],1.0f);
	}

	public void setAdd(float x, float y, int r, int g, int b) {
		setAdd(x,y,r,g,b,1.0f);
	}

	public void setAdd(float x, float y, int r, int g, int b, float p) {
		int x0 = (int)x, x1 = x0 + 1;
		int y0 = (int)y, y1 = y0 + 1;
		float px1 = x-x0, px0 = 1.0f - px1;
		float py1 = y-y0, py0 = 1.0f - py1;
		float p00 = p*px0*py0;
		float p01 = p*px0*py1;
		float p10 = p*px1*py0;
		float p11 = p*(1.0f-p00-p01-p10);//px1*py1;
		setAdd(x0,y0,r,g,b,p00);
		setAdd(x0,y1,r,g,b,p01);
		setAdd(x1,y0,r,g,b,p10);
		setAdd(x1,y1,r,g,b,p11);
	}

	public void setAdd(int x, int y, int r, int g, int b, float pColor) {
		if (x<0||x>this.width-1||y<0||y>this.height-1) return;
		R[x][y] += (int)(pColor*r);
		G[x][y] += (int)(pColor*g);
		B[x][y] += (int)(pColor*b);
		limit(x,y);
	}

	public void setAdd(int x, int y, int r, int g, int b) {
		if (x<0||x>this.width-1||y<0||y>this.height-1) return;
		R[x][y]+=r;
		G[x][y]+=g;
		B[x][y]+=b;
		limit(x,y);
	}

	public int[] getRGB(int x, int y) {
		int[] rgb = new int[3];
		if (x<0||x>this.width-1||y<0||y>this.height-1) return null;
		rgb[0]=R[x][y];
		rgb[1]=G[x][y];
		rgb[2]=B[x][y];
		return rgb;
	}

	public int[] getRGB(int x, float y) {
		int[] rgb = new int[3];

		if (x<0||x>this.width-1||y<0||y>this.height-2) return null;
		int y0 = (int)y;
		int y1 = y0+1;
		float p1 = y-y0, p0 = 1.0f-p1;

		int R0 = R[x][y0];
		int G0 = G[x][y0];
		int B0 = B[x][y0];
		int R1 = R[x][y1];
		int G1 = G[x][y1];
		int B1 = B[x][y1];
		rgb[0] = (int)(p0*R0+p1*R1);
		rgb[1] = (int)(p0*G0+p1*G1);
		rgb[2] = (int)(p0*B0+p1*B1);
		return rgb;
	}

	private int min(int n1, int n2) {
		return n1<n2?n1:n2;
	}

	private int max(int n1, int n2) {
		return n1>n2?n1:n2;
	}

	public void limit(int x, int y) {
		R[x][y] = min(max(R[x][y],0),0xFFFF);
		G[x][y] = min(max(G[x][y],0),0xFFFF);
		B[x][y] = min(max(B[x][y],0),0xFFFF);
	}

	public void updatePImage() {
		for (int x=0;x<this.width;x++) for (int y=0;y<this.height;y++) {
			set(x,y,0xFF<<24|(R[x][y]>>8)<<16|(G[x][y]>>8)<<8|B[x][y]>>8);
		}
	}

	public void copyInto(HighQualityImage targetImage) {
		for (int x=0;x<this.width;x++) for (int y=0;y<this.height;y++) { 
			targetImage.set(x,y,this.getRGB(x,y));
		}
		targetImage.updatePImage();
	}

	public void copyInto(HighQualityImage targetImage, int xo, int yo) {
		for (int x=0;x<this.width;x++) for (int y=0;y<this.height;y++) { 
			targetImage.set(x+xo,y+yo,this.getRGB(x,y));
		}
		targetImage.updatePImage();
	}

}
