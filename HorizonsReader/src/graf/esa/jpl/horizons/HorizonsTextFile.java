package graf.esa.jpl.horizons;

import java.util.Vector;

import processing.core.*;

public class HorizonsTextFile {

	String[] comment;
	String dataComment = "";
	String[] data;
	
	String[] variableNames;
	double[][] variableValues;
	
	class VarInfo {
		String varName;
		int row, column;
		public VarInfo(String varName, int row, int column) {
			this.varName = varName;
			this.row = row;
			this.column = column;
		}
		public String toString() {
			return varName+" (row:"+row+",column:"+column+")";
		}
	}
	class Var {
		String name;
		double value;
		public Var(String name, double value) {
			this.name=name;
			this.value=value;
		}
	}
	
	/**
	 * Creates a new HorizonsTextFile based on lines
	 */
	public HorizonsTextFile(String[] lines) {
		
		parseLines(lines);
		
		Vector dataPosition = parseDataComment();
		
		int variableAmount = dataPosition.size();
		//for (int i=0;i<variableAmount;i++) System.out.println((VarInfo)dataPosition.elementAt(i));
		
		Vector[] vars = new Vector[variableAmount];
		
		for (int i=0;i<data.length;i++) {
			
			String[] rowAsStrings = data[i].split("\r\n");

			for (int v=0;v<variableAmount;v++) {
				if (i==0) vars[v] = new Vector();
				VarInfo varInfo = (VarInfo)dataPosition.elementAt(v);
				String varName = varInfo.varName;
				int row = varInfo.row;
				int col = varInfo.column;
				rowAsStrings[row] = rowAsStrings[row].replaceAll("\\s{2,}"," ");
				String valAsString = rowAsStrings[row].split(" ")[col];
				vars[v].add(new Var(varName,Double.parseDouble(valAsString)));
				//System.out.println(valAsString);
			}
		}
		
		
		variableNames = new String[variableAmount];
		variableValues = new double[variableAmount][data.length];
		
		for (int v=0;v<variableAmount;v++) {
			variableNames[v] = ((VarInfo)dataPosition.elementAt(v)).varName;
			for (int i=0;i<data.length;i++)
				variableValues[v][i] = ((Var)vars[v].elementAt(i)).value;
		}
		
		
		
		
		/*for (int v=0;v<variableNames.length;v++) {
			System.out.println("VARIABLE: "+variableNames[v]);
			for (int i=0;i<variableValues[v].length;i++)
				System.out.println(variableValues[v][i]);
		}*/
		
		System.out.println("Variables loaded: "+variableNamesToString()+" ("+getNumberOfRows()+" rows)");
		
	}
	
	private Vector parseDataComment() {
		
		String[] dataCommentAsLines = dataComment.split("\r\n");
		//for (int i=0;i<dataCommentAsLines.length;i++) System.out.println(i+" "+dataCommentAsLines[i]);
		
		Vector dataPosition = new Vector();
		for (int line=0;line<dataCommentAsLines.length;line++) {
			String cleanLine = dataCommentAsLines[line];
			//String cleanLine = dataCommentAsLines[line].replaceAll("\\b\\s{2,}\\b", " "); /* replace multiple whitespaces between words with single blank */
			//cleanLine = cleanLine.replaceAll("\\s+$", ""); /* remove trailing whitespace */
			//cleanLine = cleanLine.replaceAll("^\\s+", ""); /* remove leading whitespace */
			cleanLine = cleanLine.replaceAll("\\s{2,}"," "); // replace multiple whitespaces (not at beginning)
			//System.out.println(line+": |"+cleanLine+"|");
			String[] col = cleanLine.split(" ");
			for (int j=0;j<col.length;j++) //{
				//System.out.println(line+": "+col[j]);
				if (col[j].length()>0) dataPosition.add(new VarInfo(col[j],line,j));
			//}
		}
		
		return dataPosition;
	}
	
	private void parseLines(String[] lines) {
		Vector data = new Vector();
		Vector comment = new Vector();
		boolean isData = false;
		int dataCommentPart = -1;
		
		for (int i=0;i<lines.length;i++) {
			if (lines[i].startsWith("$$EOE")) isData = false;
			if (lines[i].startsWith("***")) {
				comment.add(new Vector());
			}
			
			if (isData) {
				if (!lines[i].startsWith(" ")) data.add(lines[i]);
				else data.add((String)data.remove(data.size()-1) + "\r\n" + lines[i]);
			}
			
			else {
				if (	!lines[i].startsWith("***") 
						&&!lines[i].startsWith("$$SOE")
						&&!lines[i].startsWith("$$EOE")
				) ((Vector)comment.elementAt(comment.size()-1)).add(lines[i]);
			}
			
			if (lines[i].startsWith("$$SOE")) {
				isData = true;
				comment.remove(comment.size()-1);
				dataCommentPart = comment.size()-1;
			}
		}
		
		
		this.comment = new String[comment.size()];
		for (int i=0;i<comment.size();i++) {
			this.comment[i] = "";//(String)comment.elementAt(i);
			
			Vector part = (Vector)comment.elementAt(i);
			for (int j=0;j<part.size();j++) this.comment[i] += (String)part.elementAt(j) + (j<part.size()-1?"\r\n":"");
		}
		
		this.data = new String[data.size()];
		for (int i=0;i<data.size();i++) this.data[i] = (String)data.elementAt(i);
		
		Vector part = (Vector)comment.elementAt(dataCommentPart);
		for (int j=0;j<part.size();j++) dataComment += (String)part.elementAt(j) + (j<part.size()-1?"\r\n":"");
	}


	public String[] getVariableNames() {
		return variableNames;
	}

	public int getNumberOfRows() {
		return variableValues[0].length;
	}

	public double[] getValues(String varName) {
		for (int v=0;v<variableNames.length;v++) {
			if (varName.equals(variableNames[v])) {
				return variableValues[v];
			}
		}
		throw new RuntimeException("Variable not found! Current variables are: "+variableNamesToString());
	}
	
	public double[][] getValues(String[] varNames) {
		double[][] out = new double[varNames.length][getNumberOfRows()];
		for (int i=0;i<varNames.length;i++) {
			out[i] = getValues(varNames[i]);
		}
		return out;
	}

	public String variableNamesToString() {
		String out = "";
		for (int v=0;v<variableNames.length;v++) out += "\""+variableNames[v]+"\"" + (v<variableNames.length-1?", ":"");
		return out;
	}

	public double getValueAtRow(String varName, int row) {
		double[] values = getValues(varName);
		if (row<0||row>=values.length) throw new RuntimeException("Row number too large or small, should be 0.."+(getNumberOfRows()-1)+" here");
		return values[row];
	}
	
	public double[] getValuesAtRow(String[] varNames, int row) {
		double[] out = new double[varNames.length];
		for (int i=0;i<varNames.length;i++) {
			double[] values = getValues(varNames[i]);
			if (row<0||row>=values.length) throw new RuntimeException("Row number too large or small, should be 0.."+(getNumberOfRows()-1)+" here");
			out[i] = values[row];
		}
		return out;
	}
	
	public int getFirstRow() {
		return 0;
	}
	
	public int getLastRow() {
		return getNumberOfRows()-1;
	}

}
